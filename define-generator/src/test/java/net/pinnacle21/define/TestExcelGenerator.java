/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define;

import net.pinnacle21.define.models.*;
import net.pinnacle21.define.models.arm.*;
import net.pinnacle21.define.parsers.excel.DefineExcelParser;
import net.pinnacle21.define.util.ExcelComparator;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

import java.io.*;

import static org.junit.Assert.*;

public class TestExcelGenerator {

    /**
     * The Error Collector is declared to report all errors that occur in the comparison of excel files.
     */
    @Rule
    public ErrorCollector errorCollector = new ErrorCollector();

    @Test
    public void TestExcelExport() throws Exception {
        File control = new File("src/test/resources/CDISC01-excel-control.xlsx");

        DefineExcelParser parser = new DefineExcelParser(control);
        File exported = ExcelGenerator.writeToDirectory(parser.process().getStudy(), new File("src/test"));

        ExcelComparator excelComparator = new ExcelComparator(control, exported, errorCollector);
        excelComparator.process();
    }

    @Test
    public void TestLongStudyName() throws Exception{
        File control = new File("src/test/resources/LongStudyNameControl.xlsx");

        DefineExcelParser parser = new DefineExcelParser(control);
        File exported = ExcelGenerator.writeToDirectory(parser.process().getStudy(), new File("src/test"));

        ExcelComparator excelComparator = new ExcelComparator(control, exported, errorCollector);
        excelComparator.process();
    }

    /**
     * Tests that the invalid file name characters(\/:*?"<>|) are stripped away.
     * @throws Exception
     */
    @Test
    public void TestInvalidCharacters() throws Exception {
        Study study = new Study().setName("B\\a/d C:h*a?r\"a<c>t|ers").setStandardName("");

        File directory = new File("src/test");
        File generatedExcel = ExcelGenerator.writeToDirectory(study, directory);

        assertTrue(generatedExcel.getName().substring(0, generatedExcel.getName().indexOf("-")).equals("Bad Characters"));

        generatedExcel.deleteOnExit();
    }

    @Test
    public void TestAnalysisDisplaysExport() throws Exception {
        Study study = new Study().setName("ARM Study").setStandardName("ADaM");
        study.setResultDisplay("Display", new ResultDisplay()
                .setName("Display")
                .setDescription(new Description().setText("Description"))
                .setDocumentReference(new DocumentReference()
                        .setDocument(new Document().setOid("Document"))
                        .setPages("1, 2, 3")
                ).add(new AnalysisResult()
                        .setOID("Result 1")
                        .setDescription(new Description().setText("Description"))
                        .setAnalysisReason("Reason")
                        .setAnalysisPurpose("Purpose")
                        .setDocumentation(new Documentation()
                                .setDescription(new Description().setText("Documentation"))
                                .addDocumentReference(new DocumentReference()
                                        .setDocument(new Document().setOid("Document"))
                                        .setPages("1, 2, 3")
                                )
                                .addDocumentReference(new DocumentReference()
                                        .setDocument(new Document().setOid("Document2"))
                                        .setPages("4, 5, 6")
                                )
                        )
                        .setAnalysisDatasets(new AnalysisDatasets().setComment(new Comment().setOid("Comment")))
                        .setProgrammingCode(new ProgrammingCode()
                                .setContext("Context")
                                .setCode("Code")
                                .setDocumentReference(new DocumentReference()
                                        .setDocument(new Document().setOid("Document"))
                                        .setPages("1, 2, 3")
                                )
                        ).setAnalysisDatasets(new AnalysisDatasets()
                                .setComment(new Comment().setOid("Comment"))
                                .add(new AnalysisDataset()
                                        .setItemGroup(new ItemGroup().setName("Dataset"))
                                        .add(new AnalysisVariable()
                                                .setItem(new Item().setName("Variable1"))
                                        )
                                        .add(new AnalysisVariable()
                                                .setItem(new Item().setName("Variable2"))
                                        )
                                        .setWhereClause(new WhereClause().setOid("Where Clause"))
                                )
                        )
                ).add(new AnalysisResult()
                        .setOID("Result 2")
                        .setAnalysisReason("Reason")
                        .setAnalysisPurpose("Purpose")
                        .setAnalysisDatasets(new AnalysisDatasets()
                                .setComment(new Comment().setOid("Comment"))
                                .add(new AnalysisDataset().setItemGroup(new ItemGroup().setName("Dataset")))
                        )
                )
        );

        File control = new File("src/test/resources/arm-control.xlsx");
        File exported = ExcelGenerator.writeToDirectory(true, study, new File("src/test"));

        ExcelComparator excelComparator = new ExcelComparator(control, exported, errorCollector);
        excelComparator.process();
    }

    @Test
    public void validateSDTMExcelStructure() throws IOException, InvalidFormatException {
        String standardName = "SDTM-IG";
        String standardVersion = "3.2";
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        Study study = new Study()
                .setStandardName(standardName)
                .setStandardVersion(standardVersion);

        ExcelGenerator.writeToStream(study, outputStream);

        InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());

        Workbook workbook = WorkbookFactory.create(inputStream);

        for (ExcelGenerator.Sheets sheet : ExcelGenerator.Sheets.values()) {
            if (!sheet.isAdamOnly()) {
                assertNotNull(workbook.getSheet(sheet.getSheetName()));
            } else {
                assertNull("An ADaM only sheet exists in a SDTM workbook!", workbook.getSheet(sheet.getSheetName()));
            }
        }
    }

    @Test
    public void validateAdamExcelStructure() throws IOException, InvalidFormatException {
        String standardName = "ADaM-IG";
        String standardVersion = "1.0";
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        Study study = new Study()
                .setStandardName(standardName)
                .setStandardVersion(standardVersion);

        ExcelGenerator.writeToStream(true, study, outputStream);

        InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());

        Workbook workbook = WorkbookFactory.create(inputStream);

        // ADaM workbooks should get all of the sheets
        for (ExcelGenerator.Sheets sheet : ExcelGenerator.Sheets.values()) {
            assertNotNull("Missing sheet for an ADaM workbook!", workbook.getSheet(sheet.getSheetName()));
        }
    }

    @Test
    public void disableArmGeneration() throws IOException, InvalidFormatException {
        String standardName = "ADaM-IG";
        String standardVersion = "1.0";
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        Study study = new Study()
                .setStandardName(standardName)
                .setStandardVersion(standardVersion);

        ExcelGenerator.writeToStream(false, study, outputStream);

        InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());

        Workbook workbook = WorkbookFactory.create(inputStream);

        // If we disable ARM support, the ADaM only sheets should not exist
        for (ExcelGenerator.Sheets sheet : ExcelGenerator.Sheets.values()) {
            if (sheet.isAdamOnly()) {
                assertNull("ARM Sheets exist when ARM support is disabled!", workbook.getSheet(sheet.getSheetName()));
            } else {
                assertNotNull(workbook.getSheet(sheet.getSheetName()));
            }
        }
    }
}
