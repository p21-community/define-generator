/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.generator;

import net.pinnacle21.define.models.*;
import net.pinnacle21.define.models.arm.*;
import net.pinnacle21.parsing.xml.XmlWriter;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.Diff;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class TestARMGenerator {
    private XmlWriter writer;

    @Before
    public void setUp() throws SAXException, IOException, TransformerConfigurationException {
        DefineMetadata metadata = DefineMetadata.V2;
        Map<String, String> tmp = new HashMap<>();

        //The xml writer expects a <uri, prefix> map, but the Metadata class provides a <prefix, uri> map
        for(Map.Entry<String, String> entry : metadata.getNamespaces().xpathNamespaces().entrySet()){
            tmp.put(entry.getValue(), entry.getKey().equals(metadata.getNamespaces().xmlnsPrefix()) || entry.getKey().equals(metadata.getNamespaces().odmPrefix()) ? StringUtils.EMPTY : entry.getKey());
        }

        this.writer = new XmlWriter(new ByteArrayOutputStream(), metadata.getNamespaces().odmURI(), tmp);
        this.writer.startElement("test");
    }

    @Test
    public void generateProgrammingCodeWithCodeText() throws SAXException, IOException {
        String context = "SAS version 9.2";
        String code = "abc";
        String expectedXML = "<arm:ProgrammingCode Context=\"" + context + "\"><arm:Code>" + code + "</arm:Code></arm:ProgrammingCode>";

        ProgrammingCode programmingCode = new ProgrammingCode()
                .setContext(context)
                .setCode(code);
        ARMGenerator generator = new ARMGenerator(this.writer);

        generator.generateProgrammingCode(programmingCode);

        testXMLStrings(expectedXML, generator.getOutputStream());
    }

    @Test
    public void generateProgrammingCodeWithDocumentRef() throws SAXException, IOException {
        String documentID = "at14-5-02.sas";
        String expectedXML = "<arm:ProgrammingCode><def:DocumentRef leafID=\"LF."+ documentID +"\"/></arm:ProgrammingCode>";

        ProgrammingCode programmingCode = new ProgrammingCode()
                .setDocumentReference(new DocumentReference()
                        .setDocument(new Document().setOid(documentID)));
        ARMGenerator generator = new ARMGenerator(this.writer);

        generator.generateProgrammingCode(programmingCode);

        testXMLStrings(expectedXML, generator.getOutputStream());
    }

    @Test
    public void generateDocumentation() throws IOException, SAXException {
        String description = "abc";
        String documentID = "at14-5-02.sas";
        String pages = "5";

        String xml =
                "<arm:Documentation>" +
                    "<Description>" +
                        "<TranslatedText xml:lang=\"en\">"+ description +"</TranslatedText>" +
                    "</Description>" +
                    "<def:DocumentRef leafID=\"LF." + documentID + "\">" +
                        "<def:PDFPageRef PageRefs=\"" + pages + "\" Type=\"PhysicalRef\"/>" +
                    "</def:DocumentRef>" +
                "</arm:Documentation>";

        Documentation documentation = new Documentation()
                .setDescription(new Description().setText(description).setLanguage("en"))
                .addDocumentReference(new DocumentReference().setDocument(new Document().setOid(documentID)).setPages(pages));

        ARMGenerator generator = new ARMGenerator(this.writer);

        generator.generateDocumentation(documentation);

        testXMLStrings(xml, generator.getOutputStream());
    }

    @Test
    public void generateDocumentationDoesNotFailWithNullDocumentReference() throws IOException, SAXException {
        String description = "abc";

        String xml =
                "<arm:Documentation>" +
                        "<Description>" +
                        "<TranslatedText xml:lang=\"en\">"+ description +"</TranslatedText>" +
                        "</Description>" +
                "</arm:Documentation>";

        Documentation documentation = new Documentation()
                .setDescription(new Description().setText(description).setLanguage("en"));

        ARMGenerator generator = new ARMGenerator(this.writer);

        generator.generateDocumentation(documentation);

        testXMLStrings(xml, generator.getOutputStream());
    }

    @Test
    public void generateAnalysisDataset() throws IOException, SAXException {
        String itemGroupOID = "ADQSADAS";
        String whereClauseOID = "Table_14-3.01.R.1.ADQSADAS";
        String itemOID = "ADQSADAS.CHG";

        String xml =
                "<arm:AnalysisDataset ItemGroupOID=\"IG." + itemGroupOID + "\">" +
                        "<def:WhereClauseRef WhereClauseOID=\"WC." + whereClauseOID + "\"/>" +
                        "<arm:AnalysisVariable ItemOID=\"IT." + itemOID + "\"/>" +
                "</arm:AnalysisDataset>";

        ItemGroup group = new ItemGroup().setOID(itemGroupOID).setName(itemGroupOID);

        AnalysisDataset dataset = new AnalysisDataset()
                .setItemGroup(group)
                .setWhereClause(new WhereClause().setOid(whereClauseOID))
                .add(new AnalysisVariable().setItem(new Item().setOID(itemOID).setName(itemOID)));

        ARMGenerator generator = new ARMGenerator(this.writer);

        generator.generateAnalysisDataset(dataset);

        testXMLStrings(xml, generator.getOutputStream());
    }

    @Test
    public void generateAnalysisDatasets() throws IOException, SAXException {
        String commentOID = "ADSL";

        String xml =
                "<arm:AnalysisDatasets def:CommentOID=\"COM.ADSL\">" +
                "</arm:AnalysisDatasets>";

        AnalysisDatasets datasets = new AnalysisDatasets()
                .setComment(new Comment().setOid(commentOID));

        ARMGenerator generator = new ARMGenerator(this.writer);

        generator.generateAnalysisDatasets(datasets);

        testXMLStrings(xml, generator.getOutputStream());
    }

    @Test
    public void generateAnalysisResult() throws SAXException, IOException {
        String resultDisplayName = "Table_14-3.01";
        String analysisResultOID = "Table_14-3.01.R.1";
        String parameterOID = "ADQSADAS.PARAMCD";
        String reason = "abc";
        String purpose = "qrs";

        String xml =
                "<arm:AnalysisResult OID=\"AR." + analysisResultOID + "\" ParameterOID=\"IT." + parameterOID + "\" AnalysisReason=\"" + reason + "\" AnalysisPurpose=\"" + purpose + "\">" +
                "</arm:AnalysisResult>";

        ResultDisplay resultDisplay = new ResultDisplay()
                .setName(resultDisplayName)
                .add(new AnalysisResult()
                        .setOID(analysisResultOID)
                        .setParameterOID(parameterOID)
                        .setAnalysisReason(reason)
                        .setAnalysisPurpose(purpose)
                );

        ARMGenerator generator = new ARMGenerator(this.writer);

        generator.generateAnalysisResults(resultDisplay);

        testXMLStrings(xml, generator.getOutputStream());
    }

    @Test
    public void generateResultDisplay() throws SAXException, IOException {
        String oid = "Table 14-3.01";
        String name = "Table 14-3.01";

        String xml =
                "<arm:ResultDisplay OID=\"RD." + oid + "\" Name=\"" + name + "\">" + "</arm:ResultDisplay>";

        ResultDisplay display = new ResultDisplay()
                .setOID(oid)
                .setName(name);

        ARMGenerator generator = new ARMGenerator(this.writer);

        generator.generateResultDisplay(display);

        testXMLStrings(xml, generator.getOutputStream());
    }

    private void testXMLStrings(String expectedXML, OutputStream outputStream) throws IOException, SAXException {
        String testNamespaces = "xmlns:def=\"http://www.cdisc.org/ns/def/v2.0\" xmlns:arm=\"http://www.cdisc.org/ns/arm/v1.0\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns=\"http://www.cdisc.org/ns/odm/v1.3\" xmlns:nciodm=\"http://ncicb.nci.nih.gov/xml/odm/EVS/CDISC\"";

        expectedXML = "<test " + testNamespaces + ">" + expectedXML + "</test>";
        this.writer.endElement("test");
        this.writer.close();

        Source expected = Input.fromString(expectedXML).build();
        Source actual = Input.fromString(outputStream.toString()).build();

        Diff comparator = DiffBuilder
                .compare(expected)
                .withTest(actual)
                .ignoreWhitespace()
                .build();

        Assert.assertFalse(comparator.toString(), comparator.hasDifferences());
    }
}
