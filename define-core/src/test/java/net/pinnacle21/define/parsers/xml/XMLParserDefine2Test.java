/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.xml;

import net.pinnacle21.define.models.*;
import net.pinnacle21.define.util.GenericComparator;
import net.pinnacle21.define.util.Stubs;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.List;

public class XMLParserDefine2Test {
    private static Study controlStudy;
    private static Study define2Study;

    @BeforeClass
    public static void parseStudy() throws Exception {
        define2Study = new XMLParser(new File("src/test/resources/XmlParserdefine-2.0-control.xml")).parse();
        controlStudy = Stubs.DEFINE_2_CONTROL_STUDY;
    }

    @Test
    public void testStudy() throws Exception {
        GenericComparator<Study> comparator = new GenericComparator<>();

        boolean isEqual = comparator.compare(controlStudy, define2Study);
        if (!isEqual) {
            comparator.showDifferences();
        }

        assert isEqual;
    }

    @Test
    public void testDocuments() throws Exception {
        GenericComparator<Document> comparator = new GenericComparator<>();

        boolean isEqual = comparator.compare(
                controlStudy.getDocuments().get(0),
                define2Study.getDocuments().get(0));
        if (!isEqual) {
            comparator.showDifferences();
        }

        assert isEqual;
    }

    @Test
    public void testComments() throws Exception {
        GenericComparator<Comment> comparator = new GenericComparator<>();

        boolean isEqual = comparator.compare(controlStudy.getComments().get(0), define2Study.getComments().get(0));
        if (!isEqual) {
            comparator.showDifferences();
        }

        assert isEqual;
    }

    @Test
    public void testMethods() throws Exception {
        GenericComparator<Method> comparator = new GenericComparator<>();

        boolean isEqual = comparator.compare(controlStudy.getMethods().get(0), define2Study.getMethods().get(0));
        if (!isEqual) {
            comparator.showDifferences();
        }

        assert isEqual;
    }

    @Test
    public void testCodeLists() throws Exception {
        GenericComparator<CodeList> codeListComparator = new GenericComparator<>();
        GenericComparator<CodeListItem> codeListItemComparator = new GenericComparator<>();
        GenericComparator<Dictionary> dictionaryComparator = new GenericComparator<>();

        CodeList controlCodeList = controlStudy.getCodeList("CodeList");

        boolean isCodeListEqual = codeListComparator.compare(controlCodeList, define2Study.getCodeList("CodeList"));
        if (!isCodeListEqual) {
            codeListComparator.showDifferences();
        }

        List<CodeListItem> testItems = define2Study.getCodeList("CodeList").getCodelistItems();

        if (testItems.size() != 2) {
            assert false;
        }

        boolean isCodeListItemEqual = true;
        for (CodeListItem testItem : testItems) {
            boolean isEqual = codeListItemComparator.compare(testItem.getCodedValue().equals("CodeListItem")
                    ? controlCodeList.getItem("CodeListItem") : controlCodeList.getItem("EnumeratedItem"), testItem);
            if (!isEqual) {
                isCodeListItemEqual = false;
                codeListItemComparator.showDifferences();
            }
        }

        assert isCodeListEqual && isCodeListItemEqual;

        Dictionary externalCodeList = controlStudy.getDictionary("ExternalCodeList");

        boolean isExternalCodeListEqual = dictionaryComparator.compare(externalCodeList, define2Study.getDictionary("ExternalCodeList"));
        if (!isExternalCodeListEqual) {
            codeListComparator.showDifferences();
        }

        assert isExternalCodeListEqual;
    }

    @SuppressWarnings("Duplicates") //The control study is different than in XMLParserDefine1Test.java
    @Test
    public void testItemGroupDefs() throws Exception {
        GenericComparator<ItemGroup> comparator = new GenericComparator<>();

        ItemGroup controlItemGroup = controlStudy.getItemGroup("ItemGroupDef");


        ItemGroup testItemGroup = define2Study.getItemGroup("ItemGroupDef");

        boolean isEqual = comparator.compare(controlItemGroup, testItemGroup);
        if (!isEqual) {
            comparator.showDifferences();
        }

        boolean keysMatch = controlItemGroup.getKeyVariables().equals(testItemGroup.getKeyVariables());

        if (!keysMatch) {
            isEqual = false;
            new AssertionError(String.format("Comparison failed for class: %s\n", ItemGroup.class.getName()) +
                    String.format("Expected Keys: %s\n", controlItemGroup.getKeyVariables()) +
                    String.format("\tActual Keys: %s\n", testItemGroup.getKeyVariables())).printStackTrace();
        }

        assert isEqual;
    }

    @Test
    public void testItems() throws Exception {
        GenericComparator<Item> comparator = new GenericComparator<>();

        Item controlItem = controlStudy.getItemGroup("ItemGroupDef").getItem("Variable1");
        Item testItem = define2Study.getItemGroup("ItemGroupDef").getItem("Variable1");

        boolean isEqual = comparator.compare(controlItem, testItem);
        if (!isEqual) {
            comparator.showDifferences();
        }

        assert isEqual;

        // Check dictionary ref (DEF-171)
        controlItem = controlStudy.getItemGroup("ItemGroupDef").getItem("Variable2");
        testItem = define2Study.getItemGroup("ItemGroupDef").getItem("Variable2");

        isEqual = comparator.compare(controlItem, testItem);
        if (!isEqual) {
            comparator.showDifferences();
        }

        assert isEqual;
    }

    @Test
    public void testValueLists() throws Exception {
        GenericComparator<ValueList> valueListComparator = new GenericComparator<>();
        GenericComparator<Item> itemComparator = new GenericComparator<>();

        ValueList controlValueList = controlStudy.getValueList("ItemGroupDef", "Variable1");
        Item controlItem = controlValueList.getItems().get(0);

        ValueList testValueList = define2Study.getValueLists().get(0);
        Item testItem = testValueList.getItems().get(0);

        boolean valueListsEqual = valueListComparator.compare(controlValueList, testValueList);
        if (!valueListsEqual) {
            valueListComparator.showDifferences();
        }

        boolean itemsEqual = itemComparator.compare(controlItem, testItem);
        if (!itemsEqual) {
            itemComparator.showDifferences();
        }

        assert valueListsEqual && itemsEqual;
    }

    @Test
    public void testWhereClauses() throws Exception {
        GenericComparator<WhereClause> whereClauseComparator = new GenericComparator<>();
        GenericComparator<RangeCheck> rangeCheckComparator = new GenericComparator<>();

        WhereClause controlWhereClause = controlStudy.getWhereClause(Stubs.define2WhereClauseOid);
        WhereClause testWhereClause = define2Study.getWhereClause(Stubs.define2WhereClauseOid);

        boolean isWhereClauseEqual = whereClauseComparator.compare(controlWhereClause, testWhereClause);
        if (!isWhereClauseEqual) {
            whereClauseComparator.showDifferences();
        }

        if (testWhereClause.getRangeChecks().size() != 2) {
            assert false;
        }

        //Trusting file ordering to match the control rangechecks
        boolean isRangeCheckEqual = rangeCheckComparator.compare(
                controlWhereClause.getRangeChecks().get(0), testWhereClause.getRangeChecks().get(0)
        );
        if (!isRangeCheckEqual) {
            isRangeCheckEqual = false;
            rangeCheckComparator.showDifferences();
        }
        assert isRangeCheckEqual;
    }
}
