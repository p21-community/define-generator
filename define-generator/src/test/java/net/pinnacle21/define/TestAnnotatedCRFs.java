/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define;

import net.pinnacle21.define.parsers.excel.DefineExcelParser;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.diff.*;
import org.xmlunit.util.Predicate;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.Arrays;
import java.util.List;

public class TestAnnotatedCRFs {

    @Rule
    public ErrorCollector errorCollector = new ErrorCollector();

    private Predicate<Attr> attributeFilter = new Predicate<Attr>() {
        List<String> filterNodeNames = Arrays.asList("ODM", "Study", "MetaDataVersion");
        @Override
        public boolean test(Attr toTest) {
            String name = StringUtils.defaultString(toTest.getOwnerElement().getNodeName());
            return !filterNodeNames.contains(name); //Don't compare ODM, Study or MetaDataVersion attributes

        }
    };

    @Test
    public void TestACRF() throws Exception {
        File control = new File("src/test/resources/define-2.0-acrf-control.xml");
        File acrfExcel = new File("src/test/resources/acrf-control.xlsx");
        File output = new File("src/test");

        DefineExcelParser parser = new DefineExcelParser(acrfExcel);
        DefineGenerator generator = new DefineGenerator(parser.process().getStudy(), output);
        generator.generate();
        output = new File(generator.getFilePath());
        output.deleteOnExit();

        Document controlACRF = (DocumentBuilderFactory.newInstance().newDocumentBuilder()).parse(control);
        Document test = (DocumentBuilderFactory.newInstance().newDocumentBuilder()).parse(output);

        Diff comparator = DiffBuilder
                .compare(controlACRF)
                .withTest(test)
                .withAttributeFilter(attributeFilter)
                .ignoreWhitespace()
                .normalizeWhitespace()
                .withNodeMatcher(new DefaultNodeMatcher(ElementSelectors.byNameAndAttributes("OID", "ItemOID", "ID", "LeafID")))
                .withDifferenceEvaluator(new DifferenceEvaluator() {
                    @Override
                    public ComparisonResult evaluate(Comparison comparison, ComparisonResult outcome) {
                        if (outcome == ComparisonResult.DIFFERENT &&
                                comparison.getType() == ComparisonType.CHILD_NODELIST_SEQUENCE) {
                            return ComparisonResult.EQUAL;
                        }

                        return outcome;
                    }
                })
                .build();

        int i = 0;
        for (Difference difference : comparator.getDifferences()) {
            System.out.println(difference);
            i++;
        }

        if (i > 0) {
            System.out.println(i + " differences found!");
        }

        Assert.assertFalse(comparator.hasDifferences());
    }

}
