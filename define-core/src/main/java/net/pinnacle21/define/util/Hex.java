/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hex {
    private static final char[] HEX_TABLE = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
    };
    private static final ThreadLocal<MessageDigest> SHA1_DIGEST = ThreadLocal.withInitial(() -> {
        try {
            return MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException ignore) {}

        return null;
    });
    private static final ThreadLocal<MessageDigest> SHA512_DIGEST = ThreadLocal.withInitial(() -> {
        try {
            return MessageDigest.getInstance("SHA-512");
        } catch (NoSuchAlgorithmException ignore) {}

        return null;
    });

    public static String toString(byte[] bytes) {
        int size = bytes.length * 2;
        char[] render = new char[size];
        byte b;

        for (int i = 0; i < size; i = i + 2) {
            b = bytes[i / 2];
            render[i] = HEX_TABLE[(b >> 4) & 0x0F];
            render[i + 1] = HEX_TABLE[b & 0x0F];
        }

        return new String(render);
    }

    public static String sha1(String value) {
        return toString(SHA1_DIGEST.get().digest(value.getBytes()));
    }

    public static String sha512(String value) {
        return toString(SHA512_DIGEST.get().digest(value.getBytes()));
    }
}
