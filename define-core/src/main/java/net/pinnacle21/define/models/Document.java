/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

public class Document extends ExcelRecord {
    private String oid;
    private String title;
    private String href;
    private Type type;

    public enum Type {
        ANNOTATEDCRF,
        SUPPLEMENTAL,
        LEAF
    }

    public String getOid() {
        return oid;
    }

    public Document setOid(String oid) {
        this.oid = oid != null ? oid.trim() : null;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public Document setTitle(String title) {
        this.title = title != null ? title.trim() : null;
        return this;
    }

    public String getHref() {
        return href;
    }

    public Document setHref(String href) {
        this.href = href != null ? href.trim() : null;
        return this;
    }

    public Type getType() {
        return this.type;
    }

    public Document setType(Type type) {
        this.type = type;
        return this;
    }

    public Document setType(String href) {
        if (StringUtils.isNotBlank(href)) {
            String filename = FilenameUtils.getBaseName(href).toLowerCase();
            if (filename.endsWith("crf")) {
                this.setType(Document.Type.ANNOTATEDCRF);
            } else {
                this.setType(Document.Type.SUPPLEMENTAL);
            }
        } else {
            this.setType(Document.Type.LEAF);
        }

        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Document document = (Document) o;

        if (getOid() != null ? !getOid().equals(document.getOid()) : document.getOid() != null) return false;
        if (getTitle() != null ? !getTitle().equals(document.getTitle()) : document.getTitle() != null) return false;
        if (getHref() != null ? !getHref().equals(document.getHref()) : document.getHref() != null) return false;
        return getType() == document.getType();

    }
}
