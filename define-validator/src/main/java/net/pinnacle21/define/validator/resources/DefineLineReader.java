/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class DefineLineReader implements AutoCloseable {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefineLineReader.class);

    private final File define;
    private LineNumberReader reader;
    private String lastLine;

    public DefineLineReader(File define) {
        this.define = define;
    }

    public String getLine(int lineNumber) {
        try {
            if (this.reader == null || this.reader.getLineNumber() > lineNumber) {
                if (this.reader != null) {
                    this.reader.close();
                }

                this.reader = new LineNumberReader(new InputStreamReader(new FileInputStream(this.define)));
            }

            String line = this.lastLine;

            if (this.reader.getLineNumber() == lineNumber) {
                return line;
            }

            do {
                line = this.reader.readLine();
            } while (line != null && this.reader.getLineNumber() < lineNumber);

            if (line != null) {
                line = line.trim();
            }

            this.lastLine = line;

            return line;
        } catch (IOException ex) {
            LOGGER.error("Unexpected exception while reading '{}'. The line we were trying to get was '{}'",
                    this.define, lineNumber, ex);
        }

        return null;
    }

    @Override
    public void close() throws IOException {
        if (this.reader != null) {
            this.reader.close();
        }
    }
}
