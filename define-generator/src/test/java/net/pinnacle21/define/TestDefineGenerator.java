/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define;

import net.pinnacle21.define.models.Comment;
import net.pinnacle21.define.models.Item;
import net.pinnacle21.define.models.ItemGroup;
import net.pinnacle21.define.models.Study;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertTrue;

public class TestDefineGenerator {

    /**
     * Tests that the invalid file name characters(\/:*?"<>|) are stripped away.
     * @throws Exception
     */
    @Test
    public void TestInvalidCharacters() throws Exception {
        Study study = new Study().setName("B\\a/d C:h*a?r\"a<c>t|ers").setStandardName("");

        File directory = new File("src/test");
        DefineGenerator generator = new DefineGenerator(study, directory);
        generator.generate();
        File generatedXML = new File(generator.getFilePath());

        assertTrue(generatedXML.getName().substring(0, generatedXML.getName().indexOf("-")).equals("Bad Characters"));

        generatedXML.deleteOnExit();
    }

    /**
     * Tests <a href="https://pinnacle21.atlassian.net/browse/OPEN-251">OPEN-251</a>. Testing that no exceptions are
     * thrown on define.xml export with the provided data
     */
    @Test
    public void TestCommentCompare() throws Exception {
        Study study = new Study().setStandardName("");
        study.setComment("Test", new Comment().setOid("Test"));
        study.setComment("Test.Test", new Comment().setOid("Test.Test"));

        DefineGenerator generator = new DefineGenerator(study, new File("src/test"));
        generator.generate();

        File generatedFile = new File(generator.getFilePath());

        generatedFile.deleteOnExit();
    }

    /**
     * Tests <a href="https://pinnacle21.atlassian.net/browse/OPEN-304">OPEN-304</a>.
     * Tests that the ADaM dataset comparator does not throw a NPE when category field is null
     * @throws Exception
     */
    @Test
    public void TestADaMDatasetCompare() throws Exception {
        Study study = new Study().setStandardName("ADaM");
        study.setItemGroup("Test", new ItemGroup().setName("Test"));
        study.setItemGroup("Test2", new ItemGroup().setName("Test2").setPurpose("Test"));

        DefineGenerator generator = new DefineGenerator(study, new File("src/test"));
        generator.generate();

        File generatedFile = new File(generator.getFilePath());

        generatedFile.deleteOnExit();
    }

    /**
     * Tests that an ItemDef with a null dataType can still get exported
     * Associated ticket: <a href="https://pinnacle21.atlassian.net/browse/DEF-33">DEF-33</a>
     * @throws Exception
     */
    @Test
    public void TestNullDataType() throws Exception {
        Study study = new Study().setName("testNullDataType").setStandardName("sdtm").setStandardVersion("3.1.2");
        ItemGroup itemGroup = new ItemGroup().setName("DM");
        itemGroup.add(new Item().setName("STUDYID"));
        study.setItemGroup(itemGroup.getName(), itemGroup);

        DefineGenerator defineGenerator = new DefineGenerator(study, new File("src/test"));
        defineGenerator.generate();

        new File(defineGenerator.getFilePath()).deleteOnExit();
    }
}
