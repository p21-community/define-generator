/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.Document;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.NamedRow;
import net.pinnacle21.parsing.excel.ValidationAwareExcelSheetParser;
import org.apache.commons.lang3.StringUtils;

public class DocumentSheetParser extends ValidationAwareExcelSheetParser<ParsedStudy> {
    static final String ID_COLUMN = "ID";
    static final String TITLE_COLUMN = "Title";
    static final String HREF_COLUMN = "Href";
    static final String DUPLICATE_ERROR = "The document '%s' is already defined, duplicates are not allowed";

    DocumentSheetParser() {
        addValidatedRequiredColumn(ID_COLUMN);
        addValidatedColumn(TITLE_COLUMN, FieldValidations.MEDIUM_LENGTH);
        addValidatedColumn(HREF_COLUMN);
    }

    @Override
    public void parse(NamedRow row, ParsedStudy parsedStudy) {
        if (!this.validate(row, parsedStudy)) {
            return;
        }
        Study study = parsedStudy.getStudy();

        String id = StringUtils.trim(row.get(ID_COLUMN));

        if (study.hasDocument(id)) {
            parsedStudy.error(row.getIdentifier(), DUPLICATE_ERROR, id);

            return;
        }

        Document document = new Document()
                .setOid(id)
                .setTitle(row.get(TITLE_COLUMN))
                .setHref(row.get(HREF_COLUMN));

        document.setType(document.getHref());

        study.setDocument(id, document);
    }
}
