/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.resources;

import net.pinnacle21.define.models.CodeList;
import net.pinnacle21.define.models.CodeListItem;
import net.pinnacle21.define.models.DefineMetadata;
import net.pinnacle21.define.parsers.xml.XPathEvaluator;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;
import java.util.Set;

public interface Terminology {
    boolean hasNciCode(String nciCode);
    CodeList getCodeListByOID(String codeListOid);
    CodeListItem getCodeListItemByNCICodeAndCodeListOID(String codeListOID, String defineTermCode);
    CodeListItem getCodeListItemByCodedValueAndCodeListOID(String oid, String codedValue);
    boolean hasNamedCodeListWithCodedValue(String codelistName, String codedValue);

    static Terminology parseFiles(File... files) throws IOException, DocumentException {
        Document document = null;
        Element target = null;

        for (File file : files) {
            try (
                InputStream is = new FileInputStream(file);
                InputStream bs = new BufferedInputStream(is)
            ) {
                if (document == null) {
                    document = new SAXReader().read(bs);
                    target = new XPathEvaluator(document, DefineMetadata.V2.getNamespaces().xpathNamespaces())
                            .selectSingleElement("/ODM/odm:Study/odm:MetaDataVersion");
                } else {
                    Document supplement = new SAXReader().read(bs);
                    XPathEvaluator evaluator = new XPathEvaluator(supplement, DefineMetadata.V2.getNamespaces().xpathNamespaces());

                    for (Element codelist : evaluator.selectElements("/ODM/odm:Study/odm:MetaDataVersion/odm:CodeList")) {
                        target.add(codelist.detach());
                    }
                }
            }
        }

        return document != null
                ? new DefaultTerminology(document)
                : null;
    }
}
