/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.content.rules;

import net.pinnacle21.define.validator.report.MessageTransformer;

import java.util.LinkedHashMap;
import java.util.Map;

abstract class AbstractRules {
    protected final MessageTransformer transformer;

    AbstractRules(MessageTransformer transformer) {
        this.transformer = transformer;
    }

    // TODO: This is a super hacky and fragile method to account for not being able to use ImmutableMap.of(), which hates nulls
    @SafeVarargs
    protected static <T, V extends T> Map<V, T> mapOf(T... values) {
        Map<V, T> map = new LinkedHashMap<>();

        for (int i = 0; i < values.length; i += 2) {
            //noinspection unchecked
            map.put((V)values[i], values[i + 1]);
        }

        return map;
    }
}

