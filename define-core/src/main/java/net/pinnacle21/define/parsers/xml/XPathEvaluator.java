/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.xml;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.*;
import org.jaxen.UnresolvableException;

import java.util.*;
import java.util.stream.Collectors;

public class XPathEvaluator {
    private final Document document;
    private Map<String, String> namespaces;
    private String prefix = "";

    private Map<String, Node> xpathCache = new HashMap<>();
    private Map<String, List<Node>> xpathListCache = new HashMap<>();

    public XPathEvaluator(Document document) {
        this(document, null);
    }

    public XPathEvaluator(Document document, Map<String, String> namespaces) {
        this.document = document;
        this.namespaces = namespaces != null
                ? namespaces
                : Collections.emptyMap();
    }

    public List<Element> selectElements(String xpression) {
        return this.selectElements(xpression, null);
    }

    public List<Element> selectElements(String xpression, Map<String, Object> variables) {
        return this.selectElements(this.document, xpression, variables);
    }

    public List<Element> selectElements(Node node, String xpression) {
        return this.selectElements(node, xpression, null);
    }

    public List<Element> selectElements(Node node, String xpression, Map<String, Object> variables) {
        return this.selectNodes(node, xpression, variables)
            .stream()
            .filter(n -> n instanceof Element)
            .map(n -> (Element) n)
            .collect(Collectors.toList());
    }

    public Element selectSingleElement(String xpression) {
        return this.selectSingleElement(xpression, null);
    }

    public Element selectSingleElement(String xpression, Map<String, Object> variables) {
        return this.selectSingleElement(this.document, xpression, variables);
    }

    public Element selectSingleElement(Node node, String xpression) {
        return this.selectSingleElement(node, xpression, null);
    }

    public Element selectSingleElement(Node node, String xpression, Map<String, Object> variables) {
        Node result = this.selectSingleNode(node, xpression, variables);

        if (result instanceof Element) {
            return (Element) result;
        }

        return null;
    }

    public boolean hasNodes(String xpression) {
        return this.hasNodes(xpression, null);
    }

    public boolean hasNodes(String xpression, Map<String, Object> variables) {
        return this.hasNodes(this.document, xpression, variables);
    }

    public boolean hasNodes(Node node, String xpression) {
        return this.hasNodes(node, xpression, null);
    }

    public boolean hasNodes(Node node, String xpression, Map<String, Object> variables) {
        return !this.selectNodes(node, xpression, variables).isEmpty();
    }

    public List<Node> selectNodes(String xpression) {
        return this.selectNodes(xpression, null);
    }

    public List<Node> selectNodes(String xpression, Map<String, Object> variables) {
        return this.selectNodes(this.document, xpression, variables);
    }

    public List<Node> selectNodes(Node node, String xpression) {
        return this.selectNodes(node, xpression, null);
    }

    public List<Node> selectNodes(Node node, String xpression, Map<String, Object> variables) {
        XPath xPath = createXPath(xpression, variables);
        if (node == this.document) {
            return this.xpathListCache.computeIfAbsent(xpathAsString(xPath, variables), (e) -> xPath.selectNodes(node));
        }
        return createXPath(xpression, variables).selectNodes(node);
    }

    public Node selectSingleNode(String xpression) {
        return this.selectSingleNode(xpression, null);
    }

    public Node selectSingleNode(String xpression, Map<String, Object> variables) {
        return this.selectSingleNode(this.document, xpression, variables);
    }

    public Node selectSingleNode(Node node, String xpression) {
        return this.selectSingleNode(node, xpression, null);
    }

    public Node selectSingleNode(Node node, String xpression, Map<String, Object> variables) {
        XPath xPath = createXPath(xpression, variables);
        if (node == this.document) {
            return this.xpathCache.computeIfAbsent(
                xpathAsString(xPath, variables), (e) -> xPath.selectSingleNode(node)
            );
        }
        return xPath.selectSingleNode(node);
    }

    public XPathEvaluator setPrefix(String prefix) {
        this.prefix = prefix.endsWith(":") ? prefix : prefix + ":";
        return this;
    }

    private XPath createXPath(String xpression, Map<String, Object> variables) {
        XPath xpath = DocumentHelper.createXPath(xpression, (ns, p, name) -> {
            if (variables == null || !variables.containsKey(name)) {
                throw new UnresolvableException(String.format("xpath expression %s expects unset variable %s",
                        xpression, name));
            }

            return variables.get(name);
        });

        xpath.setNamespaceURIs(this.namespaces);

        return xpath;
    }

    // Must handle the XML files with and without a default namespace for ODM.
    private String buildXPath(String... components) {
        StringBuilder xpath = new StringBuilder();
        boolean first = true;

        for (String comp : components) {
            if (!first && !hasNamespace(comp)) {
                xpath.append(this.prefix);
            } else {
                first = false;
            }

            xpath.append(comp);
        }

        return xpath.toString();
    }

    private boolean hasNamespace(String comp) {
        return comp.matches("^\\w*:.*");
    }

    public Document getDocument() {
        return this.document;
    }

    private String xpathAsString(XPath xPath, Map<String, Object> variables) {
        if (xPath == null) {
            return null;
        }
        if (variables == null) {
            return xPath.getText();
        }
        String xpath = xPath.getText();
        for (String key : variables.keySet()) {
            Object value = variables.get(key);
            xpath = StringUtils.replace(xpath, "$" + key, Objects.toString(value));
        }
        return xpath;
    }
}
