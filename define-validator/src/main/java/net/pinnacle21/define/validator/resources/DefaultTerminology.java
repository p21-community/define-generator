/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.resources;

import net.pinnacle21.define.models.CodeList;
import net.pinnacle21.define.models.CodeListItem;
import net.pinnacle21.define.models.DefineMetadata;
import net.pinnacle21.define.parsers.xml.XPathEvaluator;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;

import java.util.*;

public class DefaultTerminology implements Terminology {
    private final XPathEvaluator evaluator;
    private Set<String> nciCodes = new HashSet<>();
    private Map<String, CodeList> codeLists;
    private Map<String, String> codeListNames = new HashMap<>();

    DefaultTerminology(Document document) {
        this.evaluator = new XPathEvaluator(document, DefineMetadata.V2.getNamespaces().xpathNamespaces());
        this.prepareCodeLists();
    }

    @Override
    public boolean hasNciCode(String nciCode) {
        return this.nciCodes.contains(nciCode);
    }

    @Override
    public CodeList getCodeListByOID(String codeListOid) {
        return this.codeLists.get(codeListOid);
    }

    @Override
    public CodeListItem getCodeListItemByNCICodeAndCodeListOID(String codeListOID, String defineTermCode) {
        if (codeListOID == null || defineTermCode == null) {
            return null;
        }

        CodeList codeList = this.codeLists.get(codeListOID);

        if (codeList == null) {
            return null;
        }

        return codeList.getCodelistItems()
                .stream()
                .filter(item -> Objects.equals(item.getCode(), defineTermCode))
                .findFirst()
                .orElse(null);
    }

    @Override
    public CodeListItem getCodeListItemByCodedValueAndCodeListOID(String codeListOID, String codedValue) {
        if (codeListOID == null || codedValue == null) {
            return null;
        }

        CodeList codeList = this.codeLists.get(codeListOID);

        if (codeList == null) {
            return null;
        }

        return codeList.getCodelistItems()
                .stream()
                .filter(item -> Objects.equals(item.getCodedValue(), codedValue))
                .findFirst()
                .orElse(null);
    }

    @Override
    public boolean hasNamedCodeListWithCodedValue(String codeListName, String codedValue) {
        return this.getCodeListItemByCodedValueAndCodeListOID(this.codeListNames.get(codeListName), codedValue) != null;
    }

    private void prepareCodeLists() {
        if (this.codeLists != null) {
            return;
        }

        this.codeLists = new HashMap<>();

        this.evaluator.selectElements("/ODM/odm:Study/odm:MetaDataVersion/odm:CodeList")
                .stream()
                .map(this::mapCodeList)
                .filter(cl -> StringUtils.isNotBlank(cl.getOid()))
                .forEach(cl -> {
                    this.nciCodes.add(cl.getCode());
                    this.codeLists.put(cl.getOid(), cl);
                    this.codeListNames.put(cl.getName(), cl.getOid());

                    cl.getCodelistItems()
                            .stream()
                            .map(CodeListItem::getCode)
                            .forEach(this.nciCodes::add);
                });
    }

    private CodeList mapCodeList(Element codeListElement) {
        if (codeListElement == null) {
            return null;
        }

        CodeList codeList = new CodeList()
                .setOid(codeListElement.attributeValue("OID"))
                .setName(codeListElement.attributeValue("Name"))
                .setDataType(codeListElement.attributeValue("DataType"))
                .setCode(codeListElement.element("Alias") != null ? codeListElement.element("Alias").attributeValue("Name") : StringUtils.EMPTY)
                .setIsExtensible(codeListElement.attributeValue("CodeListExtensible"));

        for (Element element : codeListElement.elements("CodeListItem")) {
            codeList.put(mapCodeListItemElement(element));
        }

        for (Element element : codeListElement.elements("EnumeratedItem")) {
            codeList.put(mapCodeListItemElement(element));
        }

        return codeList;
    }

    private CodeListItem mapCodeListItemElement(Element element) {
        CodeListItem item = new CodeListItem()
                .setCodedValue(element.attributeValue("CodedValue"))
                .setOrder(element.attributeValue("OrderNumber"));

        Element decode = element.element("Decode");
        Element alias = element.element("Alias");

        if (decode != null) {
            Element translatedText = decode.element("TranslatedText");

            if (translatedText != null) {
                item.setDecodedValue(translatedText.getText());
                item.setLanguage(translatedText.attributeValue("xml:lang"));
            }
        }

        if (alias != null) {
            item.setCode(alias.attributeValue("Name"));
        }

        return item;
    }
}
