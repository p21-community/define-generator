/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.Description;
import net.pinnacle21.define.models.DocumentReference;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.models.arm.ResultDisplay;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.NamedRow;
import net.pinnacle21.parsing.excel.ValidationAwareExcelSheetParser;
import org.apache.commons.lang3.StringUtils;

class AnalysisDisplaySheetParser extends ValidationAwareExcelSheetParser<ParsedStudy> {
    static final String ID_COLUMN = "ID";
    static final String TITLE_COLUMN = "Title";
    static final String DOCUMENT_COLUMN = "Document";
    static final String PAGES_COLUMN = "Pages";
    static final String DUPLICATE_ERROR = "The analysis display '%s' is already defined, duplicates are not allowed";
    static final String DOCUMENT_REF_ERROR = "'%s' references the document '%s', but that document was not found";
    static final String PAGES_WITH_NO_DOCUMENT_WARNING = "Pages has a value, but no document is referenced";

    AnalysisDisplaySheetParser() {
        this.addValidatedRequiredColumn(ID_COLUMN);
        this.addValidatedColumn(TITLE_COLUMN, FieldValidations.LONG_LENGTH);
        this.addValidatedColumn(DOCUMENT_COLUMN);
        this.addValidatedColumn(PAGES_COLUMN);
    }

    @Override
    public void parse(NamedRow row, ParsedStudy parsedStudy) {
        if (!validate(row, parsedStudy)) {
            return;
        }
        Study study = parsedStudy.getStudy();

        String name = StringUtils.trim(row.get(ID_COLUMN));

        if (study.hasResultDisplay(name)) {
            parsedStudy.error(row.getIdentifier(), DUPLICATE_ERROR, name);
            return;
        }

        ResultDisplay resultDisplay = new ResultDisplay();
        resultDisplay.setName(name);

        Description description = new Description();
        description.setText(row.get(TITLE_COLUMN));
        resultDisplay.setDescription(description);

        String documentRef = StringUtils.trim(row.get(DOCUMENT_COLUMN));
        String pages = row.get(PAGES_COLUMN);

        if (StringUtils.isNotBlank(documentRef) || StringUtils.isNotBlank(pages)) {
            DocumentReference documentReference = new DocumentReference();
            documentReference.setPages(pages);
            if (StringUtils.isBlank(documentRef)) {
                parsedStudy.warn(row.getIdentifier(), PAGES_WITH_NO_DOCUMENT_WARNING);
            }
            if (study.hasDocument(documentRef)) {
                documentReference.setDocument(study.getDocument(documentRef));
                resultDisplay.setDocumentReference(documentReference);
            } else {
                parsedStudy.error(row.getIdentifier(), DOCUMENT_REF_ERROR, DOCUMENT_COLUMN, documentRef);
                return;
            }
        }

        study.setResultDisplay(name, resultDisplay);
    }
}
