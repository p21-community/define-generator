/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.Comment;
import net.pinnacle21.define.models.Document;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.util.MockNamedRow;
import net.pinnacle21.parsing.excel.ExcelSheetParser;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.RowIdentifier;
import org.junit.Test;
import org.mockito.Mockito;

import static net.pinnacle21.define.parsers.excel.CommentSheetParser.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mock;

public class CommentSheetParserTest {
    @Test
    public void nonexistingCommentGetsCreated() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new CommentSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedID = "COMMENT";
        String expectedDescription = "descriptive";
        String expectedDocument = "Document";
        String expectedPages = "Pages";

        study.setDocument(expectedDocument, new Document());

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID + " ")
                        .with(DESCRIPTION_COLUMN, expectedDescription)
                        .with(DOCUMENT_COLUMN, expectedDocument + " ")
                        .with(PAGES_COLUMN, expectedPages),
                parsedStudy
        );

        assertTrue(study.hasComment(expectedID));

        Comment comment = study.getComment(expectedID);

        assertEquals(expectedID, comment.getOid());
        assertEquals(expectedDescription, comment.getDescription());
        assertEquals(expectedDocument, comment.getDocumentOid());
    }

    @Test
    public void idColumnIsRequiredWarning() {
        ParsedStudy parsedStudy = mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new DocumentSheetParser();

        parser.parse(
                new MockNamedRow(),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR), eq(ID_COLUMN));
    }

    @Test
    public void invalidDocumentReferenceErrorRaised() {
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(new Study()));
        ExcelSheetParser<ParsedStudy> parser = new CommentSheetParser();

        String expectedID = "COMMENT";
        String expectedDocument = "DOC";

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID)
                        .with(DOCUMENT_COLUMN, expectedDocument),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(DOCUMENT_REF_ERROR), anyVararg());
    }

    @Test
    public void validDocumentReferenceIsAssigned() {
        Study study = new Study();
        ParsedStudy parsedStudy = new ParsedStudy(study);
        ExcelSheetParser<ParsedStudy> parser = new CommentSheetParser();

        String expectedID = "COMMENT";
        String expectedDocument = "DOC";
        Document document = new Document().setOid(expectedDocument);

        study.setDocument(expectedDocument, document);

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID)
                        .with(DOCUMENT_COLUMN, expectedDocument),
                parsedStudy
        );

        Comment comment = study.getComment(expectedID);

        assertNotNull(comment);
        assertSame(document.getOid(), comment.getDocumentOid());
    }

    @Test
    public void duplicateErrorRaised() {
        ParsedStudy parsedStudy = mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new CommentSheetParser();
        Study study = mock(Study.class);

        when(parsedStudy.getStudy()).thenReturn(study);

        String expectedID = "COMMENT";

        when(study.hasComment(expectedID)).thenReturn(true);

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(DUPLICATE_ERROR), anyVararg());
    }

    @Test
    public void duplicateErrorNotRaisedWithDifferentCasing() {
        Study study = new Study();
        ParsedStudy parsedStudy = new ParsedStudy(study);
        ExcelSheetParser<ParsedStudy> parser = new CommentSheetParser();

        String expectedID = "COMMENT";

        study.setComment(expectedID.toLowerCase(), new Comment());

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID),
                parsedStudy
        );

        assertEquals(true, study.hasComment(expectedID));
    }
}
