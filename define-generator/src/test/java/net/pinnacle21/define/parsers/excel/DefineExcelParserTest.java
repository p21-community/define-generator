/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.ExcelGenerator;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.models.arm.AnalysisResult;
import net.pinnacle21.define.util.ExcelComparator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class DefineExcelParserTest {
    @Rule
    public ErrorCollector errorCollector = new ErrorCollector();

    @Test
    public void testCompleteImport() throws Exception {
        File control = new File("src/test/resources/CDISC01-excel-control.xlsx");
        DefineExcelParser parser = new DefineExcelParser(control);
        Study study = parser.process().getStudy();

        File exported = ExcelGenerator.writeToDirectory(study, new File("src/test"));

        ExcelComparator comparator = new ExcelComparator(control, exported, errorCollector);
        comparator.process();
    }

    @Test
    public void testExcelStructureValidation() throws Exception {
        File control = new File("src/test/resources/MissingSheets.xlsx");
        DefineExcelParser parser = new DefineExcelParser(control);
        ParsedStudy parsedStudy = parser.process();

        Set<String> expectedErrors = new HashSet<>();
        expectedErrors.add("Required sheet Study is missing from the document");
        expectedErrors.add("Required sheet Variables is missing from the document");
        expectedErrors.add("Required sheet Datasets is missing from the document");

        assertEquals(expectedErrors, new HashSet<>(parsedStudy.getErrors()));
    }

    @Test
    public void testARMImport() throws Exception {
        File control = new File("src/test/resources/arm-import-control.xlsx");
        DefineExcelParser parser = new DefineExcelParser(control, true);
        ParsedStudy parsedStudy = parser.process();

        String expectedDisplay = "Test Display";
        String expectedResult = "Test Result";

        assertTrue(parsedStudy.getStudy().hasResultDisplay(expectedDisplay));

        AnalysisResult analysisResult = parsedStudy.getStudy().getAnalysisResult(expectedDisplay, expectedResult);
        assertNotNull(analysisResult);
        assertNotNull(analysisResult.getAnalysisDatasets());
    }
}
