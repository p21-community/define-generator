/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.Study;
import org.junit.Test;

import static net.pinnacle21.define.parsers.excel.ParsedStudy.StudyBuilder.*;
import static org.junit.Assert.assertEquals;

public class StudyBuilderTest {
    @Test
    public void studyWithAllExpectedAttributesIsCreated() {
        ParsedStudy.StudyBuilder builder = new ParsedStudy.StudyBuilder();

        String expectedName = "test";
        String expectedDescription = "descriptive";
        String expectedProtocol = "Protocol";
        String expectedStandardName = "sdtm";
        String expectedStandardVersion = "3.1.2";
        String expectedLanguage = "en";

        builder.setAttribute(NAME_ATTRIBUTE, expectedName);
        builder.setAttribute(DESCRIPTION_ATTRIBUTE, expectedDescription);
        builder.setAttribute(PROTOCOL_ATTRIBUTE, expectedProtocol);
        builder.setAttribute(STANDARD_NAME_ATTRIBUTE, expectedStandardName);
        builder.setAttribute(STANDARD_VERSION_ATTRIBUTE, expectedStandardVersion);
        builder.setAttribute(LANGUAGE_ATTRIBUTE, expectedLanguage);

        Study study = builder.build().getStudy();

        assertEquals(expectedName, study.getName());
        assertEquals(expectedDescription, study.getDescription());
        assertEquals(expectedProtocol, study.getProtocol());
        assertEquals(expectedStandardName, study.getStandardName());
        assertEquals(expectedStandardVersion, study.getStandardVersion());
        assertEquals(expectedLanguage, study.getLanguage());
    }

    @Test
    public void studyWithMissingNameDoesntFail() {
        ParsedStudy.StudyBuilder builder = new ParsedStudy.StudyBuilder();

        String expectedDescription = "descriptive";
        String expectedProtocol = "Protocol";
        String expectedStandardName = "sdtm";
        String expectedStandardVersion = "3.1.2";
        String expectedLanguage = "en";

        builder.setAttribute(DESCRIPTION_ATTRIBUTE, expectedDescription);
        builder.setAttribute(PROTOCOL_ATTRIBUTE, expectedProtocol);
        builder.setAttribute(STANDARD_NAME_ATTRIBUTE, expectedStandardName);
        builder.setAttribute(STANDARD_VERSION_ATTRIBUTE, expectedStandardVersion);
        builder.setAttribute(LANGUAGE_ATTRIBUTE, expectedLanguage);

        Study study = builder.build().getStudy();

        assertEquals("", study.getName());
        assertEquals(expectedDescription, study.getDescription());
        assertEquals(expectedProtocol, study.getProtocol());
        assertEquals(expectedStandardName, study.getStandardName());
        assertEquals(expectedStandardVersion, study.getStandardVersion());
        assertEquals(expectedLanguage, study.getLanguage());
    }

    @Test
    public void studyWithMissingDescriptionDoesntFail() {
        ParsedStudy.StudyBuilder builder = new ParsedStudy.StudyBuilder();

        String expectedName = "name";
        String expectedProtocol = "Protocol";
        String expectedStandardName = "sdtm";
        String expectedStandardVersion = "3.1.2";
        String expectedLanguage = "en";

        builder.setAttribute(NAME_ATTRIBUTE, expectedName);
        builder.setAttribute(PROTOCOL_ATTRIBUTE, expectedProtocol);
        builder.setAttribute(STANDARD_NAME_ATTRIBUTE, expectedStandardName);
        builder.setAttribute(STANDARD_VERSION_ATTRIBUTE, expectedStandardVersion);
        builder.setAttribute(LANGUAGE_ATTRIBUTE, expectedLanguage);

        Study study = builder.build().getStudy();

        assertEquals(expectedName, study.getName());
        assertEquals("", study.getDescription());
        assertEquals(expectedProtocol, study.getProtocol());
        assertEquals(expectedStandardName, study.getStandardName());
        assertEquals(expectedStandardVersion, study.getStandardVersion());
        assertEquals(expectedLanguage, study.getLanguage());
    }

    @Test
    public void studyWithMissingProtocolDoesntFail() {
        ParsedStudy.StudyBuilder builder = new ParsedStudy.StudyBuilder();

        String expectedName = "name";
        String expectedDescription = "descriptive";
        String expectedStandardName = "sdtm";
        String expectedStandardVersion = "3.1.2";
        String expectedLanguage = "en";

        builder.setAttribute(NAME_ATTRIBUTE, expectedName);
        builder.setAttribute(DESCRIPTION_ATTRIBUTE, expectedDescription);
        builder.setAttribute(STANDARD_NAME_ATTRIBUTE, expectedStandardName);
        builder.setAttribute(STANDARD_VERSION_ATTRIBUTE, expectedStandardVersion);
        builder.setAttribute(LANGUAGE_ATTRIBUTE, expectedLanguage);

        Study study = builder.build().getStudy();

        assertEquals(expectedName, study.getName());
        assertEquals(expectedDescription, study.getDescription());
        assertEquals("", study.getProtocol());
        assertEquals(expectedStandardName, study.getStandardName());
        assertEquals(expectedStandardVersion, study.getStandardVersion());
        assertEquals(expectedLanguage, study.getLanguage());
    }

    @Test
    public void studyWithMissingStandardNameDoesntFail() {
        ParsedStudy.StudyBuilder builder = new ParsedStudy.StudyBuilder();

        String expectedName = "name";
        String expectedDescription = "descriptive";
        String expectedProtocol = "protocol";
        String expectedStandardVersion = "3.1.2";
        String expectedLanguage = "en";

        builder.setAttribute(NAME_ATTRIBUTE, expectedName);
        builder.setAttribute(DESCRIPTION_ATTRIBUTE, expectedDescription);
        builder.setAttribute(PROTOCOL_ATTRIBUTE, expectedProtocol);
        builder.setAttribute(STANDARD_VERSION_ATTRIBUTE, expectedStandardVersion);
        builder.setAttribute(LANGUAGE_ATTRIBUTE, expectedLanguage);

        Study study = builder.build().getStudy();

        assertEquals(expectedName, study.getName());
        assertEquals(expectedDescription, study.getDescription());
        assertEquals(expectedProtocol, study.getProtocol());
        assertEquals("", study.getStandardName());
        assertEquals(expectedStandardVersion, study.getStandardVersion());
        assertEquals(expectedLanguage, study.getLanguage());
    }

    @Test
    public void studyWithMissingStandardVersionDoesntFail() {
        ParsedStudy.StudyBuilder builder = new ParsedStudy.StudyBuilder();

        String expectedName = "name";
        String expectedDescription = "descriptive";
        String expectedProtocol = "protocol";
        String expectedStandardName = "sdtm";
        String expectedLanguage = "en";

        builder.setAttribute(NAME_ATTRIBUTE, expectedName);
        builder.setAttribute(DESCRIPTION_ATTRIBUTE, expectedDescription);
        builder.setAttribute(PROTOCOL_ATTRIBUTE, expectedProtocol);
        builder.setAttribute(STANDARD_NAME_ATTRIBUTE, expectedStandardName);
        builder.setAttribute(LANGUAGE_ATTRIBUTE, expectedLanguage);

        Study study = builder.build().getStudy();

        assertEquals(expectedName, study.getName());
        assertEquals(expectedDescription, study.getDescription());
        assertEquals(expectedProtocol, study.getProtocol());
        assertEquals(expectedStandardName, study.getStandardName());
        assertEquals("", study.getStandardVersion());
        assertEquals(expectedLanguage, study.getLanguage());
    }

    @Test
    public void studyWithMissingLanguageDoesntFail() {
        ParsedStudy.StudyBuilder builder = new ParsedStudy.StudyBuilder();

        String expectedName = "name";
        String expectedDescription = "descriptive";
        String expectedProtocol = "protocol";
        String expectedStandardName = "sdtm";
        String expectedStandardVersion = "3.1.2";

        builder.setAttribute(NAME_ATTRIBUTE, expectedName);
        builder.setAttribute(DESCRIPTION_ATTRIBUTE, expectedDescription);
        builder.setAttribute(PROTOCOL_ATTRIBUTE, expectedProtocol);
        builder.setAttribute(STANDARD_NAME_ATTRIBUTE, expectedStandardName);
        builder.setAttribute(STANDARD_VERSION_ATTRIBUTE, expectedStandardVersion);

        Study study = builder.build().getStudy();

        assertEquals(expectedName, study.getName());
        assertEquals(expectedDescription, study.getDescription());
        assertEquals(expectedProtocol, study.getProtocol());
        assertEquals(expectedStandardName, study.getStandardName());
        assertEquals(expectedStandardVersion, study.getStandardVersion());
        assertEquals("", study.getLanguage());
    }
}
