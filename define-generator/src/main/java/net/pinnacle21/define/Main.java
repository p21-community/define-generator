/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define;

import net.pinnacle21.define.parsers.excel.DefineExcelParser;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Usage: java -jar define-generator.jar <template> <destination>");
            System.exit(1);
        }

        File template = new File(args[0]);
        File destination = new File(args[1]);

        if (!template.isFile()) {
            System.out.println(String.format("%s is not a valid file (or is inaccessible)",
                    template.getPath()));
            System.exit(2);
        }

        if (!destination.isDirectory()) {
            System.out.println(String.format("%s is not a valid directory (or is inaccessible)",
                    destination.getPath()));
            System.exit(3);
        }

        File define = new File(destination, "define.xml");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        if (define.exists()) {
            System.out.print(String.format("The file %s already exists, overwrite? (Y/N) ",
                    define.getPath()));

            try {
                if (reader.readLine().toUpperCase().startsWith("Y")) {
                    if (!define.canWrite()) {
                        System.out.println(String.format("Cannot write to file %s",
                                define.getPath()));
                        System.exit(4);
                    }
                } else {
                    System.exit(4);
                }
            } catch (IOException ignore) {
                System.exit(10);
            }
        }

        System.out.println("Generating define.xml, please wait...");

        try {
            DefineExcelParser parser = new DefineExcelParser(template);
            DefineGenerator generator = new DefineGenerator(parser.process().getStudy(), define);

            generator.generate();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(10);
        }

        System.out.println(String.format("define.xml file saved to %s", define.getPath()));
    }
}
