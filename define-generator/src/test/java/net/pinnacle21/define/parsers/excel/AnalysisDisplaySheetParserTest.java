/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.Document;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.models.arm.ResultDisplay;
import net.pinnacle21.define.util.MockNamedRow;
import net.pinnacle21.parsing.excel.ExcelSheetParser;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.RowIdentifier;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import static net.pinnacle21.define.parsers.excel.AnalysisDisplaySheetParser.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyVararg;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class AnalysisDisplaySheetParserTest {
    @Test
    public void nonexistingStructureGetsCreated() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new AnalysisDisplaySheetParser();
        Study study = parsedStudy.getStudy();

        String expectedID = "ID";
        String expectedTitle = "title";

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID + " ")
                        .with(TITLE_COLUMN, expectedTitle),
                parsedStudy
        );

        assertTrue(study.hasResultDisplay(expectedID));

        ResultDisplay resultDisplay = study.getResultDisplay(expectedID);

        assertEquals(expectedID, resultDisplay.getName());
        assertEquals(expectedTitle, resultDisplay.getDescription().getText());
    }

    @Test
    public void nameColumnIsRequired() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new AnalysisDisplaySheetParser();

        parser.parse(
                new MockNamedRow(),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR), eq(ID_COLUMN));
    }

    @Test
    public void pagesWithNoDocumentWarningRaised() {
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(new Study()));
        ExcelSheetParser<ParsedStudy> parser = new AnalysisDisplaySheetParser();

        String expectedID = "ID";
        String expectedPages = "1, 2, 3";

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID)
                        .with(PAGES_COLUMN, expectedPages),
                parsedStudy
        );

        verify(parsedStudy).warn(any(RowIdentifier.class), eq(PAGES_WITH_NO_DOCUMENT_WARNING));
    }

    @Test
    public void invalidDocumentReferenceErrorRaised() {
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(new Study()));
        ExcelSheetParser<ParsedStudy> parser = new AnalysisDisplaySheetParser();

        String expectedID = "ID";
        String expectedDocument = "Document";

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID)
                        .with(DOCUMENT_COLUMN, expectedDocument),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(DOCUMENT_REF_ERROR), anyVararg());
    }

    @Test
    public void validDocumentReferenceIsAssigned() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new AnalysisDisplaySheetParser();
        Study study = parsedStudy.getStudy();

        String expectedID = "DATASET";
        String expectedDocument = "Document";
        String expectedPages = "1, 2, 3";
        Document document = new Document().setOid(expectedDocument);

        study.setDocument(expectedDocument, document);

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID)
                        .with(DOCUMENT_COLUMN, expectedDocument)
                        .with(PAGES_COLUMN, expectedPages),
                parsedStudy
        );

        ResultDisplay resultDisplay = study.getResultDisplay(expectedID);

        assertNotNull(resultDisplay);
        assertNotNull(resultDisplay.getDocumentReference());
        assertNotNull(resultDisplay.getDocumentReference().getDocument());
        assertEquals(document.getOid(), resultDisplay.getDocumentReference().getDocument().getOid());
        assertEquals(expectedPages, resultDisplay.getDocumentReference().getPages());
    }

    @Test
    public void duplicateErrorRaised() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new AnalysisDisplaySheetParser();
        Study study = mock(Study.class);

        when(parsedStudy.getStudy()).thenReturn(study);

        String expectedID = "ID";

        when(study.hasResultDisplay(expectedID)).thenReturn(true);

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(DUPLICATE_ERROR), anyVararg());
    }

    @Test
    public void duplicateErrorNotRaisedWithDifferentCasing() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new AnalysisDisplaySheetParser();
        Study study = parsedStudy.getStudy();

        String expectedID = "ID";

        study.setResultDisplay(expectedID.toLowerCase(), new ResultDisplay());

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID),
                parsedStudy
        );

        assertEquals(true, study.hasResultDisplay(expectedID));
    }
}
