/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.xml.bodyparsers;

import net.pinnacle21.define.models.Item;
import net.pinnacle21.define.models.ItemGroup;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.models.ValueList;
import org.dom4j.Element;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public interface Stubs {

    Study STUDY = Helpers.getStudy();
    List<Element> VALUE_LIST_ELEMENTS = Helpers.getValueListList();
    List<ValueList> EXPECTED_VALUE_LISTS = Helpers.getExpectedValueLists();

    class Helpers {
        static final String ITEM_GROUP_1_NAME = "IG1";
        static final String ITEM_GROUP_2_NAME = "IG2";

        static final String ITEM_1_NAME = "IT1";
        static final String ITEM_2_NAME = "IT2";

        static final String VALUE_LIST_ID_1 = "VL1";
        static final String VALUE_LIST_ID_2 = "VL2";

        static Study getStudy() {
            Study study = new Study();
            study.setItemGroup(ITEM_GROUP_1_NAME, getIG1());
            study.setItemGroup(ITEM_GROUP_2_NAME, getIG2());
            return study;
        }

        private static ItemGroup getIG1() {
            ItemGroup itemGroup = new ItemGroup()
                    .setName(ITEM_GROUP_1_NAME);
            itemGroup.add(getIT1());
            return itemGroup;
        }

        private static Item getIT1() {
            return new Item()
                    .setName(ITEM_1_NAME)
                    .setValueListOid(VALUE_LIST_ID_1);
        }

        private static ItemGroup getIG2() {
            ItemGroup itemGroup = new ItemGroup()
                    .setName(ITEM_GROUP_2_NAME);
            itemGroup.add(getIT2());
            return itemGroup;
        }

        private static Item getIT2() {
            return new Item()
                    .setName(ITEM_2_NAME);
        }

        private static List<Element> getValueListList() {
            List<Element> valueLists = new ArrayList<>();
            Element valueList1 = Mockito.mock(Element.class);
            Mockito.when(valueList1.attributeValue("OID"))
                    .thenReturn(VALUE_LIST_ID_1);
            Element valueList2 = Mockito.mock(Element.class);
            Mockito.when(valueList2.attributeValue("OID"))
                    .thenReturn(VALUE_LIST_ID_2);
            valueLists.add(valueList1);
            valueLists.add(valueList2);
            return valueLists;
        }

        private static List<ValueList> getExpectedValueLists() {
            List<ValueList> list = new ArrayList<>();
            ValueList valueList1 = new ValueList()
                    .setOid(ITEM_GROUP_1_NAME + "." + ITEM_1_NAME)
                    .setSourceItemGroupName(ITEM_GROUP_1_NAME)
                    .setSourceItemName(ITEM_1_NAME);
            ValueList valueList2 = new ValueList()
                    .setOid(VALUE_LIST_ID_2)
                    .setSourceItemGroupName(VALUE_LIST_ID_2)
                    .setSourceItemName(VALUE_LIST_ID_2);
            list.add(valueList1);
            list.add(valueList2);
            return list;
        }
    }
}
