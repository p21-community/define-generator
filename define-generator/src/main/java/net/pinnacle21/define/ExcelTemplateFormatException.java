/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define;

import java.util.Iterator;
import java.util.List;

public class ExcelTemplateFormatException extends RuntimeException {
    private List<String> errors;

    public ExcelTemplateFormatException(List<String> errors) {
        super(build(errors));
        this.errors = errors;
    }

    private static String build(List<String> errors) {
        StringBuilder buffer = new StringBuilder("The provided Excel template had the following structural issues:");
        Iterator i$ = errors.iterator();

        while(i$.hasNext()) {
            String error = (String)i$.next();
            buffer.append('\n').append("   - ").append(error);
        }

        return buffer.toString();
    }

    public List<String> getErrors() {
        return this.errors;
    }
}
