/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.Item;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.NamedRow;
import net.pinnacle21.parsing.excel.ValidationAwareExcelSheetParser;
import org.apache.commons.lang3.StringUtils;

public class VariableSheetParser extends ValidationAwareExcelSheetParser<ParsedStudy> {
    static final String ORDER_COLUMN = "Order";
    static final String DATASET_COLUMN = "Dataset";
    static final String VARIABLE_COLUMN = "Variable";
    static final String LABEL_COLUMN = "Label";
    static final String DATA_TYPE_COLUMN = "Data Type";
    static final String LENGTH_COLUMN = "Length";
    static final String SIGNIFICANT_DIGITS_COLUMN = "Significant Digits";
    static final String FORMAT_COLUMN = "Format";
    static final String MANDATORY_COLUMN = "Mandatory";
    static final String CODELIST_COLUMN = "Codelist";
    static final String ORIGIN_COLUMN = "Origin";
    static final String PAGES_COLUMN = "Pages";
    static final String METHOD_COLUMN = "Method";
    static final String PREDECESSOR_COLUMN = "Predecessor";
    static final String ROLE_COLUMN = "Role";
    static final String COMMENT_COLUMN = "Comment";
    static final String DUPLICATE_ERROR = "The variable '%s' has already been defined for the dataset '%s', duplicates "
            + "are not allowed";
    static final String DATASET_REF_ERROR = "'%s' references the dataset '%s', but that dataset was not found";
    static final String COMMENT_REF_ERROR = "'%s' references the comment '%s', but that comment was not found";
    static final String METHOD_REF_ERROR = "'%s' references the method '%s', but that method was not found";
    static final String CODELIST_REF_ERROR = "'%s' references the codelist or dictionary '%s', but that "
            + "codelist/dictionary was not found";

    VariableSheetParser() {
        this.addValidatedRequiredColumn(DATASET_COLUMN);
        this.addValidatedRequiredColumn(VARIABLE_COLUMN);
        this.addValidatedColumn(ORDER_COLUMN);
        this.addValidatedColumn(LABEL_COLUMN);
        this.addValidatedColumn(DATA_TYPE_COLUMN);
        this.addValidatedColumn(LENGTH_COLUMN);
        this.addValidatedColumn(SIGNIFICANT_DIGITS_COLUMN);
        this.addValidatedColumn(FORMAT_COLUMN);
        this.addValidatedColumn(MANDATORY_COLUMN);
        this.addValidatedColumn(CODELIST_COLUMN);
        this.addValidatedColumn(ORIGIN_COLUMN);
        this.addValidatedColumn(PAGES_COLUMN, FieldValidations.REALLY_LONG_LENGTH);
        this.addValidatedColumn(METHOD_COLUMN);
        this.addValidatedColumn(PREDECESSOR_COLUMN, FieldValidations.REALLY_LONG_LENGTH);
        this.addValidatedColumn(ROLE_COLUMN);
        this.addValidatedColumn(COMMENT_COLUMN);
    }

    @Override
    public void parse(NamedRow row, ParsedStudy parsedStudy) {
        if (!this.validate(row, parsedStudy)) {
            return;
        }
        Study study = parsedStudy.getStudy();

        String structureRef = StringUtils.trim(row.get(DATASET_COLUMN));
        String name = StringUtils.trim(row.get(VARIABLE_COLUMN));

        if (study.hasItem(structureRef, name)) {
            parsedStudy.error(row.getIdentifier(), DUPLICATE_ERROR, row.get(VARIABLE_COLUMN), row.get(DATASET_COLUMN));

            return;
        }

        Item item = new Item();

        item.setName(name);
        item.setOrder(row.get(ORDER_COLUMN));
        item.setLabel(row.get(LABEL_COLUMN));
        item.setDataType(row.get(DATA_TYPE_COLUMN));
        item.setLength(row.get(LENGTH_COLUMN));
        item.setSignificantDigits(row.get(SIGNIFICANT_DIGITS_COLUMN));
        item.setFormat(row.get(FORMAT_COLUMN));
        item.setMandatory(row.get(MANDATORY_COLUMN));
        item.setOrigin(row.get(ORIGIN_COLUMN));
        item.setPages(row.get(PAGES_COLUMN));
        item.setPredecessor(row.get(PREDECESSOR_COLUMN));
        item.setRole(row.get(ROLE_COLUMN));

        if (StringUtils.isBlank(structureRef) || !study.hasItemGroup(structureRef)) {
            parsedStudy.error(row.getIdentifier(), DATASET_REF_ERROR, DATASET_COLUMN, structureRef);

            return;
        }

        Integer keySequence = study.getKeyVariablePosition(structureRef, name);
        item.setKeySequence(keySequence);

        String commentRef = StringUtils.trim(row.get(COMMENT_COLUMN));

        if (StringUtils.isNotBlank(commentRef)) {
            if (study.hasComment(commentRef)) {
                item.setCommentOid(commentRef);
            } else {
                parsedStudy.error(row.getIdentifier(), COMMENT_REF_ERROR, COMMENT_COLUMN, commentRef);
            }
        }

        String methodRef = StringUtils.trim(row.get(METHOD_COLUMN));

        if (StringUtils.isNotBlank(methodRef)) {
            if (study.hasMethod(methodRef)) {
                item.setMethodOid(methodRef);
            } else {
                parsedStudy.error(row.getIdentifier(), METHOD_REF_ERROR, METHOD_COLUMN, methodRef);
            }
        }

        String codelistRef = StringUtils.trim(row.get(CODELIST_COLUMN));

        if (StringUtils.isNotBlank(codelistRef)) {
            if (study.hasCodeList(codelistRef)) {
                item.setCodeListOid(codelistRef);
            } else {
                if (study.hasDictionary(codelistRef)) {
                    item.setDictionaryOid(codelistRef);
                } else {
                    parsedStudy.error(row.getIdentifier(), CODELIST_REF_ERROR, CODELIST_COLUMN, codelistRef);
                }
            }
        }

        study.getItemGroup(structureRef).add(item);
        study.setItem(structureRef, name, item);
    }
}
