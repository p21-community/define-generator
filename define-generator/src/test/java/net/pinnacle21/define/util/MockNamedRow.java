/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.util;

import net.pinnacle21.parsing.excel.NamedRow;
import net.pinnacle21.parsing.excel.RowIdentifier;

import java.util.HashMap;
import java.util.Map;

public class MockNamedRow implements NamedRow {
    private final Map<String, String> columns = new HashMap<>();

    public MockNamedRow with(String column, String value) {
        this.columns.put(column, value);

        return this;
    }

    @Override
    public String get(String column) {
        return this.columns.get(column);
    }

    @Override
    public boolean has(String column) {
        return this.columns.containsKey(column);
    }

    @Override
    public RowIdentifier getIdentifier() {
        return null;
    }
}
