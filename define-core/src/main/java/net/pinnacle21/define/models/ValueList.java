/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models;

import org.apache.commons.lang3.tuple.Triple;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ValueList extends ExcelRecord {
    private String oid;
    private String sourceItemGroupName;
    private String sourceItemName;

    private Map<Triple<String, String, String>, Item> items = new LinkedHashMap<>();

    public ValueList() {}

    public ValueList(String datasetName, String variableName) {
        this.oid = datasetName + "." + variableName;
        setSourceItemGroupName(datasetName);
        setSourceItemName(variableName);
    }

    public Item getItem(String whereClauseId) {
        return this.items.get(Triple.of(sourceItemGroupName, sourceItemName, whereClauseId));
    }

    public boolean hasItem(String whereClauseId) {
        return this.items.containsKey(Triple.of(sourceItemGroupName, sourceItemName, whereClauseId));
    }

    public List<Item> getItems() {
        return new ArrayList<>(this.items.values());
    }

    public ValueList add(Item item) {
        this.items.put(Triple.of(sourceItemGroupName, sourceItemName, item.getWhereClauseOid()), item);
        return this;
    }

    public String getOid() {
        return oid;
    }

    public ValueList setOid(String oid) {
        this.oid = oid != null ? oid.trim() : null;
        return this;
    }

    public String getSourceItemGroupName() {
        return sourceItemGroupName;
    }

    public ValueList setSourceItemGroupName(String sourceItemGroupName) {
        this.sourceItemGroupName = sourceItemGroupName;
        return this;
    }

    public String getSourceItemName() {
        return sourceItemName;
    }

    public ValueList setSourceItemName(String sourceItemName) {
        this.sourceItemName = sourceItemName;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ValueList valueList = (ValueList) o;

        if (oid != null ? !oid.equals(valueList.oid) : valueList.oid != null) return false;
        if (sourceItemGroupName != null ? !sourceItemGroupName.equals(valueList.sourceItemGroupName) : valueList.sourceItemGroupName != null)
            return false;
        if (sourceItemName != null ? !sourceItemName.equals(valueList.sourceItemName) : valueList.sourceItemName != null)
            return false;
        return items != null ? items.equals(valueList.items) : valueList.items == null;
    }

    @Override
    public int hashCode() {
        int result = oid != null ? oid.hashCode() : 0;
        result = 31 * result + (sourceItemGroupName != null ? sourceItemGroupName.hashCode() : 0);
        result = 31 * result + (sourceItemName != null ? sourceItemName.hashCode() : 0);
        result = 31 * result + (items != null ? items.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ValueList{" +
                "oid='" + oid + '\'' +
                ", sourceItemGroupName='" + sourceItemGroupName + '\'' +
                ", sourceItemName='" + sourceItemName + '\'' +
                '}';
    }
}
