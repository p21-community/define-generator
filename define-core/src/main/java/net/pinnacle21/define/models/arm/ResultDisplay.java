/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models.arm;

import net.pinnacle21.define.models.Description;
import net.pinnacle21.define.models.DocumentReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ResultDisplay {
    private String oid;
    private String name;
    private Description description;
    private DocumentReference documentReference;
    private List<AnalysisResult> analysisResults = new ArrayList<>();

    public String getOID() {
        return oid;
    }

    public ResultDisplay setOID(String oid) {
        this.oid = oid;
        return this;
    }

    public String getName() {
        return name;
    }

    public ResultDisplay setName(String name) {
        this.name = name;
        return this;
    }

    public Description getDescription() {
        return description;
    }

    public ResultDisplay setDescription(Description description) {
        this.description = description;
        return this;
    }

    public DocumentReference getDocumentReference() {
        return documentReference;
    }

    public ResultDisplay setDocumentReference(DocumentReference document) {
        this.documentReference = document;
        return this;
    }

    public List<AnalysisResult> getAnalysisResults() {
        return Collections.unmodifiableList(analysisResults);
    }

    public ResultDisplay addAll(List<AnalysisResult> analysisResults) {
        if (analysisResults != null) {
            this.analysisResults.addAll(analysisResults);
        }
        return this;
    }

    public ResultDisplay add(AnalysisResult analysisResult) {
        this.analysisResults.add(analysisResult);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ResultDisplay display = (ResultDisplay) o;

        if (oid != null ? !oid.equals(display.oid) : display.oid != null) return false;
        if (getName() != null ? !getName().equals(display.getName()) : display.getName() != null) return false;
        if (getDescription() != null ? !getDescription().equals(display.getDescription()) : display.getDescription() != null)
            return false;
        if (getDocumentReference() != null ? !getDocumentReference().equals(display.getDocumentReference()) : display.getDocumentReference() != null)
            return false;
        return getAnalysisResults() != null ? getAnalysisResults().equals(display.getAnalysisResults()) : display.getAnalysisResults() == null;

    }
}
