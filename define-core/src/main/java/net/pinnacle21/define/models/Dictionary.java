/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models;

public class Dictionary extends ExcelRecord {
    private String oid;
    private String name;
    private String dataType;
    private String dictionary;
    private String version;

    public String getOid() {
        return oid;
    }

    public Dictionary setOid(String oid) {
        this.oid = oid != null ? oid.trim() : null;
        return this;
    }

    public String getName() {
        return name;
    }

    public Dictionary setName(String name) {
        this.name = name != null ? name.trim() : null;
        return this;
    }

    public String getDataType() {
        return dataType;
    }

    public Dictionary setDataType(String dataType) {
        this.dataType = dataType != null ? dataType.trim() : null;
        return this;
    }

    public String getDictionary() {
        return dictionary;
    }

    public Dictionary setDictionary(String dictionary) {
        this.dictionary = dictionary != null ? dictionary.trim() : null;
        return this;
    }

    public String getVersion() {
        return version;
    }

    public Dictionary setVersion(String version) {
        this.version = version != null ? version.trim() : null;
        return this;
    }
}
