/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models;

import net.pinnacle21.define.parsers.xml.util.ParserUtils;

public class Comment extends ExcelRecord {
    private String oid;
    private String description;
    private String documentOid;
    private String pages;
    private String lastPage;
    private String firstPage;

    public String getOid() {
        return oid;
    }

    public Comment setOid(String oid) {
        this.oid = oid != null ? oid.trim() : null;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Comment setDescription(String description) {
        this.description = description != null ? description.trim() : null;
        return this;
    }

    public String getDocumentOid() {
        return documentOid;
    }

    public Comment setDocumentOid(String documentOid) {
        this.documentOid = documentOid != null ? documentOid.trim() : null;
        return this;
    }

    public String getPages() {
        return pages;
    }

    public Comment setPages(String pages) {
        this.pages = pages != null ? pages.trim() : null;
        return this;
    }

    public Comment setFirstPage(String firstPage) {
        this.firstPage = firstPage != null ? firstPage.trim() : null;
        return this;
    }

    public String getFirstPage() {
        return firstPage;
    }

    public Comment setLastPage(String lastPage) {
        this.lastPage = lastPage != null ? lastPage.trim() : null;
        return this;
    }

    public String getLastPage() {
        return lastPage;
    }

    public String getAnyPages() {
        return ParserUtils.aggregatePages(getPages(), getFirstPage(), getLastPage());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comment comment = (Comment) o;

        if (getOid() != null ? !getOid().equals(comment.getOid()) : comment.getOid() != null) return false;
        if (getDescription() != null ? !getDescription().equals(comment.getDescription()) : comment.getDescription() != null)
            return false;
        if (getDocumentOid() != null ? !getDocumentOid().equals(comment.getDocumentOid()) : comment.getDocumentOid() != null)
            return false;
        return getPages() != null ? getPages().equals(comment.getPages()) : comment.getPages() == null;
    }
}
