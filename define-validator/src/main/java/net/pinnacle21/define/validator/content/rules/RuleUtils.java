/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.content.rules;

import net.pinnacle21.define.models.ItemGroup;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Element;

import java.util.regex.Pattern;

public class RuleUtils {

    /**
     * @see RuleUtils#isSplitDataset(String, String)
     */
    @Deprecated
    static boolean isSplitDataset(String domain, Element itemGroupDefElement) {
        return isSplitDataset(domain, getBaseHref(itemGroupDefElement.element("leaf")));
    }

    /**
     * @see RuleUtils#isSplitDataset(String, String)
     */
    @Deprecated
    static boolean isSplitDataset(Element itemGroupDefElement) {
        return isSplitDataset(itemGroupDefElement.attributeValue("Domain"), itemGroupDefElement);
    }

    private static boolean isSplitDataset(String domain, String href) {
        boolean isSplit = false;
        if (href.length() == 2) { // e.g, "QS" or "TA"
            return false;
        }

        // case insensitive match of ${domain}--: e.g. AE01, AE2, QSCG
        Pattern splitPattern = Pattern.compile(String.format("(?i)%S.{1,2}", domain));

        if (splitPattern.matcher(href).matches()) {
            isSplit = true;
        }

        return isSplit;
    }

    /**
     * <p>Decides whether or not an ItemGroup represents part of a Split Dataset.</p>
     *
     * <p>Based on the logic from the Define.xml and SDTM 3.2 IG.</p>
     *
     * <blockquote>
     *      <p>The Name attribute in each of the ItemGroupDef
     *      elements must contain the name of the split dataset and SASDatasetName attribute must
     *      match the name of the specific SAS Transport file (without the .xpt extension). The Domain
     *      attribute must be provided and must match the value of the DOMAIN variable in the
     *      corresponding dataset.</p>
     * </blockquote>
     *- <cite>Page 15, Define.xml 2.0 IG</cite>
     *
     * <blockquote>
     *      <p>Split dataset names can be <b>up to four characters in length.</b></p>
     * </blockquote>
     *- <cite>Page 26, SDTM 3.2 IG</cite>
     *
     * <blockquote>
     *      <p>Supplemental Qualifier datasets for split domains would also be split. The nomenclature would include the
     *      additional one-to-two characters used to identify the split dataset (e.g., SUPPQS36, SUPPFACM).</p>
     * </blockquote>
     * - <cite>Page 26, SDTM 3.2 I.G</cite>
     *
     * @return true if and only if the above logic is satisfied.
     */
    @Deprecated
    public static boolean isSplitDataset(ItemGroup defineItemGroup) {
        String name = StringUtils.defaultString(defineItemGroup.getName());
        //noinspection SimplifiableIfStatement (it's more readable expanded)
        if (name.startsWith("SUPP")) {
            if (name.length() == 6) {
                return false;
            } else if (name.length() > 6 && name.length() <= 8) {
                return true;
            }
        }
        return name.length() > 2 && name.length() <= 4;
    }

    /**
     *
     * @return the name of the file inside a 'xlink:href' for a def:leaf
     */
    static String getBaseHref(Element leaf) {
        return FilenameUtils.getBaseName(getHref(leaf));
    }

    /**
     *
     * @return the 'xlink:href' attribute value from a leaf element.
     */
    private static String getHref(Element leaf) {
        if (leaf != null) {
            return leaf.attributeValue("href");
        }
        return "";
    }

    /**
     * The 2-letter suffix in SASDatasetName attribute and Domain attribute must have the same value.
     * This only applies for split datasets.
     *
     * @param domain the value of the Domain attribute.
     * @param sasDatasetName the value of the SASDatasetName.
     * @return true if the prefixes are equal, otherwise false.
     */
    static boolean validatePrefix(String domain, String sasDatasetName) {
        if (domain == null || sasDatasetName == null) {
            return false;
        }
        return sasDatasetName.startsWith(domain);
    }

    public static String getDescription(Element elementWithDescription) {
        if (elementWithDescription.element("Description") != null
                && elementWithDescription.element("Description").element("TranslatedText") != null) {
           return elementWithDescription.element("Description").elementText("TranslatedText");
        }
        return StringUtils.EMPTY;
    }
}
