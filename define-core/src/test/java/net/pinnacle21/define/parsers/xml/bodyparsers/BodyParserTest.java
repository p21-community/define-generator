/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.xml.bodyparsers;

import net.pinnacle21.define.models.ValueList;
import net.pinnacle21.define.parsers.xml.XPathEvaluator;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.tree.DefaultDocument;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BodyParserTest {

    @Test
    public void parseValuelistsWithMatchingItemGroups() {
        XPathEvaluator mockEvaluator = Mockito.mock(XPathEvaluator.class);
        List<Element> list = Stubs.VALUE_LIST_ELEMENTS;
        Mockito.when(mockEvaluator.selectElements("/ODM/odm:Study/odm:MetaDataVersion/def:ValueListDef"))
                .thenReturn(list);
        BodyParser parser = new DefineV2BodyParser(new DefaultDocument(), true);
        parser.study = Stubs.STUDY; //should probs be encapsulated
        parser.evaluator = mockEvaluator; //should probs be encapsulated
        List<ValueList> actual = parser.parseValueLists(new HashMap<>());
        Assert.assertEquals(Stubs.EXPECTED_VALUE_LISTS, actual);
    }

    /**
     * @see <a href="https://pinnacle21.atlassian.net/browse/DEF-176">DEF-176</a>
     */
    @Test
    public void usefulNamesForOtherValueListsWhenOIDHas1() {
        String oid = "VL.ABC";
        List<ValueList> valueLevelList = createRemainingValuelistsWithOID(oid);
        ValueList expected = new ValueList()
                .setOid("ABC")
                .setSourceItemGroupName("ABC")
                .setSourceItemName("ABC");
        Assert.assertEquals(expected, valueLevelList.get(0));
    }

    /**
     * @see <a href="https://pinnacle21.atlassian.net/browse/DEF-176">DEF-176</a>
     */
    @Test
    public void usefulNamesForOtherValueListsWhenOIDHas2() {
        String oid = "VL.ABC.123";
        List<ValueList> valueLevelList = createRemainingValuelistsWithOID(oid);
        ValueList expected = new ValueList()
                .setOid("ABC.123")
                .setSourceItemGroupName("ABC")
                .setSourceItemName("123");
        Assert.assertEquals(expected, valueLevelList.get(0));
    }

    /**
     * @see <a href="https://pinnacle21.atlassian.net/browse/DEF-176">DEF-176</a>
     */
    @Test
    public void usefulNamesForOtherValueListsWhenOIDHas3() {
        String oid = "VL.ABC.123.XYZ";
        List<ValueList> valueLevelList = createRemainingValuelistsWithOID(oid);
        ValueList expected = new ValueList()
                .setOid("ABC.123.XYZ")
                .setSourceItemGroupName("ABC.123")
                .setSourceItemName("XYZ");
        Assert.assertEquals(expected, valueLevelList.get(0));
    }

    /**
     * @see <a href="https://pinnacle21.atlassian.net/browse/DEF-176">DEF-176</a>
     */
    @Test
    public void usefulNamesForOtherValueListsWhenOIDHasMore() {
        String oid = "ABC.123.XYZ.456.QRS";
        List<ValueList> valueLevelList = createRemainingValuelistsWithOID(oid);
        ValueList expected = new ValueList()
                .setOid(oid)
                .setSourceItemGroupName(oid)
                .setSourceItemName(oid);
        Assert.assertEquals(expected, valueLevelList.get(0));
    }

    /**
     * @param oid used to generate the ValueList
     * @return ValueList generated purely off of the oid passed
     */
    private List<ValueList> createRemainingValuelistsWithOID(String oid) {
        BodyParser mockParser = Mockito.mock(BodyParser.class);
        Element mockElement = Mockito.mock(Element.class);
        Mockito.when(mockElement.attributeValue("OID")).thenReturn(oid);
        List<Element> valueListDefList = new ArrayList<>();
        valueListDefList.add(mockElement);
        List<ValueList> valueLevelList = new ArrayList<>();
        String[] oids = new String[]{oid};

        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) {
                BodyParser parser = new DefineV2BodyParser(DocumentFactory.getInstance().createDocument(), false);
                Object[] args = invocation.getArguments();
                parser.createRemainingValueLists((List<Element>) args[0], (List<ValueList>) args[1], (String[]) args[2]);
                return null;
            }
        }).when(mockParser).createRemainingValueLists(valueListDefList, valueLevelList, oids);

        mockParser.createRemainingValueLists(valueListDefList, valueLevelList, oids);
        return valueLevelList;
    }

    @Test
    public void orphanValueListsNotParsedWhenNotForValidation() {
        XPathEvaluator mockEvaluator = Mockito.mock(XPathEvaluator.class);
        List<Element> list = Stubs.VALUE_LIST_ELEMENTS;
        Mockito.when(mockEvaluator.selectElements("/ODM/odm:Study/odm:MetaDataVersion/def:ValueListDef"))
                .thenReturn(list);
        BodyParser parser = new DefineV2BodyParser(new DefaultDocument(), false);
        parser.study = Stubs.STUDY; //should probs be encapsulated
        parser.evaluator = mockEvaluator; //should probs be encapsulated
        List<ValueList> valueLists = parser.parseValueLists(new HashMap<>());
        Assert.assertEquals(0, valueLists.size());
    }

}
