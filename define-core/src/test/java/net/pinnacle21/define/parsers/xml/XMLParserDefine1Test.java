/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.xml;

import net.pinnacle21.define.models.*;
import net.pinnacle21.define.util.GenericComparator;
import net.pinnacle21.define.util.Stubs;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static junit.framework.TestCase.assertTrue;

public class XMLParserDefine1Test {
    private static Study controlStudy;
    private static Study define1Study;

    @BeforeClass
    public static void parseStudy() throws Exception {
        define1Study = new XMLParser(new File("src/test/resources/XmlParserdefine-1.0-control.xml")).parse();
        controlStudy = Stubs.DEFINE_1_CONTROL_STUDY;
    }

    @Test
    public void testStudy() throws Exception {
        GenericComparator<Study> comparator = new GenericComparator<>();
        boolean isEqual = comparator.compare(controlStudy, define1Study);
        if (!isEqual) {
            comparator.showDifferences();
        }
        assert isEqual;
    }

    @Test
    public void testDocuments() throws Exception {
        GenericComparator<Document> comparator = new GenericComparator<>();

        boolean isEqual = comparator.compare(
                controlStudy.getDocuments().get(0),
                define1Study.getDocuments().get(0));
        if (!isEqual) {
            comparator.showDifferences();
        }

        assert isEqual;
    }

    @Test
    public void testComments() throws Exception {
        GenericComparator<Comment> comparator = new GenericComparator<>();

        // Test for MDR-756 to show the bug did not get carried forward
        assertTrue(String.format("%d comments were created expected 1", define1Study.getComments().size()),
            define1Study.getComments().size() == 1);

        boolean isEqual = comparator.compare(controlStudy.getComments().get(0), define1Study.getComments().get(0));
        if (!isEqual) {
            comparator.showDifferences();
        }

        assert isEqual;
    }

    @Test
    public void testMethods() throws Exception {
        GenericComparator<Method> comparator = new GenericComparator<>();

        boolean isEqual = comparator.compare(controlStudy.getMethods().get(0), define1Study.getMethods().get(0));
        if (!isEqual) {
            comparator.showDifferences();
        }

        assert isEqual;
    }

    @Test
    public void testCodeLists() throws Exception {
        GenericComparator<CodeList> codeListComparator = new GenericComparator<>();
        GenericComparator<CodeListItem> codeListItemComparator = new GenericComparator<>();
        GenericComparator<Dictionary> dictionaryComparator = new GenericComparator<>();

        CodeList controlCodeList = controlStudy.getCodeList("CodeList");

        boolean isCodeListEqual = codeListComparator.compare(controlCodeList, define1Study.getCodeList("CodeList"));
        if (!isCodeListEqual) {
            codeListComparator.showDifferences();
        }

        List<CodeListItem> testItems = define1Study.getCodeList("CodeList").getCodelistItems();

        if (testItems.size() != 2) {
            assert false;
        }

        boolean isCodeListItemEqual = true;
        for (CodeListItem testItem : testItems) {
            boolean isEqual = codeListItemComparator.compare(testItem.getCodedValue().equals("CodeListItem")
                    ? controlCodeList.getItem("CodeListItem") : controlCodeList.getItem("SameValue"), testItem);
            if (!isEqual) {
                isCodeListItemEqual = false;
                codeListItemComparator.showDifferences();
            }
        }

        assert isCodeListEqual && isCodeListItemEqual;

        Dictionary externalCodeList = controlStudy.getDictionary("ExternalCodeList");

        boolean isExternalCodeListEqual = dictionaryComparator.compare(externalCodeList, define1Study.getDictionary("ExternalCodeList"));
        if (!isExternalCodeListEqual) {
            codeListComparator.showDifferences();
        }

        assert isExternalCodeListEqual;
    }

    @SuppressWarnings("Duplicates") //The control study is different than in XMLParserDefine2Test.java
    @Test
    public void testItemGroupDefs() throws Exception {
        GenericComparator<ItemGroup> comparator = new GenericComparator<>();

        ItemGroup controlItemGroup = controlStudy.getItemGroup("ItemGroupDef");
        ItemGroup testItemGroup = define1Study.getItemGroup("ItemGroupDef");

        boolean isEqual = comparator.compare(controlItemGroup, testItemGroup);
        if (!isEqual) {
            comparator.showDifferences();
        }

        boolean keysMatch = controlItemGroup.getKeyVariables().equals(testItemGroup.getKeyVariables());

        if (!keysMatch) {
            isEqual = false;
            new AssertionError(String.format("Comparison failed for class: %s\n", ItemGroup.class.getName()) +
                    String.format("Expected Keys: %s\n", controlItemGroup.getKeyVariables()) +
                    String.format("\tActual Keys: %s\n", testItemGroup.getKeyVariables())).printStackTrace();
        }

        assert isEqual;
    }

    @Test
    public void testItems() throws Exception {
        GenericComparator<Item> comparator = new GenericComparator<>();

        Item controlItem = controlStudy.getItemGroup("ItemGroupDef").getItem("Variable1QNAM");
        Item testItem = define1Study.getItemGroup("ItemGroupDef").getItem("Variable1QNAM");

        boolean isEqual = comparator.compare(controlItem, testItem);
        if (!isEqual) {
            comparator.showDifferences();
        }

        assert isEqual;

        Item controlItem2 = controlStudy.getItemGroup("ItemGroupDef").getItem("Variable2");
        Item testItem2 = define1Study.getItemGroup("ItemGroupDef").getItem("Variable2");

        isEqual = comparator.compare(controlItem2, testItem2);
        if (!isEqual) {
            comparator.showDifferences();
        }

        assert isEqual;

        Item controlItem3 = controlStudy.getItemGroup("ItemGroupDef").getItem("Variable3");
        Item testItem3 = define1Study.getItemGroup("ItemGroupDef").getItem("Variable3");

        isEqual = comparator.compare(controlItem3, testItem3);
        if (!isEqual) {
            comparator.showDifferences();
        }

        assert isEqual;
    }

    @Test
    public void testValueLists() throws Exception {
        GenericComparator<Item> comparator = new GenericComparator<>();

        ValueList controlValueList = controlStudy.getValueList("ItemGroupDef", "Variable1QNAM");
        Item controlItem = controlValueList.getItems().get(0);

        ValueList testValueList = define1Study.getValueLists().get(0);
        Item testItem = testValueList.getItems().get(0);

        boolean isEqual = comparator.compare(controlItem, testItem);
        if (!isEqual) {
            comparator.showDifferences();
        }

        assert isEqual && controlValueList.getOid().equals(testValueList.getOid());
    }

    @Test
    public void testWhereClauses() throws Exception {
        GenericComparator<WhereClause> whereClauseComparator = new GenericComparator<>();
        GenericComparator<RangeCheck> rangeCheckComparator = new GenericComparator<>();

        WhereClause controlWhereClause = controlStudy.getWhereClause(Stubs.define1WhereClauseOid);
        WhereClause testWhereClause = define1Study.getWhereClause(Stubs.define1WhereClauseOid);

        boolean isWhereClauseEqual = whereClauseComparator.compare(controlWhereClause, testWhereClause);
        if (!isWhereClauseEqual) {
            whereClauseComparator.showDifferences();
        }

        if (testWhereClause.getRangeChecks().isEmpty()) {
            assert false;
        }

        boolean isRangeCheckEqual = rangeCheckComparator.compare(
                controlWhereClause.getRangeChecks().get(0), testWhereClause.getRangeChecks().get(0)
        );
        if (!isRangeCheckEqual) {
            isRangeCheckEqual = false;
            rangeCheckComparator.showDifferences();
        }
        assert isRangeCheckEqual;
    }
}
