package net.pinnacle21.define.parsers.xml.util;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import net.pinnacle21.define.models.Item;

public class ParserUtilsTest {
    @Test
    public void testGetPagesWithOnlyPages() {
        Item item = new Item()
            .setPages("25");

        Assert.assertEquals(item.getPages(), ParserUtils.aggregatePages(item.getPages(), null, null));
    }

    @Test
    public void testGetPagesWithOnlyFirstPage() {
        Item item = new Item()
            .setFirstPage("25");

        Assert.assertEquals(item.getFirstPage(), ParserUtils.aggregatePages(null, item.getFirstPage(), null));
    }

    @Test
    public void testGetPagesWithOnlyLastPage() {
        Item item = new Item()
            .setLastPage("25");

        Assert.assertEquals(item.getLastPage(), ParserUtils.aggregatePages(null, null, item.getLastPage()));
    }

    @Test
    public void testGetPagesWithFirstAndLastPage() {
        Item item = new Item()
            .setFirstPage("25")
            .setLastPage("35");

        Assert.assertEquals(
            item.getFirstPage() + " - " + item.getLastPage(),
            ParserUtils.aggregatePages(null, item.getFirstPage(), item.getLastPage())
        );
    }

    @Test
    public void preferPagesOverFirstAndLast() {
        Item item = new Item()
            .setPages("25")
            .setFirstPage("24")
            .setLastPage("26");

        Assert.assertEquals(
            item.getPages(),
            ParserUtils.aggregatePages(item.getPages(), item.getFirstPage(), item.getLastPage())
        );
    }

    @Test
    public void dontAllowEmptyStringPages() {
        Item item = new Item()
            .setPages(StringUtils.EMPTY);

        Assert.assertNull(ParserUtils.aggregatePages(item.getPages(), null, null));
    }

    @Test
    public void dontAllowEmptyStringFirstPage() {
        Item item = new Item()
            .setFirstPage(StringUtils.EMPTY)
            .setLastPage("25");

        Assert.assertEquals(
            item.getLastPage(),
            ParserUtils.aggregatePages(null, item.getFirstPage(), item.getLastPage())
        );
    }

    @Test
    public void dontAllowEmptyStringLastPage() {
        Item item = new Item()
            .setFirstPage("25")
            .setLastPage(StringUtils.EMPTY);

        Assert.assertEquals(
            item.getFirstPage(),
            ParserUtils.aggregatePages(null, item.getFirstPage(), item.getLastPage())
        );
    }

    @Test
    public void nullSafetyAll() {
        Assert.assertNull(ParserUtils.aggregatePages(null, null, null));
    }

    @Test
    public void nullWhenLastPageEmpty() {
        Item item = new Item()
            .setLastPage(StringUtils.EMPTY);
        Assert.assertNull(ParserUtils.aggregatePages(null, null, item.getLastPage()));
    }

    @Test
    public void sameFirstAndLastPage() {
        Item item = new Item()
            .setFirstPage("25")
            .setLastPage("25");

        Assert.assertEquals(
            item.getFirstPage() + " - " + item.getLastPage(),
            ParserUtils.aggregatePages(null, item.getFirstPage(), item.getLastPage())
        );
    }
}