/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define;

/**
 * Created by tomromanowski on 12/1/14.
 */
public interface GeneratorListener {
    /**
     * Called by a <code>Generator</code> implementation when progress is made on a
     * process.
     *
     * @param event  a <code>GeneratorEvent</code> with detailed information about the
     *     current progress
     */
    public void generatorProgressUpdated(GeneratorEvent event);

    /**
     * Called by a <code>Generator</code> implementation when a process starts.
     *
     * @param event a blank <code>GeneratorEvent</code> object
     */
    public void generatorStarted(GeneratorEvent event);

    /**
     * Called by a <code>Generator</code> implementation when a process stops.
     *
     * @param event a blank <code>GeneratorEvent</code> object
     */
    public void generatorStopped(GeneratorEvent event);
}
