/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Arm {

    public static List<Pair<String, String>> parseDocumentsAndPagesFromString(String documentsString) {
        if (documentsString != null) {
            List<Pair<String, String>> documentPagesPairList = new ArrayList<>();
            HashMap<Integer, String> pages = new HashMap<>();
            Matcher matcher = Pattern.compile("\\(.*?\\),?|,").matcher(documentsString);
            int index = 0;
            while (matcher.find()) {
                String group = matcher.group();
                if (group.matches("\\(.*?\\),?")) {
                    pages.put(index, group.substring(1, group.length() - (group.endsWith(",") ? 2 : 1)));
                }
                index++;
            }

            String[] documents = documentsString.split("\\(.*?\\),?|,");
            for (int i = 0; i < documents.length; i++) {
                String document = documents[i].trim();
                if (StringUtils.isNotBlank(document)) {
                    documentPagesPairList.add(Pair.of(document, pages.get(i)));
                }
            }
            return !documentPagesPairList.isEmpty() ? documentPagesPairList : null;
        }
        return null;
    }
}
