/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.Study;
import net.pinnacle21.parsing.excel.BaseStateBuilder;
import net.pinnacle21.parsing.excel.BaseValidatedExcelParse;
import net.pinnacle21.parsing.excel.FieldValidations;
import org.apache.commons.lang3.StringUtils;

public class ParsedStudy extends BaseValidatedExcelParse {
    private final Study study;

    ParsedStudy(Study study) {
        this.study = study;
    }

    public Study getStudy() {
        return this.study;
    }

    static class StudyBuilder extends BaseStateBuilder<ParsedStudy> {
        private final ParsedStudy parsedStudy = new ParsedStudy(new Study());

        static final String NAME_ATTRIBUTE = "studyname";
        static final String DESCRIPTION_ATTRIBUTE = "studydescription";
        static final String PROTOCOL_ATTRIBUTE = "protocolname";
        static final String STANDARD_NAME_ATTRIBUTE = "standardname";
        static final String STANDARD_VERSION_ATTRIBUTE = "standardversion";
        static final String LANGUAGE_ATTRIBUTE = "language";

        @Override
        public ParsedStudy build() {
            Study study = parsedStudy.getStudy();
            String name = StringUtils.defaultString(this.attributes.get(NAME_ATTRIBUTE)),
                    description = StringUtils.defaultString(this.attributes.get(DESCRIPTION_ATTRIBUTE)),
                    protocol = StringUtils.defaultString(this.attributes.get(PROTOCOL_ATTRIBUTE)),
                    standardName = StringUtils.defaultString(this.attributes.get(STANDARD_NAME_ATTRIBUTE)),
                    standardVersion = StringUtils.defaultString(this.attributes.get(STANDARD_VERSION_ATTRIBUTE)),
                    language = StringUtils.defaultString(this.attributes.get(LANGUAGE_ATTRIBUTE));

            // Validate attribute lengths
            FieldValidations.checkMaxlength(null, "StudyName", name, FieldValidations.DEFAULT_LENGTH, this.parsedStudy);
            FieldValidations.checkMaxlength(null, "StudyDescription", description, FieldValidations.LONG_LENGTH, this.parsedStudy);
            FieldValidations.checkMaxlength(null, "ProtocolName", protocol, FieldValidations.MEDIUM_LENGTH, this.parsedStudy);
            FieldValidations.checkMaxlength(null, "Language", language, FieldValidations.DEFAULT_LENGTH, this.parsedStudy);

            // These properties are optional, since the only one we might use is name as a file name
            study.setName(name);
            study.setDescription(description);
            study.setProtocol(protocol);
            study.setStandardName(standardName);
            study.setStandardVersion(standardVersion);
            study.setLanguage(language);

            return parsedStudy;
        }
    }
}
