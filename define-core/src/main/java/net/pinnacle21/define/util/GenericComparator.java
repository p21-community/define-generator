/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.util;

import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class GenericComparator<T> {
    private List<String> differences = new ArrayList<>();
    private String className;

    //TODO: Handle exceptions more efficiently
    public boolean compare(T expected, T actual) throws InvocationTargetException, IllegalAccessException {
        boolean isEqual = true;
        if (expected == null || actual == null) {
            if (expected == null && actual == null) {
                return true;
            } else {
                throw new RuntimeException((expected == null ? "Expected" : "Actual") + " is null");
            }
        }
        if (expected.getClass() != actual.getClass()) {
            throw new RuntimeException("Classes are not of the same type");
        }
        className = expected.getClass().getName();
        differences = new ArrayList<>();

        for (Field field : expected.getClass().getDeclaredFields()) {
            if (field.getType().equals(String.class) || field.getType().isPrimitive()) {
                Method getter = null;
                try {
                    if (!field.getType().equals(boolean.class)) {
                        String fieldName = StringUtils.capitalize(field.getName());
                        getter = expected.getClass().getMethod("get" + fieldName);
                    } else if (field.getName().startsWith("is")) {
                        getter = expected.getClass().getMethod(field.getName());
                    }

                    if (getter == null) {
                        throw new NoSuchMethodException();
                    }
                } catch (NoSuchMethodException e) {
                    System.out.println("Getter not found for field: " + field.getName());
                    continue;
                }

                Object field1 = getter.invoke(expected);
                Object field2 = getter.invoke(actual);

                if (!Objects.equals(field1, field2)) {
                    isEqual = false;
                    differences.add(
                            String.format("Expected %s: %s\n", field.getName(), field1) +
                                    String.format("  Actual %s: %s\n", field.getName(), field2)
                    );
                }
            }
        }
        return isEqual;
    }

    public void showDifferences() {
        for (String difference : differences) {
            new AssertionError(String.format("Comparison failed for class: %s\n", className) + difference).printStackTrace();
        }
    }
}
