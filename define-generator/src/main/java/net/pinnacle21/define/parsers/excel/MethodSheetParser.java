/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.Method;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.NamedRow;
import net.pinnacle21.parsing.excel.ValidationAwareExcelSheetParser;
import org.apache.commons.lang3.StringUtils;

public class MethodSheetParser extends ValidationAwareExcelSheetParser<ParsedStudy> {
    static final String ID_COLUMN = "ID";
    static final String NAME_COLUMN = "Name";
    static final String TYPE_COLUMN = "Type";
    static final String DESCRIPTION_COLUMN = "Description";
    static final String EXPRESSION_CONTEXT_COLUMN = "Expression Context";
    static final String EXPRESSION_CODE_COLUMN = "Expression Code";
    static final String DOCUMENT_COLUMN = "Document";
    static final String PAGES_COLUMN = "Pages";
    static final String DUPLICATE_ERROR = "The method '%s' is already defined, duplicates are not allowed";
    static final String DOCUMENT_REF_ERROR = "'%s' references the document '%s', but that document was not found";

    MethodSheetParser() {
        this.addValidatedRequiredColumn(ID_COLUMN);
        this.addValidatedColumn(NAME_COLUMN);
        this.addValidatedColumn(TYPE_COLUMN);
        this.addValidatedColumn(DESCRIPTION_COLUMN, FieldValidations.REALLY_LONG_LENGTH);
        this.addValidatedColumn(EXPRESSION_CONTEXT_COLUMN);
        this.addValidatedColumn(EXPRESSION_CODE_COLUMN, FieldValidations.LONG_LENGTH);
        this.addValidatedColumn(DOCUMENT_COLUMN);
        this.addValidatedColumn(PAGES_COLUMN, FieldValidations.MEDIUM_LENGTH);
    }

    @Override
    public void parse(NamedRow row, ParsedStudy parsedStudy) {
        if (!this.validate(row, parsedStudy)) {
            return;
        }
        Study study = parsedStudy.getStudy();

        String id = StringUtils.trim(row.get(ID_COLUMN));

        if (study.hasMethod(id)) {
            parsedStudy.error(row.getIdentifier(), DUPLICATE_ERROR, id);

            return;
        }

        Method method = new Method()
                .setOid(id)
                .setName(row.get(NAME_COLUMN))
                .setType(row.get(TYPE_COLUMN))
                .setDescription(row.get(DESCRIPTION_COLUMN))
                .setExpressionContext(row.get(EXPRESSION_CONTEXT_COLUMN))
                .setExpressionCode(row.get(EXPRESSION_CODE_COLUMN))
                .setPages(row.get(PAGES_COLUMN));

        String documentRef = StringUtils.trim(row.get(DOCUMENT_COLUMN));

        if (StringUtils.isNotBlank(documentRef)) {
            if (study.hasDocument(documentRef)) {
                method.setDocumentOid(documentRef);
            } else {
                parsedStudy.error(row.getIdentifier(), DOCUMENT_REF_ERROR, DOCUMENT_COLUMN, documentRef);
            }
        }

        study.setMethod(id, method);
    }
}
