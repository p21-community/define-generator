/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.xml.bodyparsers.arm;

import com.google.common.collect.ImmutableMap;
import net.pinnacle21.define.models.*;
import net.pinnacle21.define.models.arm.*;
import net.pinnacle21.define.parsers.xml.XPathEvaluator;
import net.pinnacle21.define.parsers.xml.util.ParserUtils;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Element;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ARMParser {
    private Map<String, WhereClause> whereClauseMap;
    private Study study;
    private DefineMetadata metadata;
    private XPathEvaluator evaluator;

    public ARMParser() {

    }

    public ARMParser setWhereClauseMap(Map<String, WhereClause> whereClauseMap) {
        this.whereClauseMap = whereClauseMap;
        return this;
    }

    public ARMParser setStudy(Study study) {
        this.study = study;
        this.metadata = study.getDefineMetadata();
        return this;
    }

    public ARMParser setXPathEvaluator(XPathEvaluator evaluator) {
        this.evaluator = evaluator;
        return this;
    }

    public ProgrammingCode parseProgrammingCode(Element programmingCodeElement) {
        if (programmingCodeElement == null) {
            return null;
        }

        ProgrammingCode programmingCode = new ProgrammingCode()
                .setContext(programmingCodeElement.attributeValue("Context"));

        Element codeText = programmingCodeElement.element("Code");

        if (codeText != null) {
            programmingCode.setCode(codeText.getText());
        }

        Element documentRef = programmingCodeElement.element("DocumentRef");

        programmingCode.setDocumentReference(parseDocumentReference(documentRef));

        return programmingCode;
    }

    public Documentation parseDocumentation(Element documentationElement) {
        if (documentationElement == null) {
            return null;
        }

        Documentation documentation = new Documentation();
        documentation.setDescription(parseDescription(documentationElement.element("Description")));
        documentation.addDocumentReference(parseDocumentReference(documentationElement.element("DocumentRef")));

        return documentation;
    }

    public DocumentReference parseDocumentReference(Element documentReference) {
        if (documentReference != null) {
            DocumentReference reference = new DocumentReference()
                    .setDocument(study.getDocument(ParserUtils.substringWithPrefix(
                            metadata.getPrefixes().leaf(),
                            documentReference.attributeValue("leafID"))));

            Element pageRef= documentReference.element("PDFPageRef");

            if (pageRef != null) {
                reference.setPages(pageRef.attributeValue("PageRefs"));
            }

            return reference;
        }
        return null;
    }

    public AnalysisDatasets parseAnalysisDatasets(Element analysisDatasetsElement) {
        if (analysisDatasetsElement == null) {
            return null;
        }

        AnalysisDatasets datasets = new AnalysisDatasets();

        datasets.setComment(study.getComment(
                ParserUtils.substringWithPrefix(
                        metadata.getPrefixes().comment(),
                        analysisDatasetsElement.attributeValue("CommentOID"))
        ));

        for (Object obj1 : analysisDatasetsElement.elements("AnalysisDataset")) {
            Element datasetElement = (Element) obj1;
            AnalysisDataset dataset = new AnalysisDataset()
                    .setItemGroup(study.getItemGroupByOID(
                            ParserUtils.substringWithPrefix(
                                    metadata.getPrefixes().itemGroup(),
                                    datasetElement.attributeValue("ItemGroupOID"))));
            Element whereClauseRef = ((Element) obj1).element("WhereClauseRef");

            if (whereClauseRef != null) {
                WhereClause whereClause;
                String whereClauseOID = ParserUtils.substringWithPrefix(
                        metadata.getPrefixes().whereClause(),
                        whereClauseRef.attributeValue("WhereClauseOID"));
                //If the where clause was already assigned to the study, just use that one
                if ((whereClause = study.getWhereClause(whereClauseOID)) != null) {
                    dataset.setWhereClause(whereClause);
                } else {
                    //Otherwise add it from the xml doc and set it in both places
                    whereClause = whereClauseMap.get(whereClauseRef.attributeValue("WhereClauseOID"));
                    study.setWhereClause(whereClauseOID, whereClause);
                    dataset.setWhereClause(whereClause);
                }
            }

            for (Object obj2 : datasetElement.elements("AnalysisVariable")) {
                Element variable = (Element) obj2;

                dataset.add(new AnalysisVariable()
                        .setItem(study.getItemByOID(
                                ParserUtils.substringWithPrefix(
                                        metadata.getPrefixes().item(),
                                        variable.attributeValue("ItemOID"))
                )));
            }

            datasets.add(dataset);
        }
        return datasets;
    }

    public List<AnalysisResult> parseAnalysisResult(List analysisResults) {
        if (analysisResults == null || analysisResults.isEmpty()) {
            return null;
        }

        List<AnalysisResult> results = new ArrayList<>();

        for (Object object : analysisResults) {
            Element result = (Element) object;

            results.add(parseAnalysisResult(result));
        }

        return results;
    }

    public AnalysisResult parseAnalysisResult(Element analysisResultElement) {
        if (analysisResultElement == null) {
            return null;
        }

        AnalysisResult result = new AnalysisResult();

        // Try to populate the PARAMCD Dataset field
        String parameterOID = analysisResultElement.attributeValue("ParameterOID");
        if (StringUtils.isNotBlank(parameterOID)) {
            Element paramcdDataset = this.evaluator.selectSingleElement(
                "/ODM/odm:Study/odm:MetaDataVersion/odm:ItemGroupDef[odm:ItemRef[@ItemOID=$id]]",
                ImmutableMap.of("id", parameterOID)
            );
            if (paramcdDataset != null) {
                result.setParamcdDataset(paramcdDataset.attributeValue("Name"));
            }
        }

        result.setOID(
                ParserUtils.substringWithPrefix(
                        metadata.getPrefixes().analysisResult(),
                        analysisResultElement.attributeValue("OID")))
                .setParameterOID(ParserUtils.substringWithPrefix(metadata.getPrefixes().item(), parameterOID))
                .setAnalysisReason(analysisResultElement.attributeValue("AnalysisReason"))
                .setAnalysisPurpose(analysisResultElement.attributeValue("AnalysisPurpose"))
                .setDescription(parseDescription(analysisResultElement.element("Description")))
                .setAnalysisDatasets(parseAnalysisDatasets(analysisResultElement.element("AnalysisDatasets")))
                .setDocumentation(parseDocumentation(analysisResultElement.element("Documentation")))
                .setProgrammingCode(parseProgrammingCode(analysisResultElement.element("ProgrammingCode")));

        return result;
    }

    private Description parseDescription(Element descriptionElement) {
        if (descriptionElement != null) {
            Element translatedText = descriptionElement.element("TranslatedText");
            if (translatedText != null) {
                return new Description()
                        .setText(translatedText.getText())
                        .setLanguage(translatedText.attributeValue("lang"));
            }
        }
        return null;
    }

    public ResultDisplay parseResultDisplay(Element resultDisplayElement) {
        ResultDisplay display = new ResultDisplay()
                .setOID(
                        ParserUtils.substringWithPrefix(
                                metadata.getPrefixes().resultDisplay(),
                                resultDisplayElement.attributeValue("OID")))
                .setName(resultDisplayElement.attributeValue("Name"))
                .setDescription(parseDescription(resultDisplayElement.element("Description")))
                .setDocumentReference(parseDocumentReference(resultDisplayElement.element("DocumentRef")));

        display.addAll(parseAnalysisResult(resultDisplayElement.elements("AnalysisResult")));

        return display;
    }
}
