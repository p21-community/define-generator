/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class ItemGroup extends ExcelRecord {
    static final Logger LOGGER = LoggerFactory.getLogger(ItemGroup.class);

    private String oid;
    private String name;
    private String isRepeating;
    private String isReferenceData;
    private String purpose;
    private String structure;
    private String category;
    private String commentOid;
    private String description;
    private Map<String, Integer> keys = new LinkedHashMap<>();
    private Map<String, Item> items = new LinkedHashMap<>();
    private Map<String, SoftReference> softReferences = new LinkedHashMap<>();

    public void add(Item item) {
        Integer sequence = this.keys.get(item.getName());

        if (sequence != null) {
            item.setKeySequence(sequence);
        }

        this.items.put(item.getName(), item);
    }

    public void add(SoftReference reference) {
        String itemName = reference.getItem().getName();
        Integer sequence = this.keys.get(itemName);

        if (sequence != null) {
            reference.getItem().setKeySequence(sequence);
        }

        this.softReferences.put(itemName, reference);
    }

    public void addAll(List<Item> items) {
        for (Item item : items) {
            add(item);
        }
    }

    public Item getItem(String item) {
        return this.items.get(item);
    }

    public Item getItemAt(int index) {
        return (index < items.values().size()) ? new ArrayList<>(items.values()).get(index) : null;
    }

    public Set<String> getKeyVariables() {
        return new LinkedHashSet<String>(this.keys.keySet());
    }

    public List<Item> getItems() {
        return new ArrayList<Item>(this.items.values());
    }

    public String getOID() {
        return oid;
    }

    public ItemGroup setOID(String oid) {
        this.oid = oid != null ? oid.trim() : null;
        return this;
    }

    public String getName() {
        return name;
    }

    public ItemGroup setName(String name) {
        this.name = name != null ? name.trim() : null;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public ItemGroup setDescription(String description) {
        this.description = description != null ? description.trim() : null;
        return this;
    }

    public String getStructure() {
        return structure;
    }

    public ItemGroup setStructure(String structure) {
        this.structure = structure != null ? structure.trim() : null;
        return this;
    }

    public String getCategory() {
        return category;
    }

    public ItemGroup setCategory(String category) {
        this.category = category != null ? category.trim() : null;
        return this;
    }

    public String getPurpose() {
        return purpose;
    }

    public ItemGroup setPurpose(String purpose) {
        this.purpose = purpose != null ? purpose.trim() : null;
        return this;
    }

    public String getIsRepeating() {
        return isRepeating;
    }

    public ItemGroup setIsRepeating(String isRepeating) {
        this.isRepeating = isRepeating != null ? isRepeating.trim() : null;
        return this;
    }

    public String getIsReferenceData() {
        return isReferenceData;
    }

    public ItemGroup setIsReferenceData(String isReferenceData) {
        this.isReferenceData = isReferenceData != null ? isReferenceData.trim() : null;
        return this;
    }

    public String getCommentOid() {
        return commentOid;
    }

    public ItemGroup setCommentOid(String commentOid) {
        this.commentOid = commentOid != null ? commentOid.trim() : null;
        return this;
    }

    public ItemGroup setKeys(String keys) {
        if (StringUtils.isBlank(keys)) {
            return this;
        }
        int sequence = 1;
        this.keys.clear();
        List<String> realKeys = getRealKeys(keys);
        for (String key : realKeys) {
            if (StringUtils.isNotBlank(key) && !this.keys.containsKey(key)) {
                this.keys.put(key, sequence++);
            }
        }
        return this;
    }

    private List<String> getRealKeys(String keys) {
        String[] splitKeys = keys.split("[,.]");
        List<String> realKeys = new ArrayList<>(splitKeys.length);
        for (int i = 0; i < splitKeys.length; i++) {
            String key = StringUtils.stripToNull(splitKeys[i]);;
            if (StringUtils.equals(key, "QNAM") && i + 1 < splitKeys.length) {
                String realKey = key + "." + splitKeys[i + 1];
                realKeys.add(realKey);
                ++i;
            } else if (key != null) {
                realKeys.add(key);
            }
        }
        return realKeys;
    }

    public List<SoftReference> getSoftReferences() {
        return Collections.unmodifiableList(new ArrayList<>(softReferences.values()));
    }

    public boolean isSuppqual() {
        return StringUtils.startsWithAny(getName(), "SUPP", "SQ");
    }

    public String getSuppPrefix() {
        if (isSuppqual()) {
            return null;
        }
        if (StringUtils.equals(getName(), "AP")) {
            return "SQ";
        } else {
            return "SUPP";
        }
    }

    public String getSuppName() {
        if (isSuppqual()) {
            return getName();
        }
        return getSuppPrefix() + getName();
    }

    /**
     * Make sure all items have the order attribute set
     * @return true if all items have an order property set to a non-null string and is numeric, false otherwise
     */
    private boolean allItemsHaveOrder() {
        LOGGER.debug("Checking to see if {}'s items have order number set.", StringUtils.defaultString(this.getName(), this.getOID()));
        boolean hasOrder = true;
        for (Item item : this.items.values()) {
            hasOrder = StringUtils.isNotBlank(item.getOrder()) && StringUtils.isNumeric(item.getOrder());
            LOGGER.trace("{} {}", StringUtils.defaultString(item.getName(), item.getOID()),
                    hasOrder ? "has order set. Value was " + item.getOrder() + "."  : "does not have order set.");

            if (!hasOrder) {
                break;
            }
        }
        LOGGER.debug("Check complete, {}", hasOrder ? "all items have order set." : "all items did not have order set.");
        return hasOrder;
    }

    public void sortItemsByOrder() {
        if (!allItemsHaveOrder()) {
            LOGGER.debug("Not sorting {}'s items.", StringUtils.defaultString(this.getName(), this.getOID()));
            return;
        }
        LOGGER.debug("Sorting {}'s items.", StringUtils.defaultString(this.getName(), this.getOID()));

        Object[] tmp = this.items.values().toArray();
        this.items = new LinkedHashMap<>(); //null out old values

        Arrays.sort(tmp, new Comparator<Object>() {
            @Override
            public int compare(Object o1, Object o2) {
                if (o1 instanceof Item && o2 instanceof Item) {
                    return (Integer.valueOf(((Item) o1).getOrder())).compareTo(Integer.valueOf(((Item) o2).getOrder()));
                }
                return 0;
            }
        });

        for (Object obj : tmp) {
            Item item = (Item) obj;
            this.items.put(item.getName(), item);
        }
        LOGGER.debug("Finished sorting {}'s items.", StringUtils.defaultString(this.getName(), this.getOID()));
    }

    /**
     * Determine if this represents part of a split dataset, using standard specific logic.
     * @param standard different standards have different naming conventions.
     *                 <p>Expected values are "SDTM", "ADaM", or "SEND".</p>
     * @return true if this dataset represents a split dataset.
     */
    public boolean isSplit(String standard) {
        final String name = this.getName();

        if (name.startsWith("AD") && standard.toUpperCase().contains("ADAM")) {
            // Per SME's: Although it's possible to split an ADaM dataset, in practice it's not likely
            // Also we can't reliably determine the difference between a split vs non-split ADaM dataset
            return false;
        } else if (name.equals("RELSUB") || name.equals("APRELSUB")) {
            return false;
        } else if (name.startsWith("SUPP")) {
            return name.length() == 8;
        } else if (name.startsWith("SQAP")) {
            return name.length() == 8;
        } else if (name.startsWith("AP")) {
            return name.length() == 6;
        } else if (standard.toUpperCase().contains("ADAM")) {
            final int nameLength = name.length();
            if (nameLength == 6 || nameLength == 8) {
                return true;
            } else if (nameLength == 4) {
                return !name.toUpperCase().startsWith("AD");
            }
            return false;
        }
        // An ADaM dataset should not get here. Don't call getDomain on an ADaM dataset.
        return !getDomain().equals(name);
    }

    /**
     * <p>Domain is not applicable to ADaM, so this is specific to SDTM and SEND.</p>
     * Note: This can be called on a non ADaM dataset in an ADaM datapackage. But should not
     * be called on an ADaM dataset.
     */
    public String getDomain() {
        final String name = this.getName();
        if (name.equals("RELSUB") || name.equals("APRELSUB")) {
            return name;
        } else if (name.startsWith("RELREC") || name.startsWith("POOLDEF")) {
            return getSpecialDomain();
        } else if (name.startsWith("SUPP")) {
            return getSuppDomain();
        } else if (name.startsWith("SQAP")) {
            return getSuppApDomain();
        } else if (name.length() == 4) {
            return name.substring(0, 2);
        } else if (name.length() == 6) {
            return name.substring(0, 4);
        }
        return name;
    }

    private String getSpecialDomain() {
        final String name = this.getName();
        if (name.length() == 8) {
            return name.substring(0, 6);
        } else if (name.length() == 9) {
            return name.substring(0, 7);
        }
        return name;
    }

    private String getSuppDomain() {
        final String name = this.getName();
        // Domains for SUPPQuals is the parent domain
        if (name.length() == 6 || name.length() == 8) {
            return name.substring(4, 6);
        }
        return name;
    }

    private String getSuppApDomain() {
        final String name = this.getName();
        if (name.length() == 6 || name.length() == 8) {
            return name.substring(2, 6);
        }
        return name;
    }

    public String getAlias() {
        if (this.getName().startsWith("SUPP")) {
            return "SUPP" + this.getDomain();
        } else {
            return this.getDomain();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ItemGroup itemGroup = (ItemGroup) o;

        if (oid != null ? !oid.equals(itemGroup.oid) : itemGroup.oid != null) return false;
        if (getName() != null ? !getName().equals(itemGroup.getName()) : itemGroup.getName() != null) return false;
        if (getIsRepeating() != null ? !getIsRepeating().equals(itemGroup.getIsRepeating()) : itemGroup.getIsRepeating() != null)
            return false;
        if (getIsReferenceData() != null ? !getIsReferenceData().equals(itemGroup.getIsReferenceData()) : itemGroup.getIsReferenceData() != null)
            return false;
        if (getPurpose() != null ? !getPurpose().equals(itemGroup.getPurpose()) : itemGroup.getPurpose() != null)
            return false;
        if (getStructure() != null ? !getStructure().equals(itemGroup.getStructure()) : itemGroup.getStructure() != null)
            return false;
        if (getCategory() != null ? !getCategory().equals(itemGroup.getCategory()) : itemGroup.getCategory() != null)
            return false;
        if (getCommentOid() != null ? !getCommentOid().equals(itemGroup.getCommentOid()) : itemGroup.getCommentOid() != null)
            return false;
        if (getDescription() != null ? !getDescription().equals(itemGroup.getDescription()) : itemGroup.getDescription() != null)
            return false;
        if (keys != null ? !keys.equals(itemGroup.keys) : itemGroup.keys != null) return false;
        return getItems() != null ? getItems().equals(itemGroup.getItems()) : itemGroup.getItems() == null;
    }
}
