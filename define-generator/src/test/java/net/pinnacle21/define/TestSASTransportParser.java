/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define;

import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.util.ExcelComparator;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

import java.io.File;
import java.util.Collection;

import static org.junit.Assert.fail;

public class TestSASTransportParser {

    /**
     * The Error Collector is declared to report all errors that occur in the comparison of excel files.
     */
    @Rule
    public ErrorCollector errorCollector = new ErrorCollector();

    /**
     * Checks that the Data Profiler can handle a complete set of xpt files.
     * No errors should be present in the xpt files
     * No exceptions should be thrown
     * @throws java.lang.Exception in case of errors exporting or comparing the files
     */
    @Test
    public void testDataProfilerCompleteXPTFiles() throws Exception {

        File xptDir = new File("src/test/resources/controlXPTs"); //Set directory of XPT Files

        Collection<File> datasets = FileUtils.listFiles(
                xptDir,
                new SuffixFileFilter("xpt", IOCase.INSENSITIVE),
                TrueFileFilter.INSTANCE
        );

        SASTransportParser parser = new SASTransportParser(datasets, new File("src/test/resources/SDTM 3.1.2 (FDA).xml"), null);
        Study study = parser.parse();

        if (study != null) {
            File controlFile = new File("src/test/resources/CDISC01-xpt-control.xlsx");
            File exportedFile = ExcelGenerator.writeToDirectory(study, new File("src/test"));

            ExcelComparator excelComparator = new ExcelComparator(controlFile, exportedFile, errorCollector);
            excelComparator.process();
        } else {
            fail("XPT Import failed - see log for details");
        }
    }
}
