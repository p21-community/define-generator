/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models;

import net.pinnacle21.define.models.annotations.Terminology;

public class CodeListItem extends ExcelRecord {
    private String codedValue;
    private String order;
    private String decodedValue;
    private String language;
    private String code;
    @Terminology
    private String synonyms;
    @Terminology
    private String definition;
    @Terminology
    private String preferredTerm;
    private String extendedValue;

    public String getCodedValue() {
        return codedValue;
    }

    public CodeListItem setCodedValue(String codedValue) {
        this.codedValue = codedValue != null ? codedValue.trim() : null;
        return this;
    }

    public String getDecodedValue() {
        return decodedValue;
    }

    public CodeListItem setDecodedValue(String decodedValue) {
        this.decodedValue = decodedValue != null ? decodedValue.trim() : null;
        return this;
    }

    public String getLanguage() {
        return language;
    }

    public CodeListItem setLanguage(String language) {
        this.language = language != null ? language.trim() : null;
        return this;
    }

    public String getCode() {
        return code;
    }

    public CodeListItem setCode(String code) {
        this.code = code != null ? code.trim() : null;
        return this;
    }

    @Terminology
    public String getPreferredTerm() {
        return preferredTerm;
    }

    @Terminology
    public CodeListItem setPreferredTerm(String preferredTerm) {
        this.preferredTerm = preferredTerm != null ? preferredTerm.trim() : null;
        return this;
    }

    @Terminology
    public String getSynonyms() {
        return synonyms;
    }

    @Terminology
    public CodeListItem setSynonyms(String synonyms) {
        this.synonyms = synonyms != null ? synonyms.trim() : null;
        return this;
    }

    @Terminology
    public String getDefinition() {
        return definition;
    }

    @Terminology
    public CodeListItem setDefinition(String definition) {
        this.definition = definition != null ? definition.trim() : null;
        return this;
    }

    public String getOrder() {
        return order;
    }

    public CodeListItem setOrder(String order) {
        this.order = order != null ? order.trim() : null;
        return this;
    }

    public CodeListItem setExtendedValue(String value) {
        this.extendedValue = value;
        return this;
    }

    public String getExtendedValue() {
        return extendedValue;
    }

}
