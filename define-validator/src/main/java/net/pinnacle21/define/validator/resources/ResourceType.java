/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.resources;

import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;


/**
 * Represents the root folder for a set of resources.
 *
 * E.G. the type {@link ResourceType#VALIDATION} represents all of the resources needed to run a define.xml validation
 * and the path to these resources is "resources/validation/".
 */
public enum ResourceType {
    VALIDATION("validation") {
        @Override
        boolean sanityCheck(File directory) {
            File[] files = ResourceType.getFolderContents(directory);
            boolean hasFiles = checkFilesLength(files, 2); // If length == 0, then we need to copy everything over
            // │   .LOCK
            // ├───data
            // │       4 Files
            // └───schema
            //     ├───Define1
            //     │       11 Files
            //     └───Define2
            //         ├───cdisc-arm-1.0
            //         │       3 Files
            //         ├───cdisc-define-2.0
            //         │       3 Files
            //         └───cdisc-odm-1.3.2
            //                 5 Files
            if (hasFiles) { //only continue if data and schema folders are there
                main: for (File dir : files) {
                    checking(dir.getName());
                    if ("data".equals(dir.getName())) {
                        hasFiles = checkFolder(dir, 4);
                    } else if ("schema".equals(dir.getName())) {
                        hasFiles = checkFolder(dir, 2);
                        if (!hasFiles) {
                            break;
                        } else {
                            for (File defineFolder : ObjectUtils.defaultIfNull(dir.listFiles(), new File[0])) {
                                checking(defineFolder.getName());
                                if ("Define1".equals(defineFolder.getName())) {
                                    hasFiles = checkFolder(defineFolder, 11);
                                } else if ("Define2".equals(defineFolder.getName())) {
                                    hasFiles = checkFolder(defineFolder, 3);
                                    if (!hasFiles) {
                                        break main;
                                    } else {
                                        for (File schemaFolder : ObjectUtils.defaultIfNull(defineFolder.listFiles(), new File[0])) {
                                            checking(schemaFolder.getName());
                                            if ("cdisc-arm-1.0".equals(schemaFolder.getName())) {
                                                hasFiles = checkFolder(schemaFolder, 3);
                                            } else if ("cdisc-define-2.0".equals(schemaFolder.getName())) {
                                                hasFiles = checkFolder(schemaFolder, 3);
                                            } else if ("cdisc-odm-1.3.2".equals(schemaFolder.getName())) {
                                                hasFiles = checkFolder(schemaFolder, 5);
                                            }
                                            if (!hasFiles) {
                                                break main;
                                            }
                                        }
                                    }
                                }
                                if (!hasFiles) {
                                    break main;
                                }
                            }
                        }
                    }
                    if (!hasFiles) {
                        break;
                    }
                }
            }
            if (hasFiles) {
                LOGGER.debug("The correct number of files were files were found in all directories");
                return true;
            } else {
                LOGGER.debug("The correct number of files were not found in each directory, going to extract.");
                return false;
            }
        }

        /**
         * Check that there are {@code length} number of files in {@code folder}.
         * @param folder where to check for files
         * @param length how many files to check for
         * @return true if {@code length} number of files were found in {@code folder}, false otherwise.
         */
        private boolean checkFolder(File folder, int length) {
            boolean good = checkFilesLength(folder.listFiles(), length);
            if (!good) {
                noFiles(folder.getName());
            } else {
                hasFiles(length, folder.getName());
            }
            return good;
        }

        private boolean checkFilesLength(File[] files, int length) {
            return ObjectUtils.defaultIfNull(files, new File[0]).length >= length;
        }
    };
    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceManager.class);

    String root;

    ResourceType(String root) {
        this.root = root;
    }

    /**
     * @param directory the resource root's destination folder
     * @return true if the contents of the destination folder are considered a match for what was going to be unpacked. false otherwise.
     */
    boolean sanityCheck(File directory) {
        throw new UnsupportedOperationException("Not implemented.");
    }

    private static File[] getFolderContents(File directory) {
        return directory.listFiles((File dir, String name) -> !name.equalsIgnoreCase(".LOCK"));
    }

    private static void noFiles(String name) {
        LOGGER.warn("Files missing from directory '{}', going to extract", name);
    }

    private static void hasFiles(int count, String name) {
        LOGGER.trace("Found '{}' files in directory '{}'", count, name);
    }

    private static void checking(String name) {
        LOGGER.trace("Checking to see if '{}' has files inside...", name);
    }
}
