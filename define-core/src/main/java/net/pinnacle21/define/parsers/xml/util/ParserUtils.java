/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.xml.util;

import org.apache.commons.lang3.StringUtils;

public class ParserUtils {

    public static String substringWithPrefix(String prefix, String value) {
        if (value == null) {
            return StringUtils.EMPTY;
        }
        return value.startsWith(prefix) ? value.substring(prefix.length()) : value;
    }

    // Produce an XPath literal equal to the value if possible; if not, produce
    // an XPath expression that will match the value.
    //
    // Note that this function will produce very long XPath expressions if a value
    // contains a long run of double quotes.
    // <param name="value">The value to match.</param>
    // <returns>If the value contains only single or double quotes, an XPath
    // literal equal to the value.  If it contains both, an XPath expression,
    // using concat(), that evaluates to the value.</returns>
    // See Source at: http://stackoverflow.com/questions/1341847/special-character-in-xpath-query/11585487#11585487
    public static String XPathLiteral(String value) {
        if (value == null) {
            return "";
        }

        if(!value.contains("\"") && !value.contains("'")) {
            return "'" + value + "'";
        }
        // if the value contains only single or double quotes, construct
        // an XPath literal
        if (!value.contains("\"")) {
            String s = "\"" + value + "\"";
            return s;
        }
        if (!value.contains("'")) {
            String s =  "'" + value + "'";
            return s;
        }

        // if the value contains both single and double quotes, construct an
        // expression that concatenates all non-double-quote substrings with
        // the quotes, e.g.:
        //
        //    concat("foo", '"', "bar")
        StringBuilder sb = new StringBuilder();
        sb.append("concat(");
        String[] substrings = value.split("\"");
        for (int i = 0; i < substrings.length; i++) {
            boolean needComma = (i > 0);
            if (!substrings[i].equals("")) {
                if (i > 0) {
                    sb.append(", ");
                }
                sb.append("\"");
                sb.append(substrings[i]);
                sb.append("\"");
                needComma = true;
            }
            if (i < substrings.length - 1) {
                if (needComma) {
                    sb.append(", ");
                }
                sb.append("'\"'");
            }
        }
        //This stuff is because Java is being stupid about splitting strings
        if(value.endsWith("\"")) {
            sb.append(", '\"'");
        }

        sb.append(")");
        String s = sb.toString();
        return s;
    }

    /**
     * Prefers pages attribute over first/last page attributes.
     */
    public static String aggregatePages(String pages, String firstPage, String lastPage) {
        if (StringUtils.isNotBlank(pages)) {
            return pages;
        } else {
            return !StringUtils.isAnyBlank(firstPage, lastPage)
                ? firstPage + " - " + lastPage
                : StringUtils.isNotBlank(firstPage)
                ? firstPage
                : StringUtils.trimToNull(lastPage);
        }
    }
}
