/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.*;
import net.pinnacle21.define.util.MockNamedRow;
import net.pinnacle21.parsing.excel.ExcelSheetParser;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.RowIdentifier;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import static net.pinnacle21.define.parsers.excel.ValueLevelSheetParser.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ValueLevelSheetParserTest {

    @Test
    public void nonexistentValueLevelGetsCreated() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new ValueLevelSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedOrder = "1";
        String expectedDataset = "DATASET";
        String expectedSourceVariable = "VAR";
        String expectedWhereClause = "WC";
        String expectedDescription = "description";
        String expectedDataType = "text";
        String expectedLength = "5";
        String expectedSigDigits = "6";
        String expectedFormat = "formatted";
        String expectedMandatory = "mandatory";
        String expectedCodelist = "codelist";
        String expectedOrigin = "original";
        String expectedPages = "pages";
        String expectedMethod = "method";
        String expectedPredecessor = "precedes";
        String expectedRole = "role";
        String expectedComment = "comment";

        study.setItemGroup(expectedDataset, new ItemGroup());
        study.setItem(expectedDataset, expectedSourceVariable, new Item());
        study.setWhereClause(expectedWhereClause, new WhereClause().setOid("WC"));
        study.setCodeList(expectedCodelist, new CodeList());
        study.setMethod(expectedMethod, new Method());
        study.setComment(expectedComment, new Comment());

        parser.parse(
                new MockNamedRow()
                        .with(ORDER_COLUMN, expectedOrder)
                        .with(DATASET_COLUMN, expectedDataset + " ")
                        .with(VARIABLE_COLUMN, expectedSourceVariable + " ")
                        .with(WHERE_CLAUSE_COLUMN, expectedWhereClause + " ")
                        .with(DESCRIPTION_COLUMN, expectedDescription)
                        .with(DATA_TYPE_COLUMN, expectedDataType)
                        .with(LENGTH_COLUMN, expectedLength)
                        .with(SIGNIFICANT_DIGITS_COLUMN, expectedSigDigits)
                        .with(FORMAT_COLUMN, expectedFormat)
                        .with(MANDATORY_COLUMN, expectedMandatory)
                        .with(CODELIST_COLUMN, expectedCodelist + " ")
                        .with(ORIGIN_COLUMN, expectedOrigin)
                        .with(PAGES_COLUMN, expectedPages)
                        .with(METHOD_COLUMN, expectedMethod + " ")
                        .with(PREDECESSOR_COLUMN, expectedPredecessor)
                        .with(ROLE_COLUMN, expectedRole)
                        .with(COMMENT_COLUMN, expectedComment + " "),
                parsedStudy
        );

        ValueList valueList = study.getValueList(expectedDataset, expectedSourceVariable);

        assertNotNull(valueList);
        assertEquals(1, valueList.getItems().size());

        Item item = valueList.getItems().get(0);

        assertEquals(expectedOrder, item.getOrder());
        assertEquals(expectedDataset, valueList.getSourceItemGroupName());
        assertEquals(expectedSourceVariable, valueList.getSourceItemName());
        assertEquals(expectedWhereClause, item.getWhereClauseOid());
        assertEquals(expectedSourceVariable + "." + expectedWhereClause, item.getName());
        assertEquals(expectedDescription, item.getLabel());
        assertEquals(expectedDataType, item.getDataType());
        assertEquals(expectedLength, item.getLength());
        assertEquals(expectedSigDigits, item.getSignificantDigits());
        assertEquals(expectedFormat, item.getFormat());
        assertEquals(expectedMandatory, item.getMandatory());
        assertEquals(expectedCodelist, item.getCodeListOid());
        assertEquals(expectedOrigin, item.getOrigin());
        assertEquals(expectedMethod, item.getMethodOid());
        assertEquals(expectedPredecessor, item.getPredecessor());
        assertEquals(expectedRole, item.getRole());
        assertEquals(expectedComment, item.getCommentOid());
    }

    @Test
    public void structureColumnIsRequired() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new ValueLevelSheetParser();

        String expectedVariable = "VAR";
        String expectedWhereClause = "WC";

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedVariable)
                        .with(WHERE_CLAUSE_COLUMN, expectedWhereClause),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR),
                eq(DATASET_COLUMN));
    }

    @Test
    public void variableColumnIsRequired() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new ValueLevelSheetParser();

        String expectedStructure = "DATASET";
        String expectedWhereClause = "WC";

        parser.parse(
                new MockNamedRow()
                        .with(DATASET_COLUMN, expectedStructure)
                        .with(WHERE_CLAUSE_COLUMN, expectedWhereClause),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR),
                eq(VARIABLE_COLUMN));
    }

    @Test
    public void whereClauseColumnIsRequired() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new ValueLevelSheetParser();

        String expectedVariable = "VAR";
        String expectedStructure = "DATASET";

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedVariable)
                        .with(DATASET_COLUMN, expectedStructure),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR),
                eq(WHERE_CLAUSE_COLUMN));
    }

    @Test
    public void invalidVariableReferenceErrorRaised() {
        Study study = PowerMockito.mock(Study.class);
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(study));
        ExcelSheetParser<ParsedStudy> parser = new ValueLevelSheetParser();

        String expectedVariable = "VAR";
        String expectedStructure = "DATASET";
        String expectedWhereClause = "WC";

        when(study.hasItemGroup(expectedStructure)).thenReturn(true);
        when(study.getItemGroup(expectedStructure)).thenReturn(new ItemGroup());
        when(study.hasWhereClause(expectedWhereClause)).thenReturn(true);
        when(study.getWhereClause(expectedWhereClause))
                .thenReturn(new WhereClause());

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedVariable)
                        .with(DATASET_COLUMN, expectedStructure)
                        .with(WHERE_CLAUSE_COLUMN, expectedWhereClause),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(VARIABLE_REF_ERROR), anyVararg());
    }

    @Test
    public void invalidCommentReferenceErrorRaised() {
        Study study = PowerMockito.mock(Study.class);
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(study));
        ExcelSheetParser<ParsedStudy> parser = new ValueLevelSheetParser();

        String expectedVariable = "VAR";
        String expectedStructure = "DATASET";
        String expectedWhereClause = "WC";
        String expectedComment = "COM";

        when(study.hasItemGroup(expectedStructure)).thenReturn(true);
        when(study.getItemGroup(expectedStructure)).thenReturn(new ItemGroup());
        when(study.hasItem(expectedStructure, expectedVariable)).thenReturn(true);
        when(study.getItem(expectedStructure, expectedVariable)).thenReturn(new Item());
        when(study.hasWhereClause(expectedWhereClause)).thenReturn(true);
        when(study.getWhereClause(expectedWhereClause))
                .thenReturn(new WhereClause());

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedVariable)
                        .with(DATASET_COLUMN, expectedStructure)
                        .with(WHERE_CLAUSE_COLUMN, expectedWhereClause)
                        .with(COMMENT_COLUMN, expectedComment),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(COMMENT_REF_ERROR), anyVararg());
    }

    @Test
    public void validCommentReferenceIsAssigned() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new ValueLevelSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedVariable = "VAR";
        String expectedStructure = "DATASET";
        String expectedWhereClause = "WC";
        String expectedComment = "COM";

        Comment comment = new Comment();

        study.setItem(expectedStructure, expectedVariable, new Item());
        study.setItemGroup(expectedStructure, new ItemGroup());
        study.setWhereClause(expectedWhereClause, new WhereClause());
        study.setComment(expectedComment, comment);

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedVariable)
                        .with(DATASET_COLUMN, expectedStructure)
                        .with(WHERE_CLAUSE_COLUMN, expectedWhereClause)
                        .with(COMMENT_COLUMN, expectedComment),
                parsedStudy
        );

        ValueList valueList = study.getValueList(expectedStructure, expectedVariable);

        assertNotNull(valueList);
        assertSame(expectedComment, valueList.getItem(expectedWhereClause).getCommentOid());
    }

    @Test
    public void invalidMethodReferenceErrorRaised() {
        Study study = PowerMockito.mock(Study.class);
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(study));
        ExcelSheetParser<ParsedStudy> parser = new ValueLevelSheetParser();

        String expectedVariable = "VAR";
        String expectedStructure = "DATASET";
        String expectedWhereClause = "WC";
        String expectedMethod = "METHOD";

        when(study.hasItemGroup(expectedStructure)).thenReturn(true);
        when(study.getItemGroup(expectedStructure)).thenReturn(new ItemGroup());
        when(study.hasItem(expectedStructure, expectedVariable)).thenReturn(true);
        when(study.getItem(expectedStructure, expectedVariable)).thenReturn(new Item());
        when(study.hasWhereClause(expectedWhereClause)).thenReturn(true);
        when(study.getWhereClause(expectedWhereClause))
                .thenReturn(new WhereClause());

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedVariable)
                        .with(DATASET_COLUMN, expectedStructure)
                        .with(WHERE_CLAUSE_COLUMN, expectedWhereClause)
                        .with(METHOD_COLUMN, expectedMethod),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(METHOD_REF_ERROR), anyVararg());
    }

    @Test
    public void validMethodReferenceIsAssigned() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new ValueLevelSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedVariable = "VAR";
        String expectedStructure = "DATASET";
        String expectedWhereClause = "WC";
        String expectedMethod = "METHOD";

        Method method = new Method();

        study.setItem(expectedStructure, expectedVariable, new Item());
        study.setItemGroup(expectedStructure, new ItemGroup());
        study.setWhereClause(expectedWhereClause, new WhereClause());
        study.setMethod(expectedMethod, method);

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedVariable)
                        .with(DATASET_COLUMN, expectedStructure)
                        .with(WHERE_CLAUSE_COLUMN, expectedWhereClause)
                        .with(METHOD_COLUMN, expectedMethod),
                parsedStudy
        );

        ValueList valueList = study.getValueList(expectedStructure, expectedVariable);

        assertNotNull(valueList);
        assertSame(expectedMethod, valueList.getItem(expectedWhereClause).getMethodOid());
    }

    @Test
    public void invalidCodelistReferenceErrorRaised() {
        Study study = PowerMockito.mock(Study.class);
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(study));
        ExcelSheetParser<ParsedStudy> parser = new ValueLevelSheetParser();

        String expectedVariable = "VAR";
        String expectedStructure = "DATASET";
        String expectedWhereClause = "WC";
        String expectedCodelist = "fake";

        when(study.hasItemGroup(expectedStructure)).thenReturn(true);
        when(study.getItemGroup(expectedStructure)).thenReturn(new ItemGroup());
        when(study.hasItem(expectedStructure, expectedVariable)).thenReturn(true);
        when(study.getItem(expectedStructure, expectedVariable)).thenReturn(new Item());
        when(study.hasWhereClause(expectedWhereClause)).thenReturn(true);
        when(study.getWhereClause(expectedWhereClause))
                .thenReturn(new WhereClause());

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedVariable)
                        .with(DATASET_COLUMN, expectedStructure)
                        .with(WHERE_CLAUSE_COLUMN, expectedWhereClause)
                        .with(CODELIST_COLUMN, expectedCodelist),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(CODELIST_REF_ERROR), anyVararg());
    }

    @Test
    public void validCodelistReferenceIsAssigned() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new ValueLevelSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedVariable = "VAR";
        String expectedStructure = "DATASET";
        String expectedWhereClause = "WC";
        String expectedCodelist = "CodeList";

        study.setItem(expectedStructure, expectedVariable, new Item());
        study.setItemGroup(expectedStructure, new ItemGroup());
        study.setWhereClause(expectedWhereClause, new WhereClause());
        study.setCodeList(expectedCodelist, new CodeList());

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedVariable)
                        .with(DATASET_COLUMN, expectedStructure)
                        .with(WHERE_CLAUSE_COLUMN, expectedWhereClause)
                        .with(CODELIST_COLUMN, expectedCodelist),
                parsedStudy
        );

        ValueList valueList = study.getValueList(expectedStructure, expectedVariable);

        assertNotNull(valueList);
        assertEquals(expectedCodelist, valueList.getItem(expectedWhereClause).getCodeListOid());
    }

    @Test
    public void validDictionaryReferenceIsAssigned() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new ValueLevelSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedVariable = "VAR";
        String expectedStructure = "DATASET";
        String expectedWhereClause = "WC";
        String expectedDictionary = "Dictionary";

        study.setItem(expectedStructure, expectedVariable, new Item());
        study.setItemGroup(expectedStructure, new ItemGroup());
        study.setWhereClause(expectedWhereClause, new WhereClause());
        study.setDictionary(expectedDictionary, new Dictionary());

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedVariable)
                        .with(DATASET_COLUMN, expectedStructure)
                        .with(WHERE_CLAUSE_COLUMN, expectedWhereClause)
                        .with(CODELIST_COLUMN, expectedDictionary),
                parsedStudy
        );

        ValueList valueList = study.getValueList(expectedStructure, expectedVariable);

        assertNotNull(valueList);
        assertEquals(expectedDictionary, valueList.getItem(expectedWhereClause).getDictionaryOid());
    }

    @Test
    public void duplicateErrorRaised() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new ValueLevelSheetParser();
        Study study = PowerMockito.mock(Study.class);
        ValueList valueList = PowerMockito.mock(ValueList.class);

        when(parsedStudy.getStudy()).thenReturn(study);

        String expectedVariable = "VAR";
        String expectedStructure = "DATASET";
        String expectedWhereClause = "WC";

        when(study.hasItemGroup(expectedStructure)).thenReturn(true);
        when(study.getItemGroup(expectedStructure)).thenReturn(new ItemGroup());
        when(study.hasItem(expectedStructure, expectedVariable)).thenReturn(true);
        when(study.getItem(expectedStructure, expectedVariable)).thenReturn(new Item());
        when(study.hasWhereClause(expectedWhereClause)).thenReturn(true);
        when(study.getWhereClause(expectedWhereClause))
                .thenReturn(new WhereClause());
        when(study.getValueList(expectedStructure, expectedVariable)).thenReturn(valueList);
        when(valueList.hasItem(expectedWhereClause)).thenReturn(true);

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedVariable)
                        .with(DATASET_COLUMN, expectedStructure)
                        .with(WHERE_CLAUSE_COLUMN, expectedWhereClause),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(DUPLICATE_ERROR), anyVararg());
    }
}
