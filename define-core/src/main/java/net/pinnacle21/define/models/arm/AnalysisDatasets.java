/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models.arm;

import net.pinnacle21.define.models.Comment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AnalysisDatasets {
    private Comment comment;
    private List<AnalysisDataset> datasets = new ArrayList<>();

    public Comment getComment() {
        return comment;
    }

    public AnalysisDatasets setComment(Comment comment) {
        this.comment = comment;
        return this;
    }

    public List<AnalysisDataset> getDatasets() {
        return Collections.unmodifiableList(datasets);
    }

    public AnalysisDatasets addAll(List<AnalysisDataset> datasets) {
        if (datasets != null) {
            this.datasets.addAll(datasets);
        }
        return this;
    }

    public AnalysisDatasets add(AnalysisDataset dataset) {
        this.datasets.add(dataset);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AnalysisDatasets datasets1 = (AnalysisDatasets) o;

        if (getComment() != null ? !getComment().equals(datasets1.getComment()) : datasets1.getComment() != null)
            return false;
        return getDatasets() != null ? getDatasets().equals(datasets1.getDatasets()) : datasets1.getDatasets() == null;

    }
}
