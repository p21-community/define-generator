/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.xml;

import net.pinnacle21.define.models.DefineMetadata;
import org.dom4j.Document;
import org.dom4j.DocumentFactory;

public class XPathEvaluatorFactory {

    public static XPathEvaluatorFactory newInstance() {
        return new XPathEvaluatorFactory();
    }

    public XPathEvaluator define2XPathEvaluator(Document document) {
        return new XPathEvaluator(document, DefineMetadata.V2.getNamespaces().xpathNamespaces())
                .setPrefix(DefineMetadata.V2.getNamespaces().odmPrefix());
    }

    public XPathEvaluator define1XPathEvaluator(Document document) {
        return new XPathEvaluator(document, DefineMetadata.V1.getNamespaces().xpathNamespaces())
                .setPrefix(DefineMetadata.V1.getNamespaces().odmPrefix());
    }

    public XPathEvaluator defaultEvaluator() {
        return new XPathEvaluator(DocumentFactory.getInstance().createDocument(),
                DefineMetadata.V2.getNamespaces().xpathNamespaces());
    }
}
