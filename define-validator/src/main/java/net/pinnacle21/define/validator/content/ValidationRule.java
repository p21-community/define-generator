/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.content;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;

/**
 * <p>This annotation is used to identify which methods are to be treated as validation rules.</p>
 *
 * <p>All validation rules should be of the type public and have a void return type.
 * Every validation rule should eventually use the {@link org.opencdisc.define.validator.reporting.MessageTransformer}
 * to report an issue or multiple issues.</p>
 *
 * <p>If it makes sense to run additional rules within a <code>ValidationRule</code>, then those rules should be denoted as {@link SupplementalValidationRule}s
 * and called from the initial <code>Validation Rule</code>.</p>
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {METHOD})
public @interface ValidationRule {
    String id();

    /**
     * Denotes that this rule should be run before the xml file is parsed.
     */
    boolean isPreCheck() default false;
}
