/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.Study;
import net.pinnacle21.parsing.excel.ExcelParser;
import net.pinnacle21.parsing.excel.ExcelParserBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class DefineExcelParser {
    private final File file;
    private boolean armSupport;

    public DefineExcelParser(File file) {
        this.file = file;
        this.armSupport = false;
    }

    public DefineExcelParser(File file, boolean armSupport) {
        this.file = file;
        this.armSupport = armSupport;
    }

    public ParsedStudy process() throws IOException {
        ExcelParserBuilder<ParsedStudy> builder = ExcelParserBuilder.<ParsedStudy>newBuilder("Study")
                .addOptional("Documents", new DocumentSheetParser())
                .addOptional("Comments", new CommentSheetParser())
                .addOptional("Methods", new MethodSheetParser())
                .addOptional("Codelists", new CodeListSheetParser())
                .addOptional("Dictionaries", new DictionarySheetParser())
                .addRequired("Datasets", new DatasetSheetParser())
                .addRequired("Variables", new VariableSheetParser())
                .addOptional("WhereClauses", new WhereClauseSheetParser())
                .addOptional("ValueLevel", new ValueLevelSheetParser());

        if (armSupport) {
            builder.addOptional("Analysis Displays", new AnalysisDisplaySheetParser())
                    .addOptional("Analysis Results", new AnalysisResultSheetParser())
                    .addOptional("Analysis Criteria", new AnalysisCriteriaSheetParser());
        }

        ExcelParser<ParsedStudy> parser = builder.build(new FileInputStream(file));
        ExcelParser.VerificationResult excelStructure = parser.verify();

        if (!excelStructure.isValid()) {
            ParsedStudy parsedStudy = new ParsedStudy(new Study());
            for (String error : excelStructure.getErrors()) {
                parsedStudy.error(null, error);
            }
            return parsedStudy;
        }

        return parser.parse(new ParsedStudy.StudyBuilder());
    }
}
