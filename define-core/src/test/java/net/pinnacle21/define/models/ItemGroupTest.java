package net.pinnacle21.define.models;

import org.junit.Test;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;

public class ItemGroupTest {

    @Test
    public void reslsubDatasetsareNotSplit() {
        String domain = "RELSUB";

        assertEquals(domain, new ItemGroup().setName(domain).getDomain());

        assertFalse(new ItemGroup().setName(domain).isSplit("SDTM"));
        assertFalse(new ItemGroup().setName(domain).isSplit("SEND"));
        assertFalse(new ItemGroup().setName(domain).isSplit("ADaM"));

        domain = "APRELSUB";

        assertEquals(domain, new ItemGroup().setName(domain).getDomain());

        assertFalse(new ItemGroup().setName(domain).isSplit("SDTM"));
        assertFalse(new ItemGroup().setName(domain).isSplit("SEND"));
        assertFalse(new ItemGroup().setName(domain).isSplit("ADaM"));
    }

    @Test
    public void sqapDatasetsAreNotSplit() {
        assertFalse(new ItemGroup().setName("SQAPCM").isSplit("SDTM"));
        assertFalse(new ItemGroup().setName("SQAPCM").isSplit("SEND"));
        assertFalse(new ItemGroup().setName("SQAPCM").isSplit("ADaM"));

        assertTrue(new ItemGroup().setName("SQAPCM01").isSplit("SDTM"));
        assertTrue(new ItemGroup().setName("SQAPCM01").isSplit("SEND"));
        assertTrue(new ItemGroup().setName("SQAPCM01").isSplit("ADaM"));
    }

    @Test
    public void apDatasetsAreNotSplit() {
        assertFalse(new ItemGroup().setName("APCM").isSplit("SDTM"));
        assertFalse(new ItemGroup().setName("APCM").isSplit("SEND"));
        assertFalse(new ItemGroup().setName("APCM").isSplit("ADaM"));

        assertTrue(new ItemGroup().setName("APCM01").isSplit("SDTM"));
        assertTrue(new ItemGroup().setName("APCM01").isSplit("SEND"));
        assertTrue(new ItemGroup().setName("APCM01").isSplit("ADaM"));
    }

    @Test
    public void adDatasetsAreNotSplitInAdam() {
        assertFalse(new ItemGroup().setName("ADAE").isSplit("ADaM"));

        assertFalse(new ItemGroup().setName("ADAE01").isSplit("ADaM"));

        // But SDTM datasets should still be identifiable as split in ADaM package
        assertTrue(new ItemGroup().setName("QSMM").isSplit("ADaM"));
    }

    /**
     * @see <a href="https://pinnacle21.atlassian.net/browse/ENT-2569">ENT-2569</a>
     */
    @Test
    public void domainForSuppQualIsParent() {
        String message = "SUPPQUAL's domain should equal the parent domain";

        // Not Split
        assertEquals(message, "AE", new ItemGroup().setName("SUPPAE").getDomain());
        assertEquals(message, "APCM", new ItemGroup().setName("SQAPCM").getDomain());

        // Split
        assertEquals(message, "AE", new ItemGroup().setName("SUPPAE01").getDomain());
        assertEquals(message, "APCM", new ItemGroup().setName("SQAPCM01").getDomain());
    }

    /**
     * @see <a href="https://pinnacle21.atlassian.net/browse/ENT-2569">ENT-2569</a>
     */
    @Test
    public void identifySDTMSplitDatasets() {
        testSplitDomains("SDTM", "QS", "QSCG", "Domain of a SDTM split dataset is the first two characters.");

        // SUPPs
        testSplitDomains("SDTM", "QS", "SUPPQSCG",
                "Domain of a SUPP split dataset is the first two characters after SUPP."
        );
        testSplitDomains("SDTM", "APCM", "SQAPCM01", "Domain of split AP dataset is AP--");

        // Special Datasets
        testSplitDomains("SDTM", "RELREC", "RELREC01", "Domain of split RELREC is RELREC");
        testSplitDomains("SDTM", "POOLDEF", "POOLDEF01", "Domain of split POOLDEF is POOLDEF");
    }

    /**
     * @see <a href="https://pinnacle21.atlassian.net/browse/ENT-2569">ENT-2569</a>
     */
    @Test
    public void identifyADaMplitDatasets() {
        // Don't call getDomain on an ADaM dataset
        assertFalse(new ItemGroup().setName("ADAEBC").isSplit("ADaM"));

        // Be on the look out for SDTM domains in ADaM
        testSplitDomains("ADaM", "QS", "QSCG", "Domain of a SDTM split dataset is the first two characters.");
    }

    /**
     * @see <a href="https://pinnacle21.atlassian.net/browse/ENT-2569">ENT-2569</a>
     */
    @Test
    public void aliasDerivation() {
        ItemGroup itemGroup = new ItemGroup().setName("QSCG");
        assertEquals("Alias was not derived properly", "QS", itemGroup.getAlias());

        itemGroup.setName("SUPPQSCG");
        assertEquals("Alias was not derived properly for Split SUPPQual", "SUPPQS", itemGroup.getAlias());

        itemGroup.setName("ADAECG");
        assertEquals("Alias was not derived properly for Split ADaM dataset", "ADAE", itemGroup.getAlias());

    }

    private void testSplitDomains(String standard, String parent, String fullName, String message) {
        ItemGroup itemGroup = new ItemGroup().setName(fullName);

        assertTrue(String.format("%s is considered split in %s", fullName, standard), itemGroup.isSplit(standard));
        if (standard.equals("SDTM")) {
            // Test SEND as well since SEND and SDTM have the same logic
            assertTrue(String.format("%s is considered split in %s", fullName, standard), itemGroup.isSplit("SEND"));
        }
        assertEquals(message, parent, itemGroup.getDomain());
    }
}
