/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.Description;
import net.pinnacle21.define.models.DocumentReference;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.models.arm.AnalysisDatasets;
import net.pinnacle21.define.models.arm.AnalysisResult;
import net.pinnacle21.define.models.arm.Documentation;
import net.pinnacle21.define.models.arm.ProgrammingCode;
import net.pinnacle21.define.util.Arm;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.NamedRow;
import net.pinnacle21.parsing.excel.ValidationAwareExcelSheetParser;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.List;

class AnalysisResultSheetParser extends ValidationAwareExcelSheetParser<ParsedStudy> {
    static final String DISPLAY_COLUMN = "Display";
    static final String ID_COLUMN = "ID";
    static final String DESCRIPTION_COLUMN = "Description";
    static final String REASON_COLUMN = "Reason";
    static final String PURPOSE_COLUMN = "Purpose";
    static final String JOIN_COMMENT_COLUMN = "Join Comment";
    static final String DOCUMENTATION_COLUMN = "Documentation";
    static final String DOCUMENTATION_REFS_COLUMN = "Documentation Refs";
    static final String PROGRAMMING_CONTEXT_COLUMN = "Programming Context";
    static final String PROGRAMMING_CODE_COLUMN = "Programming Code";
    static final String PROGRAMMING_DOCUMENT_COLUMN = "Programming Document";
    static final String DUPLICATE_ERROR = "The analysis result '%s' has already been defined for the analysis " +
            "display '%s', duplicates are not allowed";
    static final String DISPLAY_REF_ERROR = "'%s' references the analysis display '%s', but that analysis display" +
            " was not found";
    static final String COMMENT_REF_ERROR = "'%s' references the comment '%s', but that comment was not found";
    static final String DOCUMENT_REF_ERROR = "'%s' references the document '%s', but that document was not found";
    static final String DOCUMENTATION_DOCUMENTS_PAGES_TOO_LONG = "defines a document with pages exceeding 1000 characters in length, please shorten it";

    AnalysisResultSheetParser() {
        this.addValidatedRequiredColumn(DISPLAY_COLUMN);
        this.addValidatedRequiredColumn(ID_COLUMN);
        this.addValidatedColumn(REASON_COLUMN);
        this.addValidatedColumn(PURPOSE_COLUMN);
        this.addValidatedColumn(DESCRIPTION_COLUMN, FieldValidations.LONG_LENGTH);
        this.addValidatedColumn(JOIN_COMMENT_COLUMN);
        this.addValidatedColumn(DOCUMENTATION_COLUMN, FieldValidations.REALLY_LONG_LENGTH);
        this.addValidatedColumn(DOCUMENTATION_REFS_COLUMN, FieldValidations.UNLIMITED_LENGTH);
        this.addValidatedColumn(PROGRAMMING_CONTEXT_COLUMN);
        this.addValidatedColumn(PROGRAMMING_CODE_COLUMN, FieldValidations.REALLY_LONG_LENGTH);
        this.addValidatedColumn(PROGRAMMING_DOCUMENT_COLUMN);
    }

    @Override
    public void parse(NamedRow row, ParsedStudy parsedStudy) {
        if (!validate(row, parsedStudy)) {
            return;
        }
        final Study study = parsedStudy.getStudy();

        String displayRef = StringUtils.trim(row.get(DISPLAY_COLUMN));
        String id = StringUtils.trim(row.get(ID_COLUMN));

        if (study.hasAnalysisResult(displayRef, id)) {
            parsedStudy.error(row.getIdentifier(), DUPLICATE_ERROR, id, displayRef);
            return;
        } else if (StringUtils.isBlank(displayRef) || !study.hasResultDisplay(displayRef)) {
            parsedStudy.error(row.getIdentifier(), DISPLAY_REF_ERROR, DISPLAY_COLUMN, displayRef);
            return;
        }

        AnalysisResult analysisResult = new AnalysisResult();
        analysisResult.setOID(id);
        analysisResult.setDescription(new Description().setText(row.get(DESCRIPTION_COLUMN)));
        analysisResult.setAnalysisReason(row.get(REASON_COLUMN));
        analysisResult.setAnalysisPurpose(row.get(PURPOSE_COLUMN));

        String commentRef = StringUtils.trim(row.get(JOIN_COMMENT_COLUMN));
        if (StringUtils.isNotBlank(commentRef)) {
            if (study.hasComment(commentRef)) {
                AnalysisDatasets analysisDatasets = new AnalysisDatasets();
                analysisDatasets.setComment(study.getComment(commentRef));
                analysisResult.setAnalysisDatasets(analysisDatasets);
            } else {
                parsedStudy.error(row.getIdentifier(), COMMENT_REF_ERROR, JOIN_COMMENT_COLUMN, commentRef);
            }
        }

        Documentation documentation = new Documentation();
        documentation.setDescription(new Description().setText(row.get(DOCUMENTATION_COLUMN)));
        analysisResult.setDocumentation(documentation);
        List<Pair<String, String>> documentPagesPairList = Arm.parseDocumentsAndPagesFromString(
                StringUtils.trim(row.get(DOCUMENTATION_REFS_COLUMN)));
        if (documentPagesPairList != null) {
            for (final Pair<String, String> documentPagesPair : documentPagesPairList) {
                final String document = documentPagesPair.getLeft();
                if (!study.hasDocument(document)) {
                    parsedStudy.error(
                            row.getIdentifier(),
                            DOCUMENT_REF_ERROR,
                            DOCUMENTATION_REFS_COLUMN,
                            document
                    );
                } else {
                    if (documentPagesPair.getRight() != null) {
                        if (documentPagesPair.getRight().length() > FieldValidations.MEDIUM_LENGTH) {
                            parsedStudy.error(
                                    row.getIdentifier(),
                                    DOCUMENTATION_DOCUMENTS_PAGES_TOO_LONG,
                                    String.valueOf(FieldValidations.MEDIUM_LENGTH),
                                    documentPagesPair.getRight());
                        }
                    }
                    documentation.addDocumentReference(new DocumentReference(){{
                        setDocument(study.getDocument(document));
                        setPages(documentPagesPair.getRight());
                    }});
                }
            }
        }

        ProgrammingCode programmingCode = new ProgrammingCode();
        programmingCode.setContext(row.get(PROGRAMMING_CONTEXT_COLUMN));
        programmingCode.setCode(row.get(PROGRAMMING_CODE_COLUMN));
        final List<Pair<String, String>> pairs = Arm.parseDocumentsAndPagesFromString(
                StringUtils.trim(row.get(PROGRAMMING_DOCUMENT_COLUMN)));
        if (pairs != null && !pairs.isEmpty()) {
            final String document = pairs.get(0).getLeft();
            if (!study.hasDocument(document)) {
                parsedStudy.error(
                        row.getIdentifier(),
                        DOCUMENT_REF_ERROR,
                        PROGRAMMING_DOCUMENT_COLUMN,
                        document
                );
            } else {
                programmingCode.setDocumentReference(new DocumentReference(){{
                    setDocument(study.getDocument(document));
                    setPages(pairs.get(0).getRight());
                }});
            }
        }
        analysisResult.setProgrammingCode(programmingCode);

        study.getResultDisplay(displayRef).add(analysisResult);
        study.setAnalysisResult(displayRef, id, analysisResult);
    }
}
