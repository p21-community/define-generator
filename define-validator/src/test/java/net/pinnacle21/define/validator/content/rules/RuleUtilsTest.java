/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.content.rules;

import net.pinnacle21.define.models.ItemGroup;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RuleUtilsTest {

    @Test
    public void isSplitDatasetNull() {
        assertEquals(false, RuleUtils.isSplitDataset(new ItemGroup()));
    }

    @Test
    public void isSplitDatasetSuppqual4Chars() {
        ItemGroup itemGroup = new ItemGroup()
                .setName("SUPPQSMM");
        assertEquals(true, RuleUtils.isSplitDataset(itemGroup));
    }

    @Test
    public void isSplitDatasetSuppqual3Chars() {
        ItemGroup itemGroup = new ItemGroup()
                .setName("SUPPQSC");
        assertEquals(true, RuleUtils.isSplitDataset(itemGroup));
    }

    @Test
    public void isSplitDatasetSuppqualFullDataset() {
        ItemGroup itemGroup = new ItemGroup()
                .setName("SUPPQS");
        assertEquals(false, RuleUtils.isSplitDataset(itemGroup));
    }

    @Test
    public void isSplitDatasetFullDataset() {
        ItemGroup itemGroup = new ItemGroup()
                .setName("QS");
        assertEquals(false, RuleUtils.isSplitDataset(itemGroup));
    }

    @Test
    public void isSplitDataset4Chars() {
        ItemGroup itemGroup = new ItemGroup()
                .setName("QSCG");
        assertEquals(true, RuleUtils.isSplitDataset(itemGroup));
    }

    @Test
    public void isSplitDataset3Chars() {
        ItemGroup itemGroup = new ItemGroup()
                .setName("QSC");
        assertEquals(true, RuleUtils.isSplitDataset(itemGroup));
    }

    @Test
    public void isSplitDatasetRelrec() {
        ItemGroup itemGroup = new ItemGroup()
                .setName("RELREC");
        assertEquals(false, RuleUtils.isSplitDataset(itemGroup));
    }

    @Test
    public void isSplitDatasetPooldef() {
        ItemGroup itemGroup = new ItemGroup()
                .setName("POOLDEF");
        assertEquals(false, RuleUtils.isSplitDataset(itemGroup));
    }
}
