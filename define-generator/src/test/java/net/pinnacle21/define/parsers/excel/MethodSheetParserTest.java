/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.Document;
import net.pinnacle21.define.models.Method;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.util.MockNamedRow;
import net.pinnacle21.parsing.excel.ExcelSheetParser;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.RowIdentifier;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import static net.pinnacle21.define.parsers.excel.MethodSheetParser.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyVararg;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class MethodSheetParserTest {
    @Test
    public void nonexistingMethodGetsCreated() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new MethodSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedID = "METHOD";
        String expectedName = "Test";
        String expectedType = "Typed";
        String expectedDescription = "descriptive";
        String expectedExpressionContext = "expressive";
        String expectedExpressionCode = "coded";
        String expectedDocument = "Document";
        String expectedPages = "Pages";

        study.setDocument(expectedDocument, new Document());

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID + " ")
                        .with(TYPE_COLUMN, expectedType)
                        .with(NAME_COLUMN, expectedName)
                        .with(DESCRIPTION_COLUMN, expectedDescription)
                        .with(EXPRESSION_CONTEXT_COLUMN, expectedExpressionContext)
                        .with(EXPRESSION_CODE_COLUMN, expectedExpressionCode)
                        .with(DOCUMENT_COLUMN, expectedDocument + " ")
                        .with(PAGES_COLUMN, expectedPages),
                parsedStudy
        );

        assertTrue(study.hasMethod(expectedID));

        Method method = study.getMethod(expectedID);

        assertNotNull(method);
        assertEquals(expectedID, method.getOid());
        assertEquals(expectedName, method.getName());
        assertEquals(expectedType, method.getType());
        assertEquals(expectedDescription, method.getDescription());
        assertEquals(expectedExpressionContext, method.getExpressionContext());
        assertEquals(expectedExpressionCode, method.getExpressionCode());
        assertEquals(expectedDocument, method.getDocumentOid());
    }

    @Test
    public void idColumnIsRequiredWarning() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new MethodSheetParser();

        parser.parse(
                new MockNamedRow(),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR), eq(ID_COLUMN));
    }

    @Test
    public void invalidDocumentReferenceErrorRaised() {
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(new Study()));
        ExcelSheetParser<ParsedStudy> parser = new MethodSheetParser();

        String expectedID = "METHOD";
        String expectedDocument = "DOC";

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID)
                        .with(DOCUMENT_COLUMN, expectedDocument),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(DOCUMENT_REF_ERROR), anyVararg());
    }

    @Test
    public void validDocumentReferenceIsAssigned() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new MethodSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedID = "METHOD";
        String expectedDocument = "DOC";
        Document document = new Document().setOid(expectedDocument);

        study.setDocument(expectedDocument, document);

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID)
                        .with(DOCUMENT_COLUMN, expectedDocument),
                parsedStudy
        );

        Method method = study.getMethod(expectedID);

        assertNotNull(method);
        assertSame(document.getOid(), method.getDocumentOid());
    }

    @Test
    public void duplicateErrorRaised() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new MethodSheetParser();
        Study study = mock(Study.class);

        when(parsedStudy.getStudy()).thenReturn(study);

        String expectedID = "METHOD";

        when(study.hasMethod(expectedID)).thenReturn(true);

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(DUPLICATE_ERROR), anyVararg());
    }

    @Test
    public void duplicateErrorNotRaisedWithDifferentCasing() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new MethodSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedID = "METHOD";

        study.setMethod(expectedID.toLowerCase(), new Method());

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID),
                parsedStudy
        );

        assertEquals(true, study.hasMethod(expectedID));
    }
}
