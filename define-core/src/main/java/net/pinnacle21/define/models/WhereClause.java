/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models;

import net.pinnacle21.define.util.Templates;

import java.util.LinkedList;
import java.util.List;

public class WhereClause extends ExcelRecord {
    private String oid;
    private String commentOid;
    private List<RangeCheck> rangeChecks = new LinkedList<RangeCheck>();

    public String getOid() {
        return oid;
    }

    public WhereClause setOid(String oid) {
        this.oid = oid != null ? oid.trim() : null;
        return this;
    }

    public String getCommentOid() {
        return commentOid;
    }

    public WhereClause setCommentOid(String commentOid) {
        this.commentOid = commentOid != null ? commentOid.trim() : null;
        return this;
    }

    public List<RangeCheck> getRangeChecks() {
        return new LinkedList<>(this.rangeChecks);
    }

    public WhereClause add(RangeCheck check) {
        this.rangeChecks.add(check);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WhereClause that = (WhereClause) o;

        if (getOid() != null ? !getOid().equals(that.getOid()) : that.getOid() != null) return false;
        if (getCommentOid() != null ? !getCommentOid().equals(that.getCommentOid()) : that.getCommentOid() != null)
            return false;
        return getRangeChecks() != null ? getRangeChecks().equals(that.getRangeChecks()) : that.getRangeChecks() == null;
    }

    @Override
    public String toString() {
        return Templates.generateWhereClauseString(getRangeChecks());
    }
}
