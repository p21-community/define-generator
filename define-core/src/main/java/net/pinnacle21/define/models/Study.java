/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models;

import net.pinnacle21.define.models.annotations.Terminology;
import net.pinnacle21.define.models.arm.AnalysisResult;
import net.pinnacle21.define.models.arm.ResultDisplay;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.util.*;

public class Study extends ExcelRecord {
    private String name;
    private String description;
    private String protocol;
    private String standardName;
    private String standardVersion;
    private String baseStandardName;
    private String baseStandardVersion;
    private String publisher;
    private String language;
    private String sdtmCtVersion;
    private String adamCtVersion;
    private String sendCtVersion;
    @Terminology
    private boolean isTerminologyFile = false;
    private final Map<String, Document> documents = new LinkedHashMap<>();
    private final Map<String, Comment> comments = new LinkedHashMap<>();
    private final Map<String, Method> methods = new LinkedHashMap<>();
    private final Map<String, CodeList> codeLists = new LinkedHashMap<>();
    private final Map<String, Dictionary> dictionaries = new LinkedHashMap<>();
    private final Map<Pair<String, String>, CodeListItem> codeListItems = new LinkedHashMap<>();
    private final Map<String, ItemGroup> itemGroups = new LinkedHashMap<>();
    private final Map<String, Set<String>> identifiedKeyVariables = new HashMap<>();
    private final Map<Pair<String, String>, Item> items = new LinkedHashMap<>();
    private final Map<String, WhereClause> whereClauses = new LinkedHashMap<>();
    private final Map<Triple<Triple<String, String, String>, String, String>, RangeCheck> rangeChecks = new LinkedHashMap<>();
    private final Map<Pair<String, String>, ValueList> valueLists = new LinkedHashMap<>();
    private final Map<String, ResultDisplay> resultDisplays = new LinkedHashMap<>();
    private final Map<Pair<String, String>, AnalysisResult> analysisResults = new LinkedHashMap<>();
    private final Set<String> roleCodelistOids = new HashSet<>();
    private DefineMetadata defineMetadata;

    public String getOID() {
        return StringUtils.defaultIfBlank(this.name, "").replace(" ", "-") + "." + this.standardName + "." + this.standardVersion;
    }

    public String getName() {
        return name;
    }

    public Study setName(String name) {
        this.name = name != null ? name.trim() : null;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Study setDescription(String description) {
        this.description = description != null ? description.trim() : null;
        return this;
    }

    public String getProtocol() {
        return protocol;
    }

    public Study setProtocol(String protocol) {
        this.protocol = protocol != null ? protocol.trim() : null;
        return this;
    }

    public String getStandardName() {
        return standardName;
    }

    public Study setStandardName(String standardName) {
        this.standardName = standardName != null ? standardName.trim() : null;
        return this;
    }

    public String getStandardVersion() {
        return standardVersion;
    }

    public Study setStandardVersion(String standardVersion) {
        this.standardVersion = standardVersion != null ? standardVersion.trim() : null;
        return this;
    }

    public String getBaseStandardName() {
        return baseStandardName;
    }

    public Study setBaseStandardName(String baseStandardName) {
        this.baseStandardName = baseStandardName != null ? baseStandardName.trim() : null;
        return this;
    }

    public String getBaseStandardVersion() {
        return baseStandardVersion;
    }

    public Study setBaseStandardVersion(String baseStandardVersion) {
        this.baseStandardVersion = baseStandardVersion != null ? baseStandardVersion.trim() : null;
        return this;
    }

    public boolean isAdam() {
        return StringUtils.containsIgnoreCase(getStandardName(), "ADAM") ||
            StringUtils.containsIgnoreCase(getBaseStandardName(), "ADAM");
    }

    public String getPublisher() {
        return publisher;
    }

    public Study setPublisher(String publisher) {
        this.publisher = publisher != null ? publisher.trim() : null;
        return this;
    }

    public String getLanguage() {
        return language;
    }

    public Study setLanguage(String language) {
        this.language = language != null ? language.trim() : null;
        return this;
    }

    public String getSdtmCtVersion() { return sdtmCtVersion; }

    public Study setSdtmCtVersion(String sdtmCtVersion) {
        this.sdtmCtVersion = sdtmCtVersion != null ? sdtmCtVersion.trim() : null;
        return this;
    }

    public String getAdamCtVersion() { return adamCtVersion; }

    public Study setAdamCtVersion(String adamCtVersion) {
        this.adamCtVersion = adamCtVersion != null ? adamCtVersion.trim() : null;
        return this;
    }

    public String getSendCtVersion() { return sendCtVersion; }

    public Study setSendCtVersion(String sendCtVersion) {
        this.sendCtVersion = sendCtVersion != null ? sendCtVersion.trim() : null;
        return this;
    }

    public boolean isTerminologyFile() {
        return isTerminologyFile;
    }

    public Study setTerminologyFile(boolean terminologyFile) {
        isTerminologyFile = terminologyFile;
        return this;
    }

    public Document getDocument(String oid) {
        return documents.get(oid);
    }

    public void setDocument(String oid, Document document) {
        documents.put(oid, document);
    }

    public boolean hasDocument(String oid) {
        return documents.containsKey(oid);
    }

    public List<Document> getDocuments() {
        return new ArrayList<>(documents.values());
    }

    public List<Document> getAnnotatedCrfs() {
        List<Document> annotatedCrfs = new ArrayList<>();
        for (Document document : this.documents.values()) {
            if (Document.Type.ANNOTATEDCRF.equals(document.getType())) {
                annotatedCrfs.add(document);
            }
        }
        return annotatedCrfs;
    }

    public List<Document> getSupplementalDocuments() {
        List<Document> annotatedCrfs = new ArrayList<>();
        for (Document document : this.documents.values()) {
            if (Document.Type.SUPPLEMENTAL.equals(document.getType())) {
                annotatedCrfs.add(document);
            }
        }
        return annotatedCrfs;
    }

    public Comment getComment(String oid) {
        return comments.get(oid);
    }

    public void setComment(String oid, Comment comment) {
        comments.put(oid, comment);
    }

    public boolean hasComment(String oid) {
        return comments.containsKey(oid);
    }

    public List<Comment> getComments() {
        return new ArrayList<>(this.comments.values());
    }

    public Method getMethod(String oid) {
        return methods.get(oid);
    }

    public void setMethod(String oid, Method method) {
        methods.put(oid, method);
    }

    public boolean hasMethod(String oid) {
        return methods.containsKey(oid);
    }

    public List<Method> getMethods() {
        return new ArrayList<>(this.methods.values());
    }

    public CodeList getCodeList(String oid) {
        return codeLists.get(oid);
    }

    public void setCodeList(String oid, CodeList codeList) {
        codeLists.put(oid, codeList);
    }

    public void setCodeList(CodeList codeList) {
        codeLists.put(codeList.getOid(), codeList);
    }

    public boolean hasCodeList(String oid) {
        return codeLists.containsKey(oid);
    }

    public List<CodeList> getCodeLists() {
        return new ArrayList<>(this.codeLists.values());
    }

    public CodeListItem getCodeListItem(String codeListId, String codedValue) {
        return codeListItems.get(Pair.of(codeListId, codedValue));
    }

    public void setCodeListItem(String codeListId, String codedValue, CodeListItem codeListItem) {
        codeListItems.put(Pair.of(codeListId, codedValue), codeListItem);
    }

    public boolean hasCodeListItem(String codeListId, String codedValue) {
        return codeListItems.containsKey(Pair.of(codeListId, codedValue));
    }

    public Dictionary getDictionary(String oid) {
        return dictionaries.get(oid);
    }

    public void setDictionary(String oid, Dictionary dictionary) {
        dictionaries.put(oid, dictionary);
    }

    public boolean hasDictionary(String oid) {
        return dictionaries.containsKey(oid);
    }

    public List<Dictionary> getDictionaries() {
        return new ArrayList<>(dictionaries.values());
    }

    public ItemGroup getItemGroup(String name) {
        return itemGroups.get(name);
    }

    public ItemGroup getItemGroupByOID(String id) {
        for (ItemGroup itemGroup : itemGroups.values()) {
            if (id.equals(itemGroup.getOID())) {
                return itemGroup;
            }
        }
        return null;
    }

    public void setItemGroup(String name, ItemGroup itemGroup) {
        itemGroups.put(name, itemGroup);
    }

    public boolean hasItemGroup(String name) {
        return itemGroups.containsKey(name);
    }

    public List<ItemGroup> getItemGroups() {
        return new ArrayList<>(this.itemGroups.values());
    }

    public Integer getKeyVariablePosition(String itemGroupName, String itemName) {
        ItemGroup itemGroup = itemGroups.get(itemGroupName);

        if (itemGroup != null) {
            List<String> keyVariables = new ArrayList<>(itemGroup.getKeyVariables());
            Integer index = keyVariables.indexOf(itemName);
            if (index == -1) {
                return null;
            } else {
                Set<String> identifiedVariables = this.identifiedKeyVariables.get(itemGroupName);
                if (identifiedVariables == null) {
                    identifiedVariables = new HashSet<>();
                    this.identifiedKeyVariables.put(itemGroupName, identifiedVariables);
                }
                identifiedVariables.add(itemName);
                return index + 1;
            }
        }
        return null;
    }

    // Returns key variables by structure that were assigned
    // to a structure but did not exist
    private Map<String, Set<String>> validateKeyVariables() {
        Map<String, Set<String>> invalidKeyVariables = new HashMap<>();
        for (Map.Entry<String, ItemGroup> entry : this.itemGroups.entrySet()) {
            String itemGroupName = entry.getKey();
            ItemGroup itemGroup = entry.getValue();
            Set<String> identifiedKeyVars = this.identifiedKeyVariables.get(itemGroupName);
            if (identifiedKeyVars == null) {
                identifiedKeyVars = new HashSet<>();
            }
            if (!identifiedKeyVars.containsAll(itemGroup.getKeyVariables())) {
                Set<String> invalidKeys = new HashSet<>();
                for (String varName : itemGroup.getKeyVariables()) {
                    if (!identifiedKeyVars.contains(varName)) {
                        if (StringUtils.startsWith(varName, "QNAM.") && !itemGroup.isSuppqual()) {
                            if (!validateValueLevelKeyVariables(itemGroup, varName)) {
                                invalidKeys.add(varName);
                            }
                        } else {
                            invalidKeys.add(varName);
                        }
                    }
                }
                if (CollectionUtils.isNotEmpty(invalidKeys)) {
                    invalidKeyVariables.put(itemGroupName, invalidKeys);
                }
            }
        }
        return invalidKeyVariables;
    }

    private boolean validateValueLevelKeyVariables(ItemGroup itemGroup, String lookup) {
        ValueList valueList = this.getValueList(itemGroup.getSuppName(), "QVAL");
        for (Item item : valueList.getItems()) {
            if (StringUtils.isNotBlank(item.getWhereClauseOid())) {
                WhereClause whereClause = this.getWhereClause(item.getWhereClauseOid());
                if (CollectionUtils.isNotEmpty(whereClause.getRangeChecks())) {
                    for (RangeCheck rangeCheck : whereClause.getRangeChecks()) {
                        if (CollectionUtils.isNotEmpty(rangeCheck.getValues())) {
                            if (rangeCheck.getValues().contains(lookup.split("\\.")[1])) {
                                item.setKeySequence(this.getKeyVariablePosition(itemGroup.getName(), lookup));
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public Item getItem(String itemGroupName, String itemName) {
        return items.get(Pair.of(itemGroupName, itemName));
    }

    public Item getItemByOID(String id) {
        if (id == null) {
            return null;
        }
        for (Item item : items.values()) {
            if (Objects.equals(id, item.getOID())) {
                return item;
            }
        }
        return null;
    }

    public void setItem(String itemGroupName, String itemName, Item item) {
        items.put(Pair.of(itemGroupName, itemName), item);
    }

    public boolean hasItem(String itemGroupName, String itemName) {
        return items.containsKey(Pair.of(itemGroupName, itemName));
    }

    public WhereClause getWhereClause(String oid) {
        return whereClauses.get(oid);
    }

    public void setWhereClause(String oid, WhereClause whereClause) {
        whereClauses.put(oid, whereClause);
    }

    public boolean hasWhereClause(String oid) {
        return whereClauses.containsKey(oid);
    }

    public List<WhereClause> getWhereClauses() {
        return new ArrayList<>(this.whereClauses.values());
    }

    public RangeCheck getRangeCheck(String whereClauseId, String datasetName, String variableName, String comparator, String value) {
        return rangeChecks.get(Triple.of(Triple.of(whereClauseId, datasetName, variableName), comparator, value));
    }

    public void setRangeCheck(String whereClauseId, String datasetName, String variableName, String comparator, String value, RangeCheck rangeCheck) {
        rangeChecks.put(Triple.of(Triple.of(whereClauseId, datasetName, variableName), comparator, value), rangeCheck);
    }

    public boolean hasRangeCheck(String whereClauseId, String datasetName, String variableName, String comparator, String value) {
        return rangeChecks.containsKey(Triple.of(Triple.of(whereClauseId, datasetName, variableName), comparator, value));
    }

    public List<RangeCheck> getRangeChecks() {
        return new ArrayList<>(this.rangeChecks.values());
    }

    public ValueList getValueList(String datasetName, String variableName) {
        return valueLists.get(Pair.of(datasetName, variableName));
    }

    public ValueList getValueList(String oid) {
        for (ValueList valueList : valueLists.values()) {
            if (oid.equals(valueList.getOid())) {
                return valueList;
            }
        }
        return null;
    }

    public void setValueList(String datasetName, String variableName, ValueList valueList) {
        valueLists.put(Pair.of(datasetName, variableName), valueList);
    }

    public List<ValueList> getValueLists() {
        return new ArrayList<>(this.valueLists.values());
    }

    public Study setMetadata(DefineMetadata metadata) {
        this.defineMetadata =  metadata;
        return this;
    }

    public DefineMetadata getDefineMetadata() {
        return this.defineMetadata;
    }

    public ResultDisplay getResultDisplay(String name) {
        return this.resultDisplays.get(name);
    }

    public void setResultDisplay(String name, ResultDisplay resultDisplay) {
        this.resultDisplays.put(name, resultDisplay);
    }

    public boolean hasResultDisplay(String name) {
        return this.resultDisplays.containsKey(name);
    }

    public List<ResultDisplay> getResultDisplays() {
        return new ArrayList<>(resultDisplays.values());
    }

    public AnalysisResult getAnalysisResult(String display, String id) {
        return this.analysisResults.get(Pair.of(display, id));
    }

    public void setAnalysisResult(String display, String id, AnalysisResult analysisResult) {
        this.analysisResults.put(Pair.of(display, id), analysisResult);
    }

    public boolean hasAnalysisResult(String display, String id) {
        return this.analysisResults.containsKey(Pair.of(display, id));
    }

    public List<AnalysisResult> getAnalysisResults() {
        return new ArrayList<>(analysisResults.values());
    }

    public void addRoleCodelistOID(String roleCodelistOid) {
        this.roleCodelistOids.add(roleCodelistOid);
    }

    public Set<String> getRoleCodelistOids() {
        return this.roleCodelistOids;
    }

    public List<Item> getItems() {
        List<Item> items = new ArrayList<>();

        for (ItemGroup itemGroup : itemGroups.values()) {
            items.addAll(itemGroup.getItems());
        }

        for (ValueList valueList : valueLists.values()) {
            items.addAll(valueList.getItems());
        }

        return items;
    }

    // Validate for non-structural checks
    public void validate() {
        Map<String, Set<String>> invalidKeyVariableMap = this.validateKeyVariables();
        if (!invalidKeyVariableMap.isEmpty()) {
            Set<String> invalidKeyVariableStrings = new HashSet<>();
            for (Map.Entry<String, Set<String>> entry : invalidKeyVariableMap.entrySet()) {
                for (String keyVar : entry.getValue()) {
                    invalidKeyVariableStrings.add(entry.getKey() + "." + keyVar);
                }
            }
            List sortedInvalidKeyVars = new ArrayList<>(invalidKeyVariableStrings);
            Collections.sort(sortedInvalidKeyVars);
            String invalidKeyVariables = StringUtils.join(sortedInvalidKeyVars.toArray(), ", ");
            String errorMessage = String.format(
                "The following key variables were assigned but do not exist: %s",
                invalidKeyVariables
            );
            //throw new ExcelTemplateDataException(errorMessage);
        }
    }
}
