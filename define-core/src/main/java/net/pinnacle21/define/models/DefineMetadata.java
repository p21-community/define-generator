/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public enum DefineMetadata {
    V1("1.0.0", new DefineV1Namespaces(), new DefineV1Standards(), new DefineV1Prefixes()),
    V2("2.0.0", new DefineV2Namespaces(), new DefineV2Standards(), new DefineV2Prefixes());

    private static final int BUFFER_SIZE = 300;
    private static final int BUFFER_OVERRUN = 100;
    private static final ThreadLocal<Pattern> VERSION_REGEX = ThreadLocal.withInitial(() -> Pattern.compile(
        "[: ]DefineVersion\\s*=\\s*\"(?<version>[^\"]+)\"", Pattern.CASE_INSENSITIVE
    ));

    private final String version;
    private final Namespaces namespaces;
    private final Standards standards;
    private final Prefixes prefixes;
    private final Classes classes = new Classes() {};

    DefineMetadata(String version, Namespaces namespaces, Standards standards, Prefixes prefixes) {
        this.version = version;
        this.namespaces = namespaces;
        this.standards = standards;
        this.prefixes = prefixes;
    }

    public static DefineMetadata parseVersion(InputStream stream) throws IOException {
        byte[] readBuffer = new byte[BUFFER_SIZE];
        String inspectionBuffer = "";
        DefineMetadata fallback = null;

        while (stream.read(readBuffer) > -1) {
            inspectionBuffer += new String(readBuffer, StandardCharsets.UTF_8)
                    .replace("\n", "")
                    .replace("\r", "")
                    .replace("\t", "");

            if (inspectionBuffer.length() > (BUFFER_SIZE + BUFFER_OVERRUN)) {
                inspectionBuffer = inspectionBuffer.substring(inspectionBuffer.length() - (BUFFER_SIZE + BUFFER_OVERRUN));
            }

            Matcher matcher = VERSION_REGEX.get().matcher(inspectionBuffer);

            while (matcher.find()) {
                String version = matcher.group("version")
                        .replaceAll("\\D", ".")
                        .replaceAll("\\.+", ".");

                Optional<DefineMetadata> metadata = Stream.of(DefineMetadata.values())
                        .filter(m -> m.getVersion().startsWith(version) || version.startsWith(m.getVersion()))
                        .findFirst();

                if (metadata.isPresent()) {
                    return metadata.get();
                }
            }

            if (fallback == null) {
                for (DefineMetadata metadata : DefineMetadata.values()) {
                    if (StringUtils.containsIgnoreCase(inspectionBuffer, metadata.getNamespaces().defURI())) {
                        fallback = metadata;

                        break;
                    }
                }
            }
        }

        // Assume version 2 as a last resort
        if (fallback == null) {
            fallback = DefineMetadata.V2;
        }

        return fallback;
    }

    @Override
    public String toString() {
        return "Define.xml v" + this.version;
    }

    public String getVersion() {
        return this.version;
    }

    public Namespaces getNamespaces() {
        return this.namespaces;
    }

    public Standards getStandards() {
        return this.standards;
    }

    public Prefixes getPrefixes() {
        return this.prefixes;
    }

    public Classes getClasses() {
        return this.classes;
    }

    //region Inner Types
    public interface Namespaces {
        default String xmlPrefix() {
            return "xml";
        }

        default String xmlURI() {
            return "http://www.w3.org/XML/1998/namespace";
        }

        default String armPrefix() {
            return "arm";
        }

        default String xsiURI() {
            return "http://www.w3.org/2001/XMLSchema-instance";
        }

        default String xlinkURI() {
            return "http://www.w3.org/1999/xlink";
        }

        default String defPrefix() {
            return "def";
        }

        default String odmPrefix() {
            return "odm";
        }

        default String xmlnsPrefix() {
            return "xmlns";
        }

        default String xlinkPrefix() {
            return "xlink";
        }

        default String xsiPrefix() {
            return "xsi";
        }

        default String valPrefix() {
            return "val";
        }

        default String valURI() {
            return "http://www.opencdisc.org/schema/validator";
        }

        default String nciodmPrefix() {
            return "nciodm";
        }

        default String nciodmURI() {
            return "http://ncicb.nci.nih.gov/xml/odm/EVS/CDISC";
        }

        default String armURI(){
            return "http://www.cdisc.org/ns/arm/v1.0";
        }

        String odmVersion();
        String odmURI();
        String defURI();
        String xmlnsURI();

        default Map<String, String> xpathNamespaces() {
            Map<String, String> tmp = new HashMap<>();

            tmp.put(this.xmlnsPrefix(), this.xmlnsURI());
            tmp.put(this.xmlPrefix(), this.xmlURI());
            tmp.put(this.odmPrefix(), this.odmURI());
            tmp.put(this.defPrefix(), this.defURI());
            tmp.put(this.xsiPrefix(), this.xsiURI());
            tmp.put(this.armPrefix(), this.armURI());
            tmp.put(this.nciodmPrefix(), this.nciodmURI());

            return tmp;
        }
    }

    public interface Standards {
        String sdtm();
        String send();
        String adam();

        default Map<String, List<String>> standardToVersions() {
            Map<String, List<String>> tmp = new HashMap<>();

            tmp.put(this.sdtm(), Arrays.asList("3.1.1", "3.1.2", "3.1.3", "3.2"));
            tmp.put(this.send(), Arrays.asList("3.0", "3.1"));
            tmp.put(this.adam(), Arrays.asList("1.0", "1.1"));

            return tmp;
        }
    }

    public interface Prefixes {
        default String metaDataVersion() {
            return "MDV.";
        }

        default String leaf() {
            return "LF.";
        }

        default String codelist() {
            return "CL.";
        }

        default String whereClause() {
            return "WC.";
        }

        default String itemGroup() {
            return "IG.";
        }

        default String item() {
            return "IT.";
        }

        default String method() {
            return "MT.";
        }

        default String comment() {
            return "COM.";
        }

        default String analysisResult() {
            return "AR.";
        }

        default String resultDisplay() {
            return "RD.";
        }

        String valuelist();
    }

    public interface Classes {
        // ADaM
        String ADAM_OTHER = "ADAM OTHER";
        String BASIC_DATA_STRUCTURE = "BASIC DATA STRUCTURE";
        String INTEGRATED_BASIC_DATA_STRUCTURE = "INTEGRATED BASIC DATA STRUCTURE";
        String OCCURRENCE_DATA_STRUCTURE = "OCCURRENCE DATA STRUCTURE";
        String INTEGRATED_OCCURRENCE_DATA_STRUCTURE = "INTEGRATED OCCURRENCE DATA STRUCTURE";
        String SUBJECT_LEVEL_ANALYSIS_DATASET = "SUBJECT LEVEL ANALYSIS DATASET";
        String INTEGRATED_SUBJECT_LEVEL = "INTEGRATED SUBJECT LEVEL";

        // SDTM
        String EVENTS = "EVENTS";
        String FINDINGS = "FINDINGS";
        String FINDINGS_ABOUT = "FINDINGS ABOUT";
        String INTERVENTIONS = "INTERVENTIONS";
        String RELATIONSHIP = "RELATIONSHIP";
        String SPECIAL_PURPOSE = "SPECIAL PURPOSE";
        String STUDY_REFERENCE = "STUDY REFERENCE";
        String TRIAL_DESIGN = "TRIAL DESIGN";

        default List<String> adam() {
            return Arrays.asList(
                ADAM_OTHER,
                BASIC_DATA_STRUCTURE,
                INTEGRATED_BASIC_DATA_STRUCTURE,
                OCCURRENCE_DATA_STRUCTURE,
                INTEGRATED_OCCURRENCE_DATA_STRUCTURE,
                SUBJECT_LEVEL_ANALYSIS_DATASET,
                INTEGRATED_SUBJECT_LEVEL
            );
        }

        default List<String> sdtm() {
            return Arrays.asList(
                EVENTS,
                FINDINGS,
                FINDINGS_ABOUT,
                INTERVENTIONS,
                RELATIONSHIP,
                SPECIAL_PURPOSE,
                STUDY_REFERENCE,
                TRIAL_DESIGN
            );
        }
    }

    public final static class Attributes {
        private Attributes() {}

        public static final String ORIGIN = "Origin";
        public static final String NAME = "Name";
        public static final String OID = "OID";
    }

    public final static class DataTypes {
        private DataTypes() {}

        public static final String FLOAT = "float";
        public static final String INTEGER = "integer";
        public static final String TEXT = "text";
        public static final String DATE = "datetime";

    }

    public final static class Origins {
        private Origins() {}

        public static final String PREDECESSOR = "Predecessor";
        public static final String CRF = "CRF";
        public static final String DERIVED = "Derived";
        public static final String ASSIGNED = "Assigned";
        public static final String PROTOCOL = "Protocol";
    }

    public final static class Dictionaries {
        private Dictionaries() {}

        public static final String MEDDRA = "MedDRA";
    }

    public final static class Categories {
        private Categories() {}

        public static final String FINDINGS = "FINDINGS";
        public static final String EVENTS = "EVENTS";
        public static final String INTERVENTIONS = "INTERVENTIONS";
    }
    //endregion

    //region Define 1
    private static class DefineV1Namespaces implements Namespaces {
        @Override
        public String odmVersion() {
            return "1.2";
        }

        @Override
        public String odmURI() {
            return "http://www.cdisc.org/ns/odm/v1.2";
        }

        @Override
        public String defURI() {
            return "http://www.cdisc.org/ns/def/v1.0";
        }

        @Override
        public String xmlnsURI() {
            return "http://www.cdisc.org/ns/odm/v1.2";
        }
    }

    private static class DefineV1Standards implements Standards {
        @Override
        public String sdtm() {
            return "CDISC SDTM";
        }

        @Override
        public String send() {
            return "CDISC SEND";
        }

        @Override
        public String adam() {
            return "CDISC ADaM";
        }
    }

    private static class DefineV1Prefixes implements Prefixes {
        @Override
        public String valuelist() {
            return "ValueList.";
        }
    }
    //endregion

    //region Define 2
    private static class DefineV2Namespaces implements Namespaces {
        @Override
        public String odmVersion() {
            return "1.3.2";
        }

        @Override
        public String odmURI() {
            return "http://www.cdisc.org/ns/odm/v1.3";
        }

        @Override
        public String defURI() {
            return "http://www.cdisc.org/ns/def/v2.0";
        }

        @Override
        public String xmlnsURI() {
            return "http://www.cdisc.org/ns/odm/v1.3";
        }

        @Override
        public String armURI(){
            return "http://www.cdisc.org/ns/arm/v1.0";
        }
    }

    private static class DefineV2Standards implements Standards {
        @Override
        public String sdtm() {
            return "SDTM-IG";
        }

        @Override
        public String send() {
            return "SEND-IG";
        }

        @Override
        public String adam() {
            return "ADaM-IG";
        }
    }

    private static class DefineV2Prefixes implements Prefixes {
        @Override
        public String valuelist() {
            return "VL.";
        }
    }
    //endregion
}