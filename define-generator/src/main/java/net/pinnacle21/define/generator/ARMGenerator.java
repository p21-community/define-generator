/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.generator;

import net.pinnacle21.define.generator.util.GeneratorUtils;
import net.pinnacle21.define.models.DefineMetadata;
import net.pinnacle21.define.models.Description;
import net.pinnacle21.define.models.DocumentReference;
import net.pinnacle21.define.models.arm.*;
import net.pinnacle21.parsing.xml.XmlWriter;
import org.apache.commons.lang3.StringUtils;
import org.xml.sax.SAXException;

import java.io.OutputStream;
import java.util.List;

public class ARMGenerator {

    private XmlWriter writer;
    private DefineMetadata metadata = DefineMetadata.V2;

    public ARMGenerator(XmlWriter writer) {
        this.writer = writer;
    }

    OutputStream getOutputStream() {
        return writer.getTargetStream();
    }

    void generateProgrammingCode(ProgrammingCode programmingCode) throws SAXException {
        if (programmingCode == null) {
            return;
        }

        XmlWriter.SimpleAttributes attributes = this.writer.newAttributes();

        if (StringUtils.isNotBlank(programmingCode.getContext())) {
            attributes.addAttribute("Context", programmingCode.getContext());
        }

        this.writer.startElement(metadata.getNamespaces().armURI(), "ProgrammingCode", attributes);
        if (StringUtils.isNotBlank(programmingCode.getCode())) {
            this.writer.simpleElement(metadata.getNamespaces().armURI(), "Code", this.writer.newAttributes(), programmingCode.getCode());
        }

        if (programmingCode.getDocumentReference() != null && programmingCode.getDocumentReference().getDocument() != null) {
            XmlWriter.SimpleAttributes leafID = this.writer.newAttributes();
            leafID.addAttribute("leafID", metadata.getPrefixes().leaf() + programmingCode.getDocumentReference().getDocument().getOid());
            this.writer.simpleElement(metadata.getNamespaces().defURI(), "DocumentRef", leafID);
        }
        this.writer.endElement(metadata.getNamespaces().armURI(), "ProgrammingCode");
    }

    void generateDocumentation(Documentation documentation) throws SAXException {
        if (documentation == null) {
            return;
        }

        this.writer.startElement(metadata.getNamespaces().armURI(), "Documentation");
        Description description = documentation.getDescription();
        if (description != null) {
            GeneratorUtils.writeDescription(this.writer, this.metadata, description.getLanguage(), description.getText());
            List<DocumentReference> documentReferences = documentation.getDocumentReferences();
            if (documentReferences != null) {
                for (DocumentReference reference : documentReferences) {
                    GeneratorUtils.writeDocumentRef(this.writer, metadata, reference);
                }
            }
        }
        this.writer.endElement(metadata.getNamespaces().armURI(), "Documentation");
    }

    void generateAnalysisDataset(AnalysisDataset dataset) throws SAXException {
        if (dataset == null || dataset.getItemGroup() == null) {
            return;
        }

        this.writer.startElement(metadata.getNamespaces().armURI(), "AnalysisDataset",
                this.writer.newAttributes().addAttribute("ItemGroupOID", metadata.getPrefixes().itemGroup() + dataset.getItemGroup().getName()));

        if (dataset.getWhereClause() != null) {
            this.writer.simpleElement(metadata.getNamespaces().defURI(), "WhereClauseRef",
                    this.writer.newAttributes().addAttribute("WhereClauseOID", metadata.getPrefixes().whereClause() + dataset.getWhereClause().getOid()));
        }

        for (AnalysisVariable variable : dataset.getAnalysisVariables()) {
            this.writer.simpleElement(metadata.getNamespaces().armURI(), "AnalysisVariable",
                    this.writer.newAttributes().addAttribute("ItemOID", metadata.getPrefixes().item() + variable.getItem().getOID()));
        }

        this.writer.endElement(metadata.getNamespaces().armURI(), "AnalysisDataset");
    }

    void generateAnalysisDatasets(AnalysisDatasets datasets) throws SAXException {
        if (datasets == null) {
            return;
        }

        XmlWriter.SimpleAttributes attributes = this.writer.newAttributes();
        if (datasets.getComment() != null) {
            attributes = this.writer.newAttributes().addAttribute(metadata.getNamespaces().defURI(), "CommentOID", metadata.getPrefixes().comment() + datasets.getComment().getOid());
        }
        this.writer.startElement(metadata.getNamespaces().armURI(), "AnalysisDatasets", attributes);

        for (AnalysisDataset dataset : datasets.getDatasets()) {
            this.generateAnalysisDataset(dataset);
        }

        this.writer.endElement(metadata.getNamespaces().armURI(), "AnalysisDatasets");
    }

    public void generateResultDisplay(ResultDisplay display) throws SAXException {
        if (display == null) {
            return;
        }

        XmlWriter.SimpleAttributes attributes = this.writer.newAttributes()
                .addAttribute("OID", metadata.getPrefixes().resultDisplay() + display.getName());

        if (StringUtils.isNotBlank(display.getName())) {
            attributes.addAttribute("Name", display.getName());
        }

        this.writer.startElement(metadata.getNamespaces().armURI(), "ResultDisplay", attributes);

        if (display.getDescription() != null) {
            GeneratorUtils.writeDescription(this.writer, this.metadata, display.getDescription());
        }

        if (display.getDocumentReference() != null) {
            GeneratorUtils.writeDocumentRef(this.writer, this.metadata, display.getDocumentReference());
        }

        generateAnalysisResults(display);

        this.writer.endElement(metadata.getNamespaces().armURI(), "ResultDisplay");
    }

    void generateAnalysisResults(ResultDisplay display) throws SAXException {
        if (display == null) {
            return;
        }

        List<AnalysisResult> analysisResultList = display.getAnalysisResults();
        for (int i = 0; i < analysisResultList.size(); i++) {
            AnalysisResult result = analysisResultList.get(i);

            XmlWriter.SimpleAttributes attributes = this.writer.newAttributes()
                    .addAttribute("OID", metadata.getPrefixes().analysisResult() + result.getOid());

            if (StringUtils.isNotBlank(result.getParameterOid())) {
                attributes.addAttribute("ParameterOID", metadata.getPrefixes().item() + result.getParameterOid());
            }

            if (StringUtils.isNotBlank(result.getAnalysisReason())) {
                attributes.addAttribute("AnalysisReason", result.getAnalysisReason());
            }

            if (StringUtils.isNotBlank(result.getAnalysisPurpose())) {
                attributes.addAttribute("AnalysisPurpose", result.getAnalysisPurpose());
            }

            this.writer.startElement(metadata.getNamespaces().armURI(), "AnalysisResult", attributes);

            if (result.getDescription() != null) {
                GeneratorUtils.writeDescription(this.writer, this.metadata, result.getDescription().getLanguage(), result.getDescription().getText());
            }

            if (result.getAnalysisDatasets() != null) {
                generateAnalysisDatasets(result.getAnalysisDatasets());
            }

            if (result.getDocumentation() != null) {
                generateDocumentation(result.getDocumentation());
            }

            if (result.getProgrammingCode() != null) {
                generateProgrammingCode(result.getProgrammingCode());
            }

            this.writer.endElement(metadata.getNamespaces().armURI(), "AnalysisResult");
        }
    }
}
