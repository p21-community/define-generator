/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.xml.bodyparsers;

import com.google.common.collect.ImmutableMap;
import net.pinnacle21.define.EventDispatcher;
import net.pinnacle21.define.GeneratorListener;
import net.pinnacle21.define.StudyParser;
import net.pinnacle21.define.models.*;
import net.pinnacle21.define.models.arm.ResultDisplay;
import net.pinnacle21.define.parsers.xml.XPathEvaluator;
import net.pinnacle21.define.parsers.xml.util.ParserUtils;
import net.pinnacle21.define.util.Templates;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.dom4j.Attribute;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.tree.DefaultAttribute;

import java.util.*;

public abstract class BodyParser implements StudyParser {
    protected Study study;
    // TODO: Can't make this final due to tests, which is not ideal
    protected XPathEvaluator evaluator;
    protected final DefineMetadata metadata;
    private final boolean forValidation;

    public EventDispatcher dispatcher = new EventDispatcher();

    BodyParser(XPathEvaluator evaluator, DefineMetadata metadata, boolean forValidation) {
        this.evaluator = evaluator;
        this.metadata = metadata;
        this.forValidation = forValidation;
    }

    public Study parse() {
        return parse(true);
    }

    public Study parse(boolean armSupport) {
        final int maxProgress = armSupport ? 10 : 9;
        int currentProgress = 1;
        dispatcher.fireTaskStart("Processing Study...", currentProgress, maxProgress);
        this.study = parseStudy();
        currentProgress++;

        dispatcher.fireTaskStart("Processing Documents...", currentProgress, maxProgress);
        Map<String, Document> documentMap = parseDocuments();
        currentProgress++;

        for (Document document : documentMap.values()) {
            study.setDocument(document.getOid(), document);
        }

        dispatcher.fireTaskStart("Processing Comments...", currentProgress, maxProgress);
        Map<String, Comment> commentMap = parseComments();
        currentProgress++;

        for (Comment comment : commentMap.values()) {
            study.setComment(comment.getOid(), comment);
        }

        dispatcher.fireTaskStart("Processing Methods...", currentProgress, maxProgress);
        Map<String, Method> methodMap = parseMethods();
        currentProgress++;

        for (Method method : methodMap.values()) {
            study.setMethod(method.getOid(), method);
        }

        dispatcher.fireTaskStart("Processing CodeLists...", currentProgress, maxProgress);
        Map<String, CodeList> codeListMap = parseCodeLists();
        currentProgress++;

        for (CodeList codeList : codeListMap.values()) {
            if (StringUtils.isBlank(codeList.getDictionary())) {
                study.setCodeList(codeList.getOid(), codeList);
            } else {    // We store external codelists as dictionaries
                study.setDictionary(codeList.getOid(), new net.pinnacle21.define.models.Dictionary()
                    .setOid(codeList.getOid())
                    .setName(codeList.getName())
                    .setDataType(codeList.getDataType())
                    .setDictionary(codeList.getDictionary())
                    .setVersion(codeList.getVersion())
                );
            }

        }

        dispatcher.fireTaskStart("Processing ItemGroupDefs...", currentProgress, maxProgress);
        Map<String, ItemGroup> itemGroupMap = parseItemGroupDefs();
        currentProgress++;

        for (ItemGroup itemGroup : itemGroupMap.values()) {
            study.setItemGroup(itemGroup.getName(), itemGroup);
        }

        dispatcher.fireTaskStart("Processing WhereClauses...", currentProgress, maxProgress);
        Map<String, WhereClause> whereClauseMap = parseWhereClauses();
        currentProgress++;

        for (WhereClause whereClause : whereClauseMap.values()) {
            study.setWhereClause(whereClause.getOid(), whereClause);
        }

        dispatcher.fireTaskStart("Processing ValueLevel...", currentProgress, maxProgress);
        List<ValueList> valueLevelList = parseValueLists(whereClauseMap);
        currentProgress++;

        for (ValueList valueList : valueLevelList) {
            study.setValueList(valueList.getSourceItemGroupName(), valueList.getSourceItemName(), valueList);
        }

        if (armSupport) {
            dispatcher.fireTaskStart("Processing Analysis Results Metadata...", currentProgress, maxProgress);
            currentProgress++;

            if (study.getDefineMetadata() == DefineMetadata.V2) {
                for (ResultDisplay resultDisplay : parseResultDisplays(whereClauseMap)) {
                    study.setResultDisplay(resultDisplay.getName(), resultDisplay);
                }
            }
        }

        dispatcher.fireTaskIncrement("Generating Define...", currentProgress, maxProgress);

        return study;
    }

    protected abstract List<ResultDisplay> parseResultDisplays(Map<String, WhereClause> whereClauseMap);

    private Study parseStudy() {
        Element name = this.evaluator.selectSingleElement("/ODM/odm:Study/odm:GlobalVariables/odm:StudyName");
        Element description = this.evaluator.selectSingleElement("/ODM/odm:Study/odm:GlobalVariables/odm:StudyDescription");
        Element protocol = this.evaluator.selectSingleElement("/ODM/odm:Study/odm:GlobalVariables/odm:ProtocolName");
        Element metaDataVersion = this.evaluator.selectSingleElement("/ODM/odm:Study/odm:MetaDataVersion");
        List<Element> translatedText = this.evaluator.selectElements("//odm:TranslatedText[@xml:lang]");

        Study study = new Study();

        study.setName(name != null ? name.getText() : null);
        study.setStandardName(metaDataVersion != null
                ? metaDataVersion.attributeValue("StandardName")
                : null);
        study.setStandardVersion(metaDataVersion != null
                ? metaDataVersion.attributeValue("StandardVersion")
                : null);
        study.setDescription(description != null ? description.getText() : null);
        study.setProtocol(protocol != null ? protocol.getText() : null);
        study.setLanguage(!translatedText.isEmpty() ? getXmlLang(translatedText.get(0)) : null);
        study.setMetadata(metadata);

        return study;
    }

    private Map<String, Document> parseDocuments() {
        Map<String, Document> documentMap = new HashMap<>();
        List<Element> documents = evaluator.selectElements("/ODM/odm:Study/odm:MetaDataVersion/def:leaf");

        for (Element leafElement : documents) {
            String id = leafElement.attributeValue("ID", "");

            Document document = new Document()
                    .setOid(id.startsWith(metadata.getPrefixes().leaf()) ? id.substring(metadata.getPrefixes().leaf().length()) : id)
                    .setTitle(leafElement.elementText("title"))
                    .setHref(leafElement.attributeValue("href"));

            if (this.evaluator.hasNodes("/ODM/odm:Study/odm:MetaDataVersion/def:AnnotatedCRF/def:DocumentRef[@leafID=$id]",
                    ImmutableMap.of("id", id))) {
                document.setType(Document.Type.ANNOTATEDCRF);
            } else if (this.evaluator.hasNodes("/ODM/odm:Study/odm:MetaDataVersion/def:SupplementalDoc/def:DocumentRef[@leafID=$id]",
                    ImmutableMap.of("id", id))){
                document.setType(Document.Type.SUPPLEMENTAL);
            } else {
                document.setType(Document.Type.LEAF);
            }

            documentMap.put(
                    trim(id),
                    document
            );
        }
        return documentMap;
    }

    abstract Map<String, Comment> parseComments();

    private Map<String, Method> parseMethods() {
        Map<String, Method> methodMap = new HashMap<>();
        List methods = getMethods();

        for (Object methodDefObj : methods) {
            Element methodElement = (Element) methodDefObj;
            String oid = methodElement.attributeValue("OID", "");
            Method method = new Method().setOid(
                    oid.startsWith(metadata.getPrefixes().method()) ? oid.substring(metadata.getPrefixes().method().length()) : oid);

            methodMap.put(trim(oid), parseMethod(method, methodElement));
        }
        return methodMap;
    }

    abstract List getMethods();

    abstract Method parseMethod(Method method, Element methodElement);

    private Map<String, CodeList> parseCodeLists() {
        Map<String, CodeList> codeListMap = new HashMap<>();
        List<Element> codeLists = this.evaluator.selectElements("/ODM/odm:Study/odm:MetaDataVersion/odm:CodeList");

        for (Element codeListElement : codeLists) {
            String oid = codeListElement.attributeValue("OID", "");
            CodeList codeList = new CodeList().setOid(
                    oid.startsWith(metadata.getPrefixes().codelist()) ? oid.substring(metadata.getPrefixes().codelist().length()) : oid);
            codeList.setName(codeListElement.attributeValue("Name"));
            codeList.setDataType(codeListElement.attributeValue("DataType"));
            codeList.setIsExternal(codeListElement.element("ExternalCodeList") != null);

            //get the nciodm:CodeListExtensible attribute if available (for terminology parsing)
            if (StringUtils.isNotBlank(codeListElement.attributeValue("CodeListExtensible"))) {
                codeList.setIsExtensible(codeListElement.attributeValue("CodeListExtensible"));
            }

            Element alias = this.evaluator.selectSingleElement(codeListElement, "odm:Alias");
            if (alias != null) {
                codeList.setCode(alias.attributeValue("Name"));
            }

            Element externalCodeList = this.evaluator.selectSingleElement(codeListElement, "odm:ExternalCodeList");
            if (externalCodeList != null) {
                codeList.setDictionary(externalCodeList.attributeValue("Dictionary"));
                codeList.setVersion(externalCodeList.attributeValue("Version"));
            }

            for (Element childElement : codeListElement.elements()) {
                if (Arrays.asList("CodeListItem", "EnumeratedItem").contains(childElement.getName())) {
                    String codedValue = childElement.attributeValue("CodedValue");
                    CodeListItem codeListItem = new CodeListItem()
                            .setCodedValue(codedValue)
                            .setExtendedValue(childElement.attributeValue("ExtendedValue"));
                    Node decode = this.evaluator.selectSingleNode(childElement, "odm:Decode/odm:TranslatedText");
                    codeList.put(parseCodeListItem(codeListItem, childElement, decode));
                }
            }
            codeListMap.put(trim(oid), codeList);
        }

        return codeListMap;
    }

    abstract CodeListItem parseCodeListItem(CodeListItem codeListItem, Element childElement, Node decode);

    private Map<String, ItemGroup> parseItemGroupDefs() {
        Map<String, ItemGroup> datasetMap = new HashMap<>();
        List<Element> datasets = this.evaluator.selectElements("/ODM/odm:Study/odm:MetaDataVersion/odm:ItemGroupDef");

        for (Element itemGroupDefElement : datasets) {
            ItemGroup itemGroup = parseItemGroupDef(itemGroupDefElement);
            datasetMap.put(StringUtils.defaultString(itemGroup.getOID()), itemGroup);
        }
        return datasetMap;
    }

    private ItemGroup parseItemGroupDef(Element itemGroupElement) {
        ItemGroup itemGroup = new ItemGroup()
                .setOID(
                        ParserUtils.substringWithPrefix(
                                metadata.getPrefixes().itemGroup(),
                                itemGroupElement.attributeValue("OID")))
                .setName(itemGroupElement.attributeValue("Name"));
        itemGroup.setIsRepeating(itemGroupElement.attributeValue("Repeating"));
        itemGroup.setIsReferenceData(itemGroupElement.attributeValue("IsReferenceData"));
        itemGroup.setPurpose(itemGroupElement.attributeValue("Purpose"));
        itemGroup.setStructure(itemGroupElement.attributeValue("Structure"));
        itemGroup.setCategory(itemGroupElement.attributeValue("Class"));

        List<Item> variables = new ArrayList<>();
        List<Element> itemRefList = itemGroupElement.elements("ItemRef");
        TreeMap<Integer, String> keyMap = new TreeMap<>();
        for (Element itemRef : itemRefList) {
            if (StringUtils.isBlank(itemRef.attributeValue("ItemOID"))) {
                continue;
            }

            if (StringUtils.isNotBlank(itemRef.attributeValue("RoleCodeListOID"))) {
                String roleOid = itemRef.attributeValue("RoleCodeListOID");
                this.study.addRoleCodelistOID(roleOid.startsWith(metadata.getPrefixes().codelist()) ?
                        roleOid.substring(metadata.getPrefixes().codelist().length()) : roleOid);
            }

            Element itemDef = evaluator.selectSingleElement(String.format(
                "/ODM/odm:Study/odm:MetaDataVersion/odm:ItemDef[@OID='%s']",
                itemRef.attributeValue("ItemOID"))
            );

            if (!itemGroup.isSuppqual()) {
                Element valueListItem = evaluator.selectSingleElement(String.format(
                    "/ODM/odm:Study/odm:MetaDataVersion/def:ValueListDef/odm:ItemRef[@ItemOID='%s']",
                    itemRef.attributeValue("ItemOID"))
                );
                if (valueListItem != null && !itemGroup.isSuppqual()) {
                    Element suppGroupElement = evaluator.selectSingleElement(String.format(
                        "/ODM/odm:Study/odm:MetaDataVersion/odm:ItemGroupDef[@Name='%s']",
                        itemGroup.getSuppName()
                    ));
                    if (suppGroupElement != null) {
                        List<Attribute> attributes = valueListItem.attributes();
                        String key = itemRef.attributeValue("KeySequence");
                        attributes.add(new DefaultAttribute("KeySequence", key));
                        if (StringUtils.isNotBlank(key)
                                && StringUtils.isNumeric(key)
                                && StringUtils.isNotBlank(itemDef.attributeValue("Name"))) {
                            keyMap.put(Integer.parseInt(key), itemDef.attributeValue("Name"));
                        }
                        continue;
                    }
                }
            }

            if (itemDef != null) {
                String key = itemRef.attributeValue("KeySequence");
                String value = itemDef.attributeValue("Name");

                if (StringUtils.isNotBlank(key) && StringUtils.isNumeric(key) && StringUtils.isNotBlank(value)) {
                    keyMap.put(Integer.parseInt(key), value);
                }
                variables.add(parseItem(itemRef, itemDef, false));
            }
        }

        parseItemGroup(keyMap, itemGroup, itemGroupElement);

        itemGroup.addAll(variables);
        // Also save all items to the study for use in value level
        for (Item var : variables) {
            study.setItem(itemGroup.getName(), var.getName(), var);
        }
        return itemGroup;
    }

    abstract void parseItemGroup(Map<Integer, String> keyMap, ItemGroup itemGroup, Element itemGroupDefElement);

    private Item parseItem(Element itemRef, Element itemDef, boolean isValueLevel) {
        Item item = new Item();
        addItemDefAttributesTo(item, itemDef);
        addItemRefAttributesTo(item, itemRef, isValueLevel);
        populateItemAttributes(item, itemDef, isValueLevel);
        String methodOID = getMethodOID(itemDef, itemRef);
        item.setMethodOid(methodOID.startsWith(metadata.getPrefixes().method()) ? methodOID.substring(3) : methodOID);

        if (!isValueLevel) {
            Element valueListRef = itemDef.element("ValueListRef");
            String valueListId = valueListRef != null && StringUtils.isNotBlank(valueListRef.attributeValue("ValueListOID"))
                    ? valueListRef.attributeValue("ValueListOID") : "";
            item.setValueListOid(getValueListOID(valueListId));
        }

        return item;
    }

    private void addItemRefAttributesTo(Item item, Element itemRef, boolean isValueLevel) {
        item.setOrder(itemRef.attributeValue("OrderNumber"));
        item.setMandatory(itemRef.attributeValue("Mandatory"));
        item.setRole(!isValueLevel ? itemRef.attributeValue("Role") : null);
    }

    private void addItemDefAttributesTo(Item item, Element itemDef) {
        item.setOID(
                ParserUtils.substringWithPrefix(
                        metadata.getPrefixes().item(),
                        itemDef.attributeValue("OID")))
                .setName(itemDef.attributeValue("Name"));
        item.setDataType(itemDef.attributeValue("DataType"));
        item.setLength(itemDef.attributeValue("Length"));
        item.setSignificantDigits(itemDef.attributeValue("SignificantDigits"));
        item.setFormat(itemDef.attributeValue("DisplayFormat"));

        if (itemDef.element("Description") != null && itemDef.element("Description").element("TranslatedText") != null) {
            item.setLabel(itemDef.element("Description").element("TranslatedText").getText());
        }

        Element codeListRef = itemDef.element("CodeListRef");
        String codeListId = codeListRef != null ? codeListRef.attributeValue("CodeListOID") : "";
        codeListId = ParserUtils.substringWithPrefix(metadata.getPrefixes().codelist(), codeListId);

        if (StringUtils.isNotBlank(codeListId)) {
            // We already parse the dictionaries and codelists correctly. If we can find a dictionary make that
            // reference otherwise assume it is a codelist ref and allow OD0048 to fire. We can't differentiate
            // here if it is a dictionary or codelist without a codelist declaration
            if (this.study.hasDictionary(codeListId)) {
                item.setDictionaryOid(codeListId);
            } else {
                item.setCodeListOid(codeListId);
            }
        }
    }

    private String getValueListOID(String oid) {
        return oid.startsWith(metadata.getPrefixes().valuelist()) ? oid.substring(metadata.getPrefixes().valuelist().length()) : oid;
    }

    abstract void populateItemAttributes(Item item, Element itemDef, boolean isValueLevel);

    abstract String getMethodOID(Element itemDef, Element itemRef);

    /**
     * This method is only applied to define.xml 2.0. WhereClauses for define.xml 1.0 are created
     * when parsing value level
     */
    private Map<String, WhereClause> parseWhereClauses() {
        Map<String, WhereClause> whereClauseMap = new HashMap<>();
        List<Element> whereClauseDefList = this.evaluator.selectElements("/ODM/odm:Study/odm:MetaDataVersion/def:WhereClauseDef");

        for (Element whereClauseDef : whereClauseDefList) {
            WhereClause whereClause = new WhereClause();

            for (Element rangeCheckElement : whereClauseDef.elements("RangeCheck")) {
                RangeCheck rangeCheck = new RangeCheck();
                rangeCheck.setComparator(rangeCheckElement.attributeValue("Comparator"));

                Element itemDef = this.evaluator.selectSingleElement("/ODM/odm:Study/odm:MetaDataVersion/odm:ItemDef[@OID=$id]",
                        ImmutableMap.of("id", rangeCheckElement.attributeValue("ItemOID")));

                if (itemDef != null) {
                    rangeCheck.setItemOid(itemDef.attributeValue("Name"));
                    Element structure = this.evaluator.selectSingleElement("/ODM/odm:Study/odm:MetaDataVersion/odm:ItemGroupDef[odm:ItemRef[@ItemOID=$id]]",
                            ImmutableMap.of("id", itemDef.attributeValue("OID")));
                    rangeCheck.setItemGroupOid(structure != null ? structure.attributeValue("Name") : null);
                }

                for (Element checkValue : rangeCheckElement.elements("CheckValue")) {
                    rangeCheck.addValue(checkValue.getText());
                }

                whereClause.add(rangeCheck);
            }
            whereClause.setOid(Templates.generateWhereClauseOid(whereClause.getRangeChecks()));
            String comment = whereClauseDef.attributeValue("CommentOID", "");
            whereClause.setCommentOid(
                    comment.startsWith(metadata.getPrefixes().comment()) ? comment.substring(4) : comment);

            whereClauseMap.put(whereClauseDef.attributeValue("OID"), whereClause);
        }

        return whereClauseMap;
    }

    List<ValueList> parseValueLists(Map<String, WhereClause> whereClauseMap) {
        List<ValueList> valueLevelList = new ArrayList<>();
        List<Element> valueListDefList = this.evaluator.selectElements("/ODM/odm:Study/odm:MetaDataVersion/def:ValueListDef");
        String[] oids = new String[valueListDefList.size()];
        Arrays.fill(oids, StringUtils.EMPTY);

        int i = 0;
        for (Element valueList : valueListDefList) {
            oids[i] = valueList.attributeValue("OID");
            for (ItemGroup itemGroup : this.study.getItemGroups()) {
                for (Item item : itemGroup.getItems()) {
                    String valueListOid = item.getValueListOid();
                    if (StringUtils.isNotBlank(valueListOid) && StringUtils.isNotBlank(valueList.attributeValue("OID"))
                            && valueListOid.equals(getValueListOID(valueList.attributeValue("OID")))) {
                        // Have an item we need to create a value list for
                        String itemGroupName = itemGroup.getName();
                        String itemName = item.getName();
                        String vListOid = itemGroupName + "." + itemName;

                        ValueList newValueList = new ValueList()
                            .setSourceItemGroupName(itemGroupName)
                            .setSourceItemName(itemName)
                            .setOid(vListOid);

                        // Need to set the itemsRef in case we change the valueListName (oid)
                        item.setValueListOid(vListOid);

                        populateValueListitems(newValueList, valueList, itemName, itemGroupName, whereClauseMap);

                        valueLevelList.add(newValueList);
                        oids[i] = StringUtils.EMPTY; //Negate the item we just added at the top...
                    }
                }
            }
            i++;
        }
        //Create orphaned ValueLists just for validation
        if (forValidation) {
            createRemainingValueLists(valueListDefList, valueLevelList, oids);
        }
        return valueLevelList;
    }

    /**
     * Package level access provided for testing.
     *
     * @param oids contains the list of OIDS for ValueLists that need to be created
     */
    void createRemainingValueLists(List<Element> valueListDefList, List<ValueList> valueLevelList, String[] oids) {
        Arrays.sort(oids);
        String itemGroupName = StringUtils.EMPTY, itemName = StringUtils.EMPTY;
        for (Element valueListElement : valueListDefList) {
            String oid = valueListElement.attributeValue("OID");
            int index = Arrays.binarySearch(oids, StringUtils.defaultString(oid));
            if (index >= 0) {
                oid = ParserUtils.substringWithPrefix(metadata.getPrefixes().valuelist(), oid);
                //Can be split with . or _
                String[] parts = oid.split("_");
                //If length == 1 then the split failed and the whole string is stored in [0]
                parts = parts.length == 1 ? parts[0].split("\\.") : parts;
                ValueList valueList = new ValueList()
                        .setOid(oid);
                if (parts.length == 3) {
                    itemGroupName = StringUtils.join(new String[]{parts[0], parts[1]}, ".");
                    itemName = parts[2];
                    valueList.setSourceItemGroupName(itemGroupName)
                            .setSourceItemName(itemName);
                } else if (parts.length == 2) { //try to be useful with the OIDs
                    itemGroupName = parts[0];
                    itemName = parts[1];
                    valueList.setSourceItemGroupName(itemGroupName)
                            .setSourceItemName(itemName);
                } else { //otherwise just shove the whole thing in for both.
                    valueList.setSourceItemGroupName(oid)
                            .setSourceItemName(oid);
                }
                populateValueListitems(valueList, valueListElement, itemName, itemGroupName, new HashMap<>());
                valueLevelList.add(valueList);
            }
        }
    }

    private void populateValueListitems(ValueList newValueList, Element valueList, String itemName,
            String itemGroupName, Map<String, WhereClause> whereClauseMap) {
        for (Element vlItemRef : valueList.elements("ItemRef")) {
            Element vlItemDef = this.evaluator.selectSingleElement("/ODM/odm:Study/odm:MetaDataVersion/odm:ItemDef[@OID=$id]",
                    ImmutableMap.of("id", vlItemRef.attributeValue("ItemOID")));

            if (vlItemDef != null) {
                WhereClause whereClause =
                        getWhereClause(itemName, itemGroupName, vlItemDef,
                                vlItemRef, whereClauseMap);

                if (whereClause != null) {
                    study.setWhereClause(whereClause.getOid(), whereClause);

                    vlItemDef.addAttribute(
                            "Name", itemName + "." + whereClause.getOid());
                    Item valueListItem = parseItem(vlItemRef, vlItemDef, true);
                    valueListItem.setWhereClauseOid(whereClause.getOid());
                    if (StringUtils.isNotBlank(vlItemRef.attributeValue("KeySequence"))) {
                        valueListItem.setKeySequence(Integer.parseInt(vlItemRef.attributeValue("KeySequence")));
                    }
                    newValueList.add(valueListItem);
                    study.setItem(itemGroupName, valueListItem.getName(), valueListItem);
                }
            }
        }
    }

    abstract WhereClause getWhereClause(String itemName, String itemGroupName, Element valueListItemDef,
                                        Element itemRef, Map<String, WhereClause> whereClauseMap);


    String determineOrigin(String value) {
        if (value == null) {
            return null;
        }
        String[] origins = {"Assigned", "CRF", "Derived", "Protocol", "eDT", "Predecessor"};

        for (String origin : origins) {
            if (value.contains(origin)) {
                return origin;
            }
        }
        return null;
    }

    String getPagesFromOrigin(String origin) {
        if (origin == null) {
            return null;
        }
        StringBuilder pagesBuilder = new StringBuilder();
        String[] pages = origin.split("[\\s,]");

        for (String page : pages) {
            if (NumberUtils.isNumber(page)) {
                pagesBuilder.append(page);
                if (!pages[pages.length - 1].equals(page)) {
                    pagesBuilder.append(" ");
                }
            }
        }
        return pagesBuilder.toString();
    }

    String trim(String value) {
        return value != null ? value.trim() : null;
    }

    /**
     * Gets the xml:lang attribute value from element.
     * @return value of xml:lang or null if element is null or attribute does not exist
     */
    String getXmlLang(Element element) {
        if (element == null) {
            return null;
        }
        Attribute attribute = element.attribute("lang");

        return attribute != null
                ? ("xml".equals(attribute.getNamespacePrefix()) ? attribute.getValue() : null)
                : null;
    }

    @Override
    public void addGeneratorListener(GeneratorListener listener) {
        this.dispatcher.add(listener);
    }
}
