/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.*;
import net.pinnacle21.define.models.arm.AnalysisDataset;
import net.pinnacle21.define.models.arm.AnalysisResult;
import net.pinnacle21.define.models.arm.AnalysisVariable;
import net.pinnacle21.define.util.MockNamedRow;
import net.pinnacle21.parsing.excel.ExcelSheetParser;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.RowIdentifier;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import java.util.List;

import static net.pinnacle21.define.parsers.excel.AnalysisCriteriaSheetParser.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

public class AnalysisCriteriaSheetParserTest {

    @Test
    public void displayColumnIsRequired() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new AnalysisCriteriaSheetParser();

        parser.parse(
                new MockNamedRow(),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR), eq(DISPLAY_COLUMN));
    }

    @Test
    public void resultColumnIsRequired() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new AnalysisCriteriaSheetParser();

        parser.parse(
                new MockNamedRow(),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR), eq(RESULT_COLUMN));
    }

    @Test
    public void datasetColumnIsRequired() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new AnalysisCriteriaSheetParser();

        parser.parse(
                new MockNamedRow(),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR), eq(DATASET_COLUMN));
    }

    @Test
    public void invalidAnalysisResultReferenceErrorRaised() {
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(new Study()));
        ExcelSheetParser<ParsedStudy> parser = new AnalysisCriteriaSheetParser();

        String expectedDisplay = "Display";
        String expectedResult = "Result";

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, expectedDisplay)
                        .with(RESULT_COLUMN, expectedResult)
                        .with(DATASET_COLUMN, "Dataset"),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(RESULT_REF_ERROR), eq(RESULT_COLUMN), eq(expectedResult));
    }

    @Test
    public void invalidDatasetReferenceErrorRaised() {
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(new Study()));
        ExcelSheetParser<ParsedStudy> parser = new AnalysisCriteriaSheetParser();

        String expectedDisplay = "Display";
        String expectedResult = "Result";
        String expectedDataset = "Dataset";

        parsedStudy.getStudy().setAnalysisResult(expectedDisplay, expectedResult, new AnalysisResult());

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, expectedDisplay)
                        .with(RESULT_COLUMN, expectedResult)
                        .with(DATASET_COLUMN, expectedDataset),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(DATASET_REF_ERROR), eq(DATASET_COLUMN), eq(expectedDataset));
    }

    @Test
    public void invalidVariablesReferenceErrorRaised() {
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(new Study()));
        ExcelSheetParser<ParsedStudy> parser = new AnalysisCriteriaSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedDisplay = "Display";
        String expectedResult = "Result";
        String expectedDataset = "Dataset";
        String expectedVariable1 = "Variable1";
        String expectedVariable2 = "Variable2";

        study.setAnalysisResult(expectedDisplay, expectedResult, new AnalysisResult());
        study.setItemGroup(expectedDataset, new ItemGroup());

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, expectedDisplay)
                        .with(RESULT_COLUMN, expectedResult)
                        .with(DATASET_COLUMN, expectedDataset)
                        .with(VARIABLES_COLUMN, String.format("%s, %s", expectedVariable1, expectedVariable2)),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class),
                eq(VARIABLE_REF_ERROR), eq(VARIABLES_COLUMN), eq(expectedVariable1));
        verify(parsedStudy).error(any(RowIdentifier.class),
                eq(VARIABLE_REF_ERROR), eq(VARIABLES_COLUMN), eq(expectedVariable2));
    }

    @Test
    public void invalidWhereClauseReferenceErrorRaised() {
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(new Study()));
        ExcelSheetParser<ParsedStudy> parser = new AnalysisCriteriaSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedDisplay = "Display";
        String expectedResult = "Result";
        String expectedDataset = "Dataset";
        String expectedWhereClause = "WhereClause";

        study.setAnalysisResult(expectedDisplay, expectedResult, new AnalysisResult());
        study.setItemGroup(expectedDataset, new ItemGroup());

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, expectedDisplay)
                        .with(RESULT_COLUMN, expectedResult)
                        .with(DATASET_COLUMN, expectedDataset)
                        .with(WHERE_CLAUSE_COLUMN, expectedWhereClause),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class),
                eq(WHERE_CLAUSE_REF_ERROR), eq(WHERE_CLAUSE_COLUMN), eq(expectedWhereClause));
    }

    @Test
    public void validDatasetReferencesAreAssignedToAnalysisResult() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new AnalysisCriteriaSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedDisplay = "Display";
        String expectedResult = "Result";
        String expectedDataset = "Dataset";
        String expectedDataset2 = "Dataset2";
        ItemGroup expectedItemGroup = new ItemGroup();
        ItemGroup expectedItemGroup2 = new ItemGroup();

        study.setAnalysisResult(expectedDisplay, expectedResult, new AnalysisResult());
        study.setItemGroup(expectedDataset, expectedItemGroup);
        study.setItemGroup(expectedDataset2, expectedItemGroup2);

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, expectedDisplay + " ")
                        .with(RESULT_COLUMN, expectedResult + " ")
                        .with(DATASET_COLUMN, expectedDataset + " "),
                parsedStudy
        );

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, expectedDisplay)
                        .with(RESULT_COLUMN, expectedResult)
                        .with(DATASET_COLUMN, expectedDataset2),
                parsedStudy
        );

        AnalysisResult analysisResult = study.getAnalysisResult(expectedDisplay, expectedResult);

        assertNotNull(analysisResult);
        assertNotNull(analysisResult.getAnalysisDatasets());
        List<AnalysisDataset> actualDatasets = analysisResult.getAnalysisDatasets().getDatasets();
        assertEquals(2, actualDatasets.size());
        assertEquals(expectedItemGroup, actualDatasets.get(0).getItemGroup());
        assertEquals(expectedItemGroup2, actualDatasets.get(1).getItemGroup());
    }

    @Test
    public void validVariableReferencesAreAssignedToAnalysisResult() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new AnalysisCriteriaSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedDisplay = "Display";
        String expectedResult = "Result";
        String expectedDataset = "Dataset";
        String expectedVariable1 = "Variable1";
        String expectedVariable2 = "Variable2";
        Item expectedItem1 = new Item();
        Item expectedItem2 = new Item();
        AnalysisVariable expectedAnalysisVariable1 = new AnalysisVariable().setItem(expectedItem1);
        AnalysisVariable expectedAnalysisVariable2 = new AnalysisVariable().setItem(expectedItem2);

        study.setAnalysisResult(expectedDisplay, expectedResult, new AnalysisResult());
        study.setItemGroup(expectedDataset, new ItemGroup());
        study.setItem(expectedDataset, expectedVariable1, expectedItem1);
        study.setItem(expectedDataset, expectedVariable2, expectedItem2);

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, expectedDisplay)
                        .with(RESULT_COLUMN, expectedResult)
                        .with(DATASET_COLUMN, expectedDataset)
                        .with(VARIABLES_COLUMN, String.format("%s, %s", expectedVariable1, expectedVariable2)),
                parsedStudy
        );

        AnalysisResult analysisResult = study.getAnalysisResult(expectedDisplay, expectedResult);

        assertNotNull(analysisResult);
        assertNotNull(analysisResult.getAnalysisDatasets());
        List<AnalysisDataset> actualDatasets = analysisResult.getAnalysisDatasets().getDatasets();
        assertEquals(2, actualDatasets.get(0).getAnalysisVariables().size());
        assertEquals(expectedAnalysisVariable1, actualDatasets.get(0).getAnalysisVariables().get(0));
        assertEquals(expectedAnalysisVariable2, actualDatasets.get(0).getAnalysisVariables().get(1));
    }

    @Test
    public void validWhereClauseReferenceIsAssignedToAnalysisResult() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new AnalysisCriteriaSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedDisplay = "Display";
        String expectedResult = "Result";
        String expectedDataset = "Dataset";
        String expectedWhereClauseOid = "Where Clause";
        WhereClause expectedWhereClause = new WhereClause();

        study.setAnalysisResult(expectedDisplay, expectedResult, new AnalysisResult());
        study.setItemGroup(expectedDataset, new ItemGroup());
        study.setWhereClause(expectedWhereClauseOid, expectedWhereClause);

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, expectedDisplay)
                        .with(RESULT_COLUMN, expectedResult)
                        .with(DATASET_COLUMN, expectedDataset)
                        .with(WHERE_CLAUSE_COLUMN, expectedWhereClauseOid),
                parsedStudy
        );

        AnalysisResult analysisResult = study.getAnalysisResult(expectedDisplay, expectedResult);

        assertNotNull(analysisResult);
        assertNotNull(analysisResult.getAnalysisDatasets());
        List<AnalysisDataset> actualDatasets = analysisResult.getAnalysisDatasets().getDatasets();
        assertEquals(expectedWhereClause, actualDatasets.get(0).getWhereClause());
    }

    @Test
    public void paramcdDatasetIsAssigned() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new AnalysisCriteriaSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedDisplay = "Display";
        String expectedResult = "Result";
        String expectedDataset = "Dataset";
        String expectedWhereClauseOid = "Where Clause";

        study.setAnalysisResult(expectedDisplay, expectedResult, new AnalysisResult());
        study.setItemGroup(expectedDataset, new ItemGroup() {{
            add(new Item().setOID("PARAMCD"));
        }});
        study.setWhereClause(expectedWhereClauseOid, new WhereClause() {{
            add(new RangeCheck()
                    .setItemGroupOid("Dataset")
                    .setItemOid("PARAMCD")
            );
        }});

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, expectedDisplay)
                        .with(RESULT_COLUMN, expectedResult)
                        .with(DATASET_COLUMN, expectedDataset)
                        .with(WHERE_CLAUSE_COLUMN, expectedWhereClauseOid),
                parsedStudy
        );

        AnalysisResult analysisResult = study.getAnalysisResult(expectedDisplay, expectedResult);

        assertNotNull(analysisResult);
        assertEquals(expectedDataset, analysisResult.getParamcdDataset());
    }
}
