/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models;

import net.pinnacle21.define.parsers.xml.util.ParserUtils;

public class Method extends ExcelRecord {
    private String oid;
    private String name;
    private String type;
    private String description;
    private String language;
    private String documentOid;
    private String pages;
    private String lastPage;
    private String firstPage;
    private String expressionContext;
    private String expressionCode;

    public String getOid() {
        return oid;
    }

    public Method setOid(String oid) {
        this.oid = oid != null ? oid.trim() : null;
        return this;
    }

    public String getName() {
        return name;
    }

    public Method setName(String name) {
        this.name = name != null ? name.trim() : null;
        return this;
    }

    public String getType() {
        return type;
    }

    public Method setType(String type) {
        this.type = type != null ? type.trim() : null;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Method setDescription(String description) {
        this.description = description != null ? description.trim() : null;
        return this;
    }

    public String getLanguage() {
        return language;
    }

    public Method setLanguage(String language) {
        this.language = language != null ? language.trim() : null;
        return this;
    }

    public String getDocumentOid() {
        return documentOid;
    }

    public Method setDocumentOid(String documentOid) {
        this.documentOid = documentOid != null ? documentOid.trim() : null;
        return this;
    }

    public String getPages() {
        return pages;
    }

    public Method setPages(String pages) {
        this.pages = pages != null ? pages.trim() : null;
        return this;
    }

    public Method setFirstPage(String firstPage) {
        this.firstPage = firstPage != null ? firstPage.trim() : null;
        return this;
    }

    public String getFirstPage() {
        return firstPage;
    }

    public Method setLastPage(String lastPage) {
        this.lastPage = lastPage != null ? lastPage.trim() : null;
        return this;
    }

    public String getLastPage() {
        return lastPage;
    }

    public String getAnyPages() {
        return ParserUtils.aggregatePages(getPages(), getFirstPage(), getLastPage());
    }

    public String getExpressionContext() {
        return expressionContext;
    }

    public Method setExpressionContext(String expressionContext) {
        this.expressionContext = expressionContext != null ? expressionContext.trim() : null;
        return this;
    }

    public String getExpressionCode() {
        return expressionCode;
    }

    public Method setExpressionCode(String expressionCode) {
        this.expressionCode = expressionCode != null ? expressionCode.trim() : null;
        return this;
    }
}
