/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.terminology;

import net.pinnacle21.define.models.CodeList;
import net.pinnacle21.define.models.CodeListItem;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.parsers.xml.XPathEvaluator;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;

import java.util.HashMap;
import java.util.List;

public class TerminologyXMLParser {
    private final Document document;

    public TerminologyXMLParser(XPathEvaluator terminology) {
        this.document = terminology.getDocument();
    }

    public Study parse() throws DocumentException {
        return parse(this.document);
    }

    public Study parse(Document document) throws DocumentException {
        Element odm = (Element) document.selectSingleNode("ODM");

        HashMap<String, String> namespaces = new HashMap<>();
        namespaces.put("odm", odm.getNamespaceURI());

        XPathEvaluator xPathEvaluator = new XPathEvaluator(document, namespaces);

        Study study = new Study()
                .setName(xPathEvaluator.selectSingleNode("/ODM/odm:Study/odm:GlobalVariables/odm:StudyName")
                        .getStringValue()
                )
                .setDescription(xPathEvaluator
                            .selectSingleNode("/ODM/odm:Study/odm:GlobalVariables/odm:StudyDescription")
                            .getStringValue())
                .setProtocol(xPathEvaluator.selectSingleNode("/ODM/odm:Study/odm:GlobalVariables/odm:ProtocolName")
                            .getStringValue());

        List codeLists = xPathEvaluator.selectNodes("/ODM/odm:Study/odm:MetaDataVersion/odm:CodeList");
        for (Object obj : codeLists) {
            Element codeListElement = (Element) obj;
            Element clAlias = codeListElement.element("Alias");
            String oid = codeListElement.attributeValue("OID");
            CodeList cl = new CodeList()
                    .setOid(oid)
                    .setName(codeListElement.attributeValue("Name"))
                    .setDataType(codeListElement.attributeValue("DataType"))
                    .setIsExtensible(codeListElement.attributeValue("CodeListExtensible"));

            if (clAlias != null) {
                cl.setCode(clAlias.attributeValue("Name"));
            }

            for (Object cliObj : codeListElement.elements("EnumeratedItem")) {
                Element enumeratedItemElement = (Element) cliObj;
                Element cliAlias = enumeratedItemElement.element("Alias");
                String codedValue = enumeratedItemElement.attributeValue("CodedValue");
                CodeListItem cli = new CodeListItem().setCodedValue(codedValue);

                if (cliAlias != null) {
                    cli.setCode(cliAlias.attributeValue("Name"));
                }
                cl.put(cli);
            }
            study.setCodeList(oid, cl);
        }
        return study;
    }
}
