/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.xml.bodyparsers;

import net.pinnacle21.define.models.*;
import net.pinnacle21.define.models.arm.ResultDisplay;
import net.pinnacle21.define.parsers.xml.XPathEvaluatorFactory;
import net.pinnacle21.define.parsers.xml.bodyparsers.arm.ARMParser;
import org.dom4j.Element;
import org.dom4j.Node;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class DefineV2BodyParser extends BodyParser {
    DefineV2BodyParser(org.dom4j.Document document, boolean forValidation) {
        super(XPathEvaluatorFactory.newInstance().define2XPathEvaluator(document), DefineMetadata.V2, forValidation);
    }

    @Override
    protected List<ResultDisplay> parseResultDisplays(Map<String, WhereClause> whereClauseMap) {
        List<ResultDisplay> displays = new ArrayList<>();
        ARMParser parser = new ARMParser()
                .setWhereClauseMap(whereClauseMap)
                .setStudy(super.study)
                .setXPathEvaluator(this.evaluator);

        List displayElements = evaluator.selectNodes("/ODM/odm:Study/odm:MetaDataVersion/arm:AnalysisResultDisplays/arm:ResultDisplay");

        for (Object obj : displayElements) {
            Element displayElement = (Element) obj;
            displays.add(parser.parseResultDisplay(displayElement));
        }

        return displays;
    }

    Map<String, Comment> parseComments() {
        Map<String, Comment> commentMap = new HashMap<>();
        List<Element> comments = this.evaluator.selectElements("/ODM/odm:Study/odm:MetaDataVersion/def:CommentDef");

        for (Element commentElement : comments) {
            String oid = commentElement.attributeValue("OID", "");
            Comment comment = new Comment().setOid(
                    oid.startsWith(metadata.getPrefixes().comment()) ? oid.substring(4) : oid
            );

            Node description = this.evaluator.selectSingleNode(commentElement,"odm:Description/odm:TranslatedText");
            comment.setDescription(description != null ? description.getText() : null);

            Element documentRef = commentElement.element("DocumentRef");
            if (documentRef != null) {
                String leafID = documentRef.attributeValue("leafID", "");
                comment.setDocumentOid(
                        leafID.startsWith(metadata.getPrefixes().leaf()) ? leafID.substring(3) : leafID);

                Element pageRef = documentRef.element("PDFPageRef");
                comment.setPages(pageRef != null ? pageRef.attributeValue("PageRefs") : null);
                comment.setFirstPage(pageRef != null ? pageRef.attributeValue("FirstPage") : null);
                comment.setLastPage(pageRef != null ? pageRef.attributeValue("LastPage") : null);
            }
            commentMap.put(trim(oid), comment);
        }
        return commentMap;
    }

    List<Element> getMethods() {
        return this.evaluator.selectElements("/ODM/odm:Study/odm:MetaDataVersion/odm:MethodDef");
    }

    public Method parseMethod(Method method, Element methodElement) {
        method.setName(methodElement.attributeValue("Name"));
        method.setType(methodElement.attributeValue("Type"));

        Element description = methodElement.element("Description");
        Element translatedText = description != null ? description.element("TranslatedText") : null;
        if (translatedText != null) {
            method.setDescription(translatedText.getText());
            method.setLanguage(getXmlLang(translatedText));
        }

        Element formalExpression = methodElement.element("FormalExpression");
        if (formalExpression != null) {
            method.setExpressionContext(formalExpression.attributeValue("Context"));
            method.setExpressionCode(formalExpression.getText());
        }

        Element documentRef = methodElement.element("DocumentRef");
        if (documentRef != null) {
            String leafID = documentRef.attributeValue("leafID", "");
            method.setDocumentOid(
                    leafID.startsWith(metadata.getPrefixes().leaf()) ? leafID.substring(3) : leafID);

            Element pageRef = documentRef.element("PDFPageRef");
            method.setPages(pageRef != null ? pageRef.attributeValue("PageRefs") : null);
            method.setFirstPage(pageRef != null ? pageRef.attributeValue("FirstPage") : null);
            method.setLastPage(pageRef != null ? pageRef.attributeValue("LastPage") : null);
        }

        return method;
    }

    @Override
    CodeListItem parseCodeListItem(CodeListItem codeListItem, Element childElement, Node decode) {
        codeListItem.setOrder(childElement.attributeValue("OrderNumber"));

        if (decode != null) {
            codeListItem.setDecodedValue(decode.getText());
            codeListItem.setLanguage(getXmlLang((Element) decode));
        }

        Element itemAlias = this.evaluator.selectSingleElement(childElement, "odm:Alias");
        codeListItem.setCode(itemAlias != null ? itemAlias.attributeValue("Name") : null);

        return codeListItem;
    }

    @Override
    void parseItemGroup(Map<Integer, String> keyMap, ItemGroup itemGroup, Element itemGroupDefElement) {
        String comment = itemGroupDefElement.attributeValue("CommentOID", "");
        itemGroup.setCommentOid(
                comment.startsWith(metadata.getPrefixes().comment()) ? comment.substring(4) : comment);

        Node description = this.evaluator.selectSingleNode(itemGroupDefElement, "odm:Description/odm:TranslatedText");
        itemGroup.setDescription(description != null ? description.getText() : null);
        itemGroup.setKeys(StringUtils.join(keyMap.values(), ','));
    }

    @Override
    String getMethodOID(Element itemDef, Element itemRef) {
        return itemRef.attributeValue("MethodOID", "");
    }

    @Override
    void populateItemAttributes(Item item, Element itemDef, boolean isValueLevel) {
        String commentOID = itemDef.attributeValue("CommentOID", "");
        item.setCommentOid(commentOID.startsWith(
                metadata.getPrefixes().comment()) ? commentOID.substring(4) : commentOID);

        Node description = this.evaluator.selectSingleNode(itemDef, "odm:Description/odm:TranslatedText");
        item.setLabel(description != null ? description.getText() : null);

        Element origin = itemDef.element("Origin");
        if (origin != null) {
            item.setOrigin(origin.attributeValue("Type"));
            if (item.getOrigin().equals("Predecessor")) {
                Node originDescription = evaluator.selectSingleNode(origin, "odm:Description/odm:TranslatedText");
                item.setPredecessor(originDescription != null ? originDescription.getText() : null);
            }
            Element documentRef = origin.element("DocumentRef");
            if (documentRef != null) {
                Element pageRef = documentRef.element("PDFPageRef");
                item.setPages(pageRef != null ? pageRef.attributeValue("PageRefs") : null);
                item.setFirstPage(pageRef != null ? pageRef.attributeValue("FirstPage") : null);
                item.setLastPage(pageRef != null ? pageRef.attributeValue("LastPage") : null);
            }
        }
    }

    @Override
    WhereClause getWhereClause(String itemName, String itemGroupName, Element valueListItemDef,
                               Element itemRef, Map<String, WhereClause> whereClauseMap) {
        Element whereClauseRef = itemRef.element("WhereClauseRef");

        return whereClauseMap.get(whereClauseRef != null
                ? whereClauseRef.attributeValue("WhereClauseOID")
                : null
        );
    }

}
