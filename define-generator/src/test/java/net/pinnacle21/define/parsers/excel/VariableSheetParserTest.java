/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.*;
import net.pinnacle21.define.util.MockNamedRow;
import net.pinnacle21.parsing.excel.ExcelSheetParser;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.RowIdentifier;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import static net.pinnacle21.define.parsers.excel.VariableSheetParser.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class VariableSheetParserTest {

    @Test
    public void nonexistingVariableGetsCreated() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new VariableSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedName = "VAR";
        String expectedDataset = "DATASET";
        String expectedOrder = "1";
        String expectedLabel = "label";
        String expectedDataType = "text";
        String expectedLength = "5";
        String expectedSigDigits = "1";
        String expectedFormat = "formatted";
        String expectedMandatory = "yes";
        String expectedOrigin = "origin";
        String expectedPages = "pages";
        String expectedPredecessor = "preceding";
        String expectedRole = "role";

        study.setItemGroup(expectedDataset, new ItemGroup());

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedName + " ")
                        .with(DATASET_COLUMN, expectedDataset + " ")
                        .with(ORDER_COLUMN, expectedOrder)
                        .with(LABEL_COLUMN, expectedLabel)
                        .with(DATA_TYPE_COLUMN, expectedDataType)
                        .with(LENGTH_COLUMN, expectedLength)
                        .with(SIGNIFICANT_DIGITS_COLUMN, expectedSigDigits)
                        .with(FORMAT_COLUMN, expectedFormat)
                        .with(MANDATORY_COLUMN, expectedMandatory)
                        .with(ORIGIN_COLUMN, expectedOrigin)
                        .with(PAGES_COLUMN, expectedPages)
                        .with(PREDECESSOR_COLUMN, expectedPredecessor)
                        .with(ROLE_COLUMN, expectedRole),
                parsedStudy
        );

        assertTrue(study.hasItem(expectedDataset, expectedName));

        Item item = study.getItem(expectedDataset, expectedName);
        assertNotNull(study.getItemGroup(expectedDataset).getItem(expectedName));
        assertEquals(expectedName, item.getName());
        assertEquals(expectedOrder, item.getOrder());
        assertEquals(expectedLabel, item.getLabel());
        assertEquals(expectedDataType, item.getDataType());
        assertEquals(expectedLength, item.getLength());
        assertEquals(expectedSigDigits, item.getSignificantDigits());
        assertEquals(expectedFormat, item.getFormat());
        assertEquals(expectedMandatory, item.getMandatory());
        assertEquals(expectedOrigin, item.getOrigin());
        assertEquals(expectedPredecessor, item.getPredecessor());
        assertEquals(expectedRole, item.getRole());
    }

    @Test
    public void variableColumnIsRequiredWarning() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new VariableSheetParser();

        String expectedStructure = "DATASET";

        parser.parse(
                new MockNamedRow()
                        .with(DATASET_COLUMN, expectedStructure),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR),
                eq(VARIABLE_COLUMN));
    }

    @Test
    public void structureColumnIsRequiredWarning() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new VariableSheetParser();

        String expectedName = "VAR";

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedName),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR),
                eq(DATASET_COLUMN));
    }

    @Test
    public void keySequenceSetIfVariableIsKeyVariable() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new VariableSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedName = "VAR";
        String expectedStructure = "DATASET";

        ItemGroup itemGroup = new ItemGroup();
        itemGroup.setKeys(expectedName);
        study.setItemGroup(expectedStructure, itemGroup);

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedName)
                        .with(DATASET_COLUMN, expectedStructure),
                parsedStudy
        );

        Item item = study.getItem(expectedStructure, expectedName);

        assertNotNull(item);
        assertEquals(new Integer(1), item.getKeySequence());
    }

    @Test
    public void keySequenceNotSetIfVariableIsntKeyVariable() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new VariableSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedName = "VAR";
        String expectedStructure = "DATASET";

        ItemGroup itemGroup = new ItemGroup();
        itemGroup.setKeys(expectedName + "1");
        study.setItemGroup(expectedStructure, itemGroup);

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedName)
                        .with(DATASET_COLUMN, expectedStructure),
                parsedStudy
        );

        Item item = study.getItem(expectedStructure, expectedName);

        assertNotNull(item);
        assertNull(item.getKeySequence());
    }

    @Test
    public void invalidStructureReferenceErrorRaised() {
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(new Study()));
        ExcelSheetParser<ParsedStudy> parser = new VariableSheetParser();

        String expectedName = "VAR";
        String expectedStructure = "DATASET";

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedName)
                        .with(DATASET_COLUMN, expectedStructure),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(DATASET_REF_ERROR), anyVararg());
    }

    @Test
    public void invalidCommentReferenceErrorRaised() {
        Study study = PowerMockito.mock(Study.class);
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(study));
        ExcelSheetParser<ParsedStudy> parser = new VariableSheetParser();

        String expectedName = "VAR";
        String expectedStructure = "DATASET";
        String expectedComment = "COMMENT";

        when(study.hasItemGroup(expectedStructure)).thenReturn(true);
        when(study.getItemGroup(expectedStructure)).thenReturn(new ItemGroup());

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedName)
                        .with(DATASET_COLUMN, expectedStructure)
                        .with(COMMENT_COLUMN, expectedComment),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(COMMENT_REF_ERROR), anyVararg());
    }

    @Test
    public void validCommentReferenceLinksComment() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new VariableSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedName = "VAR";
        String expectedStructure = "DATASET";
        String expectedComment = "COMMENT";
        Comment comment = new Comment().setOid(expectedComment);

        study.setItemGroup(expectedStructure, new ItemGroup());
        study.setComment(expectedComment, comment);

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedName)
                        .with(DATASET_COLUMN, expectedStructure)
                        .with(COMMENT_COLUMN, expectedComment),
                parsedStudy
        );

        Item item = study.getItem(expectedStructure, expectedName);

        assertNotNull(item);
        assertSame(comment.getOid(), item.getCommentOid());
    }

    @Test
    public void invalidMethodReferenceErrorRaised() {
        Study study = PowerMockito.mock(Study.class);
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(study));
        ExcelSheetParser<ParsedStudy> parser = new VariableSheetParser();

        String expectedName = "VAR";
        String expectedStructure = "DATASET";
        String expectedMethod = "METHOD";

        when(study.hasItemGroup(expectedStructure)).thenReturn(true);
        when(study.getItemGroup(expectedStructure)).thenReturn(new ItemGroup());

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedName)
                        .with(DATASET_COLUMN, expectedStructure)
                        .with(METHOD_COLUMN, expectedMethod),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(METHOD_REF_ERROR), anyVararg());
    }

    @Test
    public void validMethodReferenceLinksMethod() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new VariableSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedName = "VAR";
        String expectedStructure = "DATASET";
        String expectedMethod = "METHOD";
        Method method = new Method().setOid(expectedMethod);

        study.setItemGroup(expectedStructure, new ItemGroup());
        study.setMethod(expectedMethod, method);

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedName)
                        .with(DATASET_COLUMN, expectedStructure)
                        .with(METHOD_COLUMN, expectedMethod),
                parsedStudy
        );

        Item item = study.getItem(expectedStructure, expectedName);

        assertNotNull(item);
        assertSame(method.getOid(), item.getMethodOid());
    }

    @Test
    public void invalidCodelistReferenceErrorRaised() {
        Study study = PowerMockito.mock(Study.class);
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(study));
        ExcelSheetParser<ParsedStudy> parser = new VariableSheetParser();

        String expectedName = "VAR";
        String expectedStructure = "DATASET";
        String expectedCodelist = "fake";

        when(study.hasItemGroup(expectedStructure)).thenReturn(true);
        when(study.getItemGroup(expectedStructure)).thenReturn(new ItemGroup());

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedName)
                        .with(DATASET_COLUMN, expectedStructure)
                        .with(CODELIST_COLUMN, expectedCodelist),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(CODELIST_REF_ERROR), anyVararg());
    }

    @Test
    public void validCodelistReferenceIsAssigned() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new VariableSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedName = "VAR";
        String expectedStructure = "DATASET";
        String expectedCodelist = "CODELIST";

        study.setCodeList(expectedCodelist, new CodeList());
        study.setItemGroup(expectedStructure, new ItemGroup());

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedName)
                        .with(DATASET_COLUMN, expectedStructure)
                        .with(CODELIST_COLUMN, expectedCodelist),
                parsedStudy
        );

        Item item = study.getItem(expectedStructure, expectedName);

        assertNotNull(item);
        assertNotNull(item.getCodeListOid());
        assertEquals(expectedCodelist, item.getCodeListOid());
    }

    @Test
    public void validDictionaryReferenceIsAssigned() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new VariableSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedName = "VAR";
        String expectedStructure = "DATASET";
        String expectedCodelist = "CodeList";

        study.setItemGroup(expectedStructure, new ItemGroup());
        study.setDictionary(expectedCodelist, new Dictionary());

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedName)
                        .with(DATASET_COLUMN, expectedStructure)
                        .with(CODELIST_COLUMN, expectedCodelist),
                parsedStudy
        );

        Item item = study.getItem(expectedStructure, expectedName);

        assertNotNull(item);
        assertEquals(expectedCodelist, item.getDictionaryOid());
    }

    @Test
    public void duplicateErrorRaised() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new VariableSheetParser();
        Study study = PowerMockito.mock(Study.class);

        when(parsedStudy.getStudy()).thenReturn(study);

        String expectedName = "VAR";
        String expectedStructure = "DATASET";

        when(study.hasItem(expectedStructure, expectedName)).thenReturn(true);

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedName)
                        .with(DATASET_COLUMN, expectedStructure),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(DUPLICATE_ERROR), anyVararg());
    }

    @Test
    public void duplicateErrorNotRaisedWithDifferentCasing() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new VariableSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedName = "VAR";
        String expectedStructure = "DATASET";

        Item item = new Item();

        study.setItem(expectedStructure, expectedName.toLowerCase(), item);
        study.setItemGroup(expectedStructure, new ItemGroup());

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedName)
                        .with(DATASET_COLUMN, expectedStructure),
                parsedStudy
        );

        assertEquals(true, study.hasItem(expectedStructure, expectedName));
    }

    @Test
    public void duplicateErrorNotRaisedWithDifferentDataset() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new VariableSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedName = "VAR";
        String expectedStructure = "DATASET";

        Item item = new Item();

        study.setItem(expectedStructure + "1", expectedName, item);
        study.setItemGroup(expectedStructure, new ItemGroup());

        parser.parse(
                new MockNamedRow()
                        .with(VARIABLE_COLUMN, expectedName)
                        .with(DATASET_COLUMN, expectedStructure),
                parsedStudy
        );

        assertEquals(true, study.hasItem(expectedStructure, expectedName));
    }
}
