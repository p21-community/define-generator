/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define;

import net.pinnacle21.define.models.*;
import net.pinnacle21.define.models.Comment;
import net.pinnacle21.define.models.Dictionary;
import net.pinnacle21.define.util.Templates;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.formula.FormulaParseException;
import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;

public class DefineExcelTemplateParser implements StudyParser {
    private static final ThreadLocal<DecimalFormat> DECIMAL_PATTERN = new ThreadLocal<DecimalFormat>() {
        @Override
        protected DecimalFormat initialValue() {
            DecimalFormat format = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));

            format.setMaximumFractionDigits(10);

            return format;
        }
    };

    private final File input;
    private Workbook excelFile;
    private Template template;
    public EventDispatcher dispatcher = new EventDispatcher();

    public DefineExcelTemplateParser(File excelFile) throws IOException, InvalidFormatException {
        this.input = excelFile;
        loadWorkbook();
    }

    private void loadWorkbook() throws IOException, InvalidFormatException {
        if (excelFile != null) {
            return;
        }
        this.excelFile = WorkbookFactory.create(input, null, true);
        this.template = getTemplate(this.excelFile.getSheetName(0));
    }

    private void unloadWorkbook() {
        if (this.excelFile != null) {
            try {
                this.excelFile.close();
            } catch (IOException exception) {
                // swallow any exceptions
            } finally {
                this.excelFile = null;
                this.template = null;
            }
        }
    }

    public static void main(String[] args) {
        try {
            File input = new File(args[0]);
            File output = new File(input.getParentFile(), FilenameUtils.getBaseName(input.getName()) + ".xml");
            try (OutputStream outputStream = new FileOutputStream(output)) {
                DefineExcelTemplateParser parser = new DefineExcelTemplateParser(input);
                DefineGenerator generator = new DefineGenerator(parser.parse(), outputStream);
                generator.generate();
            }
        }catch (Exception e) {
            System.err.println("Uh oh! Something bad happened. This CLI is designed for internal use only and therefore not supported, at all.");
        }
    }

    private static Template getTemplate(String firstTab) {
        switch (firstTab) {
            case "Study":
                return new StudyTemplate();
            case "Standard":
                return new StandardTemplate();
            case "Terminology":
                return new TerminologyTemplate();
        }

        throw new ExcelTemplateFormatException(Collections.singletonList(
                String.format("Unknown template type %s. Please rename to one of the supported types: Study, Standard, or Terminology", firstTab)
        ));
    }

    public Study parse() {
        try {
            loadWorkbook();
            this.verify();
            boolean hasReferencePages = false;
            boolean hasCRF = false;

            boolean isHeader;
            isHeader = true;

            Map<String, String> metadata = new HashMap<>();

            for (Row row : getNonEmptyRows(this.excelFile.getSheetAt(0))) {
                if (!isHeader || (isHeader = false)) {
                    String key = getCellString(row, 0);

                    if (key == null) {
                        continue;
                    }

                    metadata.put(key.toLowerCase(), getCellString(row, 1));
                }
            }

            Set<String> expected = new HashSet<>(this.template.getAttributes());

            expected.removeAll(metadata.keySet());
            expected.remove(null);

            if (!expected.isEmpty()) {
                throw new ExcelTemplateDataException(String.format(
                        "Sheet %s needs to define the following variables: %s",
                        this.excelFile.getSheetName(0), expected.toString()
                ));
            }

            List<String> attributes = this.template.getAttributes();

            Study study = new Study()
                    .setName(metadata.get(attributes.get(0)))
                    .setDescription(metadata.get(attributes.get(1)))
                    .setProtocol(metadata.get(attributes.get(2)))
                    .setStandardName(metadata.get(attributes.get(3)))
                    .setStandardVersion(metadata.get(attributes.get(4)))
                    .setBaseStandardName(metadata.get(attributes.get(5)))
                    .setBaseStandardVersion(metadata.get(attributes.get(6)))
                    .setPublisher(metadata.get(attributes.get(7)))
                    .setLanguage(metadata.get(attributes.get(8)));

            if (this.template.hasSheet("CodeLists")) {
                isHeader = true;
                CodeList codeList = null;

                for (Row row : getNonEmptyRows(this.excelFile.getSheet(this.template.getAlias("CodeLists")))) {
                    if (!isHeader || (isHeader = false)) {
                        String id = getCellString(row, "ID");

                        if (codeList == null || (StringUtils.isNotEmpty(id) && !codeList.getOid().equals(id))) {
                            String code = getCellString(row, "NCI CodeList Code");

                            codeList = new CodeList()
                                    .setOid(id)
                                    .setName(getCellString(row, "Name"))
                                    .setDataType(getCellString(row, "Data Type"))
                                    .setCode(code)
                                    .setIsExtensible(getCellString(row, "Extensible"));


                            codeList.setRow(row.getRowNum());
                            study.setCodeList(codeList.getOid(), codeList);
                        }


                        if (StringUtils.isNotEmpty(getCellString(row, "Term"))) {
                            CodeListItem item = new CodeListItem()
                                    .setCodedValue(getCellString(row, "Term"))
                                    .setDecodedValue(getCellString(row, "Decoded Value"))
                                    .setLanguage(study.getLanguage())
                                    .setCode(getCellString(row, "NCI Term Code"))
                                    .setPreferredTerm(getCellString(row, "NCI Preferred Term"))
                                    .setSynonyms(getCellString(row, "Synonyms"))
                                    .setDefinition(getCellString(row, "Definition"))
                                    .setOrder(getCellString(row, "Order"));

                            item.setRow(row.getRowNum());
                            codeList.put(item);
                        }
                    }
                }
            }

            if (this.template.hasSheet("Dictionaries")) {
                isHeader = true;
                for (Row row : getNonEmptyRows(this.excelFile.getSheet(this.template.getAlias("Dictionaries")))) {
                    if (!isHeader || (isHeader = false)) {

                        Dictionary dictionary = new Dictionary()
                                .setOid(getCellString(row, "ID"))
                                .setName(getCellString(row, "Name"))
                                .setDataType(getCellString(row, "Data Type"))
                                .setDictionary(getCellString(row, "Dictionary"))
                                .setVersion(getCellString(row, "Version"));

                        dictionary.setRow(row.getRowNum());
                        study.setDictionary(dictionary.getOid(), dictionary);
                    }
                }
            }

            if (this.template.hasSheet("Datasets")) {
                isHeader = true;
                for (Row row : getNonEmptyRows(this.excelFile.getSheet(this.template.getAlias("Datasets")))) {
                    if (!isHeader || (isHeader = false)) {
                        ItemGroup itemGroup = new ItemGroup()
                                .setName(getCellString(row, "Dataset"))
                                .setDescription(getCellString(row, "Description"))
                                .setStructure(getCellString(row, "Structure"))
                                .setCategory(getCellString(row, "Class"))
                                .setPurpose(getCellString(row, "Purpose"))
                                .setIsRepeating(getCellString(row, "Repeating"))
                                .setIsReferenceData(getCellString(row, "Reference Data"))
                                .setKeys(getCellString(row, "Key Variables"))
                                .setCommentOid(getCellString(row, "Comment"));

                        itemGroup.setRow(row.getRowNum());
                        study.setItemGroup(itemGroup.getName(), itemGroup);
                    }
                }
            }

            HashSet<String> valueListIDs = new HashSet<>();

            if (this.template.hasSheet("WhereClauses")) {
                isHeader = true;
                for (Row row : getNonEmptyRows(this.excelFile.getSheet(this.template.getAlias("WhereClauses")))) {
                    String oid = getCellString(row, "ID");
                    if ((!isHeader || (isHeader = false)) && StringUtils.isNotBlank(oid)) {
                        WhereClause clause = study.getWhereClause(oid);

                        if (clause == null) {
                            study.setWhereClause(oid, clause = new WhereClause().setOid(oid));
                        }

                        clause.add(new RangeCheck()
                                .setItemOid(getCellString(row, "Variable"))
                                .setItemGroupOid(getCellString(row, "Dataset"))
                                .setComparator(getCellString(row, "Comparator"))
                                .addValue(getCellString(row, "Value")));
                    }
                }
            }

            if (this.template.hasSheet("ValueLevel")) {
                isHeader = true;
                ValueList valueList;
                for (Row row : getNonEmptyRows(this.excelFile.getSheet(this.template.getAlias("ValueLevel")))) {
                    if (!isHeader || (isHeader = false)) {
                        String datasetName = getCellString(row, "Dataset");
                        String variableName = getCellString(row, "Variable");
                        String whereClauseId = StringUtils.defaultIfBlank(getCellString(row, "Where Clause"), "");
                        valueList = study.getValueList(datasetName, variableName);
                        WhereClause whereClause = study.getWhereClause(whereClauseId);
                        String generatedWCID = null;

                        if (whereClause != null) {
                            generatedWCID = Templates.generateWhereClauseOid(whereClause.getRangeChecks());
                        }

                        if (StringUtils.isNotBlank(datasetName) && StringUtils.isNotBlank(variableName) && valueList == null) {
                            valueList = new ValueList(datasetName, variableName);
                            valueList.setRow(row.getRowNum());
                            study.setValueList(datasetName, variableName, valueList);

                            valueListIDs.add(valueList.getOid());
                        }

                        Item item = new Item()
                                .setOrder(getCellString(row, "Order"))
                                .setName(generatedWCID)
                                .setDataType(getCellString(row, "Data Type"))
                                .setLength(getCellString(row, "Length"))
                                .setSignificantDigits(getCellString(row, "Significant Digits"))
                                .setFormat(getCellString(row, "Format"))
                                .setCodeListOid(getCellString(row, "CodeList"))
                                .setOrigin(getCellString(row, "Origin"))
                                .setPages(getCellString(row, "Pages"))
                                .setMethodOid(getCellString(row, "Method"))
                                .setPredecessor(getCellString(row, "Predecessor"))
                                .setMandatory(getCellString(row, "Mandatory"))
                                .setCommentOid(getCellString(row, "Comment"))
                                .setWhereClauseOid(whereClauseId)
                                .setLabel(getCellString(row, "Description"));

                        item.setRow(row.getRowNum());
                        valueList.add(item);

                        String whereClauseComment = getCellString(row, "Join Comment");

                        if (StringUtils.isNotEmpty(whereClauseComment)) {
                            if (whereClause != null) {
                                whereClause.setCommentOid(whereClauseComment);
                            }
                        }
                    }
                }
            }

            if (this.template.hasSheet("Methods")) {
                isHeader = true;
                for (Row row : getNonEmptyRows(this.excelFile.getSheet(this.template.getAlias("Methods")))) {
                    if ((!isHeader || (isHeader = false)) && StringUtils.isNotEmpty(getCellString(row, "ID"))) {
                        Method method = new Method()
                                .setOid(getCellString(row, "ID"))
                                .setName(getCellString(row, "Name"))
                                .setType(getCellString(row, "Type"))
                                .setDescription(getCellString(row, "Description"))
                                .setDocumentOid(getCellString(row, "Document"))
                                .setPages(getCellString(row, "Pages"))
                                .setExpressionContext(getCellString(row, "Expression Context"))
                                .setExpressionCode(getCellString(row, "Expression Code"));

                        method.setRow(row.getRowNum());
                        study.setMethod(method.getOid(), method);
                    }
                }
            }

            if (this.template.hasSheet("Comments")) {
                isHeader = true;
                for (Row row : getNonEmptyRows(this.excelFile.getSheet(this.template.getAlias("Comments")))) {
                    if ((!isHeader || (isHeader = false)) && StringUtils.isNotEmpty(getCellString(row, "ID"))) {
                        Comment comment = new Comment()
                                .setOid(getCellString(row, "ID"))
                                .setDescription(getCellString(row, "Description"))
                                .setDocumentOid(getCellString(row, "Document"))
                                .setPages(getCellString(row, "Pages"));

                        comment.setRow(row.getRowNum());
                        study.setComment(comment.getOid(), comment);
                    }
                }
            }

            if (this.template.hasSheet("Variables")) {
                ItemGroup itemGroup = null;
                isHeader = true;
                for (Row row : getNonEmptyRows(this.excelFile.getSheet(this.template.getAlias("Variables")))) {
                    if (!isHeader || (isHeader = false)) {
                        String domain = StringUtils.defaultIfBlank(getCellString(row, "Dataset"), "");

                        if (itemGroup == null || !domain.equals(itemGroup.getName())) {
                            itemGroup = study.getItemGroup(domain);
                        }

                        String valueListOID = domain + "." + getCellString(row, "Variable");

                        if (itemGroup != null) {
                            Item item = new Item()
                                    .setOrder(getCellString(row, "Order"))
                                    .setName(getCellString(row, "Variable"))
                                    .setLabel(getCellString(row, "Label"))
                                    .setDataType(getCellString(row, "Data Type"))
                                    .setLength(getCellString(row, "Length"))
                                    .setSignificantDigits(getCellString(row, "Significant Digits"))
                                    .setFormat(getCellString(row, "Format"))
                                    .setCodeListOid(getCellString(row, "CodeList"))
                                    .setValueListOid(valueListIDs.contains(valueListOID) ? valueListOID : null)
                                    .setOrigin(getCellString(row, "Origin"))
                                    .setPages(getCellString(row, "Pages"))
                                    .setMethodOid(getCellString(row, "Method"))
                                    .setPredecessor(getCellString(row, "Predecessor"))
                                    .setRole(getCellString(row, "Role"))
                                    .setCore(getCellString(row, "Core"))
                                    .setMandatory(getCellString(row, "Mandatory"))
                                    .setCommentOid(getCellString(row, "Comment"));

                            item.setRow(row.getRowNum());
                            itemGroup.add(item);

                            if (StringUtils.isNotBlank(item.getPages())) {
                                hasReferencePages = true;
                            }

                        } /* else {
                        // TODO: Error condition
                    } */
                    }
                }
            }

            if (this.template.hasSheet("Documents")) {
                isHeader = true;
                List<String> allowableCRFHrefs = Arrays.asList("acrf.pdf", "blankcrf.pdf");
                for (Row row : getNonEmptyRows(this.excelFile.getSheet(this.template.getAlias("Documents")))) {
                    if ((!isHeader || (isHeader = false)) && StringUtils.isNotEmpty(getCellString(row, "ID"))) {
                        Document document = new Document()
                                .setOid(getCellString(row, "ID"))
                                .setTitle(getCellString(row, "Title"))
                                .setHref(getCellString(row, "Href"))
                                .setType(getCellString(row, "Href"));

                        document.setRow(row.getRowNum());
                        study.setDocument(document.getOid(), document);

                        for (String fileName : allowableCRFHrefs) {
                            if (StringUtils.isNotBlank(document.getHref()) && document.getHref().endsWith(fileName)) {
                                hasCRF = true;
                                break;
                            }
                        }
                    }
                }

                if (hasReferencePages && !hasCRF) {
                    throw new ExcelTemplateDataException("You have a variable that references an Annotated CRF but your Documents tab does not have a file named 'acrf.pdf' or 'blankcrf.pdf'." +
                            " Please update your hrefs so that your CRF file is called 'acrf.pdf' or 'blankcrf.pdf'.");
                }
            }

            return study;
        } catch (IOException | InvalidFormatException exception) {
            throw new ExcelTemplateDataException("Failed to open the Excel file.");
        } finally {
            unloadWorkbook();
        }
    }


    private void verify() {
        int tabIndex = 0;
        List<String> errors = new ArrayList<>();

        for (String tab : this.template.getSheets()) {
            Sheet sheet = this.excelFile.getSheetAt(tabIndex);
            String name = sheet.getSheetName();

            if (name.equalsIgnoreCase(tab)) {
                Row row = sheet.getRow(0);

                if (!name.equals(tab)) {
                    this.template.setAlias(tab, name);
                }

                if (row != null) {
                    int lastCellIndex = row.getLastCellNum();
                    List<String> columns = this.template.getColumns(tab);

                    for (int i = 0; i < lastCellIndex && i < columns.size(); ++i) {
                        String header = getCellString(row, i);

                        if (!columns.get(i).equalsIgnoreCase(header)) {
                            errors.add(String.format(
                                "Sheet %s, column %d should be labeled %s but was labeled %s",
                                name, i + 1, columns.get(i), header
                            ));
                        }
                    }

                    row = sheet.getRow(1);

                    if (row == null && tabIndex < 3) {
                        errors.add(String.format(
                            "Sheet %s does not contain any data, which is unexpected",
                            sheet
                        ));
                    }
                } else {
                    errors.add(String.format("Sheet %s lacks column headers", name));
                }
            } else {
                errors.add(String.format(
                    "Sheet #%d should be named %s, but was named %s",
                    tabIndex + 1, tab, name
                ));
            }

            ++tabIndex;
        }

        if (errors.size() > 0) {
            throw new ExcelTemplateFormatException(errors);
        }
    }

    private String getCellString(Row row, String columnAlias) {
        String sheet = this.template.getReverseAlias(row.getSheet().getSheetName());

        return getCellString(row, this.template.getColumnIndex(sheet, columnAlias));
    }

    private String getCellString(Row row, int index) {
        if (index == -1) {
            return null;
        }

        Cell cell = row.getCell(index, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
        CellValue evaluatedCell = null;

        if (cell.getCellType() == Cell.CELL_TYPE_FORMULA) {
            try {
                evaluatedCell = this.excelFile.getCreationHelper().createFormulaEvaluator().evaluate(cell);
            } catch (FormulaParseException e) {
                return "FORMULA ERROR";
            }
        }

        if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC
                || (evaluatedCell != null && evaluatedCell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
            double numericValue = evaluatedCell != null
                ? evaluatedCell.getNumberValue()
                : cell.getNumericCellValue();

            return DECIMAL_PATTERN.get().format(numericValue);
        } else if (cell.getCellType() == Cell.CELL_TYPE_ERROR
                || (evaluatedCell != null && evaluatedCell.getCellType() == Cell.CELL_TYPE_ERROR)) {
            return "ERROR";
        }

        String value = evaluatedCell != null
            ? evaluatedCell.getStringValue()
            : cell.getStringCellValue();

        return StringUtils.defaultIfBlank(value.trim(), null);
    }

    public List<Row> getNonEmptyRows(Sheet sheet) {
        List<Row> nonEmptyRows = new ArrayList<>();
        for (Row row : sheet) {
            boolean isEmpty = true;
            for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
                if (StringUtils.isNotBlank(getCellString(row, i))) {
                    isEmpty = false;
                }
            }
            if (!isEmpty) {
                nonEmptyRows.add(row);
            }
        }
        return nonEmptyRows;
    }

    public void addGeneratorListener(GeneratorListener listener) {
        this.dispatcher.add(listener);
    }

    private interface Template {
        String getReverseAlias(String sheet);
        String getAlias(String sheet);
        void setAlias(String sheet, String alias);
        boolean hasSheet(String sheet);
        boolean hasColumn(String sheet, String column);
        List<String> getAttributes();
        Set<String> getSheets();
        List<String> getColumns(String sheet);
        int getColumnIndex(String sheet, String column);
    }

    private static abstract class ExcelTemplate implements Template {
        private final Map<String, String> aliases = new HashMap<>();
        private final Map<String, String> reverseAliases = new HashMap<>();
        private final Map<String, List<String>> schema;
        private final List<String> attributes;

        protected ExcelTemplate() {
            this.schema = setSchema(new LinkedHashMap<String, List<String>>());
            this.attributes = setAttributes();
        }

        protected abstract List<String> setAttributes();
        protected abstract Map<String, List<String>> setSchema(Map<String, List<String>> schema);

        public boolean hasSheet(String sheet) {
            return this.schema.containsKey(sheet);
        }

        public boolean hasColumn(String sheet, String column) {
            return this.hasSheet(sheet) && this.schema.get(sheet).indexOf(column) != -1;
        }

        public Set<String> getSheets() {
            return Collections.unmodifiableSet(this.schema.keySet());
        }

        public List<String> getColumns(String sheet) {
            if (!this.hasSheet(sheet)) {
                throw new IllegalArgumentException(String.format("Unknown sheet %s", sheet));
            }

            return this.schema.get(sheet);
        }

        public List<String> getAttributes() {
            return Collections.unmodifiableList(this.attributes);
        }

        public int getColumnIndex(String sheet, String column) {
            return this.schema.get(sheet).indexOf(column);
        }

        public void setAlias(String sheet, String alias) {
            this.aliases.put(sheet, alias);
            this.reverseAliases.put(alias, sheet);
        }

        public String getAlias(String sheet) {
            return this.aliases.containsKey(sheet) ? this.aliases.get(sheet) : sheet;
        }

        public String getReverseAlias(String alias) {
            return this.reverseAliases.containsKey(alias) ? this.reverseAliases.get(alias) : alias;
        }
    }

    private static class StudyTemplate extends ExcelTemplate {
        protected final List<String> setAttributes() {
            return Arrays.asList(
                "studyname",
                "studydescription",
                "protocolname",
                "standardname",
                "standardversion",
                null,
                null,
                null,
                "language"
            );
        }

        protected final Map<String, List<String>> setSchema(Map<String, List<String>> schema) {
            schema.put("Study", Arrays.asList(
                "Attribute",
                "Value"
            ));
            schema.put("Datasets", Arrays.asList(
                "Dataset",
                "Description",
                "Class",
                "Structure",
                "Purpose",
                "Key Variables",
                "Repeating",
                "Reference Data",
                "Comment"
            ));
            schema.put("Variables", Arrays.asList(
                "Order",
                "Dataset",
                "Variable",
                "Label",
                "Data Type",
                "Length",
                "Significant Digits",
                "Format",
                "Mandatory",
                "CodeList",
                "Origin",
                "Pages",
                "Method",
                "Predecessor",
                "Role",
                "Comment"
            ));
            schema.put("ValueLevel", Arrays.asList(
                "Order",
                "Dataset",
                "Variable",
                "Where Clause",
                "Description",
                "Data Type",
                "Length",
                "Significant Digits",
                "Format",
                "Mandatory",
                "CodeList",
                "Origin",
                "Pages",
                "Method",
                "Predecessor",
                "Comment",
                "Join Comment"
            ));
            schema.put("WhereClauses", Arrays.asList(
                "ID",
                "Dataset",
                "Variable",
                "Comparator",
                "Value"
            ));
            schema.put("CodeLists", Arrays.asList(
                "ID",
                "Name",
                "NCI CodeList Code",
                "Data Type",
                "Order",
                "Term",
                "NCI Term Code",
                "Decoded Value"
            ));
            schema.put("Dictionaries", Arrays.asList(
                "ID",
                "Name",
                "Data Type",
                "Dictionary",
                "Version"
            ));
            schema.put("Methods", Arrays.asList(
                "ID",
                "Name",
                "Type",
                "Description",
                "Expression Context",
                "Expression Code",
                "Document",
                "Pages"
            ));
            schema.put("Comments", Arrays.asList(
                "ID",
                "Description",
                "Document",
                "Pages"
            ));
            schema.put("Documents", Arrays.asList(
                "ID",
                "Title",
                "Href"
            ));

            return schema;
        }
    }

    private static class StandardTemplate extends ExcelTemplate {
        protected final List<String> setAttributes() {
            return Arrays.asList(
                "standardname",
                "standardlabel",
                "protocolname",
                "standardname",
                "standardversion",
                "parentstandardname",
                "parentstandardversion",
                "publisher",
                "language"
            );
        }

        protected final Map<String, List<String>> setSchema(Map<String, List<String>> schema) {
            schema.put("Standard", Arrays.asList(
                "Attribute",
                "Value"
            ));
            schema.put("Datasets", Arrays.asList(
                "Dataset",
                "Description",
                "Class",
                "Structure",
                "Purpose",
                "Key Variables",
                "Repeating",
                "Reference Data",
                "Comment"
            ));
            schema.put("Variables", Arrays.asList(
                "Order",
                "Dataset",
                "Variable",
                "Label",
                "Data Type",
                "Length",
                "Significant Digits",
                "Format",
                "Core",
                "CodeList",
                "Expected CodeList",
                "Expected CodeList Label",
                "Origin",
                "Method",
                "Predecessor",
                "Role",
                "Comment"
            ));
            schema.put("ValueLevel", Arrays.asList(
                "Order",
                "Dataset",
                "Variable",
                "Where Clause",
                "Data Type",
                "Length",
                "Significant Digits",
                "Format",
                "Mandatory",
                "CodeList",
                "Origin",
                "Method",
                "Predecessor",
                "Value Level Comment",
                "Join Comment"
            ));
            schema.put("WhereClauses", Arrays.asList(
                "ID",
                "Dataset",
                "Variable",
                "Comparator",
                "Value"
            ));
            schema.put("Methods", Arrays.asList(
                "ID",
                "Name",
                "Type",
                "Description",
                "Expression Context",
                "Expression Code",
                "Document"
            ));
            schema.put("Comments", Arrays.asList(
                "ID",
                "Description",
                "Document"
            ));
            schema.put("Documents", Arrays.asList(
                "ID",
                "Title",
                "Href"
            ));

            return schema;
        }
    }

    private static class TerminologyTemplate extends ExcelTemplate {
        protected final List<String> setAttributes() {
            return Arrays.asList(
                "terminologyname",
                "terminologydescription",
                null,
                "terminologyname",
                "version",
                null,
                null,
                "publisher",
                null
            );
        }

        protected final Map<String, List<String>> setSchema(Map<String, List<String>> schema) {
            schema.put("Terminology", Arrays.asList(
                "Attribute",
                "Value"
            ));
            schema.put("CodeLists", Arrays.asList(
                "ID",
                "Name",
                "Extensible",
                "NCI CodeList Code",
                "Order",
                "Term",
                "NCI Term Code",
                "NCI Preferred Term",
                "Decoded Value",
                "Synonyms",
                "Definition"
            ));

            return schema;
        }
    }

}
