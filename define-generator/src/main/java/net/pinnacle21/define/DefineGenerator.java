/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define;

import net.pinnacle21.define.generator.ARMGenerator;
import net.pinnacle21.define.generator.util.GeneratorUtils;
import net.pinnacle21.define.models.Dictionary;
import net.pinnacle21.define.models.*;
import net.pinnacle21.define.models.arm.ResultDisplay;
import net.pinnacle21.parsing.xml.XmlWriter;
import net.sf.saxon.exslt.Date;
import org.apache.commons.lang3.StringUtils;
import org.xml.sax.SAXException;

import javax.xml.transform.TransformerConfigurationException;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.apache.commons.lang3.time.DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT;

public class DefineGenerator {
    private static final String DEFINE_VERSION = "2.0.0";
    private static final String DEFINE_STYLE =
        "type=\"text/xsl\" href=\"define2-0-0.xsl\"";
    private static final String DEFAULT_LANG = "en";
    private static final String NCI_CONTEXT = "nci:ExtCodeID";
    private static DefineMetadata defineMetadata = DefineMetadata.V2;
    private boolean armSupport;

    static class Orders {
        static final Comparator<ItemGroup> SDTM = new Comparator<ItemGroup>() {
            private final Map<String, Integer> classes = new HashMap<>();

            {
                this.classes.put("trial design", 1);
                this.classes.put("special purpose", 2);
                this.classes.put("interventions", 3);
                this.classes.put("events", 4);
                this.classes.put("findings", 5);
                this.classes.put("findings about", 6);
                this.classes.put("relationship", 7);
            }

            public int compare(ItemGroup lhs, ItemGroup rhs) {
                Integer lhsGroup = this.classes.get(lhs.getCategory() != null ? lhs.getCategory().toLowerCase() : null);
                Integer rhsGroup = this.classes.get(rhs.getCategory() != null ? rhs.getCategory().toLowerCase() : null);

                if (lhsGroup == null || rhsGroup == null) {
                    // TODO: Error condition, bad group value
                    if (lhsGroup == null && rhsGroup == null) {
                        return lhs.getName().compareTo(rhs.getName());
                    } else {
                        return lhsGroup == null ? 1 : -1;
                    }
                }

                if (lhsGroup.equals(rhsGroup)) {
                    return lhs.getName().compareTo(rhs.getName());
                } else {
                    return lhsGroup - rhsGroup;
                }
            }
        };
        static final Comparator<ItemGroup> ADaM = new Comparator<ItemGroup>() {
            private final Map<String, Integer> classes = new HashMap<>();

            {
                this.classes.put("subject level analysis dataset", 1);
                this.classes.put("adverse events analysis dataset", 2);
                this.classes.put("basic data structure", 3);
                this.classes.put("adam other", 4);
            }

            public int compare(ItemGroup lhs, ItemGroup rhs) {
                Integer lhsGroup = this.classes.get(lhs.getCategory() != null ? lhs.getCategory().toLowerCase() : null);
                Integer rhsGroup = this.classes.get(rhs.getCategory() != null ? rhs.getCategory().toLowerCase() : null);

                if (lhsGroup == null || rhsGroup == null) {
                    // TODO: Error condition, bad group value
                    if (lhsGroup == null && rhsGroup == null) {
                        return lhs.getName().compareTo(rhs.getName());
                    } else {
                        return lhsGroup == null ? 1 : -1;
                    }
                }

                if (lhsGroup.equals(rhsGroup)) {
                    return lhs.getName().compareTo(rhs.getName());
                } else {
                    return lhsGroup - rhsGroup;
                }
            }
        };
        static final Comparator<ItemGroup> Other = new Comparator<ItemGroup>() {
            public int compare(ItemGroup lhs, ItemGroup rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        };

        static final Comparator<Comment> comments = new Comparator<Comment>() {
            @Override
            public int compare(Comment c1, Comment c2) {
                return c1.getOid().compareTo(c2.getOid());
            }
        };

        static final Comparator<CodeList> codeLists = new Comparator<CodeList>() {
            @Override
            public int compare(CodeList c1, CodeList c2) {
                return c1.getOid().compareTo(c2.getOid());
            }
        };

        static final Comparator<Dictionary> dictionaries = new Comparator<Dictionary>() {
            @Override
            public int compare(Dictionary c1, Dictionary c2) {
                return c1.getOid().compareTo(c2.getOid());
            }
        };
    }

    public static class Metadata {
        private String sourceSystem;
        private String sourceSystemVersion;
        private String originator;

        public Metadata setSource(String system, String version) {
            this.sourceSystem = system;
            this.sourceSystemVersion = version;

            return this;
        }

        public Metadata setOriginator(String originator) {
            this.originator = originator;

            return this;
        }

        public String getSourceSystem() {
            return sourceSystem;
        }

        public String getSourceSystemVersion() {
            return sourceSystemVersion;
        }

        public String getOriginator() {
            return originator;
        }
    }

    private XmlWriter writer;
    private Study study;
    private Metadata metadata;
    private File targetFile;
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm-ss");

    public DefineGenerator(Study study, File target)
            throws TransformerConfigurationException, SAXException, FileNotFoundException {
            this(study, target, null);
    }

    public DefineGenerator(Study study, File target, Metadata metadata)
            throws TransformerConfigurationException, SAXException, FileNotFoundException {
        prepareByFile(study, target, metadata, false);
    }

    public DefineGenerator(Study study, File target, Metadata metadata, boolean armSupport)
            throws TransformerConfigurationException, SAXException, FileNotFoundException {
        prepareByFile(study, target, metadata, armSupport);
    }

    public DefineGenerator(Study study, OutputStream outputStream)
            throws TransformerConfigurationException, SAXException {
        this(study, outputStream, null, false);
    }

    public DefineGenerator(Study study, OutputStream outputStream, Metadata metadata)
            throws TransformerConfigurationException, SAXException {
        this(study, outputStream, metadata, false);

    }

    public DefineGenerator(Study study, OutputStream outputStream, Metadata metadata, boolean armSupport)
            throws TransformerConfigurationException, SAXException {
        prepare(study, outputStream, metadata, armSupport);
    }

    private void prepareByFile(Study study, File target, Metadata metadata, boolean armSupport)
            throws FileNotFoundException, TransformerConfigurationException, SAXException {
        String dateTime = net.sf.saxon.exslt.Date.date() + "T" + Date.time().substring(0, 5).replace(":", "-");
        String studyName = StringUtils.defaultString(study.getName()).replaceAll("/|\\\\|:|\\*|\\?|\"|<|>|\\|", "");
        String fileName = studyName + "-" + dateTime + ".xml";

        if (StringUtils.isBlank(studyName) || fileName.length() > 255) {
            fileName = "define" + "-" + dateTime + ".xml";
        }

        target = new File(target, fileName);
        this.targetFile = target;
        prepare(study, new FileOutputStream(target), metadata, armSupport);
    }

    private void prepare(Study study, OutputStream outputStream, Metadata metadata, boolean armSupport)
            throws SAXException, TransformerConfigurationException {
        this.writer = new XmlWriter(
                outputStream,
                defineMetadata.getNamespaces().odmURI(),
                new XmlWriter.Instruction("xml-stylesheet", DEFINE_STYLE)
        );
        this.writer.addMapping(defineMetadata.getNamespaces().defURI(), defineMetadata.getNamespaces().defPrefix());

        if (StringUtils.defaultString(study.getStandardName()).toUpperCase().contains("ADAM")) {
            this.writer.addMapping(defineMetadata.getNamespaces().armURI(), defineMetadata.getNamespaces().armPrefix());
        }

        this.study = study;
        this.metadata = metadata != null ? metadata : new Metadata();
        this.armSupport = armSupport;
    }

    public String getFilePath() {
        return this.targetFile.getAbsolutePath();
    }

    public void generate() throws IOException, SAXException {
        XmlWriter.SimpleAttributes attributes = this.writer.newAttributes();

        /* Add the <xmlns:nciodm> tag if it is a terminology file */
        if (study.isTerminologyFile()) {
            attributes.addAttribute("xmlns:nciodm", "http://ncicb.nci.nih.gov/xml/odm/EVS/CDISC");
        }

        attributes.addAttribute("ODMVersion", defineMetadata.getNamespaces().odmVersion())
                .addAttribute("FileType", "Snapshot")
                .addAttribute("FileOID", this.study.getOID())
                .addAttribute("CreationDateTime",
                        ISO_DATETIME_TIME_ZONE_FORMAT.format(System.currentTimeMillis())
                );


        if (StringUtils.isNotEmpty(this.metadata.originator)) {
            attributes.addAttribute("Originator", this.metadata.originator);
        }

        if (StringUtils.isNotEmpty(this.metadata.sourceSystem)) {
            attributes
                .addAttribute("SourceSystem", this.metadata.sourceSystem)
                .addAttribute("SourceSystemVersion", this.metadata.sourceSystemVersion);
        }

        this.writer.startElement("ODM", attributes);

        attributes = this.writer.newAttributes()
            .addAttribute("OID", this.study.getOID());
        this.writer.startElement("Study", attributes);

        this.writer.startElement("GlobalVariables");
        this.writer.simpleElement("StudyName", this.study.getName());
        this.writer.simpleElement("StudyDescription", this.study.getDescription());
        this.writer.simpleElement("ProtocolName", this.study.getProtocol());
        this.writer.endElement("GlobalVariables");

        attributes = this.writer.newAttributes()
            .addAttribute("OID", defineMetadata.getPrefixes().metaDataVersion() + this.study.getOID())
            .addAttribute("Name", "Study " + this.study.getName() + " Data Definitions")
            .addAttribute(defineMetadata.getNamespaces().defURI(), "DefineVersion", DEFINE_VERSION)
            .addAttribute(defineMetadata.getNamespaces().defURI(), "StandardName", this.study.getStandardName())
            .addAttribute(defineMetadata.getNamespaces().defURI(), "StandardVersion", this.study.getStandardVersion());
        this.writer.startElement("MetaDataVersion", attributes);

        this.writeSupportingDocumentGroup(this.study.getAnnotatedCrfs(), "AnnotatedCRF");
        this.writeSupportingDocumentGroup(this.study.getSupplementalDocuments(), "SupplementalDoc");

        for (ValueList valueList : this.study.getValueLists()) {
            this.writeValueList(valueList);
        }

        for (WhereClause whereClause : this.study.getWhereClauses()) {
            this.writeWhereClause(whereClause);
        }

        List<ItemGroup> itemGroups = this.study.getItemGroups();
        Comparator<ItemGroup> comparator = Orders.Other;
        String standard = StringUtils.defaultString(this.study.getStandardName()).toUpperCase();

        if (standard.contains("SDTM") || standard.contains("SEND")) {
            comparator = Orders.SDTM;
        } else if (standard.contains("ADAM")) {
            comparator = Orders.ADaM;
        }

        Collections.sort(itemGroups, comparator);

        for (ItemGroup itemGroup : itemGroups) {
            this.writeItemGroupDef(itemGroup);
        }

        for (ItemGroup itemGroup : itemGroups) {
            for (Item item : itemGroup.getItems()) {
                this.writeItemDef(itemGroup.getName(), item, null);
            }
        }

        for (ValueList valueList : this.study.getValueLists()) {
            for (Item item : valueList.getItems()) {
                this.writeItemDef(valueList.getSourceItemGroupName(), item, valueList.getOid().substring(valueList.getOid().lastIndexOf('.') + 1, valueList.getOid().length()));
            }
        }

        List<CodeList> codeLists = this.study.getCodeLists();
        Collections.sort(codeLists, Orders.codeLists);
        for (CodeList codeList : codeLists) {
            this.writeCodeList(codeList);
        }

        List<Dictionary> dictionaries = this.study.getDictionaries();
        Collections.sort(dictionaries, Orders.dictionaries);
        for (Dictionary dictionary : dictionaries) {
            this.writeDictionary(dictionary);
        }

        for (Method method : this.study.getMethods()) {
            this.writeMethod(method);
        }

        List<Comment> comments = this.study.getComments();
        Collections.sort(comments, Orders.comments);

        for (Comment comment : comments) {
            this.writeComment(comment);
        }

        for (Document document : this.study.getAnnotatedCrfs()) {
            this.writeLeaf(document.getOid(), document.getHref(), document.getTitle());
        }

        for (Document document : this.study.getSupplementalDocuments()) {
            this.writeLeaf(document.getOid(), document.getHref(), document.getTitle());
        }

        generateARM();

        this.writer.endElement("MetaDataVersion");
        this.writer.endElement("Study");
        this.writer.endElement("ODM");
        this.writer.close();
    }

    private void generateARM() throws SAXException {
        if (armSupport && this.study.getResultDisplays() == null || this.study.getResultDisplays().isEmpty()) {
            return;
        }

        ARMGenerator generator = new ARMGenerator(this.writer);

        this.writer.startElement(defineMetadata.getNamespaces().armURI(), "AnalysisResultDisplays");

        for (ResultDisplay display : this.study.getResultDisplays()) {
            generator.generateResultDisplay(display);
        }

        this.writer.endElement(defineMetadata.getNamespaces().armURI(), "AnalysisResultDisplays");
    }

    private void writeSupportingDocumentGroup(List<Document> documents, String type) throws SAXException {
        if (documents.isEmpty()) {
            return;
        }

        XmlWriter.SimpleAttributes attributes;

        this.writer.startElement(defineMetadata.getNamespaces().defURI(), type);

        for (Document document : documents) {
            attributes = this.writer.newAttributes()
                .addAttribute("leafID", defineMetadata.getPrefixes().leaf() + document.getOid());
            this.writer.simpleElement(defineMetadata.getNamespaces().defURI(), "DocumentRef", attributes);
        }

        this.writer.endElement(defineMetadata.getNamespaces().defURI(), type);
    }

    private void writeItemGroupDef(ItemGroup itemGroup) throws SAXException {
        XmlWriter.SimpleAttributes attributes;

        attributes = writeItemGroupDefAttributes(itemGroup);

        if (itemGroup.getCommentOid() != null && !itemGroup.getCommentOid().isEmpty()) {
            attributes.addAttribute(defineMetadata.getNamespaces().defURI(), "CommentOID", defineMetadata.getPrefixes().comment() + itemGroup.getCommentOid());
        }

        this.writer.startElement("ItemGroupDef", attributes);
        this.writeDescription(itemGroup.getDescription());

        if (itemGroup.isSplit(this.study.getStandardName().toUpperCase())) {
            this.writeItemGroupDefAlias(itemGroup.getAlias());
        }

        int index = 1;

        for (Item item : itemGroup.getItems()) {
            this.writer.simpleElement("ItemRef", generateItemRefAttributes(item, itemGroup, index++));
        }

        for (SoftReference reference : itemGroup.getSoftReferences()) {
            this.writer.simpleElement(
                    "ItemRef",
                    generateItemRefAttributes(reference.getItem(), reference.getItemGroup(), index++)
            );
        }

        String href = itemGroup.getName().toLowerCase() + ".xpt";

        this.writeLeaf(itemGroup.getName(), href, href);
        this.writer.endElement("ItemGroupDef");
    }

    private XmlWriter.SimpleAttributes generateItemRefAttributes(Item item, ItemGroup itemGroup, int index) {
        XmlWriter.SimpleAttributes attributes = this.writer.newAttributes()
                .addAttribute(
                        "ItemOID",
                        defineMetadata.getPrefixes().item() + itemGroup.getName() + "." + item.getName()
                )
                .addAttribute("OrderNumber", Integer.toString(index))
                .addAttribute("Mandatory", item.getMandatory());

        if (item.getKeySequence() != null) {
            attributes.addAttribute("KeySequence", item.getKeySequence().toString());
        }

        if (StringUtils.isNotEmpty(item.getMethodOid())) {
            attributes.addAttribute("MethodOID", defineMetadata.getPrefixes().method() + item.getMethodOid());
        }

        if (StringUtils.isNotBlank(item.getRole()) && !study.isAdam()) {
            attributes.addAttribute("Role", item.getRole());
        }

        return attributes;
    }

    private void writeItemGroupDefAlias(String description) throws SAXException {
        this.writeAlias("DomainDescription", description);
    }

    /**
     * <p>The Alias element provides a reference to the Domain description in the case where an
     * ItemGroupDef represents a split domain or a reference to a C-Code when a CodeList,
     * CodeListItem or EnumeratedItem corresponds to a CDISC Controlled Terminology.</p>
     * @param context Allowable Values as a child element of a(n):
     *                <ul>
     *                  <li>ItemGroupDef element: <strong>DomainDescription</strong></li>
     *                  <li>CodeList, CodeListItem or EnumeratedItem element: <strong>nci:ExtCodeID</strong></li>
     *                </ul>
     * @param description Allowable Values as a child element of a(n):
     *                    <ul>
     *                      <li>
     *                          ItemGroupDef element representing a split domain,
     *                          when Context=”DomainDescription”: <strong>Description of the parent domain</strong>
     *                      </li>
     *                      <li>
     *                          CodeList element, when Context=”nci:ExtCodeID”:
     *                          <strong>C-Code for corresponding CDISC Controlled Terminology codelist</strong>
     *                      </li>
     *                      <li>
     *                          CodeListItem or EnumeratedItem, when Context=”nci:ExtCodeID”:
     *                          <strong>C-Code for corresponding CDISC Controlled Terminology Term</strong>
     *                      </li>
     *                    </ul>
     */
    private void writeAlias(String context, String description) throws SAXException {
        this.writer.simpleElement(
            "Alias",
            this.writer.newAttributes()
                .addAttribute("Context", context)
                .addAttribute("Name", description)
        );
    }

    protected XmlWriter.SimpleAttributes writeItemGroupDefAttributes(ItemGroup itemGroup) {
        XmlWriter.SimpleAttributes attributes = this.writer.newAttributes();
        String archiveID = defineMetadata.getPrefixes().leaf() + itemGroup.getName();
        attributes.addAttribute("OID", defineMetadata.getPrefixes().itemGroup() + itemGroup.getName());
        String standard = this.study.getStandardName().toUpperCase();

        if (standard.contains("SDTM") || standard.contains("SEND")) { // Domain is not used in ADaM
            attributes.addAttribute("Domain", itemGroup.getDomain());
        }

        attributes.addAttribute("Name", itemGroup.getName())
                .addAttribute("Repeating", itemGroup.getIsRepeating())
                .addAttribute("IsReferenceData", itemGroup.getIsReferenceData())
                .addAttribute("SASDatasetName", itemGroup.getName())
                .addAttribute("Purpose", itemGroup.getPurpose())
                .addAttribute(defineMetadata.getNamespaces().defURI(), "Structure", itemGroup.getStructure())
                .addAttribute(defineMetadata.getNamespaces().defURI(), "Class", StringUtils.defaultIfBlank(itemGroup.getCategory(), "").toUpperCase())
                .addAttribute(defineMetadata.getNamespaces().defURI(), "ArchiveLocationID", archiveID);

        return attributes;
    }

    private void writeItemDef(String prefix, Item item, String SASName)
            throws SAXException {
        XmlWriter.SimpleAttributes attributes;

        attributes = this.writer.newAttributes()
            .addAttribute("OID", defineMetadata.getPrefixes().item() + prefix + "." + item.getName())
            .addAttribute("Name", item.getName())
            .addAttribute("DataType", item.getDataType());

        if (StringUtils.isNotEmpty(item.getLength())) {
            attributes.addAttribute("Length", item.getLength());
        }

        if (StringUtils.isNotBlank(item.getSignificantDigits())) {
            attributes.addAttribute("SignificantDigits", item.getSignificantDigits());
        }

        if (SASName != null) {
            attributes.addAttribute("SASFieldName", SASName);
        } else {
            attributes.addAttribute("SASFieldName", item.getName());
        }

        if (StringUtils.isNotEmpty(item.getFormat())) {
            attributes.addAttribute(defineMetadata.getNamespaces().defURI(), "DisplayFormat", item.getFormat());
        }

        if (StringUtils.isNotEmpty(item.getCommentOid())) {
            attributes.addAttribute(defineMetadata.getNamespaces().defURI(), "CommentOID", defineMetadata.getPrefixes().comment() + item.getCommentOid());
        }

        this.writer.startElement("ItemDef", attributes);

        if (StringUtils.isNotBlank(item.getLabel())) {
            this.writeDescription(item.getLabel());
        }

        if (StringUtils.isNotEmpty(item.getCodeListOid())) {
            attributes = this.writer.newAttributes()
                .addAttribute("CodeListOID", defineMetadata.getPrefixes().codelist() + item.getCodeListOid());

            this.writer.simpleElement("CodeListRef", attributes);
        } else if (StringUtils.isNotEmpty(item.getDictionaryOid())) {
            attributes = this.writer.newAttributes()
                    .addAttribute("CodeListOID", defineMetadata.getPrefixes().codelist() + item.getDictionaryOid());

            this.writer.simpleElement("CodeListRef", attributes);
        }

        String origin = StringUtils.defaultString(item.getOrigin()).toUpperCase();

        if (origin.equalsIgnoreCase("CRF")) {
            attributes = this.writer.newAttributes()
                .addAttribute("Type", "CRF");

            for (Document crf : this.study.getAnnotatedCrfs()) {
                if (Arrays.asList("blankcrf.pdf", "acrf.pdf").contains(crf.getHref())) {
                    origin = crf.getHref().equals("blankcrf.pdf") ? "blankcrf" : "acrf";
                    break;
                }
            }

            if (origin.equalsIgnoreCase("CRF")) {
                this.writer.simpleElement(defineMetadata.getNamespaces().defURI(), "Origin", attributes);
            } else {
                this.writer.startElement(defineMetadata.getNamespaces().defURI(), "Origin", attributes);
                GeneratorUtils.writeDocumentRef(this.writer, defineMetadata, origin, item.getPages());
                this.writer.endElement(defineMetadata.getNamespaces().defURI(), "Origin");
            }
        } else if (origin.toUpperCase().equals("PREDECESSOR")) {
            attributes = this.writer.newAttributes()
                .addAttribute("Type", item.getOrigin());
            this.writer.startElement(defineMetadata.getNamespaces().defURI(), "Origin", attributes);
            this.writeDescription(item.getPredecessor());
            this.writer.endElement(defineMetadata.getNamespaces().defURI(), "Origin");
        } else if (StringUtils.isNotEmpty(item.getOrigin())) {
            attributes = this.writer.newAttributes()
                .addAttribute("Type", item.getOrigin());
            this.writer.simpleElement(defineMetadata.getNamespaces().defURI(), "Origin", attributes);
        }

        if (StringUtils.isNotEmpty(item.getValueListOid())) {
            attributes = this.writer.newAttributes()
                .addAttribute("ValueListOID", defineMetadata.getPrefixes().valuelist() + item.getValueListOid());

            this.writer.simpleElement(defineMetadata.getNamespaces().defURI(), "ValueListRef", attributes);
        }

        this.writer.endElement("ItemDef");
    }

    private void writeValueList(ValueList valueList) throws SAXException {
        XmlWriter.SimpleAttributes attributes;

        attributes = this.writer.newAttributes()
            .addAttribute("OID", defineMetadata.getPrefixes().valuelist() + valueList.getOid());
        this.writer.startElement(defineMetadata.getNamespaces().defURI(), "ValueListDef", attributes);

        int index = 1;

        for (Item item : valueList.getItems()) {
            attributes = this.writer.newAttributes()
                .addAttribute("ItemOID", defineMetadata.getPrefixes().item() + valueList.getSourceItemGroupName() + "." + item.getName())
                .addAttribute("OrderNumber", Integer.toString(index++))
                .addAttribute("Mandatory", item.getMandatory());

            if (StringUtils.isNotEmpty(item.getMethodOid())) {
                attributes.addAttribute("MethodOID", defineMetadata.getPrefixes().method() + item.getMethodOid());
            }

            this.writer.startElement("ItemRef", attributes);

            if (StringUtils.isNotEmpty(item.getWhereClauseOid()) && this.study.getWhereClause(item.getWhereClauseOid()) != null) {
                attributes = this.writer.newAttributes()
                    .addAttribute("WhereClauseOID", defineMetadata.getPrefixes().whereClause() + this.study.getWhereClause(item.getWhereClauseOid()).getOid());
                this.writer.simpleElement(defineMetadata.getNamespaces().defURI(), "WhereClauseRef", attributes);
            }

            this.writer.endElement("ItemRef");
        }

        this.writer.endElement("ValueListDef");
    }

    private void writeWhereClause(WhereClause whereClause) throws SAXException {
        XmlWriter.SimpleAttributes attributes;

        attributes = this.writer.newAttributes()
            .addAttribute("OID", defineMetadata.getPrefixes().whereClause() + whereClause.getOid());

        if (StringUtils.isNotEmpty(whereClause.getCommentOid())) {
            attributes.addAttribute(defineMetadata.getNamespaces().defURI(), "CommentOID", defineMetadata.getPrefixes().comment() + whereClause.getCommentOid());
        }


        this.writer.startElement(defineMetadata.getNamespaces().defURI(), "WhereClauseDef", attributes);

        for (RangeCheck check : whereClause.getRangeChecks()) {
            attributes = this.writer.newAttributes()
                .addAttribute("SoftHard", "Soft")
                .addAttribute(defineMetadata.getNamespaces().defURI(), "ItemOID", defineMetadata.getPrefixes().item() + check.getItemGroupOid() + "." + check.getItemOid())
                .addAttribute("Comparator", check.getComparator());

            this.writer.startElement("RangeCheck", attributes);

            for (String value : check.getValues()) {
                this.writer.simpleElement("CheckValue", value);
            }

            this.writer.endElement("RangeCheck");
        }

        this.writer.endElement("WhereClauseDef");
    }

    private void writeMethod(Method method) throws SAXException {
        XmlWriter.SimpleAttributes attributes;

        attributes = this.writer.newAttributes()
            .addAttribute("OID", defineMetadata.getPrefixes().method() + method.getOid())
            .addAttribute("Name", method.getName())
            .addAttribute("Type", method.getType());

        this.writer.startElement("MethodDef", attributes);
        this.writeDescription(method.getDescription());
        GeneratorUtils.writeDocumentRef(this.writer, defineMetadata, method.getDocumentOid(), method.getPages());

        if (StringUtils.isNotEmpty(method.getExpressionContext())) {
            attributes = this.writer.newAttributes()
                .addAttribute("Context", method.getExpressionContext());

            this.writer.simpleElement("FormalExpression", attributes, method.getExpressionCode());
        }

        this.writer.endElement("MethodDef");
    }

    private void writeCodeList(CodeList codeList) throws SAXException {

        XmlWriter.SimpleAttributes attributes = this.writer.newAttributes()
                .addAttribute("OID", defineMetadata.getPrefixes().codelist() + codeList.getOid())
                .addAttribute("Name", codeList.getName())
                .addAttribute("DataType", codeList.getDataType());

        /*
         * This is for writing the CodeListExtensible tag.
         * Its only used for converting terminology files and is not needed in a define.xml.
         */
        if (study.isTerminologyFile()) {
            attributes.addAttribute("nciodm:CodeListExtensible", codeList.isExtensible() ? "Yes" : "No");
        }

        this.writer.startElement("CodeList", attributes);

        boolean hasDecodeVal = false;
        for (CodeListItem item : codeList.getCodelistItems()) {
            if (StringUtils.isNotEmpty(item.getDecodedValue())) {
                hasDecodeVal = true;
                break;
            }
        }



        for (CodeListItem item : codeList.getCodelistItems()) {
            attributes = this.writer.newAttributes()
                .addAttribute("CodedValue", item.getCodedValue());

            if (item.getOrder() != null && !item.getOrder().isEmpty()) {
                attributes.addAttribute("OrderNumber", item.getOrder());
            }

            if (hasDecodeVal) {
                if (StringUtils.isEmpty(item.getCode()) && StringUtils.isNotEmpty(codeList.getCode())) {
                    attributes.addAttribute(defineMetadata.getNamespaces().defURI(), "ExtendedValue", "Yes");
                }

                this.writer.startElement("CodeListItem", attributes);
                this.writeTranslatedText("Decode", item.getDecodedValue(), study.getLanguage());

                if (StringUtils.isNotEmpty(item.getCode())) {
                    attributes = this.writer.newAttributes()
                        .addAttribute("Name", item.getCode())
                        .addAttribute("Context", NCI_CONTEXT);

                    this.writer.simpleElement("Alias", attributes);
                }

                this.writer.endElement("CodeListItem");
            } else {
                if (StringUtils.isEmpty(item.getCode()) && StringUtils.isNotEmpty(codeList.getCode())) {
                    attributes.addAttribute(defineMetadata.getNamespaces().defURI(), "ExtendedValue", "Yes");
                }

                if (StringUtils.isNotEmpty(item.getCode())) {
                    this.writer.startElement("EnumeratedItem", attributes);

                    attributes = this.writer.newAttributes()
                        .addAttribute("Name", item.getCode())
                        .addAttribute("Context", NCI_CONTEXT);

                    this.writer.simpleElement("Alias", attributes);
                    this.writer.endElement("EnumeratedItem");
                } else {
                    this.writer.simpleElement("EnumeratedItem", attributes);
                }
            }
        }

        if (StringUtils.isNotEmpty(codeList.getCode())) {
            attributes = this.writer.newAttributes()
                .addAttribute("Name", codeList.getCode())
                .addAttribute("Context", NCI_CONTEXT);

            this.writer.simpleElement("Alias", attributes);
        }

        this.writer.endElement("CodeList");
    }

    private void writeDictionary(Dictionary dictionary) throws SAXException {
        this.writer.startElement("CodeList", this.writer.newAttributes()
                .addAttribute("OID", defineMetadata.getPrefixes().codelist() + dictionary.getOid())
                .addAttribute("Name", dictionary.getName())
                .addAttribute("DataType", dictionary.getDataType()));

        this.writer.simpleElement("ExternalCodeList", this.writer.newAttributes()
                .addAttribute("Dictionary", dictionary.getDictionary())
                .addAttribute("Version", dictionary.getVersion()));

        this.writer.endElement("CodeList");
    }

    private void writeComment(Comment comment) throws SAXException {
        XmlWriter.SimpleAttributes attributes;

        attributes = this.writer.newAttributes()
            .addAttribute("OID", defineMetadata.getPrefixes().comment() + comment.getOid());

        this.writer.startElement(defineMetadata.getNamespaces().defURI(), "CommentDef", attributes);
        this.writeDescription(comment.getDescription());
        GeneratorUtils.writeDocumentRef(this.writer, defineMetadata, comment.getDocumentOid(), comment.getPages());
        this.writer.endElement(defineMetadata.getNamespaces().defURI(), "CommentDef");
    }

    private void writeLeaf(String id, String href, String title) throws SAXException {
        XmlWriter.SimpleAttributes attributes;

        attributes = this.writer.newAttributes()
            .addAttribute("ID", defineMetadata.getPrefixes().leaf() + id)
            .addAttribute(XmlWriter.XLINK_NAMESPACE_URI, "href", href);

        this.writer.startElement(defineMetadata.getNamespaces().defURI(), "leaf", attributes);
        this.writer.simpleElement(defineMetadata.getNamespaces().defURI(), "title", title);
        this.writer.endElement(defineMetadata.getNamespaces().defURI(), "leaf");
    }

    private void writeDescription(String text) throws SAXException {
        this.writeTranslatedText("Description", text, DEFAULT_LANG);
    }

    private void writeTranslatedText(String wrapper, String text, String lang) throws SAXException {
        XmlWriter.SimpleAttributes attributes;

        this.writer.startElement(wrapper);

        attributes = this.writer.newAttributes()
            .addAttribute(XmlWriter.XML_NAMESPACE_URI, "lang", lang);

        this.writer.startElement("TranslatedText", attributes);
        this.writer.characters(text);
        this.writer.endElement("TranslatedText");
        this.writer.endElement(wrapper);
    }
}
