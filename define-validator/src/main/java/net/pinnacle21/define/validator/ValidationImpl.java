/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator;

import com.google.common.collect.ImmutableMap;
import net.pinnacle21.define.models.DefineMetadata;
import net.pinnacle21.define.validator.content.ContentValidator;
import net.pinnacle21.define.validator.report.MessageTransformer;
import net.pinnacle21.define.validator.report.WritableRuleMetrics;
import net.pinnacle21.define.validator.report.WritableRuleMetricsImpl;
import net.pinnacle21.define.validator.resources.ResourceManager;
import net.pinnacle21.define.validator.resources.RuleSet;
import net.pinnacle21.define.validator.schema.SchemaResult;
import net.pinnacle21.define.validator.schema.SchemaValidator;
import net.pinnacle21.validator.api.BaseValidation;
import net.pinnacle21.validator.api.events.util.Dispatcher;
import net.pinnacle21.validator.api.model.*;
import net.pinnacle21.validator.data.InternalEntityDetails;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.function.Predicate;

public class ValidationImpl extends BaseValidation<ValidationImpl> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationImpl.class);
    private static final Dispatcher DISPATCHER = new Dispatcher(c -> LoggerFactory.getLogger(c)::error);

    private final SourceOptions source;
    private final ConfigOptions config;
    private final ValidationOptions options;
    private final ExecutorService sharedExecutor;

    ValidationImpl(SourceOptions source, ConfigOptions config, ValidationOptions options,
            ExecutorService sharedExecutor) {
        this.source = source;
        this.config = config;
        this.options = options != null
                ? options
                : ValidationOptions.builder().build();
        this.sharedExecutor = sharedExecutor;
    }

    @Override
    public CompletionStage<ValidationResult> executeAsync(CancellationToken cancellationToken) {
        SourceDetails sourceDetails = new InternalEntityDetails(SourceDetails.Reference.Data, ImmutableMap.of(
                SourceDetails.Property.Name, "DEFINE",
                SourceDetails.Property.Location, this.source.getSource(),
                SourceDetails.Property.Label, "Define.xml"
        ), null);
        ExecutorService executor = this.sharedExecutor != null
                ? this.sharedExecutor
                : ForkJoinPool.commonPool();
        WritableRuleMetrics metrics = new WritableRuleMetricsImpl();
        ValidationResult.Builder resultBuilder = ValidationResult.builder()
                .withMetrics(metrics);

        return CompletableFuture.supplyAsync(() -> {
            File define = new File(this.source.getSource());

            if (!define.isFile()) {
                LOGGER.error("Define.xml file {} unexpectedly does not exist, aborting validation", define);

                return false;
            }

            RuleSet ruleSet = RuleSet.parseConfig(this.config, this.ruleListeners);
            MessageTransformer transformer = null;

            // dom4j does some really stupid shit to load DocumentFactory as a singleton, so we need to set
            // the context classloader to prevent cast exceptions ¯\_(ツ)_/¯
            ClassLoader original = Thread.currentThread().getContextClassLoader();

            Thread.currentThread().setContextClassLoader(ValidationImpl.class.getClassLoader());

            try (ResourceManager resourceManager = new ResourceManager(define, this.config, this.options)) {
                DefineMetadata metadata;

                try (
                    InputStream is = new FileInputStream(define);
                    InputStream bs = new BufferedInputStream(is)
                ) {
                    metadata = DefineMetadata.parseVersion(bs);
                }

                transformer = new MessageTransformer(sourceDetails, ruleSet, resourceManager.getLineReader());
                SchemaValidator schemaValidator = new SchemaValidator(metadata, resourceManager, transformer);
                SchemaResult schemaResult = schemaValidator.validate();
                if (!schemaResult.isSuccessful()) {
                    return false;
                }

                ContentValidator contentValidator = new ContentValidator(metadata, resourceManager, transformer);
                return contentValidator.validate(schemaResult.getDocument());
            } catch (IOException ex) {
                LOGGER.error("An IOException interrupted processing the define.xml {}", define, ex);

                return false;
            } finally {
                List<Diagnostic> diagnostics = null;

                if (transformer != null) {
                    diagnostics = transformer.getDiagnostics();

                    diagnostics.stream()
                            // Collect metrics as we're going through so we don't have to iterate twice
                            .filter(d -> {
                                metrics.recordFailure(d);

                                return true;
                            })
                            // Get distinct issues
                            .filter(distinctBy(d -> Pair.of(d.getId(), d.getContext())))
                            .forEach(d -> DISPATCHER.dispatchTo(this.ruleListeners, l -> l::acceptInstance, ruleSet.toInstance(d)));
                }

                DISPATCHER.dispatchTo(this.ruleListeners, d -> d::complete);

                if (diagnostics != null) {
                    DISPATCHER.dispatchTo(this.diagnosticListeners, d -> d, diagnostics);
                }

                // Reset the classloader
                Thread.currentThread().setContextClassLoader(original);
            }
        }, executor).handle((wasSuccessful, throwable) -> resultBuilder
                .withCompletedSuccessfully(wasSuccessful != null && wasSuccessful)
                .withSources(Collections.singleton(sourceDetails))
                .withCause(throwable)
                .build()
        );
    }

    @Override
    protected final ValidationImpl recreate() {
        return new ValidationImpl(this.source, this.config, this.options, this.sharedExecutor);
    }

    private static <T, U> Predicate<T> distinctBy(Function<T, U> criteria) {
        Set<U> found = ConcurrentHashMap.newKeySet();

        return i -> found.add(criteria.apply(i));
    }
}
