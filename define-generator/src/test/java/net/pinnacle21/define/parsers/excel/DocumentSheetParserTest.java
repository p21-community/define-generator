/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.Document;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.util.MockNamedRow;
import net.pinnacle21.parsing.excel.ExcelSheetParser;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.RowIdentifier;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static net.pinnacle21.define.parsers.excel.DocumentSheetParser.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyVararg;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ParsedStudy.class)
public class DocumentSheetParserTest {
    @Test
    public void nonexistingDocumentGetsCreated() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new DocumentSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedID = "DOC";
        String expectedTitle = "title";
        String expectedHref = "href";

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID + " ")
                        .with(TITLE_COLUMN, expectedTitle)
                        .with(HREF_COLUMN, expectedHref),
                parsedStudy
        );

        assertTrue(study.hasDocument(expectedID));

        Document document = study.getDocument(expectedID);

        assertNotNull(document);
        assertEquals(expectedID, document.getOid());
        assertEquals(expectedTitle, document.getTitle());
        assertEquals(expectedHref, document.getHref());
    }

    @Test
    public void idColumnIsRequiredWarning() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new DocumentSheetParser();

        parser.parse(
                new MockNamedRow(),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR), eq(ID_COLUMN));
    }

    @Test
    public void duplicateErrorRaised() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new DocumentSheetParser();
        Study study = mock(Study.class);

        when(parsedStudy.getStudy()).thenReturn(study);

        String expectedID = "DOC";

        when(study.hasDocument(expectedID)).thenReturn(true);

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(DUPLICATE_ERROR), anyVararg());
    }

    @Test
    public void duplicateErrorNotRaisedWithDifferentCasing() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new DocumentSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedID = "DOC";

        study.setDocument(expectedID.toLowerCase(), new Document());

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID),
                parsedStudy
        );

        assertEquals(true, study.hasDocument(expectedID));
    }
}
