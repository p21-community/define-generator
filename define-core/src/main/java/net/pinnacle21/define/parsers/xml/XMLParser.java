/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.xml;

import net.pinnacle21.define.GeneratorListener;
import net.pinnacle21.define.StudyParser;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.parsers.xml.bodyparsers.BodyParser;
import net.pinnacle21.define.parsers.xml.bodyparsers.BodyParserFactory;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.xml.sax.InputSource;

import java.io.File;


public class XMLParser implements StudyParser {

    private BodyParser parser;

    public XMLParser(File file) throws DocumentException {
        Document document = new SAXReader().read(file);
        this.parser = BodyParserFactory.newBodyParser(document, false);
    }

    public XMLParser(Document document) throws DocumentException {
        this.parser = BodyParserFactory.newBodyParser(document, false);
    }

    public XMLParser(InputSource inputSource) throws DocumentException {
        Document document = new SAXReader().read(inputSource);
        this.parser = BodyParserFactory.newBodyParser(document, false);
    }

    public XMLParser(Document document, boolean forValidation) throws DocumentException {
        this.parser = BodyParserFactory.newBodyParser(document, forValidation);
    }

    public Study parse(boolean armSupport) {
        return this.parser.parse(armSupport);
    }

    @Override
    public Study parse() {
        return this.parser.parse();
    }

    @Override
    public void addGeneratorListener(GeneratorListener listener) {
        this.parser.addGeneratorListener(listener);
    }
}
