/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define;

import net.pinnacle21.define.models.RangeCheck;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RangeCheckTest {

    @Test
    public void testConstructorINComparator() {
        RangeCheck rangeCheck = new RangeCheck()
                .setItemOid("Var")
                .setItemGroupOid("Structure")
                .setComparator("IN")
                .addValue("Val1, Val2, Val3");
        assertEquals(3, rangeCheck.getValues().size());
    }

    @Test
    public void testConstructorNOTINComparator() {
        RangeCheck rangeCheck = new RangeCheck()
                .setItemOid("Var")
                .setItemGroupOid("Structure")
                .setComparator("NOTIN")
                .addValue("Val1, Val2, Val3");
        assertEquals(3, rangeCheck.getValues().size());
    }

    @Test
    public void testConstructorEQComparator() {
        RangeCheck rangeCheck = new RangeCheck()
                .setItemOid("Var")
                .setItemGroupOid("Structure")
                .setComparator("EQ")
                .addValue("Val1, Val2, Val3");
        assertEquals(1, rangeCheck.getValues().size());
    }
}
