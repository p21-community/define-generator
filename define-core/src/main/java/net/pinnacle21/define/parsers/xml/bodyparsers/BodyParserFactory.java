/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.xml.bodyparsers;

import net.pinnacle21.define.models.DefineMetadata;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.Namespace;

public class BodyParserFactory {
    /**
     * Default to Define 2.0.0 Parser if a namespace could not be detected,
     * otherwise return the specified Parser if it matches v 1.0.0 or 2.0.0.
     *
     * @param document the document as parsed by SAX
     * @return the BodyParser that matches the def namespace inside the Define.xml otherwise default to V2 body parser.
     */
    public static BodyParser newBodyParser(Document document, boolean forValidation) {
        Element odm = document.getRootElement();

        if (odm == null || !"ODM".equalsIgnoreCase(odm.getName())) {
            // TODO: In this case this isn't even something remotely like a define.xml, should we even continue?
            return new DefineV2BodyParser(document, forValidation);
        }

        Namespace defineNamespace;

        defineNamespace = odm.getNamespaceForURI(DefineMetadata.V1.getNamespaces().defURI());

        if (defineNamespace != null) {
            return new DefineV1BodyParser(document, forValidation);
        }

        // TODO: Should we be falling back to look for clues about the version here if the namespace is missing?
        return new DefineV2BodyParser(document, forValidation);
    }
}
