/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define;

import java.util.LinkedHashSet;
import java.util.Set;

public class EventDispatcher {
    private final Set<GeneratorListener> listeners =
        new LinkedHashSet<GeneratorListener>();

    public synchronized void add(GeneratorListener listener) {
        this.listeners.add(listener);
    }

    public synchronized void clear() {
        this.listeners.clear();
    }

    public void fireTaskStart(String taskName, long currentTask, long totalTasks) {
        this.dispatchUpdated(new GeneratorEvent(GeneratorEvent.State.Processing, currentTask,
                totalTasks, taskName));
    }

    public void fireTaskIncrement(String taskName, long currentTask, long totalTasks) {
        GeneratorEvent event = new GeneratorEvent(GeneratorEvent.State.Processing, currentTask,
                totalTasks, taskName);

        this.dispatchUpdated(event);
    }

    public void fireTaskCompleted(String taskName, long currentTask, long totalTasks) {
        this.dispatchUpdated(new GeneratorEvent(GeneratorEvent.State.Completed, currentTask, totalTasks,
                taskName));
    }


    public void fireGenerating(String resultPath) {
        this.dispatchUpdated(new GeneratorEvent(GeneratorEvent.State.Generating, resultPath));
    }

    public void fireStarted() {
        this.dispatchStarted(new GeneratorEvent());
    }

    public void fireStopped() {
        this.dispatchStopped(new GeneratorEvent());
    }

    public synchronized boolean remove(GeneratorListener listener) {
        return this.listeners.remove(listener);
    }

    private synchronized Set<GeneratorListener> copyListeners() {
        return new LinkedHashSet<>(this.listeners);
    }

    private void dispatchUpdated(GeneratorEvent event) {
        Set<GeneratorListener> listeners = this.copyListeners();

        for (GeneratorListener listener : listeners) {
            synchronized (listener) {
                listener.generatorProgressUpdated(event);
            }
        }
    }

    private void dispatchStarted(GeneratorEvent event) {
        Set<GeneratorListener> listeners = this.copyListeners();

        for (GeneratorListener listener : listeners) {
            synchronized (listener) {
                listener.generatorStarted(event);
            }
        }
    }

    private void dispatchStopped(GeneratorEvent event) {
        Set<GeneratorListener> listeners = this.copyListeners();

        for (GeneratorListener listener : listeners) {
            synchronized (listener) {
                listener.generatorStopped(event);
            }
        }
    }
}
