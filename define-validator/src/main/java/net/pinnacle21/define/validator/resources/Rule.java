/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.resources;

import net.pinnacle21.define.validator.report.DiagnosticImpl;
import net.pinnacle21.validator.api.model.*;

import java.util.Map;

public class Rule implements RuleTemplate {
    private final String id;
    private final String message;
    private final String description;
    private final String category;
    private final Diagnostic.Type type;
    private final String publisherId;

    Rule(String id, String category, String message, String description, Diagnostic.Type type, String publisherId) {
        this.id = id;
        this.category = category;
        this.message = message;
        this.description = description;
        this.type = type;
        this.publisherId = publisherId;
    }

    public String getId() {
        return id;
    }

    public String getMessage() {
        return this.message;
    }

    public String getDescription() {
        return description;
    }

    public String getCategory() {
        return category;
    }

    public Diagnostic.Type getType() {
        return type;
    }

    @Override
    public String getPublisherId() {
        return publisherId;
    }

    public Diagnostic toDiagnostic(SourceDetails sourceDetails, DataDetails dataDetails, String message,
            Map<String, String> details) {
        return new DiagnosticImpl(this.id, this.category, message, this.description, this.type, sourceDetails,
                dataDetails, details);
    }
}
