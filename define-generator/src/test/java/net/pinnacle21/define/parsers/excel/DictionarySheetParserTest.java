/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.Dictionary;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.util.MockNamedRow;
import net.pinnacle21.parsing.excel.ExcelSheetParser;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.RowIdentifier;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

import static net.pinnacle21.define.parsers.excel.DictionarySheetParser.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyVararg;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class DictionarySheetParserTest {

    @Test
    public void nonexistingDictionaryGetsCreated() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new DictionarySheetParser();
        Study study = parsedStudy.getStudy();

        String expectedId = "Id";
        String expectedName = "Name";
        String expectedDataType = "DataType";
        String expectedDictionary = "Dictionary";
        String expectedVersion = "Version";

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedId + " ")
                        .with(NAME_COLUMN, expectedName)
                        .with(DATA_TYPE_COLUMN, expectedDataType)
                        .with(DICTIONARY_COLUMN, expectedDictionary)
                        .with(VERSION_COLUMN, expectedVersion),
                parsedStudy
        );

        assertTrue(study.hasDictionary(expectedId));

        Dictionary dictionary = study.getDictionary(expectedId);

        assertEquals(expectedId, dictionary.getOid());
        assertEquals(expectedName, dictionary.getName());
        assertEquals(expectedDataType, dictionary.getDataType());
        assertEquals(expectedDictionary, dictionary.getDictionary());
        assertEquals(expectedVersion, dictionary.getVersion());
    }

    @Test
    public void idColumnIsRequiredWarning() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new DictionarySheetParser();

        when(parsedStudy.getStudy()).thenReturn(new Study());

        parser.parse(
                new MockNamedRow(),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR), eq(ID_COLUMN));
    }

    @Test
    public void duplicateErrorRaised() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new DictionarySheetParser();
        Study study = mock(Study.class);

        when(parsedStudy.getStudy()).thenReturn(study);

        String expectedID = "DICTIONARY";

        when(study.hasDictionary(expectedID)).thenReturn(true);

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(DUPLICATE_ERROR), anyVararg());
    }

    @Test
    public void duplicateErrorNotRaisedWithDifferentCasing() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new DictionarySheetParser();
        Study study = parsedStudy.getStudy();

        String expectedID = "DICTIONARY";

        study.setDictionary(expectedID.toLowerCase(), new Dictionary());

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID),
                parsedStudy
        );

        assertEquals(true, study.hasDictionary(expectedID));
    }
}
