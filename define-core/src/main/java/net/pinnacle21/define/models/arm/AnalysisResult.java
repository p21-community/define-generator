/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models.arm;

import net.pinnacle21.define.models.Description;

public class AnalysisResult {
    private String oid;
    private String parameterOid;
    private String paramcdDataset;
    private String analysisReason;
    private String analysisPurpose;
    private Description description;
    private AnalysisDatasets analysisDatasets;
    private Documentation documentation;
    private ProgrammingCode code;

    public String getOid() {
        return oid;
    }

    public AnalysisResult setOID(String oid) {
        this.oid = oid;
        return this;
    }

    public String getParameterOid() {
        return parameterOid;
    }

    public AnalysisResult setParameterOID(String parameterOid) {
        this.parameterOid = parameterOid;
        return this;
    }

    public String getParamcdDataset() {
        return paramcdDataset;
    }

    public AnalysisResult setParamcdDataset(String paramcdDataset) {
        this.paramcdDataset = paramcdDataset;
        return this;
    }

    public String getAnalysisReason() {
        return analysisReason;
    }

    public AnalysisResult setAnalysisReason(String analysisReason) {
        this.analysisReason = analysisReason;
        return this;
    }

    public String getAnalysisPurpose() {
        return analysisPurpose;
    }

    public AnalysisResult setAnalysisPurpose(String purpose) {
        this.analysisPurpose = purpose;
        return this;
    }

    public Description getDescription() {
        return description;
    }

    public AnalysisResult setDescription(Description description) {
        this.description = description;
        return this;
    }

    public AnalysisDatasets getAnalysisDatasets() {
        return analysisDatasets;
    }

    public AnalysisResult setAnalysisDatasets(AnalysisDatasets analysisDatasets) {
        this.analysisDatasets = analysisDatasets;
        return this;
    }

    public Documentation getDocumentation() {
        return documentation;
    }

    public AnalysisResult setDocumentation(Documentation documentation) {
        this.documentation = documentation;
        return this;
    }

    public ProgrammingCode getProgrammingCode() {
        return code;
    }

    public AnalysisResult setProgrammingCode(ProgrammingCode code) {
        this.code = code;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AnalysisResult result = (AnalysisResult) o;

        if (getOid() != null ? !getOid().equals(result.getOid()) : result.getOid() != null) return false;
        if (getParameterOid() != null ? !getParameterOid().equals(result.getParameterOid()) : result.getParameterOid() != null)
            return false;
        if (getParamcdDataset() != null ? !getParamcdDataset().equals(result.getParamcdDataset()) : result.getParamcdDataset() != null)
            return false;
        if (getAnalysisReason() != null ? !getAnalysisReason().equals(result.getAnalysisReason()) : result.getAnalysisReason() != null)
            return false;
        if (getAnalysisPurpose() != null ? !getAnalysisPurpose().equals(result.getAnalysisPurpose()) : result.getAnalysisPurpose() != null)
            return false;
        if (getDescription() != null ? !getDescription().equals(result.getDescription()) : result.getDescription() != null)
            return false;
        if (getAnalysisDatasets() != null ? !getAnalysisDatasets().equals(result.getAnalysisDatasets()) : result.getAnalysisDatasets() != null)
            return false;
        if (getDocumentation() != null ? !getDocumentation().equals(result.getDocumentation()) : result.getDocumentation() != null)
            return false;
        return getProgrammingCode() != null ? getProgrammingCode().equals(result.getProgrammingCode()) : result.getProgrammingCode() == null;
    }

    @Override
    public String toString() {
        return "AnalysisResult{" +
                "oid='" + oid + '\'' +
                ", parameterOid='" + parameterOid + '\'' +
                ", paramcdDataset='" + paramcdDataset + '\'' +
                ", analysisReason='" + analysisReason + '\'' +
                ", analysisPurpose='" + analysisPurpose + '\'' +
                ", description=" + description +
                ", analysisDatasets=" + analysisDatasets +
                ", documentation=" + documentation +
                ", code=" + code +
                '}';
    }
}
