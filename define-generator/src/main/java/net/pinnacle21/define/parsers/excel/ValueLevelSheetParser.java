/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.Item;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.models.ValueList;
import net.pinnacle21.define.models.WhereClause;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.NamedRow;
import net.pinnacle21.parsing.excel.ValidationAwareExcelSheetParser;
import org.apache.commons.lang3.StringUtils;

class ValueLevelSheetParser extends ValidationAwareExcelSheetParser<ParsedStudy> {
    static final String ORDER_COLUMN = "Order";
    static final String DATASET_COLUMN = "Dataset";
    static final String VARIABLE_COLUMN = "Variable";
    static final String WHERE_CLAUSE_COLUMN = "Where Clause";
    static final String DESCRIPTION_COLUMN = "Description";
    static final String DATA_TYPE_COLUMN = "Data Type";
    static final String LENGTH_COLUMN = "Length";
    static final String SIGNIFICANT_DIGITS_COLUMN = "Significant Digits";
    static final String FORMAT_COLUMN = "Format";
    static final String MANDATORY_COLUMN = "Mandatory";
    static final String CODELIST_COLUMN = "Codelist";
    static final String ORIGIN_COLUMN = "Origin";
    static final String PAGES_COLUMN = "Pages";
    static final String METHOD_COLUMN = "Method";
    static final String PREDECESSOR_COLUMN = "Predecessor";
    static final String ROLE_COLUMN = "Role";
    static final String COMMENT_COLUMN = "Comment";
    static final String DUPLICATE_ERROR = "A value level variable has already been defined with dataset '%s', variable" +
            "'%s', and where clause '%s', duplicates are not allowed";
    static final String DATASET_REF_ERROR = "'%s' references the dataset '%s', but that dataset was not found";
    static final String VARIABLE_REF_ERROR = "'%s' references the variable '%s', but that variable was not found";
    static final String WHERE_CLAUSE_REF_ERROR = "'%s' references the where clause '%s', but that where clause was "
            + "not found";
    static final String COMMENT_REF_ERROR = "'%s' references the comment '%s', but that comment was not found";
    static final String METHOD_REF_ERROR = "'%s' references the method '%s', but that method was not found";
    static final String CODELIST_REF_ERROR = "'%s' references the codelist or dictionary '%s', but that "
            + "codelist/dictionary was not found";

    ValueLevelSheetParser() {
        this.addValidatedRequiredColumn(DATASET_COLUMN);
        this.addValidatedRequiredColumn(VARIABLE_COLUMN, FieldValidations.REALLY_LONG_LENGTH);
        this.addValidatedRequiredColumn(WHERE_CLAUSE_COLUMN, FieldValidations.MEDIUM_LENGTH);
        this.addValidatedColumn(ORDER_COLUMN);
        this.addValidatedColumn(DESCRIPTION_COLUMN);
        this.addValidatedColumn(DATA_TYPE_COLUMN);
        this.addValidatedColumn(LENGTH_COLUMN);
        this.addValidatedColumn(SIGNIFICANT_DIGITS_COLUMN);
        this.addValidatedColumn(FORMAT_COLUMN);
        this.addValidatedColumn(MANDATORY_COLUMN);
        this.addValidatedColumn(CODELIST_COLUMN);
        this.addValidatedColumn(ORIGIN_COLUMN);
        this.addValidatedColumn(PAGES_COLUMN, FieldValidations.REALLY_LONG_LENGTH);
        this.addValidatedColumn(METHOD_COLUMN);
        this.addValidatedColumn(PREDECESSOR_COLUMN, FieldValidations.MEDIUM_LENGTH);
        this.addValidatedColumn(ROLE_COLUMN);
        this.addValidatedColumn(COMMENT_COLUMN);
    }

    @Override
    public void parse(NamedRow row, ParsedStudy parsedStudy) {
        if (!this.validate(row, parsedStudy)) {
            return;
        }
        Study study = parsedStudy.getStudy();

        String datasetName = StringUtils.trim(row.get(DATASET_COLUMN));
        String variableName = StringUtils.trim(row.get(VARIABLE_COLUMN));
        String whereClauseId = StringUtils.trim(row.get(WHERE_CLAUSE_COLUMN));

        if (!study.hasItemGroup(datasetName)) {
            parsedStudy.error(row.getIdentifier(), DATASET_REF_ERROR, DATASET_COLUMN, datasetName);
            return;
        }

        if (!study.hasItem(datasetName, variableName)) {
            parsedStudy.error(row.getIdentifier(), VARIABLE_REF_ERROR, VARIABLE_COLUMN, variableName);
            return;
        }

        if (!study.hasWhereClause(whereClauseId)) {
            parsedStudy.error(row.getIdentifier(), WHERE_CLAUSE_REF_ERROR, WHERE_CLAUSE_COLUMN, whereClauseId);
            return;
        }

        ValueList valueList = study.getValueList(datasetName, variableName);
        if (valueList == null) {
            valueList = new ValueList(datasetName, variableName);
            Item sourceItem = study.getItem(datasetName, variableName);
            sourceItem.setValueListOid(valueList.getOid());
            study.setValueList(datasetName, variableName, valueList);
        }

        if (valueList.hasItem(whereClauseId)) {
            parsedStudy.error(row.getIdentifier(), DUPLICATE_ERROR, datasetName, variableName, whereClauseId);
            return;
        }

        WhereClause whereClause = study.getWhereClause(whereClauseId);

        Item item = new Item();
        item.setName(variableName + "." + whereClause.getOid());
        item.setOrder(row.get(ORDER_COLUMN));
        item.setLabel(row.get(DESCRIPTION_COLUMN));
        item.setDataType(row.get(DATA_TYPE_COLUMN));
        item.setLength(row.get(LENGTH_COLUMN));
        item.setSignificantDigits(row.get(SIGNIFICANT_DIGITS_COLUMN));
        item.setFormat(row.get(FORMAT_COLUMN));
        item.setMandatory(row.get(MANDATORY_COLUMN));
        item.setOrigin(row.get(ORIGIN_COLUMN));
        item.setPages(row.get(PAGES_COLUMN));
        item.setPredecessor(row.get(PREDECESSOR_COLUMN));
        item.setRole(row.get(ROLE_COLUMN));
        item.setWhereClauseOid(whereClauseId);

        Integer keySequence = study.getKeyVariablePosition(datasetName, variableName);
        item.setKeySequence(keySequence);

        String commentRef = StringUtils.trim(row.get(COMMENT_COLUMN));

        if (StringUtils.isNotBlank(commentRef)) {
            if (study.hasComment(commentRef)) {
                item.setCommentOid(commentRef);
            } else {
                parsedStudy.error(row.getIdentifier(), COMMENT_REF_ERROR, COMMENT_COLUMN, commentRef);
            }
        }

        String methodRef = StringUtils.trim(row.get(METHOD_COLUMN));

        if (StringUtils.isNotBlank(methodRef)) {
            if (study.hasMethod(methodRef)) {
                item.setMethodOid(methodRef);
            } else {
                parsedStudy.error(row.getIdentifier(), METHOD_REF_ERROR, METHOD_COLUMN, methodRef);
            }
        }

        String codelistRef = StringUtils.trim(row.get(CODELIST_COLUMN));

        if (StringUtils.isNotBlank(codelistRef)) {
            if (study.hasCodeList(codelistRef)) {
                item.setCodeListOid(codelistRef);
            } else {
                if (study.hasDictionary(codelistRef)) {
                    item.setDictionaryOid(codelistRef);
                } else {
                    parsedStudy.error(row.getIdentifier(), CODELIST_REF_ERROR, CODELIST_COLUMN, codelistRef);
                }
            }
        }

        valueList.add(item);
    }
}
