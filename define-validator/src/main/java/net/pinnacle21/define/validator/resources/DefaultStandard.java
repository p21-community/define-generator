/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.resources;

import com.google.common.collect.ImmutableMap;
import net.pinnacle21.define.models.*;
import net.pinnacle21.define.parsers.xml.XPathEvaluator;
import net.pinnacle21.define.parsers.xml.util.ParserUtils;
import net.pinnacle21.define.validator.content.rules.RuleUtils;
import net.pinnacle21.validator.api.model.ConfigOptions;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class DefaultStandard implements Standard {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultStandard.class);
    private static final String CONFIG_NAMESPACE_URI = "http://www.opencdisc.org/schema/validator";
    private static final List<String> PERMISSIBLE_CORES = Arrays.asList("Required", "Expected", "Permissible");

    private final XPathEvaluator evaluator;
    private final DefineMetadata metadata = DefineMetadata.V2;
    private final ConfigOptions configOptions;
    private final Map<String, ItemGroup> groupNameCache = new HashMap<>();
    private final Map<String, Element> itemDefOIDCache = new HashMap<>();
    private final Map<String, Element> itemRefOIDCache = new HashMap<>();
    private final Map<String, Element> itemRefOIDToGroupCache = new HashMap<>();

    DefaultStandard(Document document, ConfigOptions configOptions) {
        this.evaluator = new XPathEvaluator(document,
                supplementNamespaces(this.metadata.getNamespaces().xpathNamespaces()));
        this.configOptions = configOptions;
    }

    @Override
    public List<String> getTerminologies() {
        String directory = this.configOptions.getProperty("TerminologyDirectory");
        return this.evaluator.selectElements("/ODM/odm:Study/odm:MetaDataVersion/val:TerminologyRef")
                .stream()
                .map(e -> e.attributeValue("href"))
                .filter(Objects::nonNull)
                .flatMap(p -> {
                    String path = p.replace("%System.TerminologyDirectory%", directory);
                    List<String> results = new ArrayList<>();
                    for (String propertyName : this.configOptions.getProperties()) {
                        String searchPattern = "%System." + propertyName + "%";
                        if (path.contains(searchPattern)) {
                            String value = this.configOptions.getProperty(propertyName);
                            String[] propertyValues = value.split(";");
                            for (String propertyValue : propertyValues) {
                                String result = path.replace(searchPattern, propertyValue);
                                results.add(result);
                            }
                        }
                    }
                    return results.stream();
                })
                .collect(Collectors.toList());
    }

    @Override
    public Item getVariableFromItemGroup(ItemGroup itemGroup, Item item) {
        if (itemGroup == null || itemGroup.getName() == null || item == null || item.getName() == null) {
            return null;
        }

        Element itemDef = this.getItemDefByOid(String.format("IT.%s.%s", itemGroup.getName(), item.getName()));

        if (itemDef == null) {
            itemDef = this.evaluator.selectSingleElement("/ODM/odm:Study/odm:MetaDataVersion/odm:ItemDef[@Name=$name]",
                    ImmutableMap.of("name", item.getName()));
        }

        if (itemDef == null) {
            return null;
        }

        return mapItem(itemDef, null, variableIsAllowed(itemGroup, item));
    }

    /**
     * @since DEF-163
     */
    private boolean variableIsAllowed(ItemGroup itemGroup, Item item) {
        String id = itemGroup.getName() + "." + item.getName();
        LOGGER.debug("Checking to see if '{}' is allowed by the standard...", id);

        Element itemRef = this.getItemRefByOID("IT." + id);

        if (itemRef == null) {
            LOGGER.debug("No ItemRef found with OID 'IT.{}'.", id);
            return false;
        }

        if (PERMISSIBLE_CORES.contains(itemRef.attributeValue("Core"))) {
            LOGGER.debug("'{}' is allowed with core '{}'.", id, itemRef.attributeValue("Core"));
            return true;
        } else {
            LOGGER.debug("'{}' is NOT allowed with core '{}'.", id, itemRef.attributeValue("Core"));
            return false;
        }
    }

    private Element getItemRefByOID(String oid) {
        if (this.itemRefOIDCache.isEmpty()) {
            for (Element ref : this.evaluator.selectElements("/ODM/odm:Study/odm:MetaDataVersion/odm:ItemGroupDef/odm:ItemRef")) {
                this.itemRefOIDCache.put(ref.attributeValue("ItemOID"), ref);
                this.itemRefOIDToGroupCache.put(ref.attributeValue("ItemOID"), ref.getParent());
            }
        }
        return itemRefOIDCache.get(oid);
    }

    @Override
    public ItemGroup getItemGroupByName(String name) {
        if (groupNameCache.isEmpty()) {
            for (Element group : this.evaluator.selectElements("/ODM/odm:Study/odm:MetaDataVersion/odm:ItemGroupDef")) {
                this.groupNameCache.put(group.attributeValue("Name"), this.mapItemGroup(group));
            }
        }
        return this.groupNameCache.get(name);
    }

    @Override
    public boolean isEventsOrInterventionsVariable(CodeListItem clItem) {
        return getVariableFromEventsOrInterventions(clItem) != null;
    }

    @Override
    public Item getVariableFromEventsOrInterventions(CodeListItem clItem) {
        List<String> allowableClasses = Arrays.asList(
            DefineMetadata.Categories.EVENTS,
            DefineMetadata.Categories.INTERVENTIONS
        );

        Element item = this.evaluator.selectSingleElement(
            "/ODM/odm:Study/odm:MetaDataVersion/odm:ItemDef[@Name=$name]",
            ImmutableMap.of("name", String.format("__%s", clItem.getCodedValue()))
        );

        if (item == null) {
            return null;
        }

        Element itemGroup = this.itemRefOIDToGroupCache.computeIfAbsent(item.attributeValue("OID"), (e) ->
            this.evaluator.selectSingleElement(
                "/ODM/odm:Study/odm:MetaDataVersion/odm:ItemGroupDef[odm:ItemRef[@ItemOID=$id]]",
                ImmutableMap.of("id", item.attributeValue("OID")
            )
        ));

        return itemGroup != null
                && allowableClasses.contains(StringUtils.defaultString(itemGroup.attributeValue("Class")).toUpperCase())
                ? mapItem(item, null, true)
                : null;
    }

    private ItemGroup mapItemGroup(Element element) {
        if (element == null) {
            return null;
        }

        ItemGroup itemGroup = new ItemGroup()
                .setOID(ParserUtils.substringWithPrefix(
                        metadata.getPrefixes().itemGroup(), element.attributeValue("OID")
                ))
                .setName(element.attributeValue("Name"))
                .setIsReferenceData(element.attributeValue("IsReferenceData"))
                .setPurpose(element.attributeValue("Purpose"))
                .setStructure(element.attributeValue("Structure"))
                .setCommentOid(ParserUtils.substringWithPrefix(
                        metadata.getPrefixes().comment(), element.attributeValue("CommentOID")
                ))
                .setDescription(RuleUtils.getDescription(element));

        for (Element itemRef : element.elements("ItemRef")) {
            Element itemDef = this.getItemDefByOid(itemRef.attributeValue("ItemOID"));
            //true probably safer here because it lets everything through
            //this method is not used when running DD0060 anyway
            itemGroup.add(mapItem(itemDef, itemRef, true));
        }

        return itemGroup;
    }

    private Element getItemDefByOid(String oid) {
        if (itemDefOIDCache.isEmpty()) {
            for (Element def : this.evaluator.selectElements("/ODM/odm:Study/odm:MetaDataVersion/odm:ItemDef")) {
                itemDefOIDCache.put(def.attributeValue("OID"), def);
            }
        }
        return itemDefOIDCache.get(oid);
    }

    private Item mapItem(Element itemDef, Element itemRef, boolean isCoreAllowed) {
        if (itemDef == null) {
            return null;
        }
        Item item = new Item()
                .setOID(ParserUtils.substringWithPrefix(metadata.getPrefixes().item(), itemDef.attributeValue("OID")))
                .setName(itemDef.attributeValue("Name"))
                .setDataType(itemDef.attributeValue("DataType"))
                .setLabel(itemDef.element("Description").elementText("TranslatedText"))
                .setCodeListOid(
                        itemDef.element("CodeListRef") != null
                                ? itemDef.element("CodeListRef").attributeValue("CodeListOID")
                                : StringUtils.EMPTY
                )
                .setValueListOid(
                        itemDef.element("ValueListRef") != null
                                ? itemDef.element("ValueListRef").attributeValue("ValueListOID")
                                : StringUtils.EMPTY
                )
                .setCoreAllowed(isCoreAllowed);


        return addFromItemRef(item, itemRef);
    }

    private Item addFromItemRef(Item item, Element itemRef) {
        if (itemRef == null) {
            return item; //return the item as is
        }

        return item
                .setMandatory(itemRef.attributeValue("Mandatory"))
                .setOrder(itemRef.attributeValue("OrderNumber"))
                .setKeySequence(
                        StringUtils.isNotBlank(itemRef.attributeValue("KeySequence"))
                                ? Integer.valueOf(itemRef.attributeValue("KeySequence"))
                                : null
                )
                .setCore(itemRef.attributeValue("Core"));
    }

    private static Map<String, String> supplementNamespaces(Map<String, String> namespaces) {
        Map<String, String> supplement = new HashMap<>(namespaces);

        supplement.put("val", CONFIG_NAMESPACE_URI);

        return supplement;
    }
}
