/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.Comment;
import net.pinnacle21.define.models.Document;
import net.pinnacle21.define.models.DocumentReference;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.models.arm.AnalysisResult;
import net.pinnacle21.define.models.arm.ResultDisplay;
import net.pinnacle21.define.util.Arm;
import net.pinnacle21.define.util.MockNamedRow;
import net.pinnacle21.parsing.excel.ExcelSheetParser;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.RowIdentifier;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import java.util.List;

import static net.pinnacle21.define.parsers.excel.AnalysisResultSheetParser.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyVararg;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class AnalysisResultSheetParserTest {

    @Test
    public void nonexistingAnalysisResultGetsCreated() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new AnalysisResultSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedDisplay = "Display";
        String expectedId = "Id";
        String expectedDescription = "descriptive";
        String expectedReason = "reason";
        String expectedPurpose = "purpose";
        String expectedDocumentation = "Documentation";
        String expectedProgrammingContext = "Programming Context";
        String expectedProgrammingCode = "Programming Code";

        study.setResultDisplay(expectedDisplay, new ResultDisplay());

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, expectedDisplay + " ")
                        .with(ID_COLUMN, expectedId + " ")
                        .with(DESCRIPTION_COLUMN, expectedDescription)
                        .with(REASON_COLUMN, expectedReason)
                        .with(PURPOSE_COLUMN, expectedPurpose)
                        .with(DOCUMENTATION_COLUMN, expectedDocumentation)
                        .with(PROGRAMMING_CONTEXT_COLUMN, expectedProgrammingContext)
                        .with(PROGRAMMING_CODE_COLUMN, expectedProgrammingCode),
                parsedStudy
        );

        assertTrue(study.hasAnalysisResult(expectedDisplay, expectedId));

        AnalysisResult analysisResult = study.getAnalysisResult(expectedDisplay, expectedId);
        assertSame(study.getResultDisplay(expectedDisplay).getAnalysisResults().get(0), analysisResult);
        assertEquals(expectedId, analysisResult.getOid());
        assertEquals(expectedDescription, analysisResult.getDescription().getText());
        assertEquals(expectedReason, analysisResult.getAnalysisReason());
        assertEquals(expectedPurpose, analysisResult.getAnalysisPurpose());
        assertEquals(expectedDocumentation, analysisResult.getDocumentation().getDescription().getText());
        assertEquals(expectedProgrammingContext, analysisResult.getProgrammingCode().getContext());
        assertEquals(expectedProgrammingCode, analysisResult.getProgrammingCode().getCode());
    }

    @Test
    public void displayColumnIsRequired() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new AnalysisResultSheetParser();

        parser.parse(
                new MockNamedRow(),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR), eq(DISPLAY_COLUMN));
    }

    @Test
    public void idColumnIsRequired() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new AnalysisResultSheetParser();

        parser.parse(
                new MockNamedRow(),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR), eq(ID_COLUMN));
    }

    @Test
    public void documentationDocumentsAndPagesAreAssigned() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new AnalysisResultSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedDisplay = "Display";
        String expectedId = "Id";
        String expectedDocument1 = "Document1";
        String expectedDocument2 = "Document2";
        String expectedPages = "12, 23-42, 44";

        study.setResultDisplay(expectedDisplay, new ResultDisplay());
        study.setDocument(expectedDocument1, new Document());
        study.setDocument(expectedDocument2, new Document());

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, expectedDisplay)
                        .with(ID_COLUMN, expectedId)
                        .with(REASON_COLUMN, "Reason")
                        .with(PURPOSE_COLUMN, "Purpose")
                        .with(
                            DOCUMENTATION_REFS_COLUMN,
                            String.format("%s, %s(%s) ", expectedDocument1, expectedDocument2, expectedPages)
                        ),
                parsedStudy
        );

        assertTrue(study.hasAnalysisResult(expectedDisplay, expectedId));

        AnalysisResult analysisResult = study.getAnalysisResult(expectedDisplay, expectedId);
        List<DocumentReference> documentReferences = analysisResult.getDocumentation().getDocumentReferences();
        assertEquals(2, documentReferences.size());
        assertEquals(study.getDocument(expectedDocument1), documentReferences.get(0).getDocument());
        assertEquals(null, documentReferences.get(0).getPages());
        assertEquals(study.getDocument(expectedDocument2), documentReferences.get(1).getDocument());
        assertEquals(expectedPages, documentReferences.get(1).getPages());
    }

    @Test
    public void programmingDocumentAndPagesAreAssigned() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new AnalysisResultSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedDisplay = "Display";
        String expectedId = "Id";
        String expectedDocument = "Document1";
        String expectedPages = "12, 23-42, 44";

        study.setResultDisplay(expectedDisplay, new ResultDisplay());
        study.setDocument(expectedDocument, new Document());

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, expectedDisplay)
                        .with(ID_COLUMN, expectedId)
                        .with(REASON_COLUMN, "Reason")
                        .with(PURPOSE_COLUMN, "Purpose")
                        .with(
                                PROGRAMMING_DOCUMENT_COLUMN,
                                String.format("%s(%s)", expectedDocument, expectedPages)
                        ),
                parsedStudy
        );

        assertTrue(study.hasAnalysisResult(expectedDisplay, expectedId));

        AnalysisResult analysisResult = study.getAnalysisResult(expectedDisplay, expectedId);
        DocumentReference documentReference = analysisResult.getProgrammingCode().getDocumentReference();
        assertNotNull(documentReference);
        assertEquals(study.getDocument(expectedDocument), documentReference.getDocument());
        assertEquals(expectedPages, documentReference.getPages());
    }

    @Test
    public void testParseDocumentsAndPagesFromString() {
        List<Pair<String, String>> pairs = Arm.parseDocumentsAndPagesFromString("Document1 ( 2, 23,&abc 24,28 - 30 )");
        assertNotNull(pairs);
        assertEquals("Document1", pairs.get(0).getLeft());
        assertEquals(" 2, 23,&abc 24,28 - 30 ", pairs.get(0).getRight());

        pairs = Arm.parseDocumentsAndPagesFromString("document 1 ,   document 2");
        assertNotNull(pairs);
        assertEquals("document 1", pairs.get(0).getLeft());
        assertEquals("document 2", pairs.get(1).getLeft());

        pairs = Arm.parseDocumentsAndPagesFromString("Document1(23) ,Document2");
        assertNotNull(pairs);
        assertEquals("Document1", pairs.get(0).getLeft());
        assertEquals("23", pairs.get(0).getRight());
        assertEquals("Document2", pairs.get(1).getLeft());
        assertNull(pairs.get(1).getRight());
    }

    @Test
    public void parseDocumentsAndPagesFromStringReturnsNullIfEmptyList() {
        assertNull(Arm.parseDocumentsAndPagesFromString(""));
    }

    @Test
    public void invalidDisplayReferenceErrorRaised() {
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(new Study()));
        ExcelSheetParser<ParsedStudy> parser = new AnalysisResultSheetParser();

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, "Display")
                        .with(ID_COLUMN, "Id")
                        .with(PURPOSE_COLUMN, "Purpose")
                        .with(REASON_COLUMN, "Reason")
                        .with(JOIN_COMMENT_COLUMN, "COMMENT"),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(DISPLAY_REF_ERROR), anyVararg());
    }

    @Test
    public void invalidCommentReferenceErrorRaised() {
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(new Study()));
        ExcelSheetParser<ParsedStudy> parser = new AnalysisResultSheetParser();

        String expectedDisplay = "Display";
        parsedStudy.getStudy().setResultDisplay(expectedDisplay, new ResultDisplay());

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, expectedDisplay)
                        .with(ID_COLUMN, "Id")
                        .with(PURPOSE_COLUMN, "Purpose")
                        .with(REASON_COLUMN, "Reason")
                        .with(JOIN_COMMENT_COLUMN, "COMMENT"),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(COMMENT_REF_ERROR), anyVararg());
    }

    @Test
    public void invalidDocumentationDocumentReferenceErrorRaised() {
        invalidDocumentReferenceErrorRaised(DOCUMENTATION_REFS_COLUMN);
    }

    @Test
    public void invalidProgrammingDocumentReferenceErrorRaised() {
        invalidDocumentReferenceErrorRaised(PROGRAMMING_DOCUMENT_COLUMN);
    }

    private void invalidDocumentReferenceErrorRaised(String errorMessage) {
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(new Study()));
        ExcelSheetParser<ParsedStudy> parser = new AnalysisResultSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedDisplay = "Display";
        String expectedId = "Id";
        String expectedDocument = "Document1";

        study.setResultDisplay(expectedDisplay, new ResultDisplay());

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, expectedDisplay)
                        .with(ID_COLUMN, expectedId)
                        .with(REASON_COLUMN, "Reason")
                        .with(PURPOSE_COLUMN, "Purpose")
                        .with(
                                errorMessage,
                                String.format("%s", expectedDocument)
                        ),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(DOCUMENT_REF_ERROR), eq(errorMessage), eq(expectedDocument));
    }

    @Test
    public void validCommentReferenceIsAssigned() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new AnalysisResultSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedDisplay = "Display";
        String expectedId = "Id";
        String expectedComment = "COMMENT";
        Comment comment = new Comment().setOid(expectedComment);

        study.setResultDisplay(expectedDisplay, new ResultDisplay());
        study.setComment(expectedComment, comment);

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, expectedDisplay)
                        .with(ID_COLUMN, expectedId)
                        .with(PURPOSE_COLUMN, "Purpose")
                        .with(REASON_COLUMN, "Reason")
                        .with(JOIN_COMMENT_COLUMN, expectedComment),
                parsedStudy
        );

        AnalysisResult analysisResult = study.getAnalysisResult(expectedDisplay, expectedId);

        assertNotNull(analysisResult);
        assertSame(comment.getOid(), analysisResult.getAnalysisDatasets().getComment().getOid());
    }

    @Test
    public void duplicateErrorRaised() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new AnalysisResultSheetParser();
        Study study = mock(Study.class);

        when(parsedStudy.getStudy()).thenReturn(study);

        String expectedDisplay = "Display";
        String expectedId = "Id";

        when(study.hasAnalysisResult(expectedDisplay, expectedId)).thenReturn(true);

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, expectedDisplay)
                        .with(ID_COLUMN, expectedId)
                        .with(REASON_COLUMN, "Reason")
                        .with(PURPOSE_COLUMN, "Purpose"),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(DUPLICATE_ERROR), anyVararg());
    }

    @Test
    public void duplicateErrorNotRaisedWithDifferentCasing() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new AnalysisResultSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedDisplay = "DISPLAY";
        String expectedId = "ID";

        study.setResultDisplay(expectedDisplay, new ResultDisplay());
        study.setAnalysisResult(expectedDisplay.toLowerCase(), expectedId.toLowerCase(), new AnalysisResult());

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, expectedDisplay)
                        .with(ID_COLUMN, expectedId)
                        .with(REASON_COLUMN, "Reason")
                        .with(PURPOSE_COLUMN, "Purpose"),
                parsedStudy
        );

        assertEquals(true, study.hasAnalysisResult(expectedDisplay, expectedId));
    }

    @Test
    public void disallowDocumentWithPagesGreaterThan1000Chars() {
        ParsedStudy parsedStudy = mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new AnalysisResultSheetParser();
        Study study = mock(Study.class);

        String expectedDisplay = "Display";
        String expectedId = "Id";
        String reallyLongDocumentationDocumentsField = "Test Document 1(1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10, 1, 4, 9-10), TestDocument2";

        when(parsedStudy.getStudy()).thenReturn(study);
        when(study.hasResultDisplay(eq(expectedDisplay))).thenReturn(true);
        when(study.getResultDisplay(eq(expectedDisplay))).thenReturn(new ResultDisplay());
        when(study.hasDocument(anyString())).thenReturn(true);

        parser.parse(
                new MockNamedRow()
                        .with(DISPLAY_COLUMN, expectedDisplay)
                        .with(ID_COLUMN, expectedId)
                        .with(REASON_COLUMN, "Reason")
                        .with(PURPOSE_COLUMN, "Purpose")
                        .with(DOCUMENTATION_REFS_COLUMN, reallyLongDocumentationDocumentsField),
                parsedStudy
        );

        verify(parsedStudy).error(
                any(RowIdentifier.class),
                eq(DOCUMENTATION_DOCUMENTS_PAGES_TOO_LONG),
                Matchers.anyVararg()
        );
    }
}
