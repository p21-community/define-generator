/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.generator.util;

import net.pinnacle21.define.models.DefineMetadata;
import net.pinnacle21.define.models.Description;
import net.pinnacle21.define.models.DocumentReference;
import net.pinnacle21.parsing.xml.XmlWriter;
import org.apache.commons.lang3.StringUtils;
import org.xml.sax.SAXException;

public class GeneratorUtils {

    public static void writeDocumentRef(XmlWriter writer, DefineMetadata metadata, DocumentReference documentReference) throws SAXException {
        if (documentReference != null) {
            writeDocumentRef(writer, metadata, documentReference.getDocument().getOid(), documentReference.getPages());
        }
    }

    public static void writeDocumentRef(XmlWriter writer, DefineMetadata metadata, String source, String location) throws SAXException {
        if (StringUtils.isEmpty(source)) {
            return;
        }

        XmlWriter.SimpleAttributes attributes = writer.newAttributes()
                .addAttribute("leafID", metadata.getPrefixes().leaf() + source);

        if (StringUtils.isNotEmpty(location)) {
            writer.startElement(metadata.getNamespaces().defURI(), "DocumentRef", attributes);

            boolean isReference = !StringUtils.isNumericSpace(location.replace("-", ""));

            attributes = writer.newAttributes()
                    .addAttribute("Type", isReference ? "NamedDestination" : "PhysicalRef");

            if (!isReference) {
                String[] pages = location
                        .replaceAll("\\s+-\\s+", "-")
                        .split("[^\\d\\-]+");

                boolean isRange = pages.length == 1 && pages[0].matches("\\d+-\\d+");

                if (isRange) {
                    pages = pages[0].split("-");

                    attributes.addAttribute("FirstPage", pages[0]);
                    attributes.addAttribute("LastPage", pages[1]);
                } else {
                    attributes.addAttribute("PageRefs",
                            StringUtils.join(pages, ' ').replaceAll("\\s{2,}", " "));
                }
            } else {
                attributes.addAttribute("PageRefs", location);
            }

            writer.simpleElement(metadata.getNamespaces().defURI(), "PDFPageRef", attributes);

            writer.endElement(metadata.getNamespaces().defURI(), "DocumentRef");
        } else {
            writer.simpleElement(metadata.getNamespaces().defURI(), "DocumentRef", attributes);
        }
    }

    public static void writeDescription(XmlWriter writer, DefineMetadata metadata, Description description) throws SAXException {
        if (description != null) {
            writeDescription(writer, metadata, description.getLanguage(), description.getText());
        }
    }

    public static void writeDescription(XmlWriter writer, DefineMetadata metadata, String language, String descriptionText) throws SAXException {
        if (StringUtils.isBlank(descriptionText)) {
            return;
        }
        writer.startElement("Description");
        XmlWriter.SimpleAttributes attributes = writer.newAttributes();
        if (StringUtils.isNotBlank(language)) {
            attributes = writer.newAttributes().addAttribute(metadata.getNamespaces().xmlURI(), "lang",language);
        }
        writer.simpleElement("TranslatedText", attributes, descriptionText);
        writer.endElement("Description");
    }
}
