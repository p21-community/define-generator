/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.content.rules;

import net.pinnacle21.define.models.*;
import net.pinnacle21.define.models.Dictionary;
import net.pinnacle21.define.models.arm.AnalysisDatasets;
import net.pinnacle21.define.models.arm.AnalysisResult;
import net.pinnacle21.define.models.arm.ProgrammingCode;
import net.pinnacle21.define.models.arm.ResultDisplay;
import net.pinnacle21.define.validator.resources.Standard;
import net.pinnacle21.define.validator.resources.Terminology;
import net.pinnacle21.define.validator.content.SupplementalValidationRule;
import net.pinnacle21.define.validator.content.ValidationRule;
import net.pinnacle21.define.validator.report.MessageTransformer;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.util.*;

public class ModelRules extends AbstractRules {
    private static final Logger LOGGER = LoggerFactory.getLogger(ModelRules.class);
    private static final Set<String> YN_VALUES = new HashSet<>(Arrays.asList("Yes", "No"));
    private static final Set<String> METHOD_TYPES = new HashSet<>(Arrays.asList("Computation", "Imputation"));
    private static final Set<String> CODELIST_DATA_TYPES = new HashSet<>(Arrays.asList("text", "integer", "float"));
    private static final Set<String> V1_VARIABLE_DATA_TYPES = new HashSet<>(Arrays.asList("text", "integer", "float", "datetime", "date", "time"));
    private static final Set<String> V2_VARIABLE_DATA_TYPES = new HashSet<>(Arrays.asList("text", "integer", "float", "datetime", "date", "time", "partialDate", "partialTime", "partialDatetime", "incompleteDatetime", "durationDatetime"));

    private final DefineMetadata metadata;
    private final Study study;
    private final Standard standard;
    private final Terminology terminology;

    private HashMap<String, HashMap<String, String>> valueLevelMetadataInfo;
    private String usubjidOrigin, visitnumOrigin = StringUtils.EMPTY;
    private Map<String, String> usubjids = new HashMap<>(), visitnums = new HashMap<>();
    private boolean referencesMeddra = false;
    private String meddraCodelistId = StringUtils.EMPTY;
    private boolean dd0109, dd0110 = false;

    private static final String KEY_DATASET_NAME = "Dataset Name";
    private static final String KEY_VARIABLE_NAME = "Variable Name";
    private static final String KEY_WHERE_CLAUSE = "Where Clause";

    public ModelRules(DefineMetadata metadata, Study study, Standard standard, Terminology terminology,
            MessageTransformer transformer) {
        super(transformer);

        this.metadata = metadata;
        this.study = study;
        this.standard = standard;
        this.terminology = terminology;
    }

    @ValidationRule(id = "OD0013")
    public void validateItems() {
        List<ItemGroup> itemGroups = this.study.getItemGroups();
        List<ValueList> valueLevel = this.study.getValueLists();

        for (final ItemGroup itemGroup : itemGroups) {
            for (final Item item : itemGroup.getItems()) {
                if (item.getOrder() != null) {
                    try {
                        Integer.parseInt(item.getOrder());
                    } catch (NumberFormatException ignore) {
                        transformer.map("OD0013", Arrays.asList("OrderNumber", "ItemRef"), ImmutableMap.of(
                            KEY_DATASET_NAME, itemGroup.getName(),
                            KEY_VARIABLE_NAME, item.getName(),
                            "Order Number", item.getOrder()
                        ));
                    }
                }
                if (item.getLength() != null) {
                    try {
                        int length = Integer.parseInt(item.getLength());
                        if (length <= 0) {
                            transformer.map("OD0013", Arrays.asList("Length", "ItemDef"), ImmutableMap.of(
                                KEY_DATASET_NAME, itemGroup.getName(),
                                KEY_VARIABLE_NAME, item.getName(),
                                "Length", item.getLength()
                            ));
                        }
                    } catch (NumberFormatException ignore) {
                        transformer.map("OD0013", Arrays.asList("Length", "ItemDef"), ImmutableMap.of(
                            KEY_DATASET_NAME, itemGroup.getName(),
                            KEY_VARIABLE_NAME, item.getName(),
                                "Length", item.getLength()
                        ));
                    }
                }
            }
        }

        for (final ValueList valueList : valueLevel) {
            for (final Item valueListItem : valueList.getItems()) {
                if (valueListItem.getOrder() != null) {
                    try {
                        Integer.parseInt(valueListItem.getOrder());
                    } catch (NumberFormatException ignore) {
                        transformer.map("OD0013", Arrays.asList("OrderNumber", "ItemRef"), ImmutableMap.of(
                            KEY_DATASET_NAME, valueList.getSourceItemGroupName(),
                            KEY_VARIABLE_NAME, valueList.getSourceItemName(),
                            KEY_WHERE_CLAUSE, getComputedWhereClauseString(valueListItem.getWhereClauseOid()),
                            "Order Number", valueListItem.getOrder()
                        ));
                    }
                }
                if (valueListItem.getLength() != null) {
                    try {
                        int length = Integer.parseInt(valueListItem.getLength());
                        if (length <= 0) {
                            transformer.map("OD0013", Arrays.asList("Length", "ItemDef"), ImmutableMap.of(
                                KEY_DATASET_NAME, valueList.getSourceItemGroupName(),
                                KEY_VARIABLE_NAME, valueList.getSourceItemName(),
                                KEY_WHERE_CLAUSE, getComputedWhereClauseString(valueListItem.getWhereClauseOid()),
                                    "Length", valueListItem.getLength()
                            ));
                        }
                    } catch (NumberFormatException ignore) {
                        transformer.map("OD0013", Arrays.asList("Length", "ItemDef"), ImmutableMap.of(
                            KEY_DATASET_NAME, valueList.getSourceItemGroupName(),
                            KEY_VARIABLE_NAME, valueList.getSourceItemName(),
                            KEY_WHERE_CLAUSE, getComputedWhereClauseString(valueListItem.getWhereClauseOid()),
                                "Length", valueListItem.getLength()
                        ));
                    }
                }
            }
        }
    }

    @ValidationRule(id = "DD0003")
    public void checkCodelistNameAndItemCodedValues() {
        List<CodeList> codeLists = this.study.getCodeLists();

        for (final CodeList codeList : codeLists) {
            if (StringUtils.isBlank(codeList.getName())) {
                this.transformer.map("DD0003", Arrays.asList("Name", "CodeList"), mapOf(
                    "CodeList OID", codeList.getOid()
                ));
            }
            for (CodeListItem item : codeList.getCodelistItems()) {
                if (StringUtils.isBlank(item.getCodedValue())) {
                    this.transformer.map("DD0003", Arrays.asList("CodedValue", "CodeListItem"), mapOf(
                        "CodeList OID", codeList.getOid()
                    ));
                }
            }
        }
    }

    private void invalidTermInCodeList(ItemGroup itemGroup, Item item) {
        if (!isValidStandardName(getDefineStandardName()) || !isValidStandardVersion(getDefineStandardName(), getDefineStandardVersion())) {
            return;
        }
        if (isValidStandardName(getDefineStandardName())) {
            validateVariableAgainstStandard(itemGroup, item);
        }
    }

    private void validateVariableAgainstStandard(ItemGroup itemGroup, final Item defineVariable) {
        if (itemGroup == null || StringUtils.isBlank(itemGroup.getDomain())) {
            return;
        }

        if (standard == null) {
            LOGGER.warn("Skipping standard checks because standard was null. This might be a web import xml job " +
                "which doesn't set standard.");
            return;
        }

        final Item standardVariable = standard.getVariableFromItemGroup(itemGroup, defineVariable);

        if (standardVariable == null) {
            return;
        }

        if (StringUtils.isNotBlank(standardVariable.getCodeListOid())
                && StringUtils.isBlank(defineVariable.getCodeListOid())) {
            return; //TODO rule missing required standard codelist??
        }

        CodeList defineCodeList = this.study.getCodeList(defineVariable.getCodeListOid());
        CodeList standardCodeList = this.terminology.getCodeListByOID(standardVariable.getCodeListOid());

        validateStandardCodeList(
                defineVariable,
                defineCodeList,
                standardVariable,
                standardCodeList
        );
    }

    private void validateStandardCodeList(final Item defineVariable, final CodeList defineCodeList, final Item standardVariable, final CodeList standardCodeList) {
        if (defineCodeList == null || defineCodeList.isExternal()) {
            return;
        }

        //Check the Code on the define CodeList against the standard CodeList
        String standardCode = ObjectUtils.defaultIfNull(standardCodeList, new CodeList()).getCode();
        String defineCode = defineCodeList.getCode();

        if (StringUtils.isNotBlank(standardCode) && StringUtils.isBlank(defineCode)) {
            if (isDefine2()) {
                transformer.map("DD0031", Collections.singletonList(defineCodeList.getName()),
                        ImmutableMap.of(
                                "Variable", defineVariable.getName(),
                                "Define Codelist", defineCodeList.getName(),
                                "Standard Codelist", standardCodeList.getName(),
                                "Standard Codelist Code", standardCodeList.getCode()

                        ));
            }
        } else if (StringUtils.isNotBlank(standardCode) && !defineCode.equals(standardCode)) {
            transformer.map(
                    "DD0118",
                    ImmutableList.of(
                            defineCodeList.getCode(), defineCodeList.getName(), defineVariable.getName(),
                            standardCodeList.getCode(), standardCodeList.getName(), standardVariable.getName()
                    ),
                    ImmutableMap.<String, String>builder()
                            .put("Define Variable", defineVariable.getName())
                            .put("Define Codelist", defineCodeList.getName())
                            .put("Define Codelist Code", defineCode)
                            .put("Standard Variable", standardVariable.getName())
                            .put("Standard Codelist", standardCodeList.getName())
                            .put("Standard Codelist Code", standardCode).build()
            );
        } else if (StringUtils.isNotBlank(defineCode) && !terminology.hasNciCode(defineCode)) {
            ImmutableMap.Builder<String, String> details = ImmutableMap.<String, String>builder()
                    .put("Define Variable", defineVariable.getName())
                    .put("Define Codelist", defineCodeList.getName())
                    .put("Define Codelist NCI Code", defineCodeList.getCode());
            if (standardCodeList != null) {
                details.put("Standard Variable", standardVariable.getName())
                        .put("Standard Codelist", standardCodeList.getName())
                        .put("Standard Codelist NCI Code", standardCodeList.getCode());
            }
            transformer.map("DD0033", Collections.singletonList(defineCodeList.getName()), details.build());
        }

        validateStandardCodeListTerms(defineVariable, defineCodeList, standardCodeList);
    }

    private void validateStandardCodeListTerms(final Item defineVariable, final CodeList defineCodeList, final CodeList standardCodeList) {
        if (standardCodeList == null) {
            return;
        }

        if (defineCodeList.getCodelistItems().isEmpty()) {
            return;
        }

        for (final CodeListItem defineTerm : defineCodeList.getCodelistItems()) {
            String defineTermCode = defineTerm.getCode();

            //Get the term by matching NCI Codes
            CodeListItem standardTerm = terminology.getCodeListItemByNCICodeAndCodeListOID(standardCodeList.getOid(), defineTermCode);

            if (standardTerm == null) {
                //Lets try by coded value then
                standardTerm = terminology.getCodeListItemByCodedValueAndCodeListOID(standardCodeList.getOid(), defineTerm.getCodedValue());
            }

            if (standardTerm == null && !standardCodeList.isExtensible()) {
                // There was no matching standard term for the term in the Define.xml CodeList and its a Non Extensible Standard CodeList
                // Then all terms in Define.xml CodeList must exist in the Standard CodeList
                // There was no matching standard term in a non extensible codelist
                this.transformer.map("DD0024", Arrays.asList(standardCodeList.getName(), defineVariable.getName()), mapOf(
                    "Standard CodeList", standardCodeList.getName(),
                    "Define CodeList", defineCodeList.getName(),
                    "Define CodedValue", defineTerm.getCodedValue(),
                    "Define Variable", defineVariable.getName()
                ));

            } else if (StringUtils.isBlank(defineTermCode) && !standardCodeList.isExtensible()) {
                 if (isDefine2() && StringUtils.isNotBlank(standardTerm.getCode())) {
                    this.transformer.map("DD0032", Collections.singletonList(defineCodeList.getName()), mapOf(
                        "CodedValue", defineTerm.getCodedValue()
                    ));
                 }
            } else if (StringUtils.isNotBlank(defineTermCode) && !this.terminology.hasNciCode(defineTermCode)) {
                // If the define term has an NCI code, we should check it
                // regardless of whether or not the standard term has a NCI code...
                this.transformer.map("DD0034", Collections.singletonList(defineCodeList.getName()), mapOf(
                    "CodedValue", defineTerm.getCodedValue(),
                    "Code", defineTermCode
                ));
            }

            if (standardTerm == null && standardCodeList.isExtensible()) {
                if (StringUtils.isBlank(defineTerm.getExtendedValue())) {
                    if (isDefine2()) {
                        this.transformer.map("DD0029", mapOf(
                            "CodeList", defineCodeList.getName(),
                            "CodedValue", defineTerm.getCodedValue()
                        ));
                    }
                }
            }

            if (standardTerm != null) {
                String standardTermCode = standardTerm.getCode();

                if (StringUtils.isNotBlank(standardTermCode)) {
                    String defineCodedValue = defineTerm.getCodedValue();
                    String standardCodedValue = standardTerm.getCodedValue();

                    if (StringUtils.isNotBlank(defineCodedValue) && StringUtils.isNotBlank(standardCodedValue)
                            && !defineCodedValue.equals(standardCodedValue)) {
                        this.transformer.map("DD0028", Collections.singletonList(defineCodeList.getName()), mapOf(
                            "Define Term", defineTerm.getCodedValue(),
                            "Define Term Code", defineTermCode,
                            "Standard Term", standardTerm.getCodedValue(),
                            "Standard Term Code", standardTermCode
                        ));
                    }
                }
            }
        }
    }

    @SupplementalValidationRule(id = "DD0035, DD0037")
    private void shouldHavePages(final Object parent, final Object child, final Item sourceItem) {
        if (!isDefine2()) {
            return;
        }

        if (parent instanceof ItemGroup) {
            ItemGroup itemGroup = (ItemGroup) parent;
            Item item = (Item) child;

            if (item.getPages() == null && item.getFirstPage() == null && item.getLastPage() == null) {
                this.transformer.map("DD0035", putLevelAppropriateData(itemGroup, item, sourceItem));
            } else {
                if (StringUtils.isBlank(item.getPages())
                        && (item.getFirstPage() == null && item.getLastPage() == null)) {

                    if (item.getPages() == null) {
                        this.transformer.map("DD0037", putLevelAppropriateData(itemGroup, item, sourceItem));
                    } else {
                        this.transformer.map("DD0035", putLevelAppropriateData(itemGroup, item, sourceItem));
                    }
                } else if ((StringUtils.isBlank(item.getFirstPage()) || StringUtils.isBlank(item.getLastPage()))
                        && item.getPages() == null) {
                    this.transformer.map("DD0037", putLevelAppropriateData(itemGroup, item, sourceItem));
                }
            }
        }
    }

    @SupplementalValidationRule(id = "DD0042")
    private void shouldHaveMethod(final ItemGroup itemGroup, final Item item, final Item sourceItem) {
        if (!isDefine2()) {
            return;
        }
        if (StringUtils.isBlank(item.getMethodOid())) {
            this.transformer.map("DD0042", putLevelAppropriateData(itemGroup, item, sourceItem));
        }
    }

    @ValidationRule(id = "DD0053, DD0069")
    public void validateItemGroupAndItems() {
        for (final ItemGroup itemGroup : this.study.getItemGroups()) {
            checkDuplicateOrderNumbers(itemGroup, null);
            checkPurpose(itemGroup);
            hasDescription(itemGroup);
            hasStructure(itemGroup);
            hasKeyVariablesValue(itemGroup);
            hasValidIsRepeatingValue(itemGroup);
            hasValidIsReferenceDataValue(itemGroup);

            if (!isMissingClass(itemGroup)) {
                validateClass(itemGroup);
            }

            if (this.isDefine2() && itemGroup.getName().startsWith("SUPP")) {
                if (!validateValueLevelMetadata(itemGroup)) {
                    this.transformer.map("DD0038", Collections.singletonList(itemGroup.getName()), mapOf(
                        KEY_DATASET_NAME, itemGroup.getName()
                    ));
                }
            }


            for (final Item item : itemGroup.getItems()) {
                checkLengthAttribute(itemGroup, item, null);
                checkSignificantDigits(itemGroup, item, null);
                checkHasLabel(itemGroup, item);
                hasValidMandatoryValue(itemGroup, item, null);
                validateDataType(itemGroup, item, null);
                validateOrigin(itemGroup, item, null);

                // Also check value level items
                ValueList valueList = this.study.getValueList(itemGroup.getName(), item.getName());
                if (valueList != null) {   // Some items don't have value lists
                    for (final Item vlItem : valueList.getItems()) {
                        checkLengthAttribute(itemGroup, vlItem, item);
                        checkSignificantDigits(itemGroup, vlItem, item);
                        // TODO: Should this be implemented on value level?
//                        checkHasLabel(itemGroup, vlItem);
                        hasValidMandatoryValue(itemGroup, vlItem, item);
                        validateDataType(itemGroup, vlItem, item);
                        validateOrigin(itemGroup, vlItem, item);

                        // TODO: Should this be implemented on value level?
//                        checkDuplicateOrderNumbers(itemGroup, vlItem, vlItemOrderNumbers);
                    }
                }
            }
        }
        if (dd0109) {
            usubjids.forEach((k, v) ->
                transformer.map("DD0109", ImmutableMap.of(
                    KEY_DATASET_NAME, k,
                    "Origin", v
                ))
            );
        }
        if (dd0110) {
            visitnums.forEach((k, v) ->
                transformer.map("DD0110", ImmutableMap.of(
                    KEY_DATASET_NAME, k,
                    "Origin", v
                ))
            );
        }
    }

    @SupplementalValidationRule(id = "DD0003")
    private void hasStructure(final ItemGroup itemGroup) {
        if (StringUtils.isBlank(itemGroup.getStructure())) {
            this.transformer.map("DD0003", Arrays.asList("Structure", "ItemGroupDef"), mapOf(
                "ItemGroupName", itemGroup.getName()
            ));
        }
    }

    private boolean validateValueLevelMetadata(ItemGroup itemGroup) {
        boolean valid = false;
        for (Item item : itemGroup.getItems()) {
            if (item.getName().contains("QVAL")) {
                if (StringUtils.isBlank(item.getValueListOid())) {
                    return false;
                }
                valid = checkValueListItems(this.study.getValueList(item.getValueListOid()));
            }
            if (valid) {
                return true;
            }
        }
        return false;
    }

    private boolean checkValueListItems(ValueList valueList) {
        boolean valid = false;

        if (valueList == null) {
            return false;
        }

        List<Item> items = valueList.getItems();
        if (items.size() == 0) {
            return false;
        }

        //Check the ValueList ItemRefs for at least one QVAL value
        for (Item item : items) {
            if (item.getName().contains("QVAL")) {
                valid = true;
            }
        }
        return valid;
    }

    @SupplementalValidationRule(id = "DD0040")
    public void hasKeyVariablesValue(final ItemGroup itemGroup) {
        if (itemGroup.getKeyVariables().isEmpty()) {
            this.transformer.map("DD0040", mapOf(
                KEY_DATASET_NAME, itemGroup.getName()
            ));
        }
    }

    @ValidationRule(id = "DD0057")
    public void validateMethods() {
        for (Method method : this.study.getMethods()) {
            hasDescription(method);
        }
    }

    @ValidationRule(id = "DD0057")
    public void validateComments() {
        for (Comment comment : this.study.getComments()) {
            hasDescription(comment);
        }
    }

    @SupplementalValidationRule(id = "DD0057")
    private void hasDescription(Object object) {
        if (object instanceof ItemGroup) {
            ItemGroup itemGroup = (ItemGroup) object;

            if (StringUtils.isBlank(itemGroup.getDescription())) {
                this.transformer.map("DD0057", mapOf(
                    KEY_DATASET_NAME, itemGroup.getName()
                ));
            }
        } else if (object instanceof Comment) {
            Comment comment = (Comment) object;

            if (StringUtils.isBlank(comment.getDescription())) {
                this.transformer.map("DD0057", mapOf(
                    "Comment ID", comment.getOid()
                ));
            }
        } else if (object instanceof Method) {
            Method method = (Method) object;

            if (StringUtils.isBlank(method.getDescription())) {
                this.transformer.map("DD0057", mapOf(
                    "Method Name", method.getName()
                ));
            }
        }
    }

    @SupplementalValidationRule(id = "DD0053")
    public void checkPurpose(final ItemGroup itemGroup) {
        String name = itemGroup.getName();
        String purpose = itemGroup.getPurpose();

        if (defineIsADaM()) {
            if (!Objects.equals(purpose, "Analysis")) {
                this.transformer.map("DD0053", mapOf(
                    "Dataset", name,
                    "Purpose", purpose
                ));
            }
        } else if ((defineIsSDTM() || defineIsSEND()) && !Objects.equals(purpose, "Tabulation")) {
            this.transformer.map("DD0053", mapOf(
                "Dataset", name,
                "Purpose", purpose
            ));
        }
    }

    @SupplementalValidationRule(id = "DD0054")
    public boolean isMissingClass(final ItemGroup itemGroup) {
        if (StringUtils.isBlank(itemGroup.getCategory())) {
            this.transformer.map("DD0054", mapOf(
                KEY_DATASET_NAME, itemGroup.getName()
            ));

            return true;
        }
        return false;
    }

    @SupplementalValidationRule(id = "DD0055")
    protected void validateClass(final ItemGroup itemGroup) {
        List<String> classValues;

        if (defineIsADaM()) {
            classValues = metadata.getClasses().adam();
        } else if (defineIsCDISC()) {
            classValues = metadata.getClasses().sdtm();
        } else {
            return;
        }

        if (!classValues.contains(itemGroup.getCategory())) {
            this.transformer.map("DD0055", mapOf(
                KEY_DATASET_NAME, itemGroup.getName(),
                "Class", itemGroup.getCategory()
            ));
        } else if (!defineIsADaM()
                && itemGroup.isSplit(getDefineStandardName())
                && !validClassForSplitDataset(itemGroup.getCategory())) {
            // TODO should this rule go off of the define itemgroups category, or the standard itemgroups?
            // TODO Ignoring suppqual for now since any split suppqual will fire this rule.
            // (SUPPQUALS are all RELATIONSHIP domains)
            if (!StringUtils.defaultString(itemGroup.getName()).toUpperCase().startsWith("SUPP")) {
                this.transformer.map("DD0114", mapOf(
                    "Dataset", itemGroup.getName(),
                    "Class", itemGroup.getCategory()
                ));
            }
        }
    }

    @SupplementalValidationRule(id = "DD0058")
    private void checkHasLabel(final ItemGroup itemGroup, final Item item) {
        // Labels or value level variables is optional, so just ignore them
        if (this.study.getValueList(itemGroup.getName(), item.getName()) == null) {
            if (StringUtils.isBlank(item.getLabel())) {
                this.transformer.map("DD0058", mapOf(
                    //  No need to use putLevelAppropriateData here because this will only ever be variable-level
                    KEY_DATASET_NAME, itemGroup.getName(),
                    KEY_VARIABLE_NAME, item.getName()
                ));
            }
        }
    }


    // This is used to generate data about value level for quick reference for quick reference
    // to reduce the need for iteration over the value level metadata of the define being
    // validated
    // This metadata map should probably be used for other rules (e.g. DD0074),
    // but is currently only used for DD0072 and OD0042
    private HashMap<String, HashMap<String, String>> generateValueLevelMetadataInfo() {
        HashMap<String, HashMap<String, String>> info = new HashMap<>();
        HashMap<String, String> variableHasValueLevelOrigins = new HashMap<>();
        HashMap<String, String> variableHasValueLevelDataTypes = new HashMap<>();

        for (ItemGroup itemGroup: this.study.getItemGroups()) {
            for (Item item : itemGroup.getItems()) {
                if (StringUtils.isNotBlank(item.getValueListOid())) {
                    String itemId = StringUtils.defaultString(item.getOID(), String.format("%s.%s", itemGroup.getDomain(), item.getName()));
                    ValueList valueList = this.study.getValueList(item.getValueListOid());
                    checkDuplicateOrderNumbers(null, valueList);
                    checkOriginAndDataType(valueList, itemId, variableHasValueLevelOrigins, variableHasValueLevelDataTypes);
                }
            }
        }
        info.put("variableHasValueLevelOrigins", variableHasValueLevelOrigins);
        info.put("variableHasValueLevelDataTypes", variableHasValueLevelDataTypes);
        return info;
    }

    private void checkOriginAndDataType(ValueList valueList, String itemId, Map<String, String> originCache, Map<String, String> dataTypeCache) {
        boolean originBreak = false, dataTypeBreak = false;
        if (valueList != null) {
            originCache.put(itemId, "true");
            dataTypeCache.put(itemId, "true");
            if (valueList.getItems().isEmpty()) {
                originCache.put(itemId, "false");
                dataTypeCache.put(itemId, "false");
            } else {
                for (Item vlItem : valueList.getItems()) {
                    if (!originBreak && StringUtils.isBlank(vlItem.getOrigin())) {
                        originCache.put(itemId, "false");
                        originBreak = true;
                    }
                    if (!dataTypeBreak && StringUtils.isBlank(vlItem.getDataType())) {
                        dataTypeCache.put(itemId, "false");
                        dataTypeBreak = true;
                    }
                    if (originBreak && dataTypeBreak) {
                        break;
                    }
                }
            }
        }
    }

    @SupplementalValidationRule(id = "DD0072")
    private boolean originIsMissing(final ItemGroup itemGroup, final Item item, final Item sourceItem) {
        if (sourceItem != null) {
            if (StringUtils.isBlank(item.getOrigin())) {
                if (StringUtils.isBlank(sourceItem.getOrigin())) {
                    transformer.map("DD0072", putLevelAppropriateData(itemGroup, item, sourceItem));
                }

                return true;
            }
        } else {
            if (StringUtils.isBlank(item.getOrigin())) {
                String itemId = StringUtils.defaultString(item.getOID(), String.format("%s.%s", itemGroup.getDomain(), item.getName()));
                Map<String, String> valueLevelOrigins = this.getValueLevelMetadataInfo().get("variableHasValueLevelOrigins");

                if (valueLevelOrigins != null && (valueLevelOrigins.get(itemId) == null ||
                        Objects.equals(this.getValueLevelMetadataInfo().get("variableHasValueLevelOrigins").get(itemId), "false"))) {
                    this.transformer.map("DD0072", putLevelAppropriateData(itemGroup, item, sourceItem));
                }

                return true;
            }
        }

        return false;
    }

    /**
     * Package level access for testing...
     * @param itemGroup parent to both variables.
     * @param item either value list item, or normal dataset item. If normal dataset item, sourceItem will be null.
     * @param sourceItem if validating a value list item, this would be the parent variable. If null, item is normal dataset item.
     */
    @SupplementalValidationRule(id = "OD0075")
    void validateDataType(final ItemGroup itemGroup, final Item item, final Item sourceItem) {
        if (sourceItem != null) { // We are dealing with a value list item then
            if (StringUtils.isBlank(item.getDataType())) { // if the value list item has no data type
                if (StringUtils.isBlank(sourceItem.getDataType())) { // then the source item should have one
                    throwOD0075(itemGroup, item, sourceItem); //if not throw dd0075
                } else {
                    checkIfValidDataTypeValue(itemGroup, sourceItem, sourceItem); //otherwise validate the data type on the source item
                }
            } else {
                checkIfValidDataTypeValue(itemGroup, item, sourceItem); //otherwise validate the data type on the value list item
            }
        } else {
            if (StringUtils.isBlank(item.getDataType())) {
                String itemId = StringUtils.defaultString(item.getOID(), String.format("%s.%s", itemGroup.getDomain(), item.getName()));
                Map<String, String> valueLevelDataTypes = this.getValueLevelMetadataInfo().get("variableHasValueLevelDataTypes");
                if (valueLevelDataTypes != null && (valueLevelDataTypes.get(itemId) == null ||
                        Objects.equals(this.getValueLevelMetadataInfo().get("variableHasValueLevelDataTypes").get(itemId), "false"))) {
                    throwOD0075(itemGroup, item, sourceItem);
                }
            } else {
                checkIfValidDataTypeValue(itemGroup, item, sourceItem); //Handles all non blank cases
            }
        }
    }

    /**
     * Package level access for testing...
     */
    void checkIfValidDataTypeValue(final ItemGroup itemGroup, final Item item, final Item sourceItem) {
        if (!((this.isDefine2() ? V2_VARIABLE_DATA_TYPES : V1_VARIABLE_DATA_TYPES)
                .contains(item.getDataType()))) {
            throwOD0075(itemGroup, item, sourceItem);
        }
    }

    /**
     * Package level access for testing...
     */
    void throwOD0075(final ItemGroup itemGroup, final Item item, final Item sourceItem) {
        this.transformer.map("OD0075", mergeMaps(
            putLevelAppropriateData(itemGroup, item, sourceItem),
            mapOf(
                "Variable OID", item.getOID(),
                "Data Type", item.getDataType()
            )
        ));
    }

    @SupplementalValidationRule(id = "OD0042")
    private void checkDuplicateOrderNumbers(final ItemGroup itemGroup, final ValueList valueList) {
        if (itemGroup == null && valueList == null) {
            LOGGER.trace("ItemGroup and Valuelist were both null!");
            return;
        }
        List<String> itemOrderNumbers = new ArrayList<>();
        boolean isValueLevel = valueList != null;
        List<Item> items = isValueLevel ? valueList.getItems() : itemGroup == null ? Collections.<Item>emptyList() : itemGroup.getItems();
        for (Item item : items) {
            if (StringUtils.isNotBlank(item.getOrder())) {
                if (itemOrderNumbers.contains(item.getOrder())) {
                    this.transformer.map("OD0042", isValueLevel ?
                        mapOf(
                            "ValueList Name", valueList.getOid(),
                            KEY_VARIABLE_NAME, valueList.getSourceItemName(),
                            KEY_WHERE_CLAUSE, getComputedWhereClauseString(item.getWhereClauseOid()),
                            "Order Number", item.getOrder()
                        ) :
                        mapOf(
                            KEY_DATASET_NAME, itemGroup.getName(),
                            KEY_VARIABLE_NAME, item.getName(),
                            "Order Number", item.getOrder()
                        ));
                } else {
                    itemOrderNumbers.add(item.getOrder());
                }
            }
        }
    }

    @SupplementalValidationRule(id = "DD0061, DD0035, DD0037, DD0073, DD0042")
    private void validateOrigin(final ItemGroup itemGroup, final Item item, final Item sourceItem) {
        if (this.valueLevelMetadataInfo == null) {
            this.setValueLevelMetadataInfo(generateValueLevelMetadataInfo());
        }

        if (defineIsSDTM()) {
            validateDayVariable(itemGroup, item, sourceItem);
        }

        if (isDefine2()) {
            if (originIsMissing(itemGroup, item, sourceItem)) {
                return;
            }
        } else {
            if (StringUtils.isBlank(item.getOrigin())) {
               return;
            }
        }

        invalidOriginTypeValue(itemGroup, item, sourceItem);

        if (!DefineMetadata.Origins.CRF.equals(item.getOrigin())) {
            shouldNotHavePages(itemGroup, item, sourceItem);
        }

        if (DefineMetadata.Origins.PREDECESSOR.equals(item.getOrigin())) {
            if (StringUtils.isBlank(item.getPredecessor())) {
                this.transformer.map("DD0061", putLevelAppropriateData(itemGroup, item, sourceItem));
            }
        } else if (DefineMetadata.Origins.CRF.equals(item.getOrigin())) {
            shouldHavePages(itemGroup, item, sourceItem);
        } else if ((defineIsSEND() && DefineMetadata.Origins.DERIVED.toUpperCase().equals(item.getOrigin())) ||
                DefineMetadata.Origins.DERIVED.equals(item.getOrigin())) {
            shouldHaveMethod(itemGroup, item, sourceItem);
        }
    }

    private void shouldNotHavePages(ItemGroup itemGroup, Item item, Item sourceItem) {
        // The Define 1 version of this check has to be xPath due to our parser limitations...
        if (isDefine2() && StringUtils.isNotBlank(item.getPages()) || StringUtils.isNotBlank(item.getFirstPage()) || StringUtils.isNotBlank(item.getLastPage())) {
            this.transformer.map("DD0111", mergeMaps(
                putLevelAppropriateData(itemGroup, item, sourceItem),
                mapOf("Variable Origin", item.getOrigin())
            ));
        }
    }

    private void validateDayVariable(final ItemGroup itemGroup, final Item item, final Item sourceItem) {
        //Some examples are AEDY, AESTDY, EXENDY
        String regex = String.format("%s$|%s$|%s$",
                StringUtils.defaultString(itemGroup.getName()).toUpperCase() + "DY",
                ".*STDY",
                ".*ENDY");
        String itemName =  StringUtils.defaultString(item.getName());
        if (itemName.toUpperCase().matches(regex)
                && !DefineMetadata.Origins.DERIVED.equals(item.getOrigin())) {
            this.transformer.map("DD0105", Collections.singletonList(itemName), mergeMaps(
                putLevelAppropriateData(itemGroup, item, sourceItem),
                mapOf(
                    "Current Origin", item.getOrigin(),
                    "Required Origin", DefineMetadata.Origins.DERIVED
                )
            ));
        }
    }

    @SupplementalValidationRule(id = "DD0068, OD0070")
    private void checkLengthAttribute(final ItemGroup itemGroup, final Item item, final Item sourceItem) {
        List<String> dataTypes = Arrays.asList(
                DefineMetadata.DataTypes.INTEGER,
                DefineMetadata.DataTypes.FLOAT,
                DefineMetadata.DataTypes.TEXT);

        if (!dataTypes.contains(item.getDataType())) {
            if (StringUtils.isNotBlank(item.getLength())) {
                this.transformer.map("DD0068", mergeMaps(
                    putLevelAppropriateData(itemGroup, item, sourceItem),
                    mapOf(
                        "Variable OID", item.getOID(),
                        "DataType", item.getDataType(),
                        "Length", item.getLength()
                    )
                ));
            }
        } else {    // numbers and text datatypes must have length
            if (StringUtils.isBlank(item.getLength())) {
                this.transformer.map("OD0070", mergeMaps(
                    putLevelAppropriateData(itemGroup, item, sourceItem),
                    mapOf("DataType", item.getDataType())
                ));
            }
        }
    }

    @SupplementalValidationRule(id = "DD0069, OD0071")
    private void checkSignificantDigits(final ItemGroup itemGroup, final Item item, final Item sourceItem) {
        if (!DefineMetadata.DataTypes.FLOAT.equals(item.getDataType())) {
            if (StringUtils.isNotBlank(item.getSignificantDigits())) {
                this.transformer.map("DD0069", mergeMaps(
                    putLevelAppropriateData(itemGroup, item, sourceItem),
                    mapOf(
                        "DataType", item.getDataType(),
                        "SignificantDigits", item.getSignificantDigits()
                    )
                ));
            }
        } else {    // type is float, so must have significant digits
            if (StringUtils.isBlank(item.getSignificantDigits())) {
                this.transformer.map("OD0071", mergeMaps(
                    putLevelAppropriateData(itemGroup, item, sourceItem),
                    mapOf("DataType", item.getDataType())
                ));
            }
        }
    }

    @SupplementalValidationRule(id = "DD0073")
    private void invalidOriginTypeValue(final ItemGroup itemGroup, final Item item, final Item sourceItem) {
        if (!isDefine2()) {
            return;
        }

        List<String> generalOrigins = Arrays.asList("CRF", "Derived", "Assigned", "Protocol", "eDT", "Predecessor");
        List<String> sendOrigins = Arrays.asList("COLLECTED", "DERIVED", "OTHER", "NOT AVAILABLE");
        List<String> origins;

        //Send has specific origins and the others are not allowed.
        //See DEF-164
        if (defineIsSEND()) {
            origins = sendOrigins;
        } else {
            origins = generalOrigins;
        }

        if (!origins.contains(item.getOrigin())) {
            this.transformer.map("DD0073", mergeMaps(
                putLevelAppropriateData(itemGroup, item, sourceItem),
                mapOf("Origin", item.getOrigin())
            ));
        } else {
            validateSpecificVariables(itemGroup, item);
        }
    }

    private void validateSpecificVariables(ItemGroup itemGroup, Item item) {
        Map<String, String> metadata = ImmutableMap.of(KEY_DATASET_NAME, itemGroup.getName(), "Origin", item.getOrigin());
        if ("DOMAIN".equalsIgnoreCase(item.getName())) {
            if (StringUtils.isNotBlank(item.getOrigin()) && !DefineMetadata.Origins.ASSIGNED.equals(item.getOrigin())
                    && !defineIsSEND()) {
                transformer.map("DD0106", metadata);
            }
        } else if ("RDOMAIN".equalsIgnoreCase(item.getName())) {
            if (StringUtils.isNotBlank(item.getOrigin()) && !DefineMetadata.Origins.ASSIGNED.equals(item.getOrigin())
                    && !defineIsSEND()) {
                transformer.map("DD0107", metadata);
            }
        } else if ("STUDYID".equalsIgnoreCase(item.getName()) && !defineIsADaM()) {
            if (StringUtils.isNotBlank(item.getOrigin()) && !DefineMetadata.Origins.PROTOCOL.equals(item.getOrigin())
                    && !defineIsSEND()) {
                transformer.map("DD0108", metadata);
            }
        } else if ("USUBJID".equalsIgnoreCase(item.getName())) {
            usubjids.put(itemGroup.getName(), item.getOrigin());
            if (StringUtils.isBlank(usubjidOrigin)) {
                usubjidOrigin = item.getOrigin();
            } else if (!dd0109 && !usubjidOrigin.equals(item.getOrigin())) {
                dd0109 = true;
            }
        } else if ("VISITNUM".equalsIgnoreCase(item.getName())) {
            visitnums.put(itemGroup.getName(), item.getOrigin());
            if (StringUtils.isBlank(visitnumOrigin)) {
                visitnumOrigin = item.getOrigin();
            } else if (!dd0110 && !visitnumOrigin.equals(item.getOrigin())) {
                dd0110 = true;
            }
        } else if ("FATESTCD".equalsIgnoreCase(item.getName())) {
            if (StringUtils.isNotBlank(item.getCodeListOid())) {
                CodeList fatestcd = this.study.getCodeList(item.getCodeListOid());
                if (fatestcd != null) {
                    for (CodeListItem clItem : fatestcd.getCodelistItems()) {
                        //fatestCD only look against EVENTS and INTERVENTIONS
                        if (standard.isEventsOrInterventionsVariable(clItem)) {
                            Item standardVariable = standard.getVariableFromEventsOrInterventions(clItem);
                            if (standardVariable != null) {
                                if (!Objects.equals(clItem.getDecodedValue(), standardVariable.getLabel())) {
                                    this.transformer.map("DD0116", Collections.singletonList(fatestcd.getName()), mapOf(
                                        "Coded Value", clItem.getCodedValue(),
                                        "Decoded Value", clItem.getDecodedValue(),
                                        "Expected Decoded Value", standardVariable.getLabel()
                                    ));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @ValidationRule(id = "DD0074")
    public void executeDD0074() {
        if (!isDefine2()) {
            return;
        }

        for (final ValueList valueList : this.study.getValueLists()) {
            for (final Item valueListItem : valueList.getItems()) {
                Item item = this.study.getItem(valueList.getSourceItemGroupName(), valueList.getSourceItemName());

                if (item == null) {
                    continue;
                }

                if (StringUtils.isNotBlank(item.getOrigin()) && StringUtils.isNotBlank(valueListItem.getOrigin())) {
                    if (!item.getOrigin().equals(valueListItem.getOrigin())) {
                        this.transformer.map("DD0074", mapOf(
                            KEY_DATASET_NAME, valueList.getSourceItemGroupName(),
                            KEY_VARIABLE_NAME, item.getName(),
                            "Variable Origin", item.getOrigin(),
                            "ValueLevel Variable Where Clause", getComputedWhereClauseString(valueListItem.getWhereClauseOid()),
                            "ValueLevel Variable Origin", valueListItem.getOrigin()
                        ));
                    }
                }
            }
        }
    }

    /*
     *   INITIAL REWRITE OF SOME RULES TO RUN AGAINST MODELS...These can be pasted into correct order once we get them done
     *   TODO: WILL THIS EVEN WORK? MOST OF THESE RULES WON'T WORK IF THE MODEL IS POPULATED FROM DB?
     */
    @ValidationRule(id = "DD0077, OD0077")
    public void codeListChecks() {
        for (final CodeList codeList : this.study.getCodeLists()) {
            final String codeListDataType = codeList.getDataType();
            hasValidCodelistDataType(codeList);

            int numItemsWithOrder = 0;
            int numItems = codeList.getCodelistItems().size();
            for (final CodeListItem term : codeList.getCodelistItems()) {

                if (StringUtils.isNotBlank(term.getOrder())) {
                    numItemsWithOrder++;
                }

                if (codeListDataType != null) {
                    switch (codeListDataType) {
                        case "integer":
                            try {
                                new BigInteger(term.getCodedValue());
                            } catch (NumberFormatException nfe) {
                                this.transformer.map("OD0077", mapOf(
                                    "Codelist ID", codeList.getOid(),
                                    "Codelist DataType", codeListDataType,
                                    "Term", term.getCodedValue()
                                ));
                            }
                            break;
                        case "float":
                            try {
                                Float.parseFloat(term.getCodedValue());
                            } catch (NumberFormatException nfe) {
                                this.transformer.map("OD0077", mapOf(
                                    "Codelist ID", codeList.getOid(),
                                    "Codelist DataType", codeListDataType,
                                    "Term", term.getCodedValue()
                                ));
                            }
                            break;
                        default:
                            break;
                    }
                }
            }

            // If any items have order, all items must have order
            if (numItemsWithOrder > 0 && numItems != numItemsWithOrder) {
                this.transformer.map("DD0077", mapOf(
                    "CodeList Name", codeList.getName()
                ));
            }
        }
    }

    // TODO: Might also want to add DD0078 here
    // TODO: Also need to check back references here like dd0079 and dd0071
    @ValidationRule(id = "DD0016,DD0071,DD0079,DD0080,DD0082,OD0048")
    public void collectObjectReferences() {
        Set<String> commentRefs = new HashSet<>();
        Set<String> methodRefs = new HashSet<>();
        Set<String> codeListRefs = new HashSet<>();

        for (ItemGroup itemGroup : this.study.getItemGroups()) {
            if (StringUtils.isNotBlank(itemGroup.getCommentOid())) {
                commentRefs.add(itemGroup.getCommentOid());
            }

            for (Item item : itemGroup.getItems()) {
                //Rules involving terminology and standard
                invalidTermInCodeList(itemGroup, item);
            }
        }

        for (Item item : this.study.getItems()) {
            if (StringUtils.isNotBlank(item.getCommentOid())) {
                commentRefs.add(item.getCommentOid());
            }
            if (StringUtils.isNotBlank(item.getMethodOid())) {
                methodRefs.add(item.getMethodOid());
            }
            if (StringUtils.isNotBlank(item.getCodeListOid())) {
                codeListRefs.add(item.getCodeListOid());
            }

            if (StringUtils.isNotBlank(item.getValueListOid())
                    && this.study.getValueList(item.getValueListOid()) != null) {  // has value Level
                for (Item valueLevelItem : this.study.getValueList(item.getValueListOid()).getItems()) {
                    if (StringUtils.isNotBlank(valueLevelItem.getCommentOid())) {
                        commentRefs.add(valueLevelItem.getCommentOid());
                    }

                    if (StringUtils.isNotBlank(valueLevelItem.getMethodOid())) {
                        methodRefs.add(valueLevelItem.getMethodOid());
                    }

                    if (StringUtils.isNotBlank(valueLevelItem.getCodeListOid())) {
                        codeListRefs.add(valueLevelItem.getCodeListOid());
                    }

                    // Also need to check the where clause for comments (Not clear in rule)
                    if (StringUtils.isNotBlank(valueLevelItem.getWhereClauseOid())) {
                        WhereClause whereClause = this.study.getWhereClause(valueLevelItem.getWhereClauseOid());

                        if (whereClause != null && StringUtils.isNotBlank(whereClause.getCommentOid())) {
                            commentRefs.add(whereClause.getCommentOid());
                        }
                    }
                }
            }
        }

        // ARM can also contain comments
        for (ResultDisplay rd : this.study.getResultDisplays()) {
            for (AnalysisResult analysisResult : rd.getAnalysisResults()) {
                AnalysisDatasets analysisDatasets = analysisResult.getAnalysisDatasets();
                if (analysisDatasets != null
                        && analysisDatasets.getComment() != null
                        && StringUtils.isNotBlank(analysisDatasets.getComment().getOid())) {
                    commentRefs.add(analysisDatasets.getComment().getOid());
                }
            }
        }

        checkCommentReferences(commentRefs);
        checkMethodReferences(methodRefs);
        checkCodeListReferences(codeListRefs);
    }

    @SupplementalValidationRule(id = "DD0071,DD0079")
    public void checkCommentReferences(Set<String> commentRefs) {
        if (!isDefine2()) {
            return;
        }

        for (Comment comment : this.study.getComments()) {
            final String commentOID = comment.getOid();

            if (StringUtils.isNotBlank(commentOID)) {
                if (!commentRefs.contains(commentOID)) {
                    this.transformer.map("DD0079", Collections.singletonList(commentOID), mapOf(
                        "Comment", commentOID
                    ));
                } else {
                    // comment ref was valid
                    commentRefs.remove(commentOID);
                }
            }
        }

        // Comment Refs left means that comment definition does not exist
        for (final String commentOID : commentRefs) {
            this.transformer.map("DD0071", mapOf(
                "Comment", commentOID
            ));
        }
    }

    @SupplementalValidationRule(id = "DD0016,DD0080")
    public void checkMethodReferences(Set<String> methodRefs) {
        for (Method method : this.study.getMethods()) {
            String methodOID = method.getOid();

            if (StringUtils.isNotBlank(methodOID)) {
                if (!methodRefs.contains(methodOID)) {
                    this.transformer.map("DD0080", Collections.singletonList(methodOID), mapOf(
                        "Method", methodOID
                    ));
                } else {
                    // method ref was valid
                    methodRefs.remove(methodOID);
                }
            }
        }

        // Method Refs left means that method definition does not exist
        for (final String methodOID : methodRefs) {
            this.transformer.map("DD0016", mapOf(
                "Method", methodOID
            ));
        }
    }

    @SupplementalValidationRule(id = "OD0048,DD0082")
    public void checkCodeListReferences(Set<String> codeListRefs) {
        Set<String> roleCodelistOIDs = this.study.getRoleCodelistOids();

        for (CodeList codeList : this.study.getCodeLists()) {
            String codeListOID = codeList.getOid();

            if (StringUtils.isNotBlank(codeListOID)) {

                // If codelist references external dictionary, we ignore these rules
                if (!codeListRefs.contains(codeListOID) && StringUtils.isBlank(codeList.getDictionary())) {
                    // Don't throw issue if the codelist was referenced by an itemGroup with roleCodelist
                    if (!roleCodelistOIDs.contains(codeListOID)) {
                        this.transformer.map("DD0082", Collections.singletonList(codeListOID), mapOf(
                            "CodeList Name", codeListOID
                        ));
                    }
                } else {
                    // codeList ref was valid
                    codeListRefs.remove(codeListOID);
                }
            }
        }

        // CodeList Refs left means that codeList definition does not exist
        for (final String codeListOID : codeListRefs) {
            // Check if the codelist was parsed as a dictionary (external codelists)
            if (this.study.getDictionary(codeListOID) == null) {
                this.transformer.map("OD0048", mapOf(
                    "Codelist", codeListOID
                ));
            }
        }
    }

    @ValidationRule(id = "DD0112")
    public void checkMeddra() {
        if (!defineIsSDTM()) {
            return;
        }
        for (Dictionary dictionary : this.study.getDictionaries()) {
            if (!referencesMeddra && StringUtils.isNotBlank(dictionary.getDictionary())) {
                referencesMeddra = DefineMetadata.Dictionaries.MEDDRA.equalsIgnoreCase(dictionary.getDictionary());
                meddraCodelistId = dictionary.getOid(); //oid returns the codelist ID, which is what other items would reference...
            }
            if (referencesMeddra) {
                break;
            }
        }
        if (!referencesMeddra) {
            this.transformer.map("DD0112");
        } else {
            checkMeddraReferences();
        }
    }

    private void checkMeddraReferences() {
        String itemRegex = "^(AE|MH)(LLT|LLTCD|DECOD|PTCD|HLT|HLTCD|HLGT|HLGTCD|BODSYS|BDSYCD|SOC|SOCCD)$";
        //Per DEF-192 only run this check on AE and MH for now
        ItemGroup itemGroup = this.study.getItemGroup("AE");
        List<Item> items = new ArrayList<>();
        if (itemGroup != null) {
            items.addAll(itemGroup.getItems());
        }
        itemGroup = this.study.getItemGroup("MH");
        if (itemGroup != null) {
            items.addAll(itemGroup.getItems());
        }
        for (Item item : items) {
            if (StringUtils.defaultString(item.getName()).matches(itemRegex)) {
                //Items could either reference the wrong dictionary or a codelist erroneously...
                if (StringUtils.isNotBlank(item.getCodeListOid()) && !meddraCodelistId.equals(item.getCodeListOid())) {
                    this.transformer.map("DD0113", Collections.singletonList(item.getName()), mapOf(
                        "Variable", item.getName(),
                        "Expected Dictionary ID", meddraCodelistId,
                        "Referenced Codelist ID", item.getCodeListOid()
                    ));
                } else if (StringUtils.isNotBlank(item.getDictionaryOid()) && !meddraCodelistId.equals(item.getDictionaryOid())) {
                    this.transformer.map("DD0113", Collections.singletonList(item.getName()), mapOf(
                        "Variable", item.getName(),
                        "Expected Dictionary ID", meddraCodelistId,
                        "Referenced Dictionary ID", item.getDictionaryOid()
                    ));
                }
            }
        }
    }

    @SupplementalValidationRule(id = "OD0072")
    private void hasValidIsRepeatingValue(final ItemGroup itemGroup) {
        if (!YN_VALUES.contains(itemGroup.getIsRepeating())) {
            this.transformer.map("OD0072", mapOf(
                KEY_DATASET_NAME, itemGroup.getName(),
                "Repeating", itemGroup.getIsRepeating()
            ));
        }
    }

    @SupplementalValidationRule(id = "OD0073")
    protected void hasValidIsReferenceDataValue(final ItemGroup itemGroup) {
        if (StringUtils.isNotBlank(itemGroup.getIsReferenceData())
                && !YN_VALUES.contains(itemGroup.getIsReferenceData())) {
            this.transformer.map("OD0073", mapOf(
                KEY_DATASET_NAME, itemGroup.getName(),
                "Reference Data", itemGroup.getIsReferenceData()
            ));
        }
    }

    @SupplementalValidationRule(id = "OD0074")
    private void hasValidMandatoryValue(final ItemGroup itemGroup, final Item item, final Item sourceItem) {
        if (!YN_VALUES.contains(item.getMandatory())) {
            this.transformer.map("OD0074", mergeMaps(
                putLevelAppropriateData(itemGroup, item, sourceItem),
                mapOf("Mandatory", item.getMandatory())
            ));
        }
    }

    @SupplementalValidationRule(id = "OD0076")
    public void hasValidCodelistDataType(final CodeList codelist) {
        if (!CODELIST_DATA_TYPES.contains(codelist.getDataType())) {
            this.transformer.map("OD0076", mapOf(
                "Codelist", codelist.getOid(),
                "Data Type", codelist.getDataType()
            ));
        }
    }

    @ValidationRule(id = "DD0078")
    public void unreferencedDocument() {
        Map<String, Document> documentIds = new HashMap<>();

        for (Document document : this.study.getDocuments()) {
            documentIds.put(document.getOid(), document);
        }

        if (documentIds.isEmpty()) {
            return;
        }

        for (Comment comment : this.study.getComments()) {
            if (documentIds.isEmpty()) {
                return;
            }

            removeObjectFromByString(comment.getDocumentOid(), documentIds);
        }

        for (Method method : this.study.getMethods()) {
            if (documentIds.isEmpty()) {
                return;
            }

            removeObjectFromByString(method.getDocumentOid(), documentIds);
        }

        for (Document document : this.study.getSupplementalDocuments()) {
            if (documentIds.isEmpty()) {
                return;
            }

            removeDocumentFrom(document, documentIds);
        }

        for (Document document : this.study.getAnnotatedCrfs()) {
            if (documentIds.isEmpty()) {
                return;
            }

            removeDocumentFrom(document, documentIds);
        }

        for (ResultDisplay display : this.study.getResultDisplays()) {

            if (display.getDocumentReference().getDocument() != null) {
                removeDocumentFrom(display.getDocumentReference().getDocument(), documentIds);
            }

            for (AnalysisResult result : display.getAnalysisResults()) {
                List<DocumentReference> refs = result.getDocumentation().getDocumentReferences();

                if (!refs.isEmpty() &&
                        (result.getDocumentation().getDescription() == null
                                || StringUtils.isBlank(result.getDocumentation().getDescription().getText()))) {
                    this.transformer.map("DD0117", mapOf("Analysis Result", result.getOid()));
                }

                for (DocumentReference ref : refs) {
                    if (ref != null && ref.getDocument() != null) {
                        removeDocumentFrom(ref.getDocument(), documentIds);
                    }

                    ProgrammingCode code = result.getProgrammingCode();
                    ref = code.getDocumentReference();

                    if (ref != null && ref.getDocument() != null) {
                        removeDocumentFrom(ref.getDocument(), documentIds);
                    }
                }
            }
        }

        for (final Document document : documentIds.values()) {
            if (StringUtils.isNotBlank(document.getOid())) {
                this.transformer.map("DD0078", Collections.singletonList(document.getOid()), mapOf(
                    "Document Title", document.getTitle()
                ));
            }
        }
    }

    private void removeObjectFromByString(String documentOid, Map collection) {
        if (StringUtils.isNotBlank(documentOid)) {
            collection.remove(documentOid);
        }
    }

    private void removeDocumentFrom(Document document, Map collection) {
        if (document != null && StringUtils.isNotBlank(document.getOid())) {
            collection.remove(document.getOid());
        }
    }

    @ValidationRule(id = "DD0102")
    public void executeDD00102() {
        if (Arrays.asList(metadata.getStandards().adam(), metadata.getStandards().send()).contains(getDefineStandardName())) {
            return;
        }

        boolean namedProperly = false;

        for (Document document : this.study.getAnnotatedCrfs()) {
            if ("acrf.pdf".equals(document.getHref())) {
                namedProperly = true;
                break;
            }
        }

        if (!namedProperly) {
            this.transformer.map("DD0102");
        }
    }

    @ValidationRule(id = "DD0104, DD0003")
    public void executeDD00104() {
        if (!isDefine2()) {
            return;
        }

        List<String> standardsToSkip = Arrays.asList(metadata.getStandards().adam(), metadata.getStandards().send());
        for (final Method method : this.study.getMethods()) {
            if (!standardsToSkip.contains(getDefineStandardName())) {
                if (StringUtils.isBlank(method.getName())) {
                    this.transformer.map("DD0003", Arrays.asList("Name", "MethodRef"), mapOf(
                        "Method OID", method.getOid()
                    ));
                }
            }

            if (StringUtils.isBlank(method.getType()) || !METHOD_TYPES.contains(method.getType())) {
                this.transformer.map("DD0104", mapOf(
                    "Method OID", method.getOid(),
                    "Method Type", method.getType()
                ));
            }
        }
    }

    @ValidationRule(id = "OD0080")
    public void executeOD0080() {
        Map<String, String> codeListDatatypes = new HashMap<>();

        for (CodeList codeList : this.study.getCodeLists()) {
            // Only check codelists that are not external dictionaries
            if (StringUtils.isBlank(codeList.getDictionary())) {
                codeListDatatypes.put(codeList.getOid(), codeList.getDataType());
            }
        }

        for (final ItemGroup itemGroup : this.study.getItemGroups()) {
            for (final Item item : itemGroup.getItems()) {
                String codeListDataType = codeListDatatypes.get(item.getCodeListOid());

                if (StringUtils.isNotBlank(codeListDataType) && !codeListDataType.equals(item.getDataType())) {
                    this.transformer.map("OD0080", mapOf(
                        KEY_DATASET_NAME, itemGroup.getName(),
                        KEY_VARIABLE_NAME, item.getName(),
                        "Variable DataType", item.getDataType(),
                        "CodeList ID", item.getCodeListOid(),
                        "CodeList DataType", codeListDataType
                    ));
                }
            }
        }

        for (final ValueList valueList : this.study.getValueLists()) {
            for (final Item item : valueList.getItems()) {
                String codeListDataType = codeListDatatypes.get(item.getCodeListOid());

                if (StringUtils.isNotBlank(codeListDataType) && !codeListDataType.equals(item.getDataType())) {
                    this.transformer.map("OD0080", mapOf(
                        KEY_DATASET_NAME, valueList.getOid(),
                        KEY_VARIABLE_NAME, valueList.getSourceItemName(),
                        KEY_WHERE_CLAUSE, getComputedWhereClauseString(item.getWhereClauseOid()),
                        "Variable DataType", item.getDataType(),
                        "CodeList ID", item.getCodeListOid(),
                        "CodeList DataType", codeListDataType
                    ));
                }
            }
        }
    }

    @SupplementalValidationRule(id = "OD0081")
    public void checkEmptyCodelist(final CodeList codelist) {
        // no items and no external dictionary = empty
        if (codelist.getCodelistItems().isEmpty() && StringUtils.isBlank(codelist.getDictionary())) {
            this.transformer.map("OD0081", Collections.singletonList(codelist.getOid()));
        }
    }

    @ValidationRule(id = "OD0082")
    public void executeOD0082() {
        for (final CodeList codeList : this.study.getCodeLists()) {
            List<CodeListItem> enumeratedItems = new ArrayList<>();
            boolean decodedValueFound = false;
            checkEmptyCodelist(codeList);

            for (CodeListItem codeListItem : codeList.getCodelistItems()) {

                if (StringUtils.isNotBlank(codeListItem.getDecodedValue())) {
                    decodedValueFound = true;
                } else if (StringUtils.isBlank(codeListItem.getDecodedValue()) && StringUtils.isNotBlank(codeListItem.getCodedValue())) {
                    enumeratedItems.add(codeListItem);
                }
            }

            if (decodedValueFound && !enumeratedItems.isEmpty()) {
                this.transformer.map("OD0082", Collections.singletonList(codeList.getOid()), mapOf(
                    "CodeList ID", codeList.getOid()
                ));
            }
        }
    }

    @ValidationRule(id = "DD0115")
    public void splitOrUnsplitOnly() {
        final String standardName = getDefineStandardName();
        for (ItemGroup itemGroup : this.study.getItemGroups()) {
            if (itemGroup.isSplit(standardName)) {
                ItemGroup nonSplitIg;

                try {
                    String name = StringUtils.defaultString(itemGroup.getName());

                    if (!name.startsWith("SUPP")) {
                        nonSplitIg = this.study.getItemGroup(name.substring(0, 2));
                    } else {
                        nonSplitIg = this.study.getItemGroup(name.substring(0, 6));
                    }
                } catch (IndexOutOfBoundsException ignore) {
                    nonSplitIg = null;
                }

                if (nonSplitIg != null) {
                    this.transformer.map("DD0115", mapOf(
                        "Split Dataset Name", itemGroup.getName(),
                        "Full Dataset Name", nonSplitIg.getName()
                    ));
                }
            }
        }
    }

    private boolean defineIsCDISC() {
        return defineIsADaM() || defineIsSDTM() || defineIsSEND();
    }

    private boolean defineIsADaM() {
        return getDefineStandardName().equals(metadata.getStandards().adam());
    }

    private boolean defineIsSEND() {
        return getDefineStandardName().equals(metadata.getStandards().send());
    }

    private boolean defineIsSDTM() {
        return getDefineStandardName().equals(metadata.getStandards().sdtm());
    }

    private String getDefineStandardVersion() {
        return this.study.getStandardVersion();
    }

    public boolean isDefine2() {
        return this.metadata == DefineMetadata.V2;
    }

    private String getDefineStandardName() {
        return StringUtils.defaultString(this.study.getStandardName());
    }

    private boolean isValidStandardName(String standardName) {
        return StringUtils.isNotBlank(standardName) && metadata.getStandards().standardToVersions().containsKey(standardName);
    }

    private boolean isValidStandardVersion(String standardName, String standardVersion) {
        return metadata.getStandards().standardToVersions().containsKey(standardName)
                && metadata.getStandards().standardToVersions().get(standardName).contains(standardVersion);
    }

    HashMap<String, HashMap<String, String>> getValueLevelMetadataInfo() {
        return valueLevelMetadataInfo;
    }

    private void setValueLevelMetadataInfo(HashMap<String, HashMap<String, String>> valueLevelMetadataInfo) {
        this.valueLevelMetadataInfo = valueLevelMetadataInfo;
    }

    /**
     * Class values allowed according to define.xml 2.0 spec.
     * SDTM and SEND: {@code
     *  SPECIAL PURPOSE,
     *  FINDINGS,
     *  EVENTS,
     *  INTERVENTIONS,
     *  TRIAL DESIGN,
     *  RELATIONSHIP}
     * ADaM: {@code
     *  SUBJECT LEVEL,
     *  ANALYSIS DATASET,
     *  BASIC DATA STRUCTURE,
     *  ADAM OTHER}
     * @param clazz a class value of a dataset
     * @return true if class is one of {@code FINDINGS, EVENTS, or INTERVENTIONS}. false otherwise.
     */
    private static boolean validClassForSplitDataset(String clazz) {
        return Arrays.asList(DefineMetadata.Categories.FINDINGS, DefineMetadata.Categories.EVENTS,
                DefineMetadata.Categories.INTERVENTIONS).contains(clazz);
    }

    /**
     * Fetches a user-friendly String representation of a where clause, given a where clause OID.
     * @param whereClauseOid
     * @return
     */
    String getComputedWhereClauseString(String whereClauseOid) {
        String result = null;
        if (StringUtils.isEmpty(whereClauseOid)) {
            LOGGER.warn("getComputedWhereClauseString was supplied with empty whereClauseOid.  Returning null result.");
        } else if (this.study != null) {
            WhereClause whereClause = this.study.getWhereClause(whereClauseOid);
            if (null != whereClause) {
                //  e.g. "ADLB.PARAMCD.EQ.ALK"
                result = whereClause.toString();
            }
        }

        return result;
    }

    boolean isItemValueLevel(Item item) {
        //  Check whether the item has a where clause
        return !StringUtils.isBlank(item.getWhereClauseOid());
    }

    /**
     * Takes data for input into MessageTransformer and puts data appropriate to the level of the item (i.e. the
     * user-friendly where clause for a value-level item or just the dataset name and variable name for a variable-level
     * item)
     * @param itemGroup
     * @param item
     * @param sourceItem
     * @return Map of level appropriate Issue details
     */
    Map<String, String> putLevelAppropriateData(ItemGroup itemGroup, Item item, Item sourceItem) {
        Map<String, String> details = new LinkedHashMap<>();
        if (isItemValueLevel(item)) {
            details.put(KEY_DATASET_NAME, itemGroup.getName());
            if (null != sourceItem && null != sourceItem.getName()) {
                details.put(KEY_VARIABLE_NAME, sourceItem.getName());
            }
            details.put(KEY_WHERE_CLAUSE, getComputedWhereClauseString(item.getWhereClauseOid()));
        } else {
            details.put(KEY_DATASET_NAME, itemGroup.getName());
            details.put(KEY_VARIABLE_NAME, item.getName());
        }

        return details;
    }

    Map<String, String> mergeMaps(Map<String, String> firstMap, Map<String, String> secondMap) {
        Map<String, String> newMap = new LinkedHashMap<>(firstMap); // In case of immutable map
        newMap.putAll(secondMap);

        return newMap;
    }
}
