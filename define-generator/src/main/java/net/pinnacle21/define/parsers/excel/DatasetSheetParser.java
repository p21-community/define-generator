/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.ItemGroup;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.NamedRow;
import net.pinnacle21.parsing.excel.ValidationAwareExcelSheetParser;
import org.apache.commons.lang3.StringUtils;

public class DatasetSheetParser extends ValidationAwareExcelSheetParser<ParsedStudy> {
    static final String NAME_COLUMN = "Dataset";
    static final String DESCRIPTION_COLUMN = "Description";
    static final String CLASS_COLUMN = "Class";
    static final String STRUCTURE_COLUMN = "Structure";
    static final String PURPOSE_COLUMN = "Purpose";
    static final String KEYS_COLUMN = "Key Variables";
    static final String REPEATING_COLUMN = "Repeating";
    static final String REFERENCE_DATA_COLUMN = "Reference Data";
    static final String COMMENT_COLUMN = "Comment";
    static final String DUPLICATE_ERROR = "The domain '%s' is already defined, duplicates are not allowed";
    static final String COMMENT_REF_ERROR = "'%s' references the comment '%s', but that comment was not found";

    DatasetSheetParser() {
        this.addValidatedRequiredColumn(NAME_COLUMN);
        this.addValidatedColumn(DESCRIPTION_COLUMN, FieldValidations.MEDIUM_LENGTH);
        this.addValidatedColumn(CLASS_COLUMN);
        this.addValidatedColumn(STRUCTURE_COLUMN, FieldValidations.MEDIUM_LENGTH);
        this.addValidatedColumn(PURPOSE_COLUMN, FieldValidations.MEDIUM_LENGTH);
        this.addValidatedColumn(KEYS_COLUMN);
        this.addValidatedColumn(REPEATING_COLUMN);
        this.addValidatedColumn(REFERENCE_DATA_COLUMN);
        this.addValidatedColumn(COMMENT_COLUMN);
    }

    @Override
    public void parse(NamedRow row, ParsedStudy parsedStudy) {
        Study study = parsedStudy.getStudy();
        ItemGroup itemGroup = new ItemGroup();
        String id = StringUtils.trim(StringUtils.defaultString(row.get(NAME_COLUMN)));

        if (!this.validate(row, parsedStudy)) {
            //DEF-172 Fallback
            itemGroup.setName(id);
            study.setItemGroup(id, itemGroup);
            return;
        }

        if (study.hasItemGroup(id)) {
            parsedStudy.error(row.getIdentifier(), DUPLICATE_ERROR, id);

            return;
        }

        itemGroup.setName(id);
        itemGroup.setDescription(row.get(DESCRIPTION_COLUMN));
        itemGroup.setCategory(row.get(CLASS_COLUMN));
        itemGroup.setStructure(row.get(STRUCTURE_COLUMN));
        itemGroup.setPurpose(row.get(PURPOSE_COLUMN));
        itemGroup.setIsRepeating(row.get(REPEATING_COLUMN));
        itemGroup.setIsReferenceData(row.get(REFERENCE_DATA_COLUMN));

        String commentRef = StringUtils.trim(row.get(COMMENT_COLUMN));

        if (StringUtils.isNotBlank(commentRef)) {
            if (study.hasComment(commentRef)) {
                itemGroup.setCommentOid(commentRef);
            } else {
                parsedStudy.error(row.getIdentifier(), COMMENT_REF_ERROR, COMMENT_COLUMN, commentRef);
            }
        }

        String keyVariables = row.get(KEYS_COLUMN);

        if (StringUtils.isNotBlank(keyVariables)) {
            itemGroup.setKeys(keyVariables.trim());
        }

        study.setItemGroup(id, itemGroup);
    }
}
