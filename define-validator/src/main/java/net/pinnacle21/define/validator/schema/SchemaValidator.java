/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.schema;

import net.pinnacle21.define.models.DefineMetadata;
import net.pinnacle21.define.validator.report.MessageTransformer;
import net.pinnacle21.define.validator.resources.ResourceManager;
import net.pinnacle21.parsing.xml.XmlUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.*;

public class SchemaValidator {
    private static final Logger LOGGER = LoggerFactory.getLogger(SchemaValidator.class);
    private static final String DEFINE1_SCHEMA = "schema/Define1/define1-0-0.xsd";
    private static final String DEFINE2_SCHEMA = "schema/Define2/cdisc-arm-1.0/arm1-0-0.xsd";

    private final ResourceManager resourceManager;
    private final MessageTransformer transformer;
    private final DefineMetadata metadata;

    public SchemaValidator(DefineMetadata metadata, ResourceManager resourceManager, MessageTransformer transformer) {
        this.resourceManager = resourceManager;
        this.metadata = metadata;
        this.transformer = transformer;
    }

    public SchemaResult validate() {
        File baseSchema = this.getDefineSchema();
        File define = this.resourceManager.getDefine();

        try (
            InputStream defineIs = new FileInputStream(define);
            InputStream defineBs = new BufferedInputStream(defineIs);
            InputStream schemaIs = new FileInputStream(baseSchema);
            InputStream schemaBs = new BufferedInputStream(schemaIs)
        ) {
            SchemaFactory schemaFactory = SchemaFactory.newInstance(
                XMLConstants.W3C_XML_SCHEMA_NS_URI,
                "org.apache.xerces.jaxp.validation.XMLSchemaFactory", 
                getClass().getClassLoader());
            Source schemaSource = new StreamSource(schemaBs, baseSchema.getAbsolutePath());
            Schema schema = schemaFactory.newSchema(schemaSource);

            XercesResultTransformer errorHandler = new XercesResultTransformer(this.transformer);
            
            Validator schemaValidator = schema.newValidator();
            schemaValidator.setFeature("http://apache.org/xml/features/validation/schema-full-checking", true);
            schemaValidator.setFeature("http://apache.org/xml/features/honour-all-schemaLocations", true);
            schemaValidator.setErrorHandler(errorHandler);
            
            DocumentBuilder parser = XmlUtils.newSafeDocumentBuilder(schema);
            parser.setErrorHandler(errorHandler);
            
            Document document = parser.parse(defineBs, define.getAbsolutePath());
            document.normalizeDocument();
            schemaValidator.validate(new DOMSource(document));

            return new SchemaResult(!errorHandler.isFatal(), document);
        } catch (SAXParseException ex) {
            LOGGER.warn("The define.xml '{}' contains at least one fatal error, ending validation.", define, ex);
        } catch (SAXException | IOException | ParserConfigurationException ex) {
            LOGGER.error("Unexpected Exception while validating '{}' against the Schema.", define, ex);
        }

        return new SchemaResult(false, null);
    }

    private File getDefineSchema() {
        String schema;

        switch (this.metadata) {
            case V1:
                schema = DEFINE1_SCHEMA;
                break;
            case V2:
            default:
                schema = DEFINE2_SCHEMA;
                break;
        }

        return this.resourceManager.getGlobalResource(schema);
    }
}
