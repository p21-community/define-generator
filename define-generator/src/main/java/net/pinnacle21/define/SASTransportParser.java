/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define;

import net.pinnacle21.define.models.Item;
import net.pinnacle21.define.models.ItemGroup;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.api.model.SourceOptions;
import net.pinnacle21.validator.api.model.ValidationOptions;
import net.pinnacle21.validator.api.model.VariableDetails;
import net.pinnacle21.validator.data.*;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class SASTransportParser implements StudyParser {
    private static final Logger LOGGER = LoggerFactory.getLogger(SASTransportParser.class);

    private final List<SourceOptions> sources = new LinkedList<>();
    private static Document xmlConfigDoc;
    private static Map<String, ItemGroup> configDomainMap = new HashMap<>();
    private static Map<String, HashMap<String, Item>> configVariableMap = new HashMap<>();
    private boolean isAdam;
    private Map<String, String> variableDataTypes;
    private Map<String, Integer> variableLengths;
    private Map<String, Integer> variableSigFigs;
    private Study study;

    public EventDispatcher dispatcher = new EventDispatcher();

    public SASTransportParser(Collection<File> datasets, File xmlConfig, Study study)
            throws SAXException, IOException, ParserConfigurationException {
        xmlConfigDoc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(xmlConfig);

        Element metaDataVersion = (Element) xmlConfigDoc.getElementsByTagName("MetaDataVersion").item(0);
        isAdam = metaDataVersion.getAttributeNode("def:StandardName").getValue().toUpperCase().contains("ADAM");

        parseXMLConfigItemDefs();
        parseXMLConfigDomains();

        for (File dataset : datasets) {
            String name = FilenameUtils.getBaseName(dataset.getName()).toUpperCase();
            SourceOptions source = SourceOptions.builder()
                    .withSource(dataset.getAbsolutePath())
                    .withName(name)
                    .withSubname(name)
                    .build();

            this.sources.add(source);
        }

        Collections.sort(this.sources, new Comparator<SourceOptions>() {
            @Override
            public int compare(SourceOptions lhs, SourceOptions rhs) {
                if (!(rhs.getName().startsWith("SUPP") && lhs.getName().startsWith("SUPP"))) {
                    if (rhs.getName().startsWith("SUPP")) {
                        return -1;
                    } else if (lhs.getName().startsWith("SUPP")) {
                        return 1;
                    }
                }

                int result = lhs.getName().compareTo(rhs.getName());
                return result != 0 ? result : lhs.getSubname().compareTo(rhs.getSubname());
            }
        });

        if (study != null) {
            this.study = study;
        }
    }

    public Study parse() {
        Element metadata = (Element) xmlConfigDoc.getElementsByTagName("MetaDataVersion").item(0);
        String standardName = metadata.getAttributeNode("def:StandardName").getValue();
        String standardVersion = metadata.getAttributeNode("def:StandardVersion").getValue();

        if (this.study == null) {
            this.study = new Study().setName("")
                      .setStandardName(standardName)
                      .setStandardVersion(standardVersion)
                      .setLanguage("en");
        }

        Set<String> parentStructures = new HashSet<>();

        for (SourceOptions source : this.sources) {
            if (!source.getName().startsWith("SUPP")) {
                parentStructures.add(source.getName());
            }
        }

        Map<String, ItemGroup> studyStructures = new HashMap<>();
        int index = 0;

        for (SourceOptions source : this.sources) {
            ++index;

            try (DataSource datasource = new SasTransportDataSource(source, new DataEntryFactory(ValidationOptions.builder().build()))) {
                this.dispatcher.fireTaskStart(source.getName(), index, this.sources.size());

                if (!datasource.test()) {
                    LOGGER.info("Unable to process file {} because source test failed", source.getSource());

                    continue;
                }

                SourceDetails details = datasource.getDetails();
                Set<String> variables = datasource.getVariables();

                ItemGroup suppqualDataset = null;
                ItemGroup structure = null;
                ItemGroup structureTemplate = null;
                boolean createdStructure = false;
                boolean structureIsNull = false;

                if (parentStructures.contains(source.getName())) {
                    structureTemplate = configDomainMap.get(source.getName().replaceAll("\\d", ""));

                    if (source.getName().replaceAll("\\d", "").length() > 2 && structureTemplate == null) {
                        structureTemplate = configDomainMap.get(source.getName().replaceAll("\\d", "").substring(0, 2));
                    }

                    boolean isBds = datasource.getVariables().contains("PARAMCD") || datasource.getVariables().contains("PARAM");

                    if (isAdam && structureTemplate == null) {
                        if (isBds) {
                            structureTemplate = configDomainMap.get("BDS");
                        } else {
                            structureTemplate = configDomainMap.get("OTHER");
                        }
                    }

                    String label = null;

                    if (details.hasProperty(SourceDetails.Property.Label)) {
                        label = details.getString(SourceDetails.Property.Label);
                    }

                    HashMap<String, String> structureMap = new HashMap<>();
                    String keysVariables = null;

                    if (structureTemplate != null) {
                        keysVariables = structureTemplate.getKeyVariables().toString();
                    }

                    structureMap.put("name", source.getName());
                    structureMap.put("purpose", "Tabulation");
                    structureMap.put("label", StringUtils.defaultIfEmpty(label, null));
                    structureMap.put("isReferenceData", "No");
                    structureMap.put("isRepeating", "No");

                    // TODO: Figure out better way to do this
                    if (isAdam) {
                        structureMap.put("purpose", "Analysis");
                    }

                    if (structureTemplate != null) {
                        structureMap.put("category", structureTemplate.getCategory());
                        structureMap.put("structure", structureTemplate.getStructure());
                        structureMap.put("keyVariables", keysVariables != null ?
                                generateKeys(keysVariables.substring(1, keysVariables.length() - 1),
                                        variables, source.getName()) : null);
                    } else {
                        if (variables.contains(structureMap.get("name")
                                .replaceAll("[0-9]", "").substring(0,2) + "TERM")) {
                            structureTemplate = configDomainMap.get("EVENTS");
                        } else if (variables.contains(structureMap.get("name")
                                .replaceAll("[0-9]", "").substring(0, 2) + "TESTCD")) {
                            structureTemplate = configDomainMap.get("FINDINGS");
                        } else if (variables.contains(structureMap.get("name")
                                .replaceAll("[0-9]", "").substring(0, 2) + "TRT")) {
                            structureTemplate = configDomainMap.get("INTERVENTIONS");
                        }

                        if (structureTemplate != null) {
                            keysVariables = structureTemplate.getKeyVariables().toString();
                        }

                        if (structureTemplate != null) {
                            structureMap.put("category", structureTemplate.getCategory());
                            structureMap.put("keyVariables", keysVariables != null ?
                                    generateKeys(keysVariables.substring(1, keysVariables.length() - 1),
                                            variables, source.getName()) : null);
                        }
                    }

                    if (structureMap.get("category")  != null) {
                        if (structureMap.get("category").toUpperCase().equals("TRIAL DESIGN")) {
                            structureMap.put("isReferenceData", "Yes");
                        } else if (!structureMap.get("name").equals("DM")) {
                            structureMap.put("isRepeating", "Yes");
                        }
                    } else {
                        structureMap.put("isRepeating", "Yes");
                    }

                    structure = new ItemGroup()
                            .setName(structureMap.get("name"))
                            .setDescription(structureMap.get("label"))
                            .setStructure(structureMap.get("structure"))
                            .setCategory(structureMap.get("category") != null ? structureMap.get("category") : "")
                            .setPurpose(structureMap.get("purpose"))
                            .setIsRepeating(structureMap.get("isRepeating"))
                            .setIsReferenceData(structureMap.get("isReferenceData"))
                            .setKeys(structureMap.get("keyVariables"));

                    parentStructures.remove(structure.getName());
                    studyStructures.put(structure.getName(), structure);
                    createdStructure = true;
                } else if (source.getName().toUpperCase().contains("SUPP")) {
                    String label = null;

                    if (details.hasProperty(SourceDetails.Property.Label)) {
                        label = details.getString(SourceDetails.Property.Label);
                    }

                    structureTemplate = configDomainMap.get("SUPPQUAL");
                    String keyVariables = null;

                    if (structureTemplate != null) {
                        keyVariables = structureTemplate.getKeyVariables().toString();
                    }
                    suppqualDataset = new ItemGroup()
                            .setName(source.getName())
                            .setDescription(label)
                            .setStructure(structureTemplate != null ? structureTemplate.getStructure() : null)
                            .setCategory(structureTemplate != null ? structureTemplate.getCategory() : "")
                            .setPurpose("Tabulation")
                            .setIsRepeating("Yes")
                            .setIsReferenceData(structureTemplate != null ? structureTemplate.getCategory().equals("TRIAL DESIGN") ? "Yes" : "No" : null)
                            .setKeys(keyVariables != null
                                    ? generateKeys(keyVariables.substring(1, keyVariables.length() - 1),
                                                   variables,
                                                   source.getName()) : null
                                    );

                    studyStructures.put(suppqualDataset.getName(), suppqualDataset);
                    createdStructure = true;
                }

                if (structure == null) {
                    structure = studyStructures.get(getParentStructureName(source.getName().replaceAll("\\d", "")));
                    structureIsNull = true;
                }

                variableDataTypes = new HashMap<>();
                variableLengths = new HashMap<>();
                variableSigFigs = new HashMap<>();

                // For Float Values, Length is calculated by Largest Integer Value + the Largest decimal Value
                Map<String, Integer> leftFloatLengths = new HashMap<>();
                Map<String, Integer> rightFloatLengths = new HashMap<>();

                while (datasource.hasRecords()) {
                    for (DataRecord record : datasource.getRecords()) {
                        for (String variable: variables) {
                            processOneVariable(variable, datasource, record.getValue(variable), variableDataTypes,
                                    variableLengths, variableSigFigs, leftFloatLengths, rightFloatLengths);
                        }
                    }
                }

                boolean isSuppqual = source.getName().contains("SUPP");
                this.parseVariables(datasource, isSuppqual, suppqualDataset,
                        structure, structureTemplate, createdStructure, !structureIsNull);
            } catch (InvalidDataException ex) {
                LOGGER.info("Aborting import of datasets due to exception while processing file {}", source.getSource());
            } finally {
                this.dispatcher.fireTaskCompleted(source.getName(), index, this.sources.size());
            }
        }

        return this.study;
    }

    public static void processOneVariable(String variable, DataSource datasource, DataEntry entry,
                                          Map<String, String> variableDataTypes, Map<String, Integer> variableLengths,
                                          Map<String, Integer> variableSigFigs, Map<String, Integer> leftFloatLengths,
                                          Map<String, Integer> rightFloatLengths) throws InvalidDataException {
        String recordValue = entry.toString();

        if (variableDataTypes.get(variable) == null ||
                !variableDataTypes.get(variable).toUpperCase().contains("DATE")) {
            String sasDataType = (String) datasource.getVariableProperty(variable, DataSource.VariableProperty.Type);
            String dataType = getDataType(recordValue, sasDataType);

            if (variableDataTypes.get(variable) != null) {
                String currentDataType = variableDataTypes.get(variable);

                if (currentDataType.equals("integer")) {
                    if (dataType != null && dataType.equals("float") && isFloat(recordValue)) {
                        variableDataTypes.put(variable, dataType);
                    }
                } else if (currentDataType.equals("text")) {
                    variableLengths.put(variable, null);
                } else if (dataType != null) {
                    if (!currentDataType.equals("float") || !dataType.equals("integer")) {
                        variableDataTypes.put(variable, dataType);
                    }
                }
            } else {
                variableDataTypes.put(variable, dataType);
            }
        }

        // TODO: LOGIC FOR FLOAT NEEDS TO BE CHANGED
        if (variableDataTypes.get(variable) != null && variableDataTypes.get(variable).equals("float")) {
            int val;

            if (recordValue.isEmpty() || Float.parseFloat(recordValue) == 0){
                val = 0;
            } else {
                int leftValueLength , rightValueLength = 0;
                String[] splitFloat = recordValue.split("\\.");
                leftValueLength = splitFloat[0].length();

                if (splitFloat.length > 1){
                    rightValueLength = splitFloat[1].length();
                }

                if ((int) ObjectUtils.defaultIfNull(leftFloatLengths.get(variable), 0) <= leftValueLength) {
                    leftFloatLengths.put(variable, leftValueLength);
                }

                if ((int) ObjectUtils.defaultIfNull(rightFloatLengths.get(variable), 0) <= rightValueLength) {
                    rightFloatLengths.put(variable, rightValueLength);
                }

                val = ((int) ObjectUtils.defaultIfNull(leftFloatLengths.get(variable), 0))
                        + ((int) ObjectUtils.defaultIfNull(rightFloatLengths.get(variable), 0));
            }

            if ((int) ObjectUtils.defaultIfNull(variableLengths.get(variable), 0) <= val) {
                variableLengths.put(variable, val);
            }

            if (recordValue.contains(".")) {
                val = recordValue.split("\\.")[1].length();

                if ((int) ObjectUtils.defaultIfNull(variableSigFigs.get(variable), 0) <= val) {
                    variableSigFigs.put(variable, val);
                }
            }
        } else if (variableDataTypes.get(variable) != null && variableDataTypes.get(variable).equals("integer")) {
//          double val;
//
//          if (recordValue.isEmpty() || Double.parseDouble(recordValue) == 0){
//              val = 0;
//          } else {
//              val =  recordValue.contains(".") ? recordValue.length()- 1 : recordValue.length(); //Math.floor(Math.log10(Math.abs(Double.parseDouble(recordValue))) + 1);
//          }
//          if ((int) ObjectUtils.defaultIfNull(variableLengths.get(variable), 0) <= val) {
//              variableLengths.put(variable, (int) val);
//          }
        } else {
            variableLengths.put(variable, ((Number) datasource.getVariableProperty(variable,
                    DataSource.VariableProperty.Length)).intValue());
        }
    }

    private void parseVariables(DataSource datasource, boolean isSuppqual,
                                ItemGroup suppqualDataset, ItemGroup structure,
                                ItemGroup structureTemplate, boolean createdStructure,
                                boolean addStructure) throws InvalidDataException {
        Set<String> variables = datasource.getVariables();
        Map<String, Item> studyStructureVariables = new HashMap<>();
        Map<String, Item> standardStructureVariables = new HashMap<>();
        Map<String, Item> baseVariables = new HashMap<>();

        if (!createdStructure) {
            for (Item variable : structure.getItems()) {
                studyStructureVariables.put(variable.getName().contains("SUPP") ?
                        variable.getName() + "-" + variable.getLabel() : variable.getName(), variable);
            }
        }

        if (structureTemplate != null) {
            for (Item variable : configVariableMap.get(structureTemplate.getName().replaceAll("[0-9]", "")).values()) {
                standardStructureVariables.put(variable.getName(), variable);
            }
        }

        if (isAdam) {
            for (HashMap<String, Item> items : configVariableMap.values()) {
                for (Item variable : items.values()) {
                    baseVariables.put(variable.getName().replace("__", ""), variable);
                }
            }
        }

        List<String> keyVariables;

        if (suppqualDataset != null && structure != null) {
            keyVariables = Arrays.asList(suppqualDataset.getKeyVariables().toArray(new String[structure.getKeyVariables().size()]));
        } else if (structure != null) {
            keyVariables = Arrays.asList(structure.getKeyVariables().toArray(new String[structure.getKeyVariables().size()]));
        } else {
            keyVariables = Collections.emptyList();
        }

        int order = 1;
        SourceDetails details = datasource.getDetails();

        for (String variableName : variables) {
            if (studyStructureVariables.containsKey(variableName)) {
                continue;
            }

            VariableDetails variableDetails = details.getVariable(variableName);
            Item variableTemplate = standardStructureVariables.get(variableName);
            HashMap<String, String> variableMap = new HashMap<>();
            variableMap.put("name", variableName);

            if (datasource.getVariableProperty(variableName, DataSource.VariableProperty.Label) != null){
                variableMap.put("label", ((String) datasource.getVariableProperty(variableName,
                        DataSource.VariableProperty.Label)).replaceAll("\\s+", " ").replace("<Domain>", variableName));
            }

            if (variableDetails.hasProperty(VariableDetails.Property.FullFormat)){
                variableMap.put("format", StringUtils.defaultIfBlank(variableDetails.getString(VariableDetails.Property.FullFormat), null));
            }

            if (suppqualDataset != null && structure != null) {
                variableMap.put("structure", isSuppqual ? suppqualDataset.getName() : structure.getName());
            }

            variableMap.put("dataType", StringUtils.defaultIfEmpty(variableDataTypes.get(variableName), ""));
            variableMap.put("order", Integer.toString(order++));
            int position = keyVariables.indexOf(variableName);

            if (position > -1) {
                variableMap.put("keySequence", Integer.toString(position + 1));
            }

            if (variableTemplate == null) {
                if (structure != null) {
                    if (variableName.startsWith(structure.getName())) {
                        variableTemplate = baseVariables.get(variableName.replaceFirst(structure.getName(), ""));
                    }
                } else {
                    variableTemplate = baseVariables.get(variableName);
                }
            }

            if (variableTemplate != null) {
                if (variableMap.get("label") == null || variableMap.get("label").isEmpty() ||
                        variableMap.get("label").equals("null")) {
                    variableMap.put("label", variableTemplate.getLabel().replace("<Domain>", variableName));
                }

                boolean isMandatory = variableTemplate.getMandatory() == null ||
                        !variableTemplate.getMandatory().equals("Required");
                variableMap.put("mandatory", isMandatory ? "No" : "Yes");
                variableMap.put("role", StringUtils.defaultIfEmpty(variableTemplate.getRole(), ""));
            }

            String dataType = (String) ObjectUtils.defaultIfNull(variableMap.get("dataType"), "");
            boolean isNotDateTime = !dataType.toUpperCase().contains("DATE") &&
                    !dataType.toUpperCase().contains("TIME");

            if (isNotDateTime) {
                variableMap.put("length", variableLengths.get(variableName) != null ?
                        Integer.toString(variableLengths.get(variableName)) : null);
            }

            if (dataType.equals("float")) {
                variableMap.put("significantDigits", Integer.toString(variableSigFigs.get(variableName)));
            }

            if (variableMap.get("dataType").isEmpty() && variableDetails.hasProperty(VariableDetails.Property.Type)) {
                variableMap.put("dataType", ((String) datasource.getVariableProperty(variableName,
                        DataSource.VariableProperty.Type)).toUpperCase().equals("CHAR") ? "text" : "integer");
            }

            if (variableMap.get("length") == null &&
                    (variableMap.get("dataType").equals("text") || variableMap.get("dataType").equals("integer"))) {
                double length = (double) datasource.getVariableProperty(variableName,
                        DataSource.VariableProperty.Length);
                variableMap.put("length", Integer.toString((int) length));
            }

            Item variable = new Item()
                    .setOrder(variableMap.get("order"))
                    .setName(variableMap.get("name"))
                    .setLabel(variableMap.get("label"))
                    .setDataType(variableMap.get("dataType"))
                    .setLength(variableMap.get("length"))
                    .setSignificantDigits(variableMap.get("significantDigits"))
                    .setFormat(variableMap.get("format"))
                    .setRole(variableMap.get("role"))
                    .setMandatory(variableMap.get("mandatory"));
            if (variableMap.get("KeySequence") != null) {
                variable.setKeySequence(Integer.parseInt(variableMap.get("KeySequence")));
            }

            if (addStructure && structure != null) {
                structure.add(variable);
                study.setItemGroup(structure.getName(), structure);
            }

            if (suppqualDataset != null) {
                suppqualDataset.add(variable);
                study.setItemGroup(suppqualDataset.getName(), suppqualDataset);
            }
        }
    }

    /**
     * Fills the configDomainMap with "ItemGroup" model objects using the xml config standard file.
     **/
    private static void parseXMLConfigDomains(){
        NodeList ItemGroupList = xmlConfigDoc.getElementsByTagName("ItemGroupDef");
        for (int i = 0; i < ItemGroupList.getLength(); i++){
            HashMap<Integer, String> keys = new HashMap<>();
            Element domain = (Element) ItemGroupList.item(i);
            String domainName = domain.getAttributeNode("Name").getValue();

            for (Item item : configVariableMap.get(domainName).values()){
                if (item.getKeySequence() != null) {
                    keys.put(item.getKeySequence(), item.getName());
                }
            }

            ItemGroup itemGroup = new ItemGroup()
                    .setName(domainName)
                    .setDescription(domain.getElementsByTagName("TranslatedText").item(0).getTextContent())
                    .setStructure(domain.getAttributeNode("def:Structure").getValue())
                    .setCategory(domain.getAttributeNode("def:Class").getValue())
                    .setPurpose(domain.getAttributeNode("Purpose").getValue())
                    .setIsRepeating(domain.getAttributeNode("Repeating").getValue())
                    .setIsReferenceData(domain.getAttributeNode("IsReferenceData").getValue())
                    .setKeys(sortedKeyVariables(keys));

            configDomainMap.put(domainName, itemGroup);
        }
    }

    /**
     * Fills the configVariableMap with a HashMap of "Item" model objects using the xml config standard file.
     */
    private static void parseXMLConfigItemDefs(){
        HashMap<String, Element> itemDefMap = new HashMap<>();
        NodeList itemDefs = xmlConfigDoc.getElementsByTagName("ItemDef");

        for (int i = 0; i < itemDefs.getLength(); i++){
            Element itemDef = (Element) itemDefs.item(i);
            itemDefMap.put(itemDef.getAttributeNode("OID").getValue(), itemDef);
        }

        NodeList ItemGroup = xmlConfigDoc.getElementsByTagName("ItemGroupDef");

        for (int d = 0; d < ItemGroup.getLength(); d++) {
            Element domain = (Element) ItemGroup.item(d);
            HashMap<String, Item> variableMap = new HashMap<>();

            for (int v = 0; v < domain.getElementsByTagName("ItemRef").getLength(); v++) {
                Element itemRef = (Element) domain.getElementsByTagName("ItemRef").item(v);
                String itemOID = itemRef.getAttributeNode("ItemOID").getValue();
                Item configVariable = new Item()
                        .setName(itemDefMap.get(itemOID).getAttributeNode("Name").getValue())
                        .setLabel(itemDefMap.get(itemOID).getElementsByTagName("TranslatedText").item(0).getTextContent())
                        .setRole(itemRef.hasAttribute("Role") ? itemRef.getAttributeNode("Role").getValue() : null)
                        .setMandatory(itemRef.hasAttribute("val:Core") ? itemRef.getAttributeNode("val:Core").getValue() : null);

                if (itemRef.hasAttribute("KeySequence")){
                    configVariable.setKeySequence(Integer.parseInt(itemRef.getAttributeNode("KeySequence").getValue()));
                }

                variableMap.put(itemDefMap.get(itemOID).getAttributeNode("Name").getValue(), configVariable);
            }

            configVariableMap.put(domain.getAttributeNode("Name").getValue(), variableMap);
        }
    }

    private static String sortedKeyVariables(HashMap<Integer, String> keys){
        String keyVariables = "";
        Collections.sort(Arrays.asList(keys.keySet().toArray(new Integer[keys.keySet().size()])));

        for (Integer key : keys.keySet()){
            if (keyVariables.isEmpty()){
                keyVariables = keys.get(key);
            } else {
                keyVariables += "," + keys.get(key);
            }
        }

        return keyVariables;
    }

    public static String getParentStructureName(String name) {
        return name.toUpperCase().startsWith("SUPP") ? name.substring(4, name.length()) : name;
    }

    public static String generateKeys(String structureKeys, Set<String> variables, String sourceName ){
        String structurekeyVariables = null;

        if (structureKeys == null) {
            return null;
        }

        for (String keyHolder : structureKeys.replace("__", sourceName).split(",")) {
            if (variables.contains(keyHolder.trim())) {
                if (structurekeyVariables == null) {
                    structurekeyVariables = keyHolder.trim();
                } else {
                    structurekeyVariables = structurekeyVariables + "," + keyHolder.trim();
                }
            }
        }

        return structurekeyVariables;
    }

    public static String getDataType(String data, String sasDataType) {
        if (data.length() == 0) {
            return null;
        }

        boolean isNumeric = sasDataType.equals("Num");

        if (data.matches("-?\\d+?") && isNumeric) {
            return "integer";
        }

        if (data.length() >= 10) {
            if (data.substring(0, 10).matches("\\d{4}-\\d{2}-\\d{2}")) {
                return "datetime";
            }
        }

        return isNumeric ? "float" : "text";
    }

    public static boolean isFloat(String num) {
        try {
            Float.parseFloat(num);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void addGeneratorListener(GeneratorListener listener) {
        dispatcher.add(listener);
    }
}
