/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.util;

import net.pinnacle21.define.models.RangeCheck;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Templates {
    public static final String RANGE_CHECK_DELIMITER = ", ";

    /**
     * Generate a string value for the WhereClause with the OID appended to the end
     * @param rangeChecks
     * @return
     */
    public static String generateWhereClauseOid(List<RangeCheck> rangeChecks) {
        return generateWhereClauseStringInternal(rangeChecks, true);
    }

    /**
     * Generate a user-friendly string value for the WhereClause
     * @param rangeChecks
     * @return
     */
    public static String generateWhereClauseString(List<RangeCheck> rangeChecks) {
        return generateWhereClauseStringInternal(rangeChecks, false);
    }

    //  The includeOid flag tries to maintain the legacy way of generating these strings, warts and all.  For example,
    //  if you generate the OID string for a where clause with multiple range checks, you end up getting something that
    //  is fairly ugly, like this: "ADLB.PARAMCD.EQ.ADLB.PARAMCD.EQ.ADLB.PARAMCD.EQ.ADLB.PARAMCD.EQ.d9b33ce29fa17ba2b9decdfa1c65440aea8b85a2"
    private static String generateWhereClauseStringInternal(List<RangeCheck> rangeChecks, boolean includeOid) {
        List<String> rangeChecksStrings = new LinkedList<String>();
        StringBuilder whereClauseStringBuilder = new StringBuilder();
        StringBuilder rangeCheckValueStringBuilder = new StringBuilder();
        for (RangeCheck rangeCheck : sortRangeChecks(rangeChecks)) {
            whereClauseStringBuilder.append(rangeCheck.getItemGroupOid());
            whereClauseStringBuilder.append(".");
            whereClauseStringBuilder.append(rangeCheck.getItemOid());
            whereClauseStringBuilder.append(".");
            whereClauseStringBuilder.append(rangeCheck.getComparator());
            whereClauseStringBuilder.append(".");
            if (includeOid) {
                // Build the string that will become our hash
                for (String value : rangeCheck.getValues()) {
                    rangeCheckValueStringBuilder.append(value);
                }
            } else {
                //  Make a user-friendly version of the range checks string
                if (rangeCheck.getValues().size() > 1) {
                    whereClauseStringBuilder.append(" (");
                }
                whereClauseStringBuilder.append(String.join(RANGE_CHECK_DELIMITER, rangeCheck.getValues()));
                if (rangeCheck.getValues().size() > 1) {
                    whereClauseStringBuilder.append(") ");
                }
                rangeChecksStrings.add(whereClauseStringBuilder.toString());
                //  Clear our string builder so that we don't build upon the string that we just added
                whereClauseStringBuilder.setLength(0);
            }
        }
        if (includeOid) {
            whereClauseStringBuilder.append(Hex.sha1(rangeCheckValueStringBuilder.toString()).toLowerCase());
            return whereClauseStringBuilder.toString();
        }
        return String.join(" AND ", rangeChecksStrings);
    }

    private static List<RangeCheck> sortRangeChecks(List<RangeCheck> rangeChecks){
        List<RangeCheck> rangeCheckList = new ArrayList<>();
        for (RangeCheck rangeCheck : rangeChecks){
            if (rangeCheck.getItemOid() != null) {
                if (rangeCheck.getItemOid().toLowerCase().endsWith("testcd") || rangeCheck.getItemOid().toLowerCase().endsWith("qnam")) {
                    rangeCheckList.add(0, rangeCheck);
                } else {
                    rangeCheckList.add(rangeCheck);
                }
            }
        }
        return rangeCheckList;
    }
}
