package net.pinnacle21.define.models;

public class SoftReference {

    private final ItemGroup itemGroup;
    private final Item item;

    public SoftReference(ItemGroup itemGroup, Item item) {
        this.itemGroup = itemGroup;
        this.item = item;
    }

    public ItemGroup getItemGroup() {
        return itemGroup;
    }

    public Item getItem() {
        return item;
    }

}
