/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.resources;

import net.pinnacle21.define.models.CodeListItem;
import net.pinnacle21.define.models.Item;
import net.pinnacle21.define.models.ItemGroup;
import net.pinnacle21.validator.api.model.ConfigOptions;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

import java.io.*;
import java.util.List;

public interface Standard {
    Item getVariableFromItemGroup(ItemGroup itemGroup, Item item);
    ItemGroup getItemGroupByName(String name);
    boolean isEventsOrInterventionsVariable(CodeListItem clItem);
    Item getVariableFromEventsOrInterventions(CodeListItem clItem);
    List<String> getTerminologies();

    static Standard parseFile(File file, ConfigOptions configOptions) throws IOException, DocumentException {
        try (
            InputStream is = new FileInputStream(file);
            InputStream bs = new BufferedInputStream(is)
        ) {
            Document standard = new SAXReader().read(bs, file.getAbsolutePath());

            return new DefaultStandard(standard, configOptions);
        }
    }
}
