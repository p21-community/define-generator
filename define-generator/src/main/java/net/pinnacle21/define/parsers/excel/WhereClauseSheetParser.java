/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.RangeCheck;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.models.WhereClause;
import net.pinnacle21.define.util.Templates;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.NamedRow;
import net.pinnacle21.parsing.excel.ValidationAwareExcelSheetParser;
import org.apache.commons.lang3.StringUtils;

public class WhereClauseSheetParser extends ValidationAwareExcelSheetParser<ParsedStudy> {
    static final String ID_COLUMN = "ID";
    static final String DATASET_COLUMN = "Dataset";
    static final String VARIABLE_COLUMN = "Variable";
    static final String COMPARATOR_COLUMN = "Comparator";
    static final String VALUE_COLUMN = "Value";
    static final String COMMENT_COLUMN = "Comment";
    static final String DUPLICATE_ERROR = "The range check '%s' has already been defined for the variable '%s', "
            + "duplicates are not allowed";
    static final String VARIABLE_REF_ERROR = "'%s' references the variable '%s', but that variable was not found";

    WhereClauseSheetParser() {
        this.addValidatedRequiredColumn(ID_COLUMN, FieldValidations.MEDIUM_LENGTH);
        this.addValidatedRequiredColumn(DATASET_COLUMN);
        this.addValidatedRequiredColumn(VARIABLE_COLUMN);
        this.addValidatedRequiredColumn(COMPARATOR_COLUMN);
        this.addValidatedColumn(VALUE_COLUMN, FieldValidations.REALLY_LONG_LENGTH);
        this.addValidatedColumn(COMMENT_COLUMN);
    }

    @Override
    public void parse(NamedRow row, ParsedStudy parsedStudy) {
        if (!this.validate(row, parsedStudy)) {
            return;
        }
        Study study = parsedStudy.getStudy();

        String id = StringUtils.trim(row.get(ID_COLUMN));
        String datasetRef = StringUtils.trim(row.get(DATASET_COLUMN));
        String variableRef = StringUtils.trim(row.get(VARIABLE_COLUMN));
        String comparator = row.get(COMPARATOR_COLUMN);
        String value = row.get(VALUE_COLUMN);

        if (study.hasItem(datasetRef, variableRef)) {
            WhereClause whereClause = study.getWhereClause(id);
            if (whereClause == null) {
                whereClause = new WhereClause();
                whereClause.setOid(id);
                whereClause.setCommentOid(StringUtils.trim(row.get(COMMENT_COLUMN)));
                study.setWhereClause(id, whereClause);
            }

            if (study.hasRangeCheck(id, datasetRef, variableRef, comparator, value)) {
                parsedStudy.error(row.getIdentifier(), DUPLICATE_ERROR, id, variableRef);
                return;
            }

            RangeCheck rangeCheck = new RangeCheck();
            rangeCheck.setItemGroupOid(datasetRef);
            rangeCheck.setItemOid(variableRef);
            rangeCheck.setComparator(comparator);
            rangeCheck.addValue(value);

            whereClause.add(rangeCheck);
            String generateWcId = Templates.generateWhereClauseOid(whereClause.getRangeChecks());
            whereClause.setOid(StringUtils.defaultIfBlank(generateWcId, id));

            study.setRangeCheck(id, datasetRef, variableRef, comparator, value, rangeCheck);
            study.setWhereClause(id, whereClause);
        } else {
            parsedStudy.error(row.getIdentifier(), VARIABLE_REF_ERROR, VARIABLE_COLUMN, variableRef);
        }
    }
}
