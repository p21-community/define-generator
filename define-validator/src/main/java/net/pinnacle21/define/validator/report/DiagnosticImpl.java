/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.report;

import net.pinnacle21.define.util.Hex;
import net.pinnacle21.validator.api.model.DataDetails;
import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.api.model.SourceDetails;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

public class DiagnosticImpl implements Diagnostic {
    private final String id;
    private final String category;
    private final String context;
    private final String message;
    private final String description;
    private final long timestamp;
    private final Diagnostic.Type type;
    private final SourceDetails sourceDetails;
    private final DataDetails dataDetails;
    private final Map<String, String> values;

    public DiagnosticImpl(String id, String category, String message, String description, Diagnostic.Type type,
            SourceDetails sourceDetails, DataDetails dataDetails, Map<String, String> values) {
        this.id = id;
        this.category = category;
        this.message = message;
        this.description = description;
        this.type = type;
        this.sourceDetails = sourceDetails;
        this.dataDetails = dataDetails;
        this.context = hashContext(message);
        this.timestamp = System.currentTimeMillis();
        this.values = values;
    }

    @Override
    public String toString() {
        StringBuilder additional = new StringBuilder();
        for (String variable : this.getVariables()) {
            additional.append(String.format("%s: %s", variable, this.getVariable(variable))).append(",");
        }

        String tmp = additional.toString();
        return String.format("%s: %s (%s/%s%s)",
                this.getId(),
                this.getMessage(),
                this.getCategory(),
                this.getType().toString(),
                StringUtils.isNotBlank(additional.toString()) ? "/" + tmp.substring(0, tmp.length() - 1): ""
        );
    }

    //region Getters
    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public String getCategory() {
        return this.category;
    }

    @Override
    public Type getType() {
        return this.type;
    }

    @Override
    public String getContext() {
        return this.context;
    }

    @Override
    public long getTimestamp() {
        return this.timestamp;
    }

    @Override
    public SourceDetails getSourceDetails() {
        return this.sourceDetails;
    }

    @Override
    public DataDetails getDataDetails() {
        return this.dataDetails;
    }

    @Override
    public Set<String> getVariables() {
        return Collections.unmodifiableSet(this.values.keySet());
    }

    @Override
    public String getVariable(String variable) {
        return this.values.get(variable);
    }

    @Override
    public Map<String, String> getVariableValues() {
        return Collections.unmodifiableMap(this.values);
    }

    @Override
    public Set<String> getProperties() {
        return Collections.emptySet();
    }

    @Override
    public String getProperty(String property) {
        return null;
    }

    @Override
    public Map<String, String> getPropertyValues() {
        return null;
    }
    //endregion

    private static String hashContext(String message) {
        return Hex.sha512(message).substring(0, 15);
    }
}
