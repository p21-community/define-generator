/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models;

import net.pinnacle21.define.parsers.xml.util.ParserUtils;

public class Item extends ExcelRecord {
    private String OID;
    private String order;
    private String mandatory;
    private Integer keySequence;
    private String methodOid;
    private String role;
    private String name;
    private String dataType;
    private String length;
    private String significantDigits;
    private String format;
    private String commentOid;
    private String label;
    private String codeListOid;
    private String dictionaryOid;
    private String valueListOid;
    private String origin;
    private String pages;
    private String firstPage;
    private String lastPage;
    private String predecessor;
    private String whereClauseOid;
    private String core;

    /**
     * Should only be true when the standard defines the variable as Reqired, Expected, or Permissible
     * @since DEF-163
     */
    private boolean isCoreAllowed;

    public String getOID() {
        return OID;
    }

    public Item setOID(String OID) {
        this.OID = OID;
        return this;
    }

    public Integer getKeySequence() {
        return this.keySequence;
    }

    public Item setKeySequence(Integer sequence) {
        this.keySequence = sequence;
        return this;
    }

    public String getOrder() {
        return order;
    }

    public Item setOrder(String order) {
        this.order = order != null ? order.trim() : null;
        return this;
    }

    public String getName() {
        return name;
    }

    public Item setName(String name) {
        this.name = name != null ? name.trim() : null;
        return this;
    }

    public String getLabel() {
        return label;
    }

    public Item setLabel(String label) {
        this.label = label != null ? label.trim() : null;
        return this;
    }

    public String getDataType() {
        return dataType;
    }

    public Item setDataType(String dataType) {
        this.dataType = dataType != null ? dataType.trim() : null;
        return this;
    }

    public String getLength() {
        return length;
    }

    public Item setLength(String length) {
        this.length = length != null ? length.trim() : null;
        return this;
    }

    public String getSignificantDigits() {
        return significantDigits;
    }

    public Item setSignificantDigits(String significantDigits) {
        this.significantDigits = significantDigits != null ? significantDigits.trim() : null;
        return this;
    }

    public String getFormat() {
        return format;
    }

    public Item setFormat(String format) {
        this.format = format != null ? format.trim() : null;
        return this;
    }

    public String getCodeListOid() {
        return codeListOid;
    }

    public Item setCodeListOid(String codeListOid) {
        this.codeListOid = codeListOid != null ? codeListOid.trim() : null;
        return this;
    }

    public String getDictionaryOid() {
        return dictionaryOid;
    }

    public Item setDictionaryOid(String dictionaryOid) {
        this.dictionaryOid = dictionaryOid != null ? dictionaryOid.trim() : null;
        return this;
    }

    public String getValueListOid() {
        return valueListOid;
    }

    public Item setValueListOid(String valueListOid) {
        this.valueListOid = valueListOid != null ? valueListOid.trim() : null;
        return this;
    }

    public String getOrigin() {
        return origin;
    }

    public Item setOrigin(String origin) {
        this.origin = origin != null ? origin.trim() : null;
        return this;
    }

    public String getPages() {
        return pages;
    }

    public Item setPages(String pages) {
        this.pages = pages != null ? pages.trim() : null;
        return this;
    }

    public String getFirstPage() {
        return firstPage;
    }

    public Item setFirstPage(String firstPage) {
        this.firstPage = firstPage;
        return this;
    }

    public String getLastPage() {
        return lastPage;
    }

    public Item setLastPage(String lastPage) {
        this.lastPage = lastPage;
        return this;
    }

    public String getMethodOid() {
        return methodOid;
    }

    public Item setMethodOid(String methodOid) {
        this.methodOid = methodOid != null ? methodOid.trim() : null;
        return this;
    }

    public String getPredecessor() {
        return predecessor;
    }

    public Item setPredecessor(String predecessor) {
        this.predecessor = predecessor != null ? predecessor.trim() : null;
        return this;
    }

    public String getRole() {
        return role;
    }

    public Item setRole(String role) {
        this.role = role != null ? role.trim() : null;
        return this;
    }

    public String getMandatory() {
        return mandatory;
    }

    public Item setMandatory(String mandatory) {
        this.mandatory = mandatory != null ? mandatory.trim() : null;
        return this;
    }

    public String getCommentOid() {
        return commentOid;
    }

    public Item setCommentOid(String commentOid) {
        this.commentOid = commentOid != null ? commentOid.trim() : null;
        return this;
    }

    public String getWhereClauseOid() {
        return whereClauseOid;
    }

    public Item setWhereClauseOid(String whereClauseOid) {
        this.whereClauseOid = whereClauseOid != null ? whereClauseOid.trim() : null;
        return this;
    }

    public String getCore() {
        return core;
    }

    public Item setCore(String core) {
        this.core = core != null ? core.trim() : null;
        return this;
    }

    /**
     * @see {@link #isCoreAllowed}.
     */
    public boolean isCoreAllowed() {
        return isCoreAllowed;
    }

    /**
     * @see {@link #isCoreAllowed}.
     */
    public Item setCoreAllowed(boolean isCoreAllowed) {
        this.isCoreAllowed = isCoreAllowed;
        return this;
    }

    public String getAnyPages() {
        return ParserUtils.aggregatePages(getPages(), getFirstPage(), getLastPage());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        if (getOrder() != null ? !getOrder().equals(item.getOrder()) : item.getOrder() != null) return false;
        if (getMandatory() != null ? !getMandatory().equals(item.getMandatory()) : item.getMandatory() != null)
            return false;
        if (getKeySequence() != null ? !getKeySequence().equals(item.getKeySequence()) : item.getKeySequence() != null)
            return false;
        if (getMethodOid() != null ? !getMethodOid().equals(item.getMethodOid()) : item.getMethodOid() != null)
            return false;
        if (getRole() != null ? !getRole().equals(item.getRole()) : item.getRole() != null) return false;
        if (getName() != null ? !getName().equals(item.getName()) : item.getName() != null) return false;
        if (getDataType() != null ? !getDataType().equals(item.getDataType()) : item.getDataType() != null)
            return false;
        if (getLength() != null ? !getLength().equals(item.getLength()) : item.getLength() != null) return false;
        if (getSignificantDigits() != null ? !getSignificantDigits().equals(item.getSignificantDigits()) : item.getSignificantDigits() != null)
            return false;
        if (getFormat() != null ? !getFormat().equals(item.getFormat()) : item.getFormat() != null) return false;
        if (getCommentOid() != null ? !getCommentOid().equals(item.getCommentOid()) : item.getCommentOid() != null)
            return false;
        if (getLabel() != null ? !getLabel().equals(item.getLabel()) : item.getLabel() != null) return false;
        if (getCodeListOid() != null ? !getCodeListOid().equals(item.getCodeListOid()) : item.getCodeListOid() != null)
            return false;
        if (getDictionaryOid() != null ? !getDictionaryOid().equals(item.getDictionaryOid()) : item.getDictionaryOid() != null)
            return false;
        if (getValueListOid() != null ? !getValueListOid().equals(item.getValueListOid()) : item.getValueListOid() != null)
            return false;
        if (getOrigin() != null ? !getOrigin().equals(item.getOrigin()) : item.getOrigin() != null) return false;
        if (getPages() != null ? !getPages().equals(item.getPages()) : item.getPages() != null) return false;
        if (getFirstPage() != null ? !getFirstPage().equals(item.getFirstPage()) : item.getFirstPage() != null)
            return false;
        if (getLastPage() != null ? !getLastPage().equals(item.getLastPage()) : item.getLastPage() != null)
            return false;
        if (getPredecessor() != null ? !getPredecessor().equals(item.getPredecessor()) : item.getPredecessor() != null)
            return false;
        if (getWhereClauseOid() != null ? !getWhereClauseOid().equals(item.getWhereClauseOid()) : item.getWhereClauseOid() != null)
            return false;
        if (getCore() != null ? !getCore().equals(item.getCore()) : item.getCore() != null) return false;
        return getOID() != null ? getOID().equals(item.getOID()) : item.getOID() == null;
    }
}
