/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.report;

import net.pinnacle21.define.validator.resources.DefineLineReader;
import net.pinnacle21.define.validator.resources.RuleSet;
import net.pinnacle21.validator.api.model.DataDetails;
import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.data.InternalDataDetails;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MessageTransformer {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageTransformer.class);
    private static final String MESSAGE_SEPARATOR = "`\\|";
    private static final ThreadLocal<Pattern> HINT = ThreadLocal.withInitial(() -> Pattern.compile("'(.*)'\\."));
    private static final ThreadLocal<Pattern> NAMESPACE = ThreadLocal.withInitial(() -> Pattern.compile(
        "invalid '(?<namespace>[^']+)' namespace"
    ));
    private static final ThreadLocal<Pattern> DD0002 = ThreadLocal.withInitial(() -> Pattern.compile(
        "\"([A-Za-z]*)\""
    ));
    private static final ThreadLocal<Pattern> DD0003 = ThreadLocal.withInitial(() -> Pattern.compile(
        "The value '.*' of attribute ('.*') on element ('.*') .*"
    ));
    private static final ThreadLocal<Pattern> DD0004 = ThreadLocal.withInitial(() -> Pattern.compile(
        "Attribute ('.*') is not allowed to appear in element ('.*')"
    ));
    private static final ThreadLocal<Pattern> DD0006 = ThreadLocal.withInitial(() -> Pattern.compile(
        "\\{\".*\":(.*)}"
    ));
    private static final ThreadLocal<Pattern> OD0013_1 = ThreadLocal.withInitial(() -> Pattern.compile(
        "The value '.*' of attribute ('.*') on element '.*' is not valid with respect to its type, 'positiveInteger'"
    ));
    private static final ThreadLocal<Pattern> OD0013_2 = ThreadLocal.withInitial(() -> Pattern.compile(
        "of attribute ('.*') on"
    ));
    private static final ThreadLocal<Pattern> OD0013_3 = ThreadLocal.withInitial(() -> Pattern.compile(
        "('.*') is not a valid"
    ));
    private static final ThreadLocal<Pattern> OD0017 = ThreadLocal.withInitial(() -> Pattern.compile(
        "The value '.*' of attribute ('.*') on element '.*' is not valid with respect to its type, 'datetime'."
    ));
    // TODO: Strictly speaking InternalDataDetails shouldn't be accessible to us, but oh well
    private static final DataDetails DATA_DETAILS = new InternalDataDetails("", DataDetails.Info.Variable);

    /** A list of all <code>Diagnostic</code>s generated for a validation run. */
    private final List<Diagnostic> diagnostics = new ArrayList<>();
    private final Set<String> namespaces = new HashSet<>();
    private final Set<String> nonschemaDD0007 = new HashSet<>();
    private final RuleSet ruleSet;
    private final SourceDetails sourceDetails;
    private final DefineLineReader lineReader;

    public MessageTransformer(SourceDetails sourceDetails, RuleSet ruleSet, DefineLineReader lineReader) {
        this.ruleSet = ruleSet;
        this.sourceDetails = sourceDetails;
        this.lineReader = lineReader;
    }

    /**
     * <p>Mapping for messages that have a template.</p>
     *
     * @param ruleID the Rule ID
     * @param replacements a List of objects that are needed to complete the <code>Message</code> generation.
     *                     These could be templating strings or OIDs of Elements from a Define.xml, etc.
     */
    @Deprecated
    public void map(String ruleID, List<String> replacements) {
        List<String> temp = getMessage(ruleID, null, replacements);
        createMessage(ruleID, temp.get(0), toMap(temp));
    }

    /**
     * <p>Simple lookup for messages that do not have a template.</p>
     *
     * @param ruleID a Rule ID
     */
    @Deprecated
    public void map(String ruleID) {
        List<String> temp = getMessage(ruleID, null, null);

        if (temp != null && temp.get(0) != null) {
            createMessage(ruleID, temp.get(0), toMap(temp));
        } else {
            map(ruleID, Collections.<String, String>emptyMap());
        }
    }

    /**
     * Mapping for a <code>XercesResultTransformer</code> message.
     * @param ruleID an Rule ID
     * @param cleanedMessage the message after being cleaned by the <code>XercesResultTransformer</code> instance.
     */
    public void map(String ruleID, String cleanedMessage, final int lineNumber) {
        Map<String, String> variableValueMap = new LinkedHashMap<>();
        List<String> temp = getMessage(ruleID, cleanedMessage, null);

        variableValueMap.put("Line Number", Integer.toString(lineNumber));
        variableValueMap.put("XML", this.lineReader.getLine(lineNumber));

        if (temp == null || temp.isEmpty()) {
            createMessage(ruleID, null, variableValueMap);
        } else {
            createMessage(ruleID, temp.get(0), variableValueMap);
        }
    }

    public void map(String ruleId, Map<String, String> variableValueMap) {
        createMessage(ruleId, getMessage(ruleId, null), variableValueMap);
    }

    public void map(String ruleId, List<String> messageReplacements, Map<String, String> variableValueMap) {
        createMessage(ruleId, getMessage(ruleId, messageReplacements), variableValueMap);
    }

    private Map<String, String> toMap(List<String> original) {
        Map<String, String> newMap = new HashMap<>();

        if (original.get(1).contains("`|")) { // `| is the separator for multiple variables in the content defineValidator
            String[] variables = original.get(1).split(MESSAGE_SEPARATOR);
            //Null safe setup. in case there were missing/empty attributes in the define.xml (the split will not make empty indexes)
            String[] values = nullSafe(variables, original.get(2).split(MESSAGE_SEPARATOR));

            for (int i = 0; i < variables.length; i++) {
                newMap.put(variables[i] != null ? variables[i] : "",
                        values[i] != null && i < values.length ? values[i] : "");
            }

        } else {
            newMap.put(original.get(1), original.get(2));
        }

        return newMap;
    }

    /**
     * <p>Creates a message object based off of the information gathered in <code>{@link MessageTransformer#getMessage(String, String, List)}</code>.</p>
     * <p>After creating the {@link Diagnostic}, it is added to the <code>List</code>.</p>
     *
     * @param ruleID an Rule ID
     * the format will be:
     *                Index 0 - The message string
     *                Index 1 - The variable or Element/Attribute in the Define.xml with the issue.
     *                Index 2 - The value that caused the issue.
     *                          This can be a single value. a concatenated string of more than one value
     *                          or a line taken directly from the Define.xml document.
     */
    private void createMessage(String ruleID, String message, Map<String, String> variableValuePairs) {
        if (!this.ruleSet.has(ruleID)) {
            return;
        }

        // Do some preprocessing, skipping messages we don't want, collecting namespaces, etc
        switch (ruleID) {
            case "DD0002":
                Matcher matcher = NAMESPACE.get().matcher(message);

                if (matcher.find()) {
                    String namespace = matcher.group("namespace");
                    int index = namespace.indexOf(':');

                    if (index > -1) {
                        namespace = namespace.substring(index + 1);
                    }
                    namespaces.add(namespace.trim() + ":");
                }

                break;
            case "DD0003":
                // Skip the Schema version of the 'Missing CodedValue' Check
                if (message.startsWith("Attribute 'CodedValue")) {
                    return;
                }

                break;
            case "DD0007":
                // TODO: Can we avoid saving these in the first place?
                if (!variableValuePairs.containsKey("Line Number")) {
                    this.nonschemaDD0007.add(message + ".");
                }

                break;
            case "DD0004":
            case "DD0029":
            case "DD0015":
            case "DD0020":
                for (String value : variableValuePairs.values()) {
                    // all schema messages will start with <. We don't want the schema versions of these 4 rules.
                    if (value.startsWith("<")) {
                        return;
                    }
                }

                break;
        }

        this.diagnostics.add(this.ruleSet.get(ruleID).toDiagnostic(this.sourceDetails, DATA_DETAILS,
                message, variableValuePairs));
    }

    private String getMessage(String ruleId, List<String> replacements) {
        if (!this.ruleSet.has(ruleId)) {
            return "Rule " + ruleId + " is not found in the metadata config!";
        }

        Function<Integer, String> getter = getter(replacements);
        String message = this.ruleSet.get(ruleId).getMessage();

        switch (ruleId) {
            // Rules which return the default message
            case "DD0018":
            case "DD0029":
            default:
                break;
            // Rules which require replacements
            case "DD0003":
                message = replace(message, getter, "<attribute>", "<object>");
                break;
            case "DD0004":
                message = replace(message, getter, "<attribute>", "<element>");
                break;
            case "DD0006":
            case "DD0007":
            case "DD0011":
                message = replace(message, getter,"<element>");
                break;
            case "DD0010":
            case "OD0013":
                message = replace(message, getter,"<attribute>");
                break;
            case "DD0021":
                message = replace(message, getter,"<value>");
                break;
            case "DD0024":
                message = replace(message, getter, "<codelist>", "<variable>");
                break;
            case "DD0028":
            case "DD0031":
            case "DD0032":
            case "DD0033":
            case "DD0034":
            case "DD0082":
            case "OD0082":
            case "DD0116":
                message = replace(message, getter, "<codelist>");
                break;
            case "DD0038":
            case "DD0039":
                message = replace(message, getter, "<dataset>");
                break;
            case "DD0078":
                message = replace(message, getter, "<document>");
                break;
            case "DD0079":
                message = replace(message, getter, "<comment>");
                break;
            case "DD0080":
                message = replace(message, getter,"<method>");
                break;
            case "DD0081":
                message = replace(message, getter, "<value level>");
                break;
            case "DD0105":
            case "DD0113":
                message = replace(message, getter, "<define variable>");
                break;
            case "DD0118":
                message = replace(message, getter, 
                    "<define codelist code>",
                    "<define codelist>", 
                    "<define variable>",
                    "<standard codelist code>",
                    "<standard codelist>",
                    "<standard variable>"
                );
                break;
        }

        return message;
    }

    /**
     * @param ruleID an Rule ID
     * @param originalMessage the message from schema validation.
     * @param replacements a list of strings that will be used to construct a message.
     *
     * @return A list of objects to be used in {@link MessageTransformer#createMessage(String, String, Map)}.
     */
    private List<String> getMessage(String ruleID, String originalMessage, List<String> replacements) {
        // If the rule is not in the config...
        if (!this.ruleSet.has(ruleID)) {
            // Then don't report it
            return null;
        }

        Function<Integer, String> getter = getter(replacements);
        String variable = "", value = "", template = originalMessage;
        String message = this.ruleSet.get(ruleID).getMessage();
        Matcher matcher;

        switch (ruleID) {
            case "DD0001":
                template = addHints(originalMessage);
                break;
            case "DD0002":
                if (originalMessage != null) { //If the message is from the Xerces Transformer
                    matcher = DD0002.get().matcher(originalMessage);

                    if (matcher.find()) {
                        template = replace(message, getter(matcher), "<namespace>");
                    }
                } else { //Otherwise its from the ContentValidator
                    template = replace(message, getter, "<namespace>");
                }

                break;
            case "DD0003":
                matcher = DD0003.get().matcher(originalMessage);

                if (matcher.find()) {
                    template = replace(message, getter(matcher), "<attribute>", "<object>");
                }

                break;
            case "DD0004":
                matcher = DD0004.get().matcher(originalMessage);

                if (matcher.find()) {
                    template = replace(message, getter(matcher), "<attribute>", "<element>");
                }

                break;
            case "DD0006":
                matcher = DD0006.get().matcher(StringUtils.defaultString(originalMessage));

                if (matcher.find()) {
                    template = replace(message, getter(matcher), "<element>");
                } else if (replacements != null) {
                    template = replace(message, getter, "<element>");
                    variable = getter.apply(1);
                    value = getter.apply(2);
                }

                break;
            case "DD0007":
                template = template.split(" One of")[0].split(" No child")[0];
                break;
            case "DD0009":
                template = replace(message, getter, "<element>");
                break;
            case "DD0012":
            case "DD0013":
            case "DD0014":
            case "DD0029":
            case "DD0030":
            case "DD0044":
            case "DD0048":
            case "DD0049":
            case "DD0062":
            case "OD0010":
            case "OD0011":
            case "OD0012":
            case "OD0019":
            case "OD0021":
            case "OD0072":
            case "OD0074":
            case "OD0075":
            case "OD0076":
            case "OD0077":
                template = message;
                break;
            case "DD0016":
            case "DD0017":
            case "DD0041":
            case "DD0042":
            case "DD0045":
            case "DD0047":
            case "DD0051":
            case "DD0052":
            case "DD0060":
            case "DD0063":
            case "DD0064":
            case "DD0070":
            case "DD0071":
            case "DD0084":
            case "DD0085":
            case "OD0022":
            case "OD0027":
            case "OD0030":
            case "OD0031":
            case "OD0032":
            case "OD0041":
            case "OD0046":
            case "OD0048":
                template = message;
                variable = getter.apply(0);
                value = getter.apply(1);
                break;
            case "OD0001":
                template = message + " " + originalMessage;
                break;
            case "DD0015":
            case "DD0019":
                template = message;

                if (originalMessage == null) {
                    variable = getter.apply(0);
                    value = getter.apply(1);
                }

                break;
            case "DD0020":
                template = message;

                if (originalMessage == null) {
                    variable = "DefineVersion";
                    value = getter.apply(0);
                }

                break;
            case "DD0022":
                template = replace(message, getter, "<value>");

                String test = getter.apply(1);

                if (test.equals("MetaDataVersion")) { //iIf the define has a Standard Version but an invalid Standard Name, then the version is still invalid.
                    template = template.substring(0, 39) + ".";
                    variable = getter.apply(1);
                    value = getter.apply(2);
                } else {
                    template = replace(template, getter, 1, "<standard>");
                    variable = getter.apply(2);
                    value = getter.apply(3);
                }

                break;
            case "DD0025":
                template = replace(message, getter, "<version>");
                break;
            case "DD0031":
            case "OD0081":
                template = replace(message, getter, "<codelist>");
                break;
            case "DD0067":
                template = replace(message, getter, "<variable>");
                break;
            case "OD0013":
                for (ThreadLocal<Pattern> pattern : Arrays.asList(OD0013_1, OD0013_2, OD0013_3)) {
                    matcher = pattern.get().matcher(originalMessage);

                    if (matcher.find()) {
                        template = replace(message, getter(matcher), "<attribute>");

                        break;
                    }
                }

                break;
            case "OD0017":
                matcher = OD0017.get().matcher(originalMessage);

                if (matcher.find()) {
                    template = replace(message, getter(matcher), "<attribute>");
                }

                break;
            case "OD0079":
                template = replace(message, getter,"<codelist>");
                variable = getter.apply(1);
                value = getter.apply(2);

                break;
            default:
                //No match was found, return the original message.
        }

        return Arrays.asList(template, variable, value);
    }

    private String addHints(String originalMessage) {
        if (StringUtils.isBlank(originalMessage)) {
            return originalMessage;
        }

        if (originalMessage.startsWith("Cannot find the declaration of element")) {
            String affectedElement = "the element"; //Fallback phrasing
            Matcher matcher = HINT.get().matcher(originalMessage); //Get the element name located in quotes

            if (matcher.find()) {
                affectedElement = (String) quote(matcher.group(1));
            }
            // The -1 is to remove the period at the end of the sentence.
            return StringUtils.join(originalMessage.substring(0, originalMessage.length() - 1),
                    String.format(" using the namespace(s) found on the element. Confirm that %s is allowed in the Define.xml Standard and has the correct namespace(s).", affectedElement));
        }

        return originalMessage;
    }

    /**
     * @return the current reporter's list of messages
     */
    public List<Diagnostic> getDiagnostics() {
        List<Pattern> namespaceFilters = this.namespaces.stream()
        .map(n -> Pattern.compile("'" + n + "[^']+'"))
        .collect(Collectors.toList());

        // TODO: This is supposed to filter duplicate DD0007 but I'm unclear how they get duplicated to begin with?
        List<Diagnostic> results = this.diagnostics.stream()
                .filter(d -> {
                    if ("DD0007".equals(d.getId()) && d.getVariables().contains("Line Number")) {
                        if (this.nonschemaDD0007.contains(d.getMessage())) {
                            return false;
                        }
                    }
                    
                    for (Pattern namespaceFilter : namespaceFilters) {
                        if (namespaceFilter.matcher(d.getMessage()).find()) {
                            return false;
                        }
                    }

                    return true;
                })
                .collect(Collectors.toList());
        return results;
    }

    private String[] nullSafe(String[] variables, String[] values) {
        if (variables.length != values.length) {
            String[] tmp = values;
            values = new String[variables.length];

            System.arraycopy(tmp, 0, values, 0, tmp.length);

            int diff = variables.length - tmp.length;

            for (; diff != 0; diff--) {
                values[(variables.length - 2) + diff] = "";
            }
        }

        return values;
    }

    private static String replace(String message, Function<Integer, String> getter, String... tokens) {
        return replace(message, getter, 0, tokens);
    }

    private static String replace(String message, Function<Integer, String> getter, int offset, String... tokens) {
        for (int i = 0; i < tokens.length; i++) {
            message = message.replace(tokens[i], quote(getter.apply(i + offset)));
        }

        return message;
    }

    private static Function<Integer, String> getter(Matcher matcher) {
        return i -> {
            if (i < matcher.groupCount()) {
                return matcher.group(i + 1);
            }

            return "";
        };
    }

    private static Function<Integer, String> getter(List<String> replacements) {
        return i -> {
            if (replacements != null && i < replacements.size()) {
                return replacements.get(i);
            }

            return "";
        };
    }

    private static CharSequence quote(String s) {
        String open = s.startsWith("'") ? "" : "'";
        String close = s.endsWith("'") ? "" : "'";

        return open + s + close;
    }
}
