/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.xml.bodyparsers.arm;

import net.pinnacle21.define.models.DefineMetadata;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.models.arm.*;
import net.pinnacle21.define.parsers.xml.XMLParser;
import net.pinnacle21.define.parsers.xml.XPathEvaluator;
import net.pinnacle21.define.parsers.xml.bodyparsers.arm.util.ARMStubs;
import org.dom4j.tree.DefaultElement;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;

public class ARMParserTest {

    @Test
    public void testProgrammingCodeWithCode() throws Exception {
        ARMParser parser = new ARMParser();
        ProgrammingCode code = parser.parseProgrammingCode(ARMStubs.programmingCodeElementWithCode);
        Assert.assertEquals(ARMStubs.programmingCodeObjectWithCode, code);
    }

    @Test
    public void testProgrammingCodeWithDocument() throws Exception {
        Study mockStudy = Mockito.mock(Study.class);
        Mockito.when(mockStudy.getDocument(ARMStubs.leafID)).thenReturn(ARMStubs.document);
        Mockito.when(mockStudy.getDefineMetadata()).thenReturn(DefineMetadata.V2);

        ARMParser parser = new ARMParser().setStudy(mockStudy);
        ProgrammingCode code = parser.parseProgrammingCode(ARMStubs.programmingCodeElementWithDocument);
        Assert.assertEquals(ARMStubs.programmingCodeObjectWithDocument, code);
    }

    @Test
    public void testDocumentation() throws Exception {
        Study mockStudy = Mockito.mock(Study.class);
        Mockito.when(mockStudy.getDocument(ARMStubs.leafID)).thenReturn(ARMStubs.document);
        Mockito.when(mockStudy.getDefineMetadata()).thenReturn(DefineMetadata.V2);

        ARMParser parser = new ARMParser();
        parser.setStudy(mockStudy);

        Documentation documentation = parser.parseDocumentation(ARMStubs.documentationElement);
        Assert.assertEquals(ARMStubs.documentationObject, documentation);
    }

    @Test
    public void testDocumentationWithNoDocumentReference() {
        Study mockStudy = Mockito.mock(Study.class);
        Mockito.when(mockStudy.getDefineMetadata()).thenReturn(DefineMetadata.V2);

        ARMParser parser = new ARMParser();
        parser.setStudy(mockStudy);

        Documentation documentation = parser.parseDocumentation(ARMStubs.documentationElementNoDocumentReference);

        Assert.assertNull(documentation.getDocumentReferences());
        Assert.assertEquals(ARMStubs.documentationObjectNoDocumentReference, documentation);
    }

    @Test
    public void testAnalysisDatasets() throws Exception {
        Study mockStudy = Mockito.mock(Study.class);
        Mockito.when(mockStudy.getDocument(ARMStubs.leafID)).thenReturn(ARMStubs.document);
        Mockito.when(mockStudy.getComment(ARMStubs.commentID)).thenReturn(ARMStubs.comment);
        Mockito.when(mockStudy.getItemGroupByOID(ARMStubs.itemGroupID1)).thenReturn(ARMStubs.itemGroup1);
        Mockito.when(mockStudy.getItemGroupByOID(ARMStubs.itemGroupID2)).thenReturn(ARMStubs.itemGroup2);
        Mockito.when(mockStudy.getItemByOID(ARMStubs.itemID1)).thenReturn(ARMStubs.item1);
        Mockito.when(mockStudy.getItemByOID(ARMStubs.itemID2)).thenReturn(ARMStubs.item2);
        Mockito.when(mockStudy.getWhereClause(ARMStubs.whereClauseID1)).thenReturn(ARMStubs.whereclause1);
        Mockito.when(mockStudy.getWhereClause(ARMStubs.whereClauseID2)).thenReturn(ARMStubs.whereclause2);
        Mockito.when(mockStudy.getDefineMetadata()).thenReturn(DefineMetadata.V2);

        ARMParser parser = new ARMParser();
        parser.setStudy(mockStudy);

        AnalysisDatasets datasets = parser.parseAnalysisDatasets(ARMStubs.analysisDatasetsElement);
        Assert.assertEquals(ARMStubs.analysisDatasetsObject, datasets);
    }

    @Test
    public void testAnalysisResult() throws Exception {
        /*
        XPathEvaluator mockEvaluator = Mockito.mock(XPathEvaluator.class);
        Study mockStudy = Mockito.mock(Study.class);
        Mockito.when(mockStudy.getDefineMetadata()).thenReturn(DefineMetadata.V2);
        Mockito.when(
                mockEvaluator.selectSingleNodeF(
                        "/ODM/odm:Study/odm:MetaDataVersion/odm:ItemGroupDef[odm:ItemRef[@ItemOID='%s']]",
                        ARMStubs.itemOID1))
                .thenReturn(new DefaultElement("ItemGroup") {
                    @Override
                    public String attributeValue(String string) {
                        return ARMStubs.itemGroupID1;
                    }
                });

        ARMParser parser = new ARMParser()
                .setXPathEvaluator(mockEvaluator)
                .setStudy(mockStudy);

        AnalysisResult result = parser.parseAnalysisResult(ARMStubs.analysisResultElement);
        Assert.assertEquals(ARMStubs.analysisResultObject, result);
        */
    }

    @Test
    public void testResultDisplay() throws Exception {
        Study mockStudy = Mockito.mock(Study.class);
        Mockito.when(mockStudy.getDocument(ARMStubs.leafID)).thenReturn(ARMStubs.document);
        Mockito.when(mockStudy.getDefineMetadata()).thenReturn(DefineMetadata.V2);

        ARMParser parser = new ARMParser();
        parser.setStudy(mockStudy);

        ResultDisplay result = parser.parseResultDisplay(ARMStubs.resultDisplayElement);
        Assert.assertEquals(ARMStubs.resultDisplayObject, result);
    }

    @Test
    public void testWholeImport() throws Exception {
        Study define2Study = new XMLParser(new File("src/test/resources/parsers/xml/bodyparsers/arm/arm-example.xml")).parse();

        //Check counts of result displays
        Assert.assertEquals(define2Study.getResultDisplays().size(), 2);

        //Check first display for some counts...
        ResultDisplay display = define2Study.getResultDisplays().get(0);
        Assert.assertEquals(display.getAnalysisResults().size(), 2);
        Assert.assertEquals(display.getAnalysisResults().get(0).getAnalysisDatasets().getDatasets().size(), 2);
        Assert.assertEquals(display.getAnalysisResults().get(0).getAnalysisDatasets().getDatasets().get(0).getAnalysisVariables().size(), 2);
    }
}
