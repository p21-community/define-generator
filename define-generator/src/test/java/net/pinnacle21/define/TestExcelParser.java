/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define;

import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.parsers.excel.DefineExcelParser;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertTrue;

public class TestExcelParser {

    /**
     * Tests that empty rows will not be read when importing excel
     */
    @Test
    public void testNonPhysicalRows() throws Exception {
        DefineExcelParser parser = new DefineExcelParser(new File("src/test/resources/empty-rows.xlsx"));
        Study study = parser.process().getStudy();

        int datasets = study.getItemGroups().size();
        int variables = study.getItemGroups().get(0).getItems().size();
        int valuelevelVariables = study.getValueLists().get(0).getItems().size();
        int whereClauses = study.getWhereClauses().size();
        int codelists = study.getCodeLists().size();
        int dictionaries = study.getDictionaries().size();
        int methods = study.getMethods().size();
        int comments = study.getComments().size();
        int documents = study.getSupplementalDocuments().size();

        assertTrue("Expected 1 dataset but got " + datasets, datasets == 1);
        assertTrue("Expected 16 variables but got " + variables, variables == 16);
        assertTrue("Expected 2 value level variables but got " + valuelevelVariables, valuelevelVariables == 2);
        assertTrue("Expected 2 whereClauses but got " + whereClauses, whereClauses == 2);
        assertTrue("Expected 3 codelists but got " + codelists, codelists == 3);
        assertTrue("Expected 1 dictionary but got " + dictionaries, dictionaries == 1);
        assertTrue("Expected 8 methods but got " + methods, methods == 8);
        assertTrue("Expected 3 comments but got " + comments, comments == 3);
        assertTrue("Expected 1 document but got " + documents, documents == 1);

    }

    /**
     * Associated ticket: <a href="https://pinnacle21.atlassian.net/browse/DEF-44">DEF-44</a>
     * Tests that a formula cell will not throw a {@code FormulaParseException} when a bad formula is used in a formula
     * cell
     */
    @Test
    public void testFormulaCellParser() throws Exception {
        DefineExcelParser parser = new DefineExcelParser(new File("src/test/resources/FormulaCellTest.xlsx"));
        parser.process();
    }

}
