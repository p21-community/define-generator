/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.RangeCheck;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.models.WhereClause;
import net.pinnacle21.define.models.arm.AnalysisDataset;
import net.pinnacle21.define.models.arm.AnalysisDatasets;
import net.pinnacle21.define.models.arm.AnalysisResult;
import net.pinnacle21.define.models.arm.AnalysisVariable;
import net.pinnacle21.parsing.excel.NamedRow;
import net.pinnacle21.parsing.excel.ValidationAwareExcelSheetParser;
import org.apache.commons.lang3.StringUtils;

import static net.pinnacle21.parsing.excel.FieldValidations.MEDIUM_LENGTH;

class AnalysisCriteriaSheetParser extends ValidationAwareExcelSheetParser<ParsedStudy> {
    static final String DISPLAY_COLUMN = "Display";
    static final String RESULT_COLUMN = "Result";
    static final String DATASET_COLUMN = "Dataset";
    static final String VARIABLES_COLUMN = "Variables";
    static final String WHERE_CLAUSE_COLUMN = "Where Clause";
    static final String RESULT_REF_ERROR = "'%s' references the analysis result '%s', but that analysis result was" +
            " not found";
    static final String DATASET_REF_ERROR = "'%s' references the dataset '%s', but that dataset was not found";
    static final String VARIABLE_REF_ERROR = "'%s' references the variable '%s', but that variable was not found";
    static final String WHERE_CLAUSE_REF_ERROR = "'%s' references the where clause '%s', but that where clause was "
            + "not found";

    AnalysisCriteriaSheetParser() {
        this.addValidatedRequiredColumn(DISPLAY_COLUMN);
        this.addValidatedRequiredColumn(RESULT_COLUMN);
        this.addValidatedRequiredColumn(DATASET_COLUMN);
        this.addValidatedColumn(VARIABLES_COLUMN);
        this.addValidatedColumn(WHERE_CLAUSE_COLUMN, MEDIUM_LENGTH);
    }

    @Override
    public void parse(NamedRow row, ParsedStudy parsedStudy) {
        if (!validate(row, parsedStudy)) {
            return;
        }
        final Study study = parsedStudy.getStudy();

        String displayRef = StringUtils.trim(row.get(DISPLAY_COLUMN));
        String resultRef = StringUtils.trim(row.get(RESULT_COLUMN));
        final String datasetRef = StringUtils.trim(row.get(DATASET_COLUMN));

        if (!study.hasAnalysisResult(displayRef, resultRef)) {
            parsedStudy.error(row.getIdentifier(), RESULT_REF_ERROR, RESULT_COLUMN, resultRef);
            return;
        }

        if (StringUtils.isBlank(datasetRef) || !study.hasItemGroup(datasetRef)) {
            parsedStudy.error(row.getIdentifier(), DATASET_REF_ERROR, DATASET_COLUMN, datasetRef);
            return;
        }

        AnalysisResult analysisResult = study.getAnalysisResult(displayRef, resultRef);
        AnalysisDatasets analysisDatasets = analysisResult.getAnalysisDatasets() == null
                ? new AnalysisDatasets() : analysisResult.getAnalysisDatasets();
        AnalysisDataset analysisDataset = new AnalysisDataset();
        analysisDataset.setItemGroup(study.getItemGroup(datasetRef));

        String variableRefs = StringUtils.trim(row.get(VARIABLES_COLUMN));
        if (variableRefs != null) {
            for (String variableRef : variableRefs.split(",")) {
                variableRef = variableRef.trim();
                if (StringUtils.isNotBlank(variableRef) && !study.hasItem(datasetRef, variableRef)) {
                    parsedStudy.error(row.getIdentifier(), VARIABLE_REF_ERROR, VARIABLES_COLUMN, variableRef);
                } else if (StringUtils.isNotBlank(variableRef)) {
                    analysisDataset.add(new AnalysisVariable().setItem(study.getItem(datasetRef, variableRef)));
                }
            }
        }

        if (!parsedStudy.getErrors().isEmpty()) {
            return;
        }

        String whereClauseRef = StringUtils.trim(row.get(WHERE_CLAUSE_COLUMN));
        WhereClause whereClause = study.getWhereClause(whereClauseRef);
        if (StringUtils.isNotBlank(whereClauseRef) && whereClause == null) {
            parsedStudy.error(row.getIdentifier(), WHERE_CLAUSE_REF_ERROR, WHERE_CLAUSE_COLUMN, whereClauseRef);
            return;
        } else if (StringUtils.isNotBlank(whereClauseRef)) {
            analysisDataset.setWhereClause(whereClause);
            for (RangeCheck rangeCheck : whereClause.getRangeChecks()) {
                if ("PARAMCD".equals(rangeCheck.getItemOid())) {
                    analysisResult.setParamcdDataset(rangeCheck.getItemGroupOid());
                }
            }
        }

        analysisDatasets.add(analysisDataset);
        study.setAnalysisResult(displayRef, resultRef, analysisResult.setAnalysisDatasets(analysisDatasets));
    }
}
