/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

public class RangeCheck extends ExcelRecord {
    private String itemGroupOid;
    private String itemOid;
    private String comparator;
    private Set<String> values = new LinkedHashSet<String>();

    public Set<String> getValues() {
        return new LinkedHashSet<>(this.values);
    }

    public RangeCheck addValue(String value) {
        value = StringUtils.defaultIfBlank(value, "");
        if (!Arrays.asList("IN", "NOTIN").contains(this.comparator)) {
            this.values.add(value);
            return this;
        }

        if (value.startsWith("(") && value.endsWith(")")) {
            value = value.substring(1, value.length() - 1);
        }

        String[] valueArr = value.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");

        for (String val : valueArr) {
            String trimmedValue = val.trim();
            if (trimmedValue.startsWith("\"") && trimmedValue.endsWith("\"")) {
                values.add(trimmedValue.substring(1, trimmedValue.length() - 1));
            } else {
                values.add(trimmedValue);
            }
        }

        return this;
    }

    public String getComparator() {
        return comparator;
    }

    public RangeCheck setComparator(String comparator) {
        this.comparator = comparator != null ? comparator.trim() : null;
        return this;
    }

    public String getItemOid() {
        return itemOid;
    }

    public RangeCheck setItemOid(String itemOid) {
        this.itemOid = itemOid != null ? itemOid.trim() : null;
        return this;
    }

    public String getItemGroupOid() {
        return itemGroupOid;
    }

    public RangeCheck setItemGroupOid(String itemGroupOid) {
        this.itemGroupOid = itemGroupOid != null ? itemGroupOid.trim() : null;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RangeCheck that = (RangeCheck) o;

        if (getItemGroupOid() != null ? !getItemGroupOid().equals(that.getItemGroupOid()) : that.getItemGroupOid() != null)
            return false;
        if (getItemOid() != null ? !getItemOid().equals(that.getItemOid()) : that.getItemOid() != null) return false;
        if (getComparator() != null ? !getComparator().equals(that.getComparator()) : that.getComparator() != null)
            return false;
        return getValues() != null ? getValues().equals(that.getValues()) : that.getValues() == null;

    }
}
