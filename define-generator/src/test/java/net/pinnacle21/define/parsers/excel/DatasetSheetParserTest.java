/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.Comment;
import net.pinnacle21.define.models.ItemGroup;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.util.MockNamedRow;
import net.pinnacle21.parsing.excel.ExcelSheetParser;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.RowIdentifier;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import static net.pinnacle21.define.parsers.excel.DatasetSheetParser.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyVararg;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class DatasetSheetParserTest {

    @Test
    public void nonexistingStructureGetsCreated() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new DatasetSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedName = "DATASET";
        String expectedDescription = "descriptive";
        String expectedClass = "classy";
        String expectedStructure = "structured";
        String expectedPurpose = "purposeful";
        String expectedKeys = "A,B,C";
        String expectedRepeating = "yes";
        String expectedReference = "no";

        parser.parse(
                new MockNamedRow()
                        .with(NAME_COLUMN, expectedName + " ")
                        .with(DESCRIPTION_COLUMN, expectedDescription)
                        .with(CLASS_COLUMN, expectedClass)
                        .with(STRUCTURE_COLUMN, expectedStructure)
                        .with(PURPOSE_COLUMN, expectedPurpose)
                        .with(KEYS_COLUMN, expectedKeys)
                        .with(REPEATING_COLUMN, expectedRepeating)
                        .with(REFERENCE_DATA_COLUMN, expectedReference),
                parsedStudy
        );

        assertTrue(study.hasItemGroup(expectedName));

        ItemGroup itemGroup = study.getItemGroup(expectedName);

        assertEquals(expectedName, itemGroup.getName());
        assertEquals(expectedDescription, itemGroup.getDescription());
        assertEquals(expectedStructure, itemGroup.getStructure());
        assertEquals(expectedPurpose, itemGroup.getPurpose());
        assertEquals(expectedRepeating, itemGroup.getIsRepeating());
        assertEquals(expectedReference, itemGroup.getIsReferenceData());
    }

    @Test
    public void nameColumnIsRequired() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        Mockito.when(parsedStudy.getStudy()).thenReturn(new Study());
        ExcelSheetParser<ParsedStudy> parser = new DatasetSheetParser();

        parser.parse(
                new MockNamedRow(),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR), eq(NAME_COLUMN));
    }

    @Test
    public void invalidCommentReferenceErrorRaised() {
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(new Study()));
        ExcelSheetParser<ParsedStudy> parser = new DatasetSheetParser();

        String expectedName = "DATASET";
        String expectedComment = "COMMENT";

        parser.parse(
                new MockNamedRow()
                        .with(NAME_COLUMN, expectedName)
                        .with(COMMENT_COLUMN, expectedComment),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(COMMENT_REF_ERROR), anyVararg());
    }

    @Test
    public void validCommentReferenceIsAssigned() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new DatasetSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedName = "DATASET";
        String expectedComment = "COMMENT";
        Comment comment = new Comment().setOid(expectedComment);

        study.setComment(expectedComment, comment);

        parser.parse(
                new MockNamedRow()
                        .with(NAME_COLUMN, expectedName)
                        .with(COMMENT_COLUMN, expectedComment),
                parsedStudy
        );

        ItemGroup itemGroup = study.getItemGroup(expectedName);

        assertNotNull(itemGroup);
        assertSame(comment.getOid(), itemGroup.getCommentOid());
    }

    @Test
    public void duplicateErrorRaised() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new DatasetSheetParser();
        Study study = mock(Study.class);

        when(parsedStudy.getStudy()).thenReturn(study);

        String expectedName = "DATASET";

        when(study.hasItemGroup(expectedName)).thenReturn(true);

        parser.parse(
                new MockNamedRow()
                        .with(NAME_COLUMN, expectedName),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(DUPLICATE_ERROR), anyVararg());
    }

    @Test
    public void duplicateErrorNotRaisedWithDifferentCasing() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new DatasetSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedName = "DATASET";

        study.setItemGroup(expectedName.toLowerCase(), new ItemGroup());

        parser.parse(
                new MockNamedRow()
                        .with(NAME_COLUMN, expectedName),
                parsedStudy
        );

        assertEquals(true, study.hasItemGroup(expectedName));
    }
}
