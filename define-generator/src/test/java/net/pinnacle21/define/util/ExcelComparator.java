/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.util;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.rules.ErrorCollector;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ExcelComparator {
    private static final ThreadLocal<DecimalFormat> DECIMAL_PATTERN = new ThreadLocal<DecimalFormat>() {
        @Override
        protected DecimalFormat initialValue() {
        return new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        }
    };

    private final File controlFile;
    private final File exportFile;
    private final ErrorCollector errorCollector;

    /**
     * Instantiates an Excel Comparator that will return all differences in the files
     * @param controlFile the control file from the resources folder
     * @param exportFile the file exported from <code>ExportExcel</code>
     */
    public ExcelComparator(File controlFile, File exportFile, ErrorCollector errorCollector) {
        this.controlFile = controlFile;
        this.exportFile = exportFile;
        this.errorCollector = errorCollector;
    }

    /**
     * Compares both control excel file against another file and reports differences between them.
     * @return Error Collector with all differences/errors
     * @throws java.lang.Exception in case of errors with the comparison
     */
    public ErrorCollector process() throws Exception {
        Workbook control = null;
        Workbook export = null;
        try {
            control = new XSSFWorkbook(OPCPackage.open(this.controlFile, PackageAccess.READ));
            export = new XSSFWorkbook(this.exportFile);

            int controlSheetCount = control.getNumberOfSheets();
            int exportSheetCount = export.getNumberOfSheets();

            assertEquals(controlSheetCount, exportSheetCount);

            for (int sheetIndex = 0; sheetIndex < controlSheetCount; ++sheetIndex) {
                Sheet controlSheet = control.getSheetAt(sheetIndex);
                Sheet exportSheet = export.getSheetAt(sheetIndex);

                assertEquals(controlSheet.getSheetName(), exportSheet.getSheetName());

                int lastRowIndex = Math.max(controlSheet.getLastRowNum(), exportSheet.getLastRowNum()) + 1;

                for (int rowIndex = 0; rowIndex < lastRowIndex; ++rowIndex) {
                    Row controlRow = controlSheet.getRow(rowIndex);
                    Row exportRow = exportSheet.getRow(rowIndex);

                    assertNotNull(String.format(
                        "Control row %d on sheet %s was null but a corresponding record exists in export",
                        rowIndex + 1, controlSheet.getSheetName()
                    ), controlRow);
                    assertNotNull(String.format(
                        "Export row %d on sheet %s was null but a corresponding record exists in control",
                        rowIndex + 1, exportSheet.getSheetName()
                    ), exportRow);

                    int lastColumnIndex = Math.max(controlRow.getLastCellNum(), exportRow.getLastCellNum());

                    for (int columnIndex = 0; columnIndex < lastColumnIndex; ++columnIndex) {
                        Cell controlCell = controlRow.getCell(columnIndex, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
                        Cell exportCell = exportRow.getCell(columnIndex, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);

                        if (controlCell.getCellType() != exportCell.getCellType()) {
                            System.out.println(String.format("WARNING: Cell type mismatch on %s (%d, %d) [control: %d, export %d]",
                                controlSheet.getSheetName(), rowIndex, columnIndex, controlCell.getCellType(), exportCell.getCellType()));
                        }

                        String reason = String.format("In %s sheet on row %d column %d",
                                    controlSheet.getSheetName(),
                                    controlCell.getRowIndex() + 1,
                                    controlCell.getColumnIndex() + 1
                                );

                        errorCollector.checkThat(reason, getCellValue(exportCell), equalTo(getCellValue(controlCell)));
                    }
                }
            }
        } finally {
            if (control != null) {
                try {
                    control.close();
                } catch (IOException ignore) {}
            }

            if (export != null) {
                try {
                    export.close();
                } catch (IOException ignore) {}

                if (!this.exportFile.delete()) {
                    System.out.println("ERROR: Couldn't clean up export file");
                }
            }
        }

        return errorCollector;
    }

    private static String getCellValue(Cell cell) {
        Object value;

        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_BOOLEAN:
                value = cell.getBooleanCellValue();
                break;
            case Cell.CELL_TYPE_ERROR:
                value = cell.getErrorCellValue();
                break;
            case Cell.CELL_TYPE_NUMERIC:
                value = DECIMAL_PATTERN.get().format(cell.getNumericCellValue());
                break;
            default:
                value = cell.getStringCellValue();
                break;
        }

        return value != null ? value.toString() : null;
    }
}
