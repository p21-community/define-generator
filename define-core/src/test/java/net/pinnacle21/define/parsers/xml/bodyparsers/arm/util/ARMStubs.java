/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.xml.bodyparsers.arm.util;

import net.pinnacle21.define.models.*;
import net.pinnacle21.define.models.arm.*;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.Namespace;
import org.dom4j.QName;

import java.util.Map;

public interface ARMStubs {

    String context = "abc";
    String codeText = "return 123";
    String leafID = "123.sas";
    String leafHref = "../123.sas";
    String leafTitle = "Return 123.sas";
    String descriptionText = "qwerty";
    String pageRefs = "5";
    String language = "en";
    String commentID = "comment";
    String commentOID = "COM." + commentID;
    String itemGroupID1 = "dataset1";
    String itemGroupOID1 = "IG." + itemGroupID1;
    String itemGroupID2 = "dataset1";
    String itemGroupOID2 = "IG." + itemGroupID2;
    String whereClauseID1 = "whereclause1";
    String whereClauseOID1 = "WC." + whereClauseID1;
    String whereClauseID2 = "whereclause2";
    String whereClauseOID2 = "WC." + whereClauseID2;
    String itemID1 = "item1";
    String itemOID1 = "IT." + itemID1;
    String itemID2 = "item2";
    String itemOID2 = "IT." + itemID2;
    String resultID = "result1";
    String resultOID = "AR." + resultID;
    String analysisReason = "xyz";
    String analysisPurpose = "asd";
    String displayID = "display1";
    String displayOID = "RD." + displayID;
    String displayName = displayID;


    net.pinnacle21.define.models.ItemGroup itemGroup1 = Helpers.getItemGroup1();
    net.pinnacle21.define.models.ItemGroup itemGroup2 = Helpers.getItemGroup2();
    net.pinnacle21.define.models.Document document = Helpers.getDocument();
    Comment comment = Helpers.getComment();
    WhereClause whereclause1 = Helpers.getWhereClause1();
    WhereClause whereclause2 = Helpers.getWhereClause2();
    net.pinnacle21.define.models.Item item1 = Helpers.getItem1();
    net.pinnacle21.define.models.Item item2 = Helpers.getItem2();



    DefineMetadata metadata = DefineMetadata.V2;
    Map<String, String> namepsaces = DefineMetadata.V2.getNamespaces().xpathNamespaces();

    Element programmingCodeElementWithCode = Helpers.getProgrammingCodeElement();
    ProgrammingCode programmingCodeObjectWithCode = Helpers.getProgrammingCodeObject();
    Element programmingCodeElementWithDocument = Helpers.programmingCodeElementWithDocument();
    ProgrammingCode programmingCodeObjectWithDocument = Helpers.programmingCodeObjectWithDocument();
    Element documentationElement = Helpers.getDocumentationElement();
    Documentation documentationObject = Helpers.getDocumentationObject();
    Element documentationElementNoDocumentReference = Helpers.getDocumentationElementNoDocumentReference();
    Documentation documentationObjectNoDocumentReference = Helpers.getDocumentationObjectNoDocumentReference();
    Element analysisDatasetsElement = Helpers.getAnalysisDatasetsElement();
    AnalysisDatasets analysisDatasetsObject = Helpers.getAnalysisDatasetsObject();
    Element analysisResultElement = Helpers.getAnalysisResultElement();
    AnalysisResult analysisResultObject = Helpers.getAnalysisResultObject();
    Element resultDisplayElement = Helpers.getResultDisplayElement();
    ResultDisplay resultDisplayObject = Helpers.getResultDisplayObject();


    class Helpers {
        private static DocumentFactory factory = getDocumentFactory();
        private static Documentation documentation;
        private static Element resultDisplayElement;

        static Element getProgrammingCodeElement() {
            Element codeElement = factory.createElement("arm:ProgrammingCode");
            codeElement.addNamespace(metadata.getNamespaces().armPrefix(), metadata.getNamespaces().armURI());
            codeElement.addAttribute("Context", context);
            codeElement.addElement("arm:Code").setText(codeText);

            return codeElement;
        }

        static ProgrammingCode getProgrammingCodeObject() {
            return new ProgrammingCode().setContext(context).setCode(codeText);
        }

        public static Element programmingCodeElementWithDocument() {
            Element codeElement = factory.createElement("arm:ProgrammingCode");
            codeElement.addNamespace(metadata.getNamespaces().armPrefix(), metadata.getNamespaces().armURI());
            codeElement.addNamespace(metadata.getNamespaces().defPrefix(), metadata.getNamespaces().defURI());
            codeElement.addAttribute("Context", context);
            codeElement.addElement("def:DocumentRef").addAttribute("leafID", leafID);

            return codeElement;
        }

        public static ProgrammingCode programmingCodeObjectWithDocument() {
            return new ProgrammingCode()
                    .setContext(context)
                    .setDocumentReference(new DocumentReference()
                            .setDocument(getDocument()));
        }

        private static DocumentFactory getDocumentFactory() {
            DocumentFactory factory = new DocumentFactory();
            factory.setXPathNamespaceURIs(namepsaces);
            return factory;
        }

        public static Document getDocument() {
            return new Document().setOid(leafID).setHref(leafHref).setTitle(leafTitle);
        }

        public static WhereClause getWhereClause1() {
            return new WhereClause()
                    .setOid(whereClauseID1)
                    .add(new RangeCheck()
                            .setComparator("EQ")
                            .setItemGroupOid(itemGroupOID1)
                            .setItemOid(itemOID1));
        }

        public static WhereClause getWhereClause2() {
            return new WhereClause()
                    .setOid(whereClauseID2)
                    .add(new RangeCheck()
                            .setComparator("EQ")
                            .setItemGroupOid(itemGroupID2)
                            .setItemOid(itemOID2));
        }

        public static Comment getComment() {
            return new Comment()
                    .setOid(commentID);
        }

        public static ItemGroup getItemGroup1() {
            return new ItemGroup().setOID(itemGroupID1);
        }

        public static Item getItem1() {
            return new Item().setOID(itemID1);
        }

        public static ItemGroup getItemGroup2() {
            return new ItemGroup().setOID(itemGroupID2);
        }

        public static Item getItem2() {
            return new Item().setOID(itemID2);
        }

        public static Description getDescription() {
            return new Description().setText(descriptionText).setLanguage(language);
        }

        public static Element getDocumentationElement() {
            Element documentation = factory.createElement("arm:Documentation");
            documentation.addNamespace(metadata.getNamespaces().odmPrefix(), metadata.getNamespaces().odmURI());
            documentation.addNamespace(metadata.getNamespaces().armPrefix(), metadata.getNamespaces().armURI());
            documentation.addNamespace(metadata.getNamespaces().defPrefix(), metadata.getNamespaces().defURI());

            documentation.addElement("Description")
                    .addElement("TranslatedText")
                    .addAttribute(new QName("lang", new Namespace(metadata.getNamespaces().xmlPrefix(), metadata.getNamespaces().xmlURI())), language)
                    .setText(descriptionText);
            documentation.addElement("def:DocumentRef")
                    .addNamespace(metadata.getNamespaces().defPrefix(), metadata.getNamespaces().defURI())
                    .addAttribute("leafID", leafID)
                    .addElement("def:PDFPageRef")
                    .addNamespace(metadata.getNamespaces().defPrefix(), metadata.getNamespaces().defURI())
                    .addAttribute("PageRefs", pageRefs).addAttribute("Type", "PhysicalRef");
            return documentation;
        }

        public static Documentation getDocumentationObject() {
            return new Documentation()
                    .setDescription(new Description().setText(descriptionText).setLanguage(language))
                    .addDocumentReference(new DocumentReference()
                            .setDocument(getDocument())
                            .setPages(pageRefs));
        }

        public static Element getDocumentationElementNoDocumentReference() {
            Element documentation = factory.createElement("arm:Documentation");
            documentation.addNamespace(metadata.getNamespaces().odmPrefix(), metadata.getNamespaces().odmURI());
            documentation.addNamespace(metadata.getNamespaces().armPrefix(), metadata.getNamespaces().armURI());
            documentation.addNamespace(metadata.getNamespaces().defPrefix(), metadata.getNamespaces().defURI());

            documentation.addElement("Description")
                .addElement("TranslatedText")
                .addAttribute(new QName("lang", new Namespace(metadata.getNamespaces().xmlPrefix(), metadata.getNamespaces().xmlURI())), language)
                .setText(descriptionText);
            return documentation;
        }

        public static Documentation getDocumentationObjectNoDocumentReference() {
            return new Documentation()
                .setDescription(new Description().setText(descriptionText).setLanguage(language));
        }

        public static Element getAnalysisDatasetsElement() {
            Element datasets = factory.createElement("arm:AnalysisDatasets")
                    .addNamespace(metadata.getNamespaces().armPrefix(), metadata.getNamespaces().armURI())
                    .addNamespace(metadata.getNamespaces().defPrefix(), metadata.getNamespaces().defURI())
                    .addAttribute(new QName("CommentOID", new Namespace(metadata.getNamespaces().defPrefix(), metadata.getNamespaces().defURI())), commentOID);

            Element dataset1 = datasets.addElement("arm:AnalysisDataset")
                    .addNamespace(metadata.getNamespaces().armPrefix(), metadata.getNamespaces().armURI())
                    .addNamespace(metadata.getNamespaces().defPrefix(), metadata.getNamespaces().defURI())
                    .addAttribute("ItemGroupOID", itemGroupOID1);
            dataset1.addElement("def:WhereClauseRef").addAttribute("WhereClauseOID", whereClauseOID1);
            dataset1.addElement("arm:AnalysisVariable")
                    .addNamespace(metadata.getNamespaces().armPrefix(), metadata.getNamespaces().armURI())
                    .addAttribute("ItemOID", itemOID1);
            dataset1.addElement("arm:AnalysisVariable")
                    .addNamespace(metadata.getNamespaces().armPrefix(), metadata.getNamespaces().armURI())
                    .addAttribute("ItemOID", itemOID2);

            Element dataset2 = datasets.addElement("arm:AnalysisDataset")
                    .addNamespace(metadata.getNamespaces().armPrefix(), metadata.getNamespaces().armURI())
                    .addNamespace(metadata.getNamespaces().defPrefix(), metadata.getNamespaces().defURI())
                    .addAttribute("ItemGroupOID", itemGroupOID2);
            dataset2.addElement("def:WhereClauseRef").addAttribute("WhereClauseOID", whereClauseOID2);
            return datasets;
        }

        public static AnalysisDatasets getAnalysisDatasetsObject() {
            AnalysisDatasets datasets = new AnalysisDatasets();
            datasets.setComment(getComment());

            datasets.add(new AnalysisDataset()
                    .setItemGroup(getItemGroup1())
                    .setWhereClause(getWhereClause1())
                    .add(new AnalysisVariable().setItem(getItem1()))
                    .add(new AnalysisVariable().setItem(getItem2())));

            datasets.add(new AnalysisDataset()
                    .setItemGroup(getItemGroup2())
                    .setWhereClause(getWhereClause2()));
            return datasets;
        }

        public static Element getAnalysisResultElement() {
            Element result = factory.createElement("arm:AnalysisResult")
                    .addNamespace(metadata.getNamespaces().armPrefix(), metadata.getNamespaces().armURI())
                    .addNamespace(metadata.getNamespaces().defPrefix(), metadata.getNamespaces().defURI())
                    .addAttribute("OID", resultOID)
                    .addAttribute("ParameterOID", itemOID1)
                    .addAttribute("AnalysisReason", analysisReason)
                    .addAttribute("AnalysisPurpose", analysisPurpose);
            result.addElement("Description")
                    .addElement("TranslatedText")
                    .addAttribute(new QName("lang", new Namespace(metadata.getNamespaces().xmlPrefix(), metadata.getNamespaces().xmlURI())), "en")
                    .setText(descriptionText);
            return result;
        }

        public static AnalysisResult getAnalysisResultObject() {
            AnalysisResult result = new AnalysisResult()
                    .setOID(resultID)
                    .setParameterOID(itemID1)
                    .setParamcdDataset(itemGroupID1)
                    .setAnalysisReason(analysisReason)
                    .setAnalysisPurpose(analysisPurpose)
                    .setDescription(getDescription());

            return result;
        }

        public static Element getResultDisplayElement() {
            Element result = factory.createElement("arm:ResultDisplay")
                    .addNamespace(metadata.getNamespaces().armPrefix(), metadata.getNamespaces().armURI())
                    .addNamespace(metadata.getNamespaces().defPrefix(), metadata.getNamespaces().defURI())
                    .addAttribute("OID", displayOID)
                    .addAttribute("Name", displayName);
            result.addElement("Description")
                    .addElement("TranslatedText")
                    .addAttribute(new QName("lang", new Namespace(metadata.getNamespaces().xmlPrefix(), metadata.getNamespaces().xmlURI())), "en")
                    .setText(descriptionText);
            result.addElement("def:DocumentRef")
                    .addNamespace(metadata.getNamespaces().defPrefix(), metadata.getNamespaces().defURI())
                    .addAttribute("leafID", leafID)
                    .addElement("def:PDFPageRef")
                    .addNamespace(metadata.getNamespaces().defPrefix(), metadata.getNamespaces().defURI())
                    .addAttribute("PageRefs", pageRefs).addAttribute("Type", "PhysicalRef");
            return result;
        }

        public static ResultDisplay getResultDisplayObject() {
            ResultDisplay display = new ResultDisplay()
                    .setOID(displayID)
                    .setName(displayName)
                    .setDescription(getDescription())
                    .setDocumentReference(new DocumentReference().setDocument(getDocument()).setPages(pageRefs));

            return display;
        }
    }
}

