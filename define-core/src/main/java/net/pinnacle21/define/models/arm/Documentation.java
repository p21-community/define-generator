/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models.arm;

import net.pinnacle21.define.models.Description;
import net.pinnacle21.define.models.DocumentReference;

import java.util.ArrayList;
import java.util.List;

public class Documentation {
    private Description description;
    private List<DocumentReference> documentReferences;

    public Description getDescription() {
        return description;
    }

    public Documentation setDescription(Description description) {
        this.description = description;
        return this;
    }

    public List<DocumentReference> getDocumentReferences() {
        return documentReferences;
    }

    public Documentation addDocumentReference(DocumentReference documentReference) {
        if (documentReference != null) {
            documentReferences = documentReferences != null ? documentReferences : new ArrayList<>();
            this.documentReferences.add(documentReference);
        }

        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Documentation that = (Documentation) o;

        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        return documentReferences != null ? documentReferences.equals(that.documentReferences) : that.documentReferences == null;

    }
}
