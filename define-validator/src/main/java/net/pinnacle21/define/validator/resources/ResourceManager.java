/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.resources;

import net.pinnacle21.define.models.DefineMetadata;
import net.pinnacle21.validator.api.model.ConfigOptions;
import net.pinnacle21.validator.api.model.ValidationOptions;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ResourceManager implements AutoCloseable {
    private static final String LOCAL_BUILD_VERSION = "0-LOCAL-SNAPSHOT";

    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceManager.class);

    private enum UnpackState {
        Failure,
        Success,
        Pending
    }

    private final File define;
    private final File globalTarget;
    private final DefineLineReader lineReader;
    private final ValidationOptions options;
    private final ConfigOptions config;

    private UnpackState unpackState = UnpackState.Pending;

    public ResourceManager(File define, ConfigOptions config, ValidationOptions options) {
        this.define = define;
        this.config = config;
        this.options = options;
        this.lineReader = new DefineLineReader(define);
        this.globalTarget = getUnpackDirectory(options, config);
    }

    public DefineLineReader getLineReader() {
        return this.lineReader;
    }

    public File getDefine() {
        return this.define;
    }

    public ConfigOptions getConfigOptions() {
        return this.config;
    }

    public File getDefineResource(String path) {
        // FIXME: SECURITY: Make sure we protect where this path sniffing can go
        return new File(this.define.getParentFile(), path);
    }

    public File getGlobalResource(String path) {
        if (this.unpackState == UnpackState.Pending) {
            this.unpack(ResourceType.VALIDATION);

            if (this.unpackState == UnpackState.Failure) {
                throw new IllegalStateException("Global resource folder " + this.globalTarget.getAbsolutePath()
                        + " was not created correctly so validation cannot continue");
            }
        }

        return new File(this.globalTarget, path);
    }

    @Override
    public void close() throws IOException {
        this.lineReader.close();
    }

    public Document getOdmMetadata(DefineMetadata metadata) {
        String version;

        switch (metadata) {
            case V1:
                version = "1.2.1";
                break;
            case V2:
            default:
                version = "1.3.2";
                break;
        }

        return this.parseFile(this.getGlobalResource(String.format("data/ODM Metadata %s.xml", version)));
    }

    public Document getDefineMetadata(DefineMetadata metadata) {
        String version;

        switch (metadata) {
            case V1:
                version = "1.0";
                break;
            case V2:
            default:
                version = "2.0";
                break;
        }

        return this.parseFile(this.getGlobalResource(String.format("data/Define Metadata %s.xml", version)));
    }

    private void unpack(ResourceType type) {
        boolean result = false;

        try {
            switch (type) {
                case VALIDATION:
                    try {
                        result = unpackToDirectory(type, this.globalTarget, getJar());
                    } catch (URISyntaxException ex) {
                        LOGGER.error("Unable to parse jar location as URI to extract files.", ex);
                    }
                    break;
                default:
                    throw new UnsupportedOperationException(String.format("Unknown ResourceType '%s'", type));
            }
        } finally {
            this.unpackState = result ? UnpackState.Success : UnpackState.Failure;
        }
    }

    private synchronized boolean unpackToDirectory(ResourceType type, File directory, File root) {
        boolean result = true;

        try {
            LOGGER.debug("Unpacking resources to root '{}'.", directory.getPath());

            if (!directory.mkdirs() && !directory.isDirectory()) {
                LOGGER.error("Unable to create or access directory " + directory.getAbsolutePath());

                return false;
            }

            if (root.isDirectory()) { //Running locally, the jar file is actually an open folder in a 'target' directory
                // The files should already be available to us to copy directly
                root = new File(root, type.root);
                FileUtils.copyDirectory(root, directory);
            } else { //Files are in a jar file
                try (
                    FileChannel channel = FileChannel.open(directory.toPath().resolve(".LOCK"),
                        StandardOpenOption.CREATE, StandardOpenOption.WRITE);
                    FileLock lock = acquireLock(channel)
                ) {
                    // You can run into concurrency problems because of the local build version behaviour, so don't use
                    // that for production!
                    if (directory.getName().endsWith(LOCAL_BUILD_VERSION)) {
                        return false;
                    }

                    if (!type.sanityCheck(directory)) {
                        result = doCopy(new JarFile(root), type, directory);
                    }

                    //check again to make sure we succeeded
                    if (!type.sanityCheck(directory)) {
                        result = false;
                    }
                }
            }

            LOGGER.debug("Unpacking complete!");
        } catch (IOException ex) {
            LOGGER.error("Could not unpack resources to '{}'.", directory, ex);

            return false;
        }

        return result;
    }

    private boolean doCopy(JarFile jar, ResourceType type, File directory) {
        for (Enumeration<JarEntry> e = jar.entries(); e.hasMoreElements();) {
            JarEntry entry = e.nextElement();
            String name = entry.getName();
            String prefix = type.root + "/";
            if (name.startsWith(prefix) && !entry.isDirectory() && !name.endsWith(".class")) { //Get every file that isn't a .class file or directory
                File targetFile = new File(directory, name.substring(prefix.length()));
                File targetFolder = targetFile.getParentFile(); //Get that files parent folder

                boolean exists = targetFolder.exists();
                boolean mkdirs = targetFolder.mkdirs();
                LOGGER.trace("Status for folder '{}': exists={}, mkdirs={}", targetFolder.getName(), exists, mkdirs);

                if (exists || mkdirs) {
                    try {
                        LOGGER.trace("Copying file '{}'", targetFile.getName());
                        long bytes = Files.copy(jar.getInputStream(entry), targetFile.toPath(),
                                StandardCopyOption.REPLACE_EXISTING); //Copy that file over
                        if (bytes == 0) {
                            LOGGER.error("Copied file '{}' was empty, aborting extract", targetFile.getName());
                            throw new IOException(String.format("Copied file '%s' was empty when not expecting an empty file", targetFile.getName()));
                        } else {
                            LOGGER.trace("Copy complete. Size was '{}' bytes", bytes);
                        }
                    } catch (IOException | SecurityException ex) {
                        LOGGER.error("Unable to copy file '{}'", targetFile.getName(), ex);

                        return false;
                    }
                } else {
                    // else we couldn't create the folder, sad days
                    LOGGER.error("Unable to create folder '{}' for housing define validation metadata!",
                            targetFolder.getPath());

                    return false;
                }
            }
        }

        return true;
    }

    private Document parseFile(File file) {
        try (
                InputStream is = new FileInputStream(file);
                InputStream bs = new BufferedInputStream(is)
        ) {
            return new SAXReader().read(bs);
        } catch (IOException | DocumentException ex) {
            LOGGER.error("Unable to parse {} due to an exception", file, ex);

            // TODO: Not ideal that we have to bubble this up with a runtime exception here
            throw new RuntimeException(ex);
        }
    }

    private static File getJar() throws URISyntaxException {
        String location = ResourceManager.class.getResource("").getPath();

        if (!location.contains(".jar")) {
            return new File(ResourceManager.class.getProtectionDomain().getCodeSource().getLocation().toURI());
        }

        int end = location.lastIndexOf("!");
        int start = location.indexOf("file:/") + "file:/".length();

        return new File(location.substring(start, end));
    }

    private static FileLock acquireLock(FileChannel channel) {
        FileLock lock = null;
        try {
            LOGGER.trace("Attempting to acquire resource lock");
            while ((lock = channel.tryLock()) == null) {
                LOGGER.trace("Sleeping to wait for lock to open.");
                sleep();
            }
            LOGGER.trace("Lock acquired.");
        } catch (IOException e) {
            LOGGER.warn("Failed to acquire lock file with exception. Continuing anyway.", e);
            LOGGER.trace("Attempting to sleep because acquiring lock failed.");
            sleep();
            LOGGER.trace("Awake.");
        }
        return lock;
    }

    private static void sleep() {
        try {
            Thread.sleep((long) (Math.random() * 1000));
        } catch (InterruptedException e) {
            LOGGER.warn("Thread sleep interrupted with exception.", e);
        }
    }

    private static File getUnpackDirectory(ValidationOptions options, ConfigOptions configOptions) {
        Package pack = ResourceManager.class.getPackage();
        File target = Paths.get(System.getProperty("java.io.tmpdir"), pack.getName(),
                StringUtils.defaultString(pack.getImplementationVersion(), LOCAL_BUILD_VERSION)).toFile();

        if (options.hasProperty("Resources.UnpackDirectory")) {
            String unpack = options.getProperty("Resources.UnpackDirectory");

            LOGGER.debug("Picking alternative unpack directory based on value {}", unpack);

            if ("$ConfigRoot".equalsIgnoreCase(unpack)) {
                target = new File(configOptions.getConfigs().iterator().next()).getParentFile();
            } else {
                target = new File(unpack);
            }

            if (!target.isDirectory() && !target.mkdirs()) {
                throw new IllegalArgumentException("Unable to create resource unpack directory "
                        + target.getAbsolutePath());
            }
        }

        LOGGER.debug("Unpack directory will be {}", target);

        return target;
    }
}
