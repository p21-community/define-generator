/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.xml.bodyparsers;

import net.pinnacle21.define.models.*;
import net.pinnacle21.define.models.arm.ResultDisplay;
import net.pinnacle21.define.parsers.xml.XPathEvaluatorFactory;
import net.pinnacle21.define.util.Templates;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Element;
import org.dom4j.Node;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class DefineV1BodyParser extends BodyParser {
    DefineV1BodyParser(org.dom4j.Document document, boolean forValidation) {
        super(XPathEvaluatorFactory.newInstance().define1XPathEvaluator(document), DefineMetadata.V1, forValidation);
    }

    @Override
    protected List<ResultDisplay> parseResultDisplays(Map<String, WhereClause> whereClauseMap) {
        return null; //Define 1 doesn't have ARM.
    }

    Map<String, Comment> parseComments() {
        Map<String, Comment> commentMap = new HashMap<>();
        List<Element> itemDefs = this.evaluator.selectElements("/ODM/odm:Study/odm:MetaDataVersion/odm:ItemDef");

        for (Element itemDefElement : itemDefs) {
            if (itemDefElement.attribute("Comment") != null && StringUtils.isNotBlank(itemDefElement.attributeValue("Comment"))) {
                String oid = itemDefElement.attributeValue("OID", "");
                Comment comment = new Comment().setOid(
                        oid.startsWith(metadata.getPrefixes().item()) ? oid.substring(3) : oid);
                comment.setDescription(itemDefElement.attributeValue("Comment"));
                commentMap.put(oid, comment);
            }
        }
        return commentMap;
    }

    Method parseMethod(Method method, Element methodElement) {
        method.setName("Algorithm to derive " + method.getOid());
        method.setType("Computation");
        method.setDescription(methodElement.getText());

        return method;
    }

    @Override
    CodeListItem parseCodeListItem(CodeListItem codeListItem, Element childElement, Node decode) {
        codeListItem.setOrder(childElement.attributeValue("Rank"));
        if (decode != null) {
            codeListItem.setDecodedValue(decode.getText());
            codeListItem.setLanguage(getXmlLang((Element) decode));
        }

        return codeListItem;
    }

    @Override
    void parseItemGroup(Map<Integer, String> keyMap, ItemGroup itemGroup, Element itemGroupDefElement) {
        itemGroup.setDescription(itemGroupDefElement.attributeValue("Label"));
        itemGroup.setKeys(itemGroupDefElement.attributeValue("DomainKeys"));
    }

    List<Element> getMethods() {
        return this.evaluator.selectElements("/ODM/odm:Study/odm:MetaDataVersion/def:ComputationMethod");
    }

    @Override
    String getMethodOID(Element itemDef, Element itemRef) {
        return itemDef.attributeValue("ComputationMethodOID", "");
    }

    @Override
    void populateItemAttributes(Item item, Element itemDef, boolean isValueLevel) {
        item.setLabel(itemDef.attributeValue("Label"));
        item.setOrigin(determineOrigin(itemDef.attributeValue("Origin")));
        item.setPages(getPagesFromOrigin(itemDef.attributeValue("Origin")));
        item.setCommentOid(StringUtils.isNotBlank(itemDef.attributeValue("Comment"))
                ? itemDef.attributeValue("OID").startsWith(metadata.getPrefixes().item())
                ? itemDef.attributeValue("OID").substring(3)
                : itemDef.attributeValue("OID")
                : null);

        String method = itemDef.attributeValue("ComputationMethodOID", "");
        item.setMethodOid(method.startsWith("MT.") ? method.substring(3) : method);
    }

    @Override
    WhereClause getWhereClause(String sourceVariableName, String itemGroupName, Element valueListItemDef,
                               Element itemRef, Map<String, WhereClause> whereClauseMap) {
        WhereClause whereClause = new WhereClause();
            RangeCheck rangeCheck = new RangeCheck();
            rangeCheck.setItemGroupOid(itemGroupName);
            rangeCheck.setItemOid(sourceVariableName);
            rangeCheck.setComparator("EQ");
            rangeCheck.addValue(valueListItemDef.attributeValue("Name", ""));
            whereClause.add(rangeCheck);
            whereClause.setOid(Templates.generateWhereClauseOid(Collections.singletonList(rangeCheck)));
        return whereClause;
    }

}
