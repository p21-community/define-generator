/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define;

import net.pinnacle21.define.models.*;
import net.pinnacle21.define.models.Comment;
import net.pinnacle21.define.models.arm.*;
import net.sf.saxon.exslt.Date;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.POIXMLProperties;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ExcelGenerator {

    private static boolean armSupport = true;

    private static final DefineGenerator.Metadata DEFAULT_METADATA =
            new DefineGenerator.Metadata().setOriginator("Pinnacle 21");

    private static void enableArmSupport() {
        armSupport = true;
    }

    private static void disableArmSupport() {
        armSupport = false;
    }

    enum Sheets {
        Study,
        Datasets,
        Variables,
        ValueLevel,
        WhereClauses,
        Codelists,
        Dictionaries,
        Methods,
        Comments,
        Documents,
        Analysis_Displays("Analysis Displays", true),
        Analysis_Results("Analysis Results", true),
        Analysis_Criteria("Analysis Criteria", true);

        private boolean isAdamOnly = false;
        private String name;

        Sheets() {

        }

        Sheets(String name, boolean isAdamOnly) {
            this.name = name;
            this.isAdamOnly = isAdamOnly;
        }

        boolean isAdamOnly() {
            return isAdamOnly;
        }

        public String getSheetName() {
            return this.name == null ? super.name() : this.name;
        }
    }

    private static class Orders{

        static final Comparator<ItemGroup> itemGroups = new Comparator<ItemGroup>() {
            @Override
            public int compare(ItemGroup o1, ItemGroup o2) {
                return o1.getName().compareTo(o2.getName());
            }
        };

    }

    private static Workbook writeToWorkbook(Study study) {
        return writeToWorkbook(study, DEFAULT_METADATA);
    }

    private static Workbook writeToWorkbook(Study study, DefineGenerator.Metadata metadata) {
        //Create blank workbook
        Workbook workbook = new XSSFWorkbook();
        applyWorkbookMetadata((XSSFWorkbook) workbook, metadata);

        boolean isAdam = study.isAdam();

        //Create blank sheets
        XSSFSheet studySheet = (XSSFSheet) workbook.createSheet(Sheets.Study.getSheetName());
        studySheet.setZoom(125); //sets zoom to 125%
        studySheet.createFreezePane(0, 1); //freezes header
        XSSFSheet domains = (XSSFSheet) workbook.createSheet(Sheets.Datasets.getSheetName());
        domains.setZoom(125); //sets zoom to 125%
        domains.createFreezePane(0, 1); //freezes header
        XSSFSheet vars = (XSSFSheet) workbook.createSheet(Sheets.Variables.getSheetName());
        vars.setZoom(125); //sets zoom to 125%
        vars.createFreezePane(3, 1); //freezes header
        XSSFSheet valLists = (XSSFSheet) workbook.createSheet(Sheets.ValueLevel.getSheetName());
        valLists.setZoom(125); //sets zoom to 125%
        valLists.createFreezePane(0, 1); //freezes header
        XSSFSheet whereClauses = (XSSFSheet) workbook.createSheet(Sheets.WhereClauses.getSheetName());
        whereClauses.setZoom(125); //sets zoom to 125%
        whereClauses.createFreezePane(0, 1); //freezes header
        XSSFSheet codeLists = (XSSFSheet) workbook.createSheet(Sheets.Codelists.getSheetName());
        codeLists.setZoom(125); //sets zoom to 125%
        codeLists.createFreezePane(1, 1); //freezes header
        XSSFSheet dictionaries = (XSSFSheet) workbook.createSheet(Sheets.Dictionaries.getSheetName());
        dictionaries.setZoom(125); //sets zoom to 125%
        dictionaries.createFreezePane(1, 1); //freezes header
        XSSFSheet methods = (XSSFSheet) workbook.createSheet(Sheets.Methods.getSheetName());
        methods.setZoom(125); //sets zoom to 125%
        methods.createFreezePane(0, 1); //freezes header
        XSSFSheet comments = (XSSFSheet) workbook.createSheet(Sheets.Comments.getSheetName());
        comments.setZoom(125); //sets zoom to 125%
        comments.createFreezePane(0, 1); //freezes header
        XSSFSheet documents = (XSSFSheet) workbook.createSheet(Sheets.Documents.getSheetName());
        documents.setZoom(125); //sets zoom to 125%
        documents.createFreezePane(0, 1); //freezes header
        XSSFSheet analysisDisplays = null;
        XSSFSheet analysisResults = null;
        XSSFSheet analysisCriteria = null;
        if (isAdam && armSupport) {
            analysisDisplays = (XSSFSheet) workbook.createSheet(Sheets.Analysis_Displays.getSheetName());
            analysisDisplays.setZoom(125); //sets zoom to 125%
            analysisDisplays.createFreezePane(0, 1); //freezes header
            analysisResults = (XSSFSheet) workbook.createSheet(Sheets.Analysis_Results.getSheetName());
            analysisResults.setZoom(125); //sets zoom to 125%
            analysisResults.createFreezePane(0, 1); //freezes header
            analysisCriteria = (XSSFSheet) workbook.createSheet(Sheets.Analysis_Criteria.getSheetName());
            analysisCriteria.setZoom(125); //sets zoom to 125%
            analysisCriteria.createFreezePane(0, 1); //freezes header
        }

        /* *CREATE STYLES* */

        DataFormat format = workbook.createDataFormat();

        Font defaultFont = workbook.createFont();
        defaultFont.setFontHeightInPoints((short) 11);
        defaultFont.setFontName("Calibri");
        defaultFont.setColor(IndexedColors.BLACK.getIndex());
        CellStyle defaultStyle = workbook.createCellStyle();
        defaultStyle.setDataFormat(format.getFormat(BuiltinFormats.getBuiltinFormat(0)));
        defaultStyle.setFont(defaultFont);

        Font headerFont = workbook.createFont();
        headerFont.setFontHeightInPoints((short) 11);
        headerFont.setFontName("Calibri");
        headerFont.setBold(true);
        CellStyle headerStyle = workbook.createCellStyle();
        headerStyle.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerStyle.setFont(headerFont);

        CellStyle wrappedCell = workbook.createCellStyle();
        wrappedCell.setFont(defaultFont);
        wrappedCell.setWrapText(true);

        List<CellStyle> stylesNotToChange = new ArrayList<>();
        stylesNotToChange.add(wrappedCell);
//        stylesNotToChange.add(numberFormattedCell);

        //Create headerRows for each sheet
        Row studyHeader = studySheet.createRow(0);
        Row domainHeader = domains.createRow(0);
        Row varsHeader = vars.createRow(0);
        Row codeListHeader = codeLists.createRow(0);
        Row dictionariesHeader = dictionaries.createRow(0);
        Row valListHeader = valLists.createRow(0);
        Row whereClausesHeader = whereClauses.createRow(0);
        Row methodsHeader = methods.createRow(0);
        Row commentsHeader = comments.createRow(0);
        Row documentsHeader = documents.createRow(0);
        Row analysisDisplaysHeader = null;
        Row analysisResultsHeader = null;
        Row analysisCriteriaHeader = null;
        if (isAdam && armSupport) {
            analysisDisplaysHeader = analysisDisplays.createRow(0);
            analysisResultsHeader = analysisResults.createRow(0);
            analysisCriteriaHeader = analysisCriteria.createRow(0);
        }

        //Create variables to increment columns and rows
        int rowCount;
        int studyCellColumn = 0;
        int domainCellColumn = 0;
        int varCellColumn = 0;
        int codelistCellColumn = 0;
        int dictionariesCellColumn = 0;
        int valListCellColumn = 0;
        int whereClausesCellColumn = 0;
        int methodsCellColumn = 0;
        int commentsCellColumn = 0;
        int documentsCellColumn = 0;
        int analysisDisplaysCellColumn = 0;
        int analysisResultsCellColumn = 0;
        int analysisCriteriaCellColumn = 0;


        //Create list of Study Structures
        List<ItemGroup> itemGroups =  study.getItemGroups();
        Comparator<ItemGroup> comparator = Orders.itemGroups;
        Collections.sort(itemGroups, comparator);
        //Create list of Items(Variables)
        List<Item> itemList = new ArrayList<>();
        for (ItemGroup structure : itemGroups){
            for (Item item : structure.getItems()){
                itemList.add(item);
            }
        }
        //Create list of Study Value Level Metadata
        List<ValueList> valueLevelMetadata = study.getValueLists();
        //Create list of Dictionaries
        List<Dictionary> dictionaryList = study.getDictionaries();
        //Create list of Codelists
        List<CodeList> studyCodelists = study.getCodeLists();
        //Create list of Methods
        List<Method> methodList = study.getMethods();
        //Create list of Comments
        List<Comment> commentList = study.getComments();
        //Create list of Documents
        List<Document> documentList = study.getAnnotatedCrfs();
        for (Document document : study.getSupplementalDocuments()) {
            documentList.add(document);
        }
        //Create list of WhereClause
        List<WhereClause> whereClauseList = study.getWhereClauses();
        List<ResultDisplay> resultDisplayList = new ArrayList<>();
        if (isAdam && armSupport) {
            resultDisplayList = study.getResultDisplays();
        }

        /* ****START STUDY SHEET CREATE**** */
        studySheet.getRow(0).createCell(studyCellColumn).setCellValue("Attribute");
        studySheet.setColumnWidth(studyCellColumn, 4000);
        rowCount = 1;
        studySheet.createRow(rowCount).createCell(studyCellColumn).setCellValue("StudyName");
        rowCount++;
        studySheet.createRow(rowCount).createCell(studyCellColumn).setCellValue("StudyDescription");
        rowCount++;
        studySheet.createRow(rowCount).createCell(studyCellColumn).setCellValue("ProtocolName");
        rowCount++;
        studySheet.createRow(rowCount).createCell(studyCellColumn).setCellValue("StandardName");
        rowCount++;
        studySheet.createRow(rowCount).createCell(studyCellColumn).setCellValue("StandardVersion");
        rowCount++;
        studySheet.createRow(rowCount).createCell(studyCellColumn).setCellValue("Language");
        if (StringUtils.isNotBlank(study.getSdtmCtVersion())) {
            rowCount++;
            studySheet.createRow(rowCount).createCell(studyCellColumn).setCellValue("SDTMCTVersion");
        }
        if (StringUtils.isNotBlank(study.getAdamCtVersion())) {
            rowCount++;
            studySheet.createRow(rowCount).createCell(studyCellColumn).setCellValue("ADaMCTVersion");
        }
        if (StringUtils.isNotBlank(study.getSendCtVersion())) {
            rowCount++;
            studySheet.createRow(rowCount).createCell(studyCellColumn).setCellValue("SENDCTVersion");
        }

        studyCellColumn++;

        studySheet.getRow(0).createCell(studyCellColumn).setCellValue("Value");
        studySheet.setColumnWidth(studyCellColumn, 16000);
        rowCount = 1;
        studySheet.getRow(rowCount).createCell(studyCellColumn).setCellValue(study.getName());
        rowCount++;
        studySheet.getRow(rowCount).createCell(studyCellColumn).setCellValue(study.getDescription());
        rowCount++;
        if (StringUtils.isNotEmpty(study.getProtocol())) {
            studySheet.getRow(rowCount).createCell(studyCellColumn).setCellValue(study.getProtocol());
        }
        rowCount++;
        studySheet.getRow(rowCount).createCell(studyCellColumn).setCellValue(study.getStandardName().split(" ")[0]);
        rowCount++;
        studySheet.getRow(rowCount).createCell(studyCellColumn).setCellValue(study.getStandardVersion());
        rowCount++;
        studySheet.getRow(rowCount).createCell(studyCellColumn).setCellValue(study.getLanguage());
        if (StringUtils.isNotBlank(study.getSdtmCtVersion())) {
            rowCount++;
            studySheet.getRow(rowCount).createCell(studyCellColumn).setCellValue(study.getSdtmCtVersion());
        }
        if (StringUtils.isNotBlank(study.getAdamCtVersion())) {
            rowCount++;
            studySheet.getRow(rowCount).createCell(studyCellColumn).setCellValue(study.getAdamCtVersion());
        }
        if (StringUtils.isNotBlank(study.getSendCtVersion())) {
            rowCount++;
            studySheet.getRow(rowCount).createCell(studyCellColumn).setCellValue(study.getSendCtVersion());
        }

        /* ****START DOMAINS SHEET CREATE**** */
        domains.getRow(0).createCell(domainCellColumn).setCellValue("Dataset");
        domains.setColumnWidth(domainCellColumn, 3000);
        rowCount = 1;
        for (ItemGroup structure : itemGroups){
            domains.createRow(rowCount).createCell(domainCellColumn).setCellValue(structure.getName());
            rowCount++;
        }
        domainCellColumn++;

        domains.getRow(0).createCell(domainCellColumn).setCellValue("Description");
        domains.setColumnWidth(domainCellColumn, 6000);
        rowCount = 1;
        for (ItemGroup structure : itemGroups){
            if (StringUtils.isNotEmpty(structure.getDescription())) {
                domains.getRow(rowCount).createCell(domainCellColumn).setCellValue(structure.getDescription());
            }
            rowCount++;
        }
        domainCellColumn++;

        domains.getRow(0).createCell(domainCellColumn).setCellValue("Class");
        domains.setColumnWidth(domainCellColumn, 4000);
        rowCount = 1;
        for (ItemGroup structure : itemGroups){
            if (StringUtils.isNotEmpty(structure.getCategory())) {
                domains.getRow(rowCount).createCell(domainCellColumn).setCellValue(structure.getCategory());
            }
            rowCount++;
        }
        domainCellColumn++;

        domains.getRow(0).createCell(domainCellColumn).setCellValue("Structure");
        domains.setColumnWidth(domainCellColumn, 13000);
        rowCount = 1;
        for (ItemGroup structure : itemGroups){
            if (StringUtils.isNotEmpty(structure.getStructure())) {
                domains.getRow(rowCount).createCell(domainCellColumn).setCellValue(structure.getStructure());
            }
            rowCount++;
        }
        domainCellColumn++;

        domains.getRow(0).createCell(domainCellColumn).setCellValue("Purpose");
        domains.setColumnWidth(domainCellColumn, 3200);
        rowCount = 1;
        for (ItemGroup structure : itemGroups){
            if (StringUtils.isNotEmpty(structure.getPurpose())) {
                domains.getRow(rowCount).createCell(domainCellColumn).setCellValue(structure.getPurpose());
            }
            rowCount++;
        }
        domainCellColumn++;

        domains.getRow(0).createCell(domainCellColumn).setCellValue("Key Variables");
        domains.setColumnWidth(domainCellColumn, 15000);
        rowCount = 1;
        for (ItemGroup structure : itemGroups){
            if (!structure.getKeyVariables().isEmpty()) {
                domains.getRow(rowCount).createCell(domainCellColumn).setCellValue(structure.getKeyVariables().toString().substring(1, structure.getKeyVariables().toString().length() - 1).replace(", ", ","));
            }
            rowCount++;
        }
        domainCellColumn++;

        domains.getRow(0).createCell(domainCellColumn).setCellValue("Repeating");
        domains.setColumnWidth(domainCellColumn, 3200);
        rowCount = 1;
        for (ItemGroup structure : itemGroups){
            if (StringUtils.isNotEmpty(structure.getIsRepeating())) {
                domains.getRow(rowCount).createCell(domainCellColumn).setCellValue(structure.getIsRepeating());
            }
            rowCount++;
        }
        domainCellColumn++;

        domains.getRow(0).createCell(domainCellColumn).setCellValue("Reference Data");
        domains.setColumnWidth(domainCellColumn, 4000);
        rowCount = 1;
        for (ItemGroup structure : itemGroups){
            if (StringUtils.isNotEmpty(structure.getIsReferenceData())) {
                domains.getRow(rowCount).createCell(domainCellColumn).setCellValue(structure.getIsReferenceData());
            }
            rowCount++;
        }
        domainCellColumn++;

        domains.getRow(0).createCell(domainCellColumn).setCellValue("Comment");
        domains.setColumnWidth(domainCellColumn, 5000);
        rowCount = 1;
        for (ItemGroup structure : itemGroups){
            if (StringUtils.isNotEmpty(structure.getCommentOid())) {
                domains.getRow(rowCount).createCell(domainCellColumn).setCellValue(structure.getCommentOid());
            }
            rowCount++;
        }

        autoFilter(domains);

        /* ****START CREATING VARIABLES SHEET**** */

        vars.getRow(0).createCell(varCellColumn).setCellValue("Order");
        vars.setColumnWidth(varCellColumn, 2200);
        rowCount = 1;
        for (Item studyVar : itemList) {
            vars.createRow(rowCount).createCell(varCellColumn).setCellValue(studyVar.getOrder());
            rowCount++;
        }
        varCellColumn++;

        vars.getRow(0).createCell(varCellColumn).setCellValue("Dataset");
        vars.setColumnWidth(varCellColumn, 2500);
        rowCount = 1;
        for (ItemGroup domain : itemGroups) {
            for (int i = 0; i < domain.getItems().size(); i++) {
                vars.getRow(rowCount).createCell(varCellColumn).setCellValue(domain.getName());
                rowCount++;
            }
        }
        varCellColumn++;

        vars.getRow(0).createCell(varCellColumn).setCellValue("Variable");
        vars.setColumnWidth(varCellColumn, 3700);
        rowCount = 1;
        for (Item studyVar : itemList) {
            vars.getRow(rowCount).createCell(varCellColumn).setCellValue(studyVar.getName());
            rowCount++;
        }
        varCellColumn++;

        vars.getRow(0).createCell(varCellColumn).setCellValue("Label");
        vars.setColumnWidth(varCellColumn, 6000);
        rowCount = 1;
        for (Item studyVar : itemList) {
            if (StringUtils.isNotEmpty(studyVar.getLabel())) {
                vars.getRow(rowCount).createCell(varCellColumn).setCellValue(studyVar.getLabel());
            }
            rowCount++;
        }
        varCellColumn++;

        vars.getRow(0).createCell(varCellColumn).setCellValue("Data Type");
        vars.setColumnWidth(varCellColumn, 2800);
        rowCount = 1;
        for (Item studyVar : itemList) {
            if (StringUtils.isNotEmpty(studyVar.getDataType())) {
                vars.getRow(rowCount).createCell(varCellColumn).setCellValue(studyVar.getDataType());
            }
            rowCount++;
        }
        varCellColumn++;

        vars.getRow(0).createCell(varCellColumn).setCellValue("Length");
        vars.setColumnWidth(varCellColumn, 2200);
        rowCount = 1;
        for (Item studyVar : itemList) {
            if (!StringUtils.isEmpty(studyVar.getLength())){
                vars.getRow(rowCount).createCell(varCellColumn).setCellValue(studyVar.getLength());
            }
            rowCount++;
        }
        varCellColumn++;

        vars.getRow(0).createCell(varCellColumn).setCellValue("Significant Digits");
        vars.setColumnWidth(varCellColumn, 4000);
        rowCount = 1;
        for (Item studyVar : itemList) {
            if (!StringUtils.isEmpty(studyVar.getSignificantDigits())) {
                vars.getRow(rowCount).createCell(varCellColumn).setCellValue(studyVar.getSignificantDigits());
            }
            rowCount++;
        }
        varCellColumn++;

        vars.getRow(0).createCell(varCellColumn).setCellValue("Format");
        vars.setColumnWidth(varCellColumn, 2800);
        rowCount = 1;
        for (Item studyVar : itemList) {
            if (StringUtils.isNotEmpty(studyVar.getFormat())) {
                vars.getRow(rowCount).createCell(varCellColumn).setCellValue(studyVar.getFormat());
            }
            rowCount++;
        }
        varCellColumn++;

        vars.getRow(0).createCell(varCellColumn).setCellValue("Mandatory");
        vars.setColumnWidth(varCellColumn, 3200);
        rowCount = 1;
        for (Item studyVar : itemList) {
            if (StringUtils.isNotEmpty(studyVar.getMandatory())) {
                vars.getRow(rowCount).createCell(varCellColumn).setCellValue(studyVar.getMandatory());
            }
            rowCount++;
        }
        varCellColumn++;

        vars.getRow(0).createCell(varCellColumn).setCellValue("Codelist");
        vars.setColumnWidth(varCellColumn, 2800);
        rowCount = 1;
        for (Item studyVar : itemList) {
            if (StringUtils.isNotEmpty(studyVar.getCodeListOid())){
                vars.getRow(rowCount).createCell(varCellColumn).setCellValue(studyVar.getCodeListOid());
            } else if (StringUtils.isNotEmpty(studyVar.getDictionaryOid())) {
                vars.getRow(rowCount).createCell(varCellColumn).setCellValue(studyVar.getDictionaryOid());
            }
            rowCount++;
        }
        varCellColumn++;

        vars.getRow(0).createCell(varCellColumn).setCellValue("Origin");
        vars.setColumnWidth(varCellColumn, 4500);
        rowCount = 1;
        for (Item studyVar : itemList) {
            if (StringUtils.isNotEmpty(studyVar.getOrigin())) {
                vars.getRow(rowCount).createCell(varCellColumn).setCellValue(studyVar.getOrigin());
            }
            rowCount++;
        }
        varCellColumn++;

        vars.getRow(0).createCell(varCellColumn).setCellValue("Pages");
        vars.setColumnWidth(varCellColumn, 3000);
        rowCount = 1;
        for (Item studyVar : itemList) {
            if (StringUtils.isNotEmpty(studyVar.getPages())) {
                vars.getRow(rowCount).createCell(varCellColumn).setCellValue(studyVar.getPages());
            }
            rowCount++;
        }
        varCellColumn++;

        vars.getRow(0).createCell(varCellColumn).setCellValue("Method");
        vars.setColumnWidth(varCellColumn, 3000);
        rowCount = 1;
        for (Item studyVar : itemList) {
            if (StringUtils.isNotEmpty(studyVar.getMethodOid()))
                vars.getRow(rowCount).createCell(varCellColumn).setCellValue(studyVar.getMethodOid());
            rowCount++;
        }
        varCellColumn++;

        vars.getRow(0).createCell(varCellColumn).setCellValue("Predecessor");
        vars.setColumnWidth(varCellColumn, 3900);
        rowCount = 1;
        for (Item studyVar : itemList) {
            if (StringUtils.isNotEmpty(studyVar.getPredecessor()))
                vars.getRow(rowCount).createCell(varCellColumn).setCellValue(studyVar.getPredecessor());
            rowCount++;
        }
        varCellColumn++;

        vars.getRow(0).createCell(varCellColumn).setCellValue("Role");
        vars.setColumnWidth(varCellColumn, 4000);
        rowCount = 1;
        for (Item studyVar : itemList) {
            if (StringUtils.isNotEmpty(studyVar.getRole())) {
                vars.getRow(rowCount).createCell(varCellColumn).setCellValue(studyVar.getRole());
            }
            rowCount++;
        }
        varCellColumn++;

        vars.getRow(0).createCell(varCellColumn).setCellValue("Comment");
        vars.setColumnWidth(varCellColumn, 6300);
        rowCount = 1;
        for (Item studyVar : itemList) {
            if (StringUtils.isNotEmpty(studyVar.getCommentOid())) {
                vars.getRow(rowCount).createCell(varCellColumn).setCellValue(studyVar.getCommentOid());
            }
            rowCount++;
        }

        autoFilter(vars);

        /* ****START VALUELISTS SHEET CREATE**** */
        valLists.getRow(0).createCell(valListCellColumn).setCellValue("Order");
        valLists.setColumnWidth(valListCellColumn, 3000);
        rowCount = 1;
        for (ValueList valueLevel : valueLevelMetadata) {
            for (Item item : valueLevel.getItems()) {
                valLists.createRow(rowCount).createCell(valListCellColumn).setCellValue(item.getOrder());
                rowCount++;
            }
        }
        valListCellColumn++;

        valLists.getRow(0).createCell(valListCellColumn).setCellValue("Dataset");
        valLists.setColumnWidth(valListCellColumn, 4800);
        rowCount = 1;
        for (ValueList valueLevel : valueLevelMetadata) {
            for (int i = 0; i < valueLevel.getItems().size(); i++) {
                valLists.getRow(rowCount).createCell(valListCellColumn).setCellValue(valueLevel.getOid().split("\\.")[0]);
                rowCount++;
            }
        }
        valListCellColumn++;

        valLists.getRow(0).createCell(valListCellColumn).setCellValue("Variable");
        valLists.setColumnWidth(valListCellColumn, 4800);
        rowCount = 1;
        for (ValueList valueLevel : valueLevelMetadata) {
            for (int i = 0; i < valueLevel.getItems().size(); i++) {
                valLists.getRow(rowCount).createCell(valListCellColumn).setCellValue(valueLevel.getOid().split("\\.")[1]);
                rowCount++;
            }
        }
        valListCellColumn++;

        valLists.getRow(0).createCell(valListCellColumn).setCellValue("Where Clause");
        valLists.setColumnWidth(valListCellColumn, 5200);
        rowCount = 1;
        for (ValueList valueLevel : valueLevelMetadata) {
            for (Item item : valueLevel.getItems()){
                WhereClause whereClause = study.getWhereClause(item.getWhereClauseOid());
                if (whereClause != null) {
                    valLists.getRow(rowCount).createCell(valListCellColumn).setCellValue(whereClause.getOid());
                }
                rowCount++;
            }
        }
        valListCellColumn++;

        valLists.getRow(0).createCell(valListCellColumn).setCellValue("Description");
        valLists.setColumnWidth(valListCellColumn, 6000);
        rowCount = 1;
        for (ValueList valueLevel : valueLevelMetadata) {
            for (Item item : valueLevel.getItems()){
                if (StringUtils.isNotBlank(item.getLabel())) {
                    valLists.getRow(rowCount).createCell(valListCellColumn).setCellValue(item.getLabel());
                }
                rowCount++;
            }
        }
        valListCellColumn++;

        valLists.getRow(0).createCell(valListCellColumn).setCellValue("Data Type");
        valLists.setColumnWidth(valListCellColumn, 3000);
        rowCount = 1;
        for (ValueList valueLevel : valueLevelMetadata) {
            for (Item item : valueLevel.getItems()) {
                if (StringUtils.isNotEmpty(item.getDataType())) {
                    valLists.getRow(rowCount).createCell(valListCellColumn).setCellValue(item.getDataType());
                }
                rowCount++;
            }
        }
        valListCellColumn++;

        valLists.getRow(0).createCell(valListCellColumn).setCellValue("Length");
        valLists.setColumnWidth(valListCellColumn, 2600);
        rowCount = 1;
        for (ValueList valueLevel : valueLevelMetadata) {
            for (Item item : valueLevel.getItems()) {
                if (!StringUtils.isEmpty(item.getLength())) {
                    valLists.getRow(rowCount).createCell(valListCellColumn).setCellValue(item.getLength());
                }
                rowCount++;
            }
        }
        valListCellColumn++;

        valLists.getRow(0).createCell(valListCellColumn).setCellValue("Significant Digits");
        valLists.setColumnWidth(valListCellColumn, 4000);
        rowCount = 1;
        for (ValueList valueLevel : valueLevelMetadata) {
            for (Item item : valueLevel.getItems()) {
                if (!StringUtils.isEmpty(item.getSignificantDigits())) {
                    valLists.getRow(rowCount).createCell(valListCellColumn).setCellValue(item.getSignificantDigits());
                }
                rowCount++;
            }
        }
        valListCellColumn++;

        valLists.getRow(0).createCell(valListCellColumn).setCellValue("Format");
        valLists.setColumnWidth(valListCellColumn, 2800);
        rowCount = 1;
        for (ValueList valueLevel : valueLevelMetadata) {
            for (Item item : valueLevel.getItems()) {
                if (StringUtils.isNotEmpty(item.getFormat())) {
                    valLists.getRow(rowCount).createCell(valListCellColumn).setCellValue(item.getFormat());
                }
                rowCount++;
            }
        }
        valListCellColumn++;

        valLists.getRow(0).createCell(valListCellColumn).setCellValue("Mandatory");
        valLists.setColumnWidth(valListCellColumn, 3000);
        rowCount =1;
        for (ValueList valueLevel : valueLevelMetadata) {
            for (Item item : valueLevel.getItems()) {
                if (StringUtils.isNotEmpty(item.getMandatory())) {
                    valLists.getRow(rowCount).createCell(valListCellColumn).setCellValue(item.getMandatory());
                }
                rowCount++;
            }
        }
        valListCellColumn++;

        valLists.getRow(0).createCell(valListCellColumn).setCellValue("Codelist");
        valLists.setColumnWidth(valListCellColumn, 3300);
        rowCount = 1;
        for (ValueList valueLevel : valueLevelMetadata) {
            for (Item item : valueLevel.getItems()) {
                if (StringUtils.isNotEmpty(item.getCodeListOid())) {
                    valLists.getRow(rowCount).createCell(valListCellColumn).setCellValue(item.getCodeListOid());
                } else if (StringUtils.isNotEmpty(item.getDictionaryOid())) {
                    valLists.getRow(rowCount).createCell(valListCellColumn).setCellValue(item.getDictionaryOid());
                }
                rowCount++;
            }
        }
        valListCellColumn++;

        valLists.getRow(0).createCell(valListCellColumn).setCellValue("Origin");
        valLists.setColumnWidth(valListCellColumn, 3300);
        rowCount = 1;
        for (ValueList valueLevel : valueLevelMetadata) {
            for (Item item : valueLevel.getItems()) {
                if (StringUtils.isNotEmpty(item.getOrigin())) {
                    valLists.getRow(rowCount).createCell(valListCellColumn).setCellValue(item.getOrigin());
                }
                rowCount++;
            }
        }
        valListCellColumn++;

        valLists.getRow(0).createCell(valListCellColumn).setCellValue("Pages");
        valLists.setColumnWidth(valListCellColumn, 3000);
        rowCount = 1;
        for (ValueList valueLevel : valueLevelMetadata) {
            for (Item item : valueLevel.getItems()) {
                if (StringUtils.isNotEmpty(item.getPages())) {
                    valLists.getRow(rowCount).createCell(valListCellColumn).setCellValue(item.getPages());
                }
                rowCount++;
            }
        }
        valListCellColumn++;

        valLists.getRow(0).createCell(valListCellColumn).setCellValue("Method");
        valLists.setColumnWidth(valListCellColumn, 3500);
        rowCount = 1;
        for (ValueList valueLevel : valueLevelMetadata) {
            for (Item item : valueLevel.getItems()) {
                if (StringUtils.isNotEmpty(item.getMethodOid())) {
                    valLists.getRow(rowCount).createCell(valListCellColumn).setCellValue(item.getMethodOid());
                }
                rowCount++;
            }
        }
        valListCellColumn++;

        valLists.getRow(0).createCell(valListCellColumn).setCellValue("Predecessor");
        valLists.setColumnWidth(valListCellColumn, 3900);
        rowCount = 1;
        for (ValueList valueLevel : valueLevelMetadata) {
            for (Item item : valueLevel.getItems()) {
                if (StringUtils.isNotEmpty(item.getPredecessor())) {
                    valLists.getRow(rowCount).createCell(valListCellColumn).setCellValue(item.getPredecessor());
                }
                rowCount++;
            }
        }
        valListCellColumn++;

        valLists.getRow(0).createCell(valListCellColumn).setCellValue("Comment");
        valLists.setColumnWidth(valListCellColumn, 4000);
        rowCount = 1;
        for (ValueList valueLevel : valueLevelMetadata) {
            for (Item item : valueLevel.getItems()) {
                if (StringUtils.isNotEmpty(item.getCommentOid())) {
                    valLists.getRow(rowCount).createCell(valListCellColumn).setCellValue(item.getCommentOid());
                }
                rowCount++;
            }
        }

        autoFilter(valLists);

        /* ****START WHERECLAUSES SHEET CREATE**** */

        whereClauses.getRow(0).createCell(whereClausesCellColumn).setCellValue("ID");
        whereClauses.setColumnWidth(whereClausesCellColumn, 6000);
        rowCount = 1;
        for (WhereClause whereClause : whereClauseList) {
            for (int i = 0; i < whereClause.getRangeChecks().size(); i++) {
                whereClauses.createRow(rowCount).createCell(whereClausesCellColumn).setCellValue(whereClause.getOid());
                rowCount++;
            }
        }
        whereClausesCellColumn++;

        whereClauses.getRow(0).createCell(whereClausesCellColumn).setCellValue("Dataset");
        whereClauses.setColumnWidth(whereClausesCellColumn, 4000);
        rowCount = 1;
        for (WhereClause whereClause : whereClauseList) {
            for (RangeCheck check : whereClause.getRangeChecks()) {
                if (whereClauses.getRow(rowCount) == null){
                    whereClauses.createRow(rowCount).createCell(whereClausesCellColumn).setCellValue(check.getItemGroupOid());
                } else {
                    whereClauses.getRow(rowCount).createCell(whereClausesCellColumn).setCellValue(check.getItemGroupOid());
                }
                rowCount++;
            }
        }
        whereClausesCellColumn++;

        whereClauses.getRow(0).createCell(whereClausesCellColumn).setCellValue("Variable");
        whereClauses.setColumnWidth(whereClausesCellColumn, 4500);
        rowCount = 1;
        for (WhereClause whereClause : whereClauseList) {
            for (RangeCheck check : whereClause.getRangeChecks()) {
                whereClauses.getRow(rowCount).createCell(whereClausesCellColumn).setCellValue(check.getItemOid());
                rowCount++;
            }
        }
        whereClausesCellColumn++;

        whereClauses.getRow(0).createCell(whereClausesCellColumn).setCellValue("Comparator");
        whereClauses.setColumnWidth(whereClausesCellColumn, 4000);
        rowCount = 1;
        for (WhereClause whereClause : whereClauseList) {
            for (RangeCheck check : whereClause.getRangeChecks()) {
                whereClauses.getRow(rowCount).createCell(whereClausesCellColumn).setCellValue(check.getComparator());
                rowCount++;
            }
        }
        whereClausesCellColumn++;

        whereClauses.getRow(0).createCell(whereClausesCellColumn).setCellValue("Value");
        whereClauses.setColumnWidth(whereClausesCellColumn, 3000);
        rowCount = 1;
        for (WhereClause whereClause : whereClauseList) {
            for (RangeCheck check : whereClause.getRangeChecks()) {
                StringBuilder valueBuilder = new StringBuilder();
                String[] values = check.getValues().toArray(new String[check.getValues().size()]);
                for (int i = 0, size = values.length; i < size; i++) {
                    String value = values[i];
                    if (value.contains(",") && !value.startsWith("\"")) {
                        valueBuilder.append("\"");
                    }
                    valueBuilder.append(value);
                    if (value.contains(",") && !value.endsWith("\"")) {
                        valueBuilder.append("\"");
                    }
                    if (i < size - 1) {
                        valueBuilder.append(", ");
                    }
                }
                whereClauses.getRow(rowCount).createCell(whereClausesCellColumn).setCellValue(valueBuilder.toString());
                rowCount++;
            }
        }

        autoFilter(whereClauses);

        /* *****BEGIN CODELISTS SHEET CREATE**** */
        codeLists.getRow(0).createCell(codelistCellColumn).setCellValue("ID");
        codeLists.setColumnWidth(codelistCellColumn, 3000);
        rowCount = 1;
        for (CodeList studyCodelist : studyCodelists){
            List<CodeListItem> codelistItems = studyCodelist.getCodelistItems();
            if (codelistItems.isEmpty()) {
                codeLists.createRow(rowCount).createCell(codelistCellColumn).setCellValue(studyCodelist.getOid());
                rowCount++;
            }
            else {
                for (int i = 0; i < codelistItems.size(); i++) {
                    codeLists.createRow(rowCount).createCell(codelistCellColumn).setCellValue(studyCodelist.getOid());
                    rowCount++;
                }
            }
        }
        codelistCellColumn++;

        codeLists.getRow(0).createCell(codelistCellColumn).setCellValue("Name");
        codeLists.setColumnWidth(codelistCellColumn, 6700);
        rowCount = 1;
        for (CodeList studyCodelist : studyCodelists){
            boolean firstCodeListItem = true;
            List<CodeListItem> codelistItems = studyCodelist.getCodelistItems();
            if (codelistItems.isEmpty()) {
                if (StringUtils.isNotEmpty(studyCodelist.getName())) {
                    codeLists.getRow(rowCount).createCell(codelistCellColumn).setCellValue(studyCodelist.getName());
                }
                rowCount++;
            }
            else {
                for (int i = 0; i < codelistItems.size(); i++) {
                    if (firstCodeListItem) {
                        if (StringUtils.isNotEmpty(studyCodelist.getName())) {
                            codeLists.getRow(rowCount).createCell(codelistCellColumn).setCellValue(studyCodelist.getName());
                        }
                        rowCount++;
                        firstCodeListItem = false;
                    } else {
                        if (StringUtils.isNotEmpty(studyCodelist.getName())) {
                            codeLists.getRow(rowCount).createCell(codelistCellColumn).setCellValue(studyCodelist.getName());
                        }
                        rowCount++;
                    }
                }
            }
        }
        codelistCellColumn++;

        codeLists.getRow(0).createCell(codelistCellColumn).setCellValue("NCI Codelist Code");
        codeLists.setColumnWidth(codelistCellColumn, 4500);
        rowCount = 1;
        for (CodeList studyCodelist : studyCodelists){
            List<CodeListItem> codelistItems = studyCodelist.getCodelistItems();
            if (codelistItems.isEmpty()) {
                codeLists.getRow(rowCount).createCell(codelistCellColumn).setCellValue("");
                rowCount++;
            }
            else {
                for (CodeListItem codelistItem : codelistItems) {
                    if (StringUtils.isNotEmpty(studyCodelist.getCode())) {
                        codeLists.getRow(rowCount).createCell(codelistCellColumn).setCellValue(studyCodelist.getCode());
                    }
                    rowCount++;
                }
            }
        }
        codelistCellColumn++;

        codeLists.getRow(0).createCell(codelistCellColumn).setCellValue("Data Type");
        codeLists.setColumnWidth(codelistCellColumn, 3000);
        rowCount = 1;
        for (CodeList studyCodelist : studyCodelists){
            List<CodeListItem> codelistItems = studyCodelist.getCodelistItems();
            if (codelistItems.isEmpty()) {
                if (StringUtils.isNotEmpty(studyCodelist.getDataType())) {
                    codeLists.getRow(rowCount).createCell(codelistCellColumn).setCellValue(studyCodelist.getDataType());
                }
                rowCount++;
            }
            else {
                for (int i = 0; i < codelistItems.size(); i++) {
                    if (StringUtils.isNotEmpty(studyCodelist.getDataType())) {
                        codeLists.getRow(rowCount).createCell(codelistCellColumn).setCellValue(studyCodelist.getDataType());
                    }
                    rowCount++;
                }
            }
        }
        codelistCellColumn++;

        codeLists.getRow(0).createCell(codelistCellColumn).setCellValue("Order");
        codeLists.setColumnWidth(codelistCellColumn, 2500);
        rowCount = 1;
        for (CodeList studyCodelist : studyCodelists){
            List<CodeListItem> codelistItems = studyCodelist.getCodelistItems();
            if (codelistItems.isEmpty()) {
                codeLists.getRow(rowCount).createCell(codelistCellColumn).setCellValue("");
                rowCount++;
            }
            else {
                for (CodeListItem codelistItem : codelistItems) {
                    if (!StringUtils.isEmpty(codelistItem.getOrder())) {
                        codeLists.getRow(rowCount).createCell(codelistCellColumn).setCellValue(codelistItem.getOrder());
                    }
                    rowCount++;
                }
            }
        }
        codelistCellColumn++;

        codeLists.getRow(0).createCell(codelistCellColumn).setCellValue("Term");
        codeLists.setColumnWidth(codelistCellColumn, 6700);
        rowCount = 1;
        for (CodeList studyCodelist : studyCodelists){
            List<CodeListItem> codelistItems = studyCodelist.getCodelistItems();
            if (codelistItems.isEmpty()) {
                codeLists.getRow(rowCount).createCell(codelistCellColumn).setCellValue("");
                rowCount++;
            }
            else {
                for (CodeListItem codelistItem : codelistItems) {
                    codeLists.getRow(rowCount).createCell(codelistCellColumn).setCellValue(codelistItem.getCodedValue());
                    rowCount++;
                }
            }
        }
        codelistCellColumn++;

        codeLists.getRow(0).createCell(codelistCellColumn).setCellValue("NCI Term Code");
        codeLists.setColumnWidth(codelistCellColumn, 4500);
        rowCount = 1;
        for (CodeList studyCodelist : studyCodelists){
            List<CodeListItem> codelistItems = studyCodelist.getCodelistItems();
            if (codelistItems.isEmpty()) {
                codeLists.getRow(rowCount).createCell(codelistCellColumn).setCellValue("");
                rowCount++;
            }
            else {
                for (CodeListItem codelistItem : codelistItems) {
                    if (StringUtils.isNotEmpty(codelistItem.getCode())) {
                        codeLists.getRow(rowCount).createCell(codelistCellColumn).setCellValue(codelistItem.getCode());
                    }
                    rowCount++;
                }
            }
        }
        codelistCellColumn++;

        codeLists.getRow(0).createCell(codelistCellColumn).setCellValue("Decoded Value");
        codeLists.setColumnWidth(codelistCellColumn, 6700);
        rowCount = 1;
        for (CodeList studyCodelist : studyCodelists){
            List<CodeListItem> codelistItems = studyCodelist.getCodelistItems();
            if (codelistItems.isEmpty()) {
                codeLists.getRow(rowCount).createCell(codelistCellColumn).setCellValue("");
                rowCount++;
            }
            else {
                for (CodeListItem codelistItem : codelistItems) {
                    if (StringUtils.isNotEmpty(codelistItem.getDecodedValue())) {
                        codeLists.getRow(rowCount).createCell(codelistCellColumn).setCellValue(codelistItem.getDecodedValue());
                    }
                    rowCount++;
                }
            }
        }

        autoFilter(codeLists);

        /* ****START DICTIONARIES SHEET CREATE**** */
        dictionaries.getRow(0).createCell(dictionariesCellColumn).setCellValue("ID");
        dictionaries.setColumnWidth(dictionariesCellColumn, 4000);
        rowCount = 1;
        for (Dictionary dictionary : dictionaryList) {
            dictionaries.createRow(rowCount).createCell(dictionariesCellColumn).setCellValue(dictionary.getOid());
            rowCount++;
        }
        dictionariesCellColumn++;

        dictionaries.getRow(0).createCell(dictionariesCellColumn).setCellValue("Name");
        dictionaries.setColumnWidth(dictionariesCellColumn, 6500);
        rowCount = 1;
        for (Dictionary dictionary : dictionaryList) {
            if (StringUtils.isNotEmpty(dictionary.getName())) {
                dictionaries.getRow(rowCount).createCell(dictionariesCellColumn).setCellValue(dictionary.getName());
            }
            rowCount++;
        }
        dictionariesCellColumn++;

        dictionaries.getRow(0).createCell(dictionariesCellColumn).setCellValue("Data Type");
        dictionaries.setColumnWidth(dictionariesCellColumn, 4000);
        rowCount = 1;
        for (Dictionary dictionary : dictionaryList) {
            if (StringUtils.isNotEmpty(dictionary.getDataType())) {
                dictionaries.getRow(rowCount).createCell(dictionariesCellColumn).setCellValue(dictionary.getDataType());
            }
            rowCount++;
        }
        dictionariesCellColumn++;

        dictionaries.getRow(0).createCell(dictionariesCellColumn).setCellValue("Dictionary");
        dictionaries.setColumnWidth(dictionariesCellColumn, 4000);
        rowCount = 1;
        for (Dictionary dictionary : dictionaryList) {
            if (StringUtils.isNotEmpty(dictionary.getDictionary())) {
                dictionaries.getRow(rowCount).createCell(dictionariesCellColumn).setCellValue(dictionary.getDictionary());
            }
            rowCount++;
        }
        dictionariesCellColumn++;

        dictionaries.getRow(0).createCell(dictionariesCellColumn).setCellValue("Version");
        dictionaries.setColumnWidth(dictionariesCellColumn, 4000);
        rowCount = 1;
        for (Dictionary dictionary : dictionaryList) {
            if (StringUtils.isNotEmpty(dictionary.getVersion())) {
                dictionaries.getRow(rowCount).createCell(dictionariesCellColumn).setCellValue(dictionary.getVersion());
            }
            rowCount++;
        }

        autoFilter(dictionaries);

        /* ****START METHODS SHEET CREATE**** */
        methods.getRow(0).createCell(methodsCellColumn).setCellValue("ID");
        methods.setColumnWidth(methodsCellColumn, 4000);
        rowCount = 1;
        for (Method method : methodList) {
            methods.createRow(rowCount).createCell(methodsCellColumn).setCellValue(method.getOid());
            rowCount++;
        }
        methodsCellColumn++;

        methods.getRow(0).createCell(methodsCellColumn).setCellValue("Name");
        methods.setColumnWidth(methodsCellColumn, 6500);
        rowCount = 1;
        for (Method method : methodList) {
            if (StringUtils.isNotEmpty(method.getName())) {
                methods.getRow(rowCount).createCell(methodsCellColumn).setCellValue(method.getName());
            }
            rowCount++;
        }
        methodsCellColumn++;

        methods.getRow(0).createCell(methodsCellColumn).setCellValue("Type");
        methods.setColumnWidth(methodsCellColumn, 3000);
        rowCount = 1;
        for (Method method : methodList) {
            if (StringUtils.isNotEmpty(method.getType())) {
                methods.getRow(rowCount).createCell(methodsCellColumn).setCellValue(method.getType());
            }
            rowCount++;
        }
        methodsCellColumn++;

        methods.getRow(0).createCell(methodsCellColumn).setCellValue("Description");
        methods.setColumnWidth(methodsCellColumn, 12000);
        rowCount = 1;
        for (Method method : methodList) {
            if (StringUtils.isNotEmpty(method.getDescription())) {
                methods.getRow(rowCount).createCell(methodsCellColumn).setCellStyle(wrappedCell);
                methods.getRow(rowCount).getCell(methodsCellColumn).setCellValue(method.getDescription());
            }
            rowCount++;
        }
        methodsCellColumn++;

        methods.getRow(0).createCell(methodsCellColumn).setCellValue("Expression Context");
        methods.setColumnWidth(methodsCellColumn, 4800);
        rowCount = 1;
        for (Method method : methodList) {
            if (StringUtils.isNotEmpty(method.getExpressionContext())) {
                methods.getRow(rowCount).createCell(methodsCellColumn).setCellValue(method.getExpressionContext());
            }
            rowCount++;
        }
        methodsCellColumn++;

        methods.getRow(0).createCell(methodsCellColumn).setCellValue("Expression Code");
        methods.setColumnWidth(methodsCellColumn, 4000);
        rowCount = 1;
        for (Method method : methodList) {
            if (StringUtils.isNotEmpty(method.getExpressionCode())) {
                methods.getRow(rowCount).createCell(methodsCellColumn).setCellValue(method.getExpressionCode());
            }
            rowCount++;
        }
        methodsCellColumn++;

        methods.getRow(0).createCell(methodsCellColumn).setCellValue("Document");
        methods.setColumnWidth(methodsCellColumn, 5000);
        rowCount = 1;
        for (Method method : methodList) {
            if (StringUtils.isNotEmpty(method.getDocumentOid())) {
                methods.getRow(rowCount).createCell(methodsCellColumn).setCellValue(method.getDocumentOid());
            }
            rowCount++;
        }
        methodsCellColumn++;

        methods.getRow(0).createCell(methodsCellColumn).setCellValue("Pages");
        methods.setColumnWidth(methodsCellColumn, 3000);
        rowCount = 1;
        for (Method method : methodList) {
            if (StringUtils.isNotEmpty(method.getPages())) {
                methods.getRow(rowCount).createCell(methodsCellColumn).setCellValue(method.getPages());
            }
            rowCount++;
        }

        autoFilter(methods);

        /* ****START COMMENTS SHEET CREATE**** */
        comments.getRow(0).createCell(commentsCellColumn).setCellValue("ID");
        comments.setColumnWidth(commentsCellColumn, 5000);
        rowCount = 1;
        for (Comment comment : commentList) {
            comments.createRow(rowCount).createCell(commentsCellColumn).setCellValue(comment.getOid());
            rowCount++;
        }
        commentsCellColumn++;

        comments.getRow(0).createCell(commentsCellColumn).setCellValue("Description");
        comments.setColumnWidth(commentsCellColumn, 16000);
        rowCount = 1;
        for (Comment comment : commentList) {
            if (StringUtils.isNotEmpty(comment.getDescription())) {
                comments.getRow(rowCount).createCell(commentsCellColumn).setCellStyle(wrappedCell);
                comments.getRow(rowCount).getCell(commentsCellColumn).setCellValue(comment.getDescription());
            }
            rowCount++;
        }
        commentsCellColumn++;

        comments.getRow(0).createCell(commentsCellColumn).setCellValue("Document");
        comments.setColumnWidth(commentsCellColumn, 5000);
        rowCount = 1;
        for (Comment comment : commentList) {
            if (StringUtils.isNotEmpty(comment.getDocumentOid())) {
                comments.getRow(rowCount).createCell(commentsCellColumn).setCellValue(comment.getDocumentOid());
            }
            rowCount++;
        }
        commentsCellColumn++;

        comments.getRow(0).createCell(commentsCellColumn).setCellValue("Pages");
        comments.setColumnWidth(commentsCellColumn, 5000);
        rowCount = 1;
        for (Comment comment : commentList) {
            if (StringUtils.isNotEmpty(comment.getPages())) {
                comments.getRow(rowCount).createCell(commentsCellColumn).setCellValue(comment.getPages());
            }
            rowCount++;
        }

        autoFilter(comments);

        /* ****START DOCUMENTS SHEET CREATE**** */
        documents.getRow(0).createCell(documentsCellColumn).setCellValue("ID");
        documents.setColumnWidth(documentsCellColumn, 5000);
        rowCount = 1;
        for (Document document : documentList) {
            documents.createRow(rowCount).createCell(documentsCellColumn).setCellValue(document.getOid());
            rowCount++;
        }
        documentsCellColumn++;

        documents.getRow(0).createCell(documentsCellColumn).setCellValue("Title");
        documents.setColumnWidth(documentsCellColumn, 16000);
        rowCount = 1;
        for (Document document : documentList) {
            if (StringUtils.isNotEmpty(document.getTitle())) {
                documents.getRow(rowCount).createCell(documentsCellColumn).setCellValue(document.getTitle());
            }
            rowCount++;
        }
        documentsCellColumn++;

        documents.getRow(0).createCell(documentsCellColumn).setCellValue("Href");
        documents.setColumnWidth(documentsCellColumn, 5000);
        rowCount = 1;
        for (Document document : documentList) {
            if (StringUtils.isNotEmpty(document.getHref())) {
                documents.getRow(rowCount).createCell(documentsCellColumn).setCellValue(document.getHref());
            }
            rowCount++;
        }

        autoFilter(documents);

        if (isAdam && armSupport) {
            /* ****START CREATING ANALYSIS DISPLAYS SHEET**** */
            analysisDisplays.getRow(0).createCell(analysisDisplaysCellColumn).setCellValue("ID");
            analysisDisplays.setColumnWidth(analysisDisplaysCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                analysisDisplays.createRow(rowCount).createCell(analysisDisplaysCellColumn).setCellValue(resultDisplay.getName());
                rowCount++;
            }
            analysisDisplaysCellColumn++;

            analysisDisplays.getRow(0).createCell(analysisDisplaysCellColumn).setCellValue("Title");
            analysisDisplays.setColumnWidth(analysisDisplaysCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                analysisDisplays.getRow(rowCount).createCell(analysisDisplaysCellColumn).setCellValue(resultDisplay.getDescription().getText());
                rowCount++;
            }
            analysisDisplaysCellColumn++;

            analysisDisplays.getRow(0).createCell(analysisDisplaysCellColumn).setCellValue("Document");
            analysisDisplays.setColumnWidth(analysisDisplaysCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                if (resultDisplay.getDocumentReference() != null && resultDisplay.getDocumentReference().getDocument() != null) {
                    analysisDisplays.getRow(rowCount).createCell(analysisDisplaysCellColumn).setCellValue(
                            resultDisplay.getDocumentReference().getDocument().getOid());
                }
                rowCount++;
            }
            analysisDisplaysCellColumn++;

            analysisDisplays.getRow(0).createCell(analysisDisplaysCellColumn).setCellValue("Pages");
            analysisDisplays.setColumnWidth(analysisDisplaysCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                if (resultDisplay.getDocumentReference() != null) {
                    analysisDisplays.getRow(rowCount).createCell(analysisDisplaysCellColumn).setCellValue(
                            resultDisplay.getDocumentReference().getPages());
                }
                rowCount++;
            }

            autoFilter(analysisDisplays);

            /* ****START CREATING ANALYSIS RESULTS SHEET**** */
            analysisResults.getRow(0).createCell(analysisResultsCellColumn).setCellValue("Display");
            analysisResults.setColumnWidth(analysisResultsCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                for (int i = 0; i < resultDisplay.getAnalysisResults().size(); i++) {
                    analysisResults.createRow(rowCount).createCell(analysisResultsCellColumn).setCellValue(resultDisplay.getName());
                    rowCount++;
                }
            }
            analysisResultsCellColumn++;

            analysisResults.getRow(0).createCell(analysisResultsCellColumn).setCellValue("ID");
            analysisResults.setColumnWidth(analysisResultsCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                for (AnalysisResult analysisResult : resultDisplay.getAnalysisResults()) {
                    analysisResults.getRow(rowCount).createCell(analysisResultsCellColumn).setCellValue(analysisResult.getOid());
                    rowCount++;
                }
            }
            analysisResultsCellColumn++;

            analysisResults.getRow(0).createCell(analysisResultsCellColumn).setCellValue("Description");
            analysisResults.setColumnWidth(analysisResultsCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                for (AnalysisResult analysisResult : resultDisplay.getAnalysisResults()) {
                    if (analysisResult.getDescription() != null) {
                        analysisResults.getRow(rowCount).createCell(analysisResultsCellColumn).setCellValue(analysisResult.getDescription().getText());
                    }
                    rowCount++;
                }
            }
            analysisResultsCellColumn++;

            analysisResults.getRow(0).createCell(analysisResultsCellColumn).setCellValue("Reason");
            analysisResults.setColumnWidth(analysisResultsCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                for (AnalysisResult analysisResult : resultDisplay.getAnalysisResults()) {
                    analysisResults.getRow(rowCount).createCell(analysisResultsCellColumn).setCellValue(analysisResult.getAnalysisReason());
                    rowCount++;
                }
            }
            analysisResultsCellColumn++;

            analysisResults.getRow(0).createCell(analysisResultsCellColumn).setCellValue("Purpose");
            analysisResults.setColumnWidth(analysisResultsCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                for (AnalysisResult analysisResult : resultDisplay.getAnalysisResults()) {
                    analysisResults.getRow(rowCount).createCell(analysisResultsCellColumn).setCellValue(analysisResult.getAnalysisPurpose());
                    rowCount++;
                }
            }
            analysisResultsCellColumn++;

            analysisResults.getRow(0).createCell(analysisResultsCellColumn).setCellValue("Join Comment");
            analysisResults.setColumnWidth(analysisResultsCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                for (AnalysisResult analysisResult : resultDisplay.getAnalysisResults()) {
                    if (analysisResult.getAnalysisDatasets().getComment() != null) {
                        analysisResults.getRow(rowCount).createCell(analysisResultsCellColumn).setCellValue(
                                analysisResult.getAnalysisDatasets().getComment().getOid());
                    }
                    rowCount++;
                }
            }
            analysisResultsCellColumn++;

            analysisResults.getRow(0).createCell(analysisResultsCellColumn).setCellValue("Documentation");
            analysisResults.setColumnWidth(analysisResultsCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                for (AnalysisResult analysisResult : resultDisplay.getAnalysisResults()) {
                    Documentation documentation = analysisResult.getDocumentation();
                    if (documentation != null && documentation.getDescription() != null) {
                        analysisResults.getRow(rowCount).createCell(analysisResultsCellColumn).setCellValue(
                                documentation.getDescription().getText());
                    }
                    rowCount++;
                }
            }
            analysisResultsCellColumn++;

            analysisResults.getRow(0).createCell(analysisResultsCellColumn).setCellValue("Documentation Refs");
            analysisResults.setColumnWidth(analysisResultsCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                for (AnalysisResult analysisResult : resultDisplay.getAnalysisResults()) {
                    Documentation documentation = analysisResult.getDocumentation();
                    if (documentation != null) {
                        analysisResults.getRow(rowCount).createCell(analysisResultsCellColumn).setCellValue(
                                buildDocumentReferencesString(documentation.getDocumentReferences()));
                    }
                    rowCount++;
                }
            }
            analysisResultsCellColumn++;

            analysisResults.getRow(0).createCell(analysisResultsCellColumn).setCellValue("Programming Context");
            analysisResults.setColumnWidth(analysisResultsCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                for (AnalysisResult analysisResult : resultDisplay.getAnalysisResults()) {
                    if (analysisResult.getProgrammingCode() != null) {
                        analysisResults.getRow(rowCount).createCell(analysisResultsCellColumn).setCellValue(
                                analysisResult.getProgrammingCode().getContext());
                    }
                    rowCount++;
                }
            }
            analysisResultsCellColumn++;

            analysisResults.getRow(0).createCell(analysisResultsCellColumn).setCellValue("Programming Code");
            analysisResults.setColumnWidth(analysisResultsCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                for (AnalysisResult analysisResult : resultDisplay.getAnalysisResults()) {
                    if (analysisResult.getProgrammingCode() != null) {
                        analysisResults.getRow(rowCount).createCell(analysisResultsCellColumn).setCellValue(
                                analysisResult.getProgrammingCode().getCode());
                    }
                    rowCount++;
                }
            }
            analysisResultsCellColumn++;

            analysisResults.getRow(0).createCell(analysisResultsCellColumn).setCellValue("Programming Document");
            analysisResults.setColumnWidth(analysisResultsCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                for (AnalysisResult analysisResult : resultDisplay.getAnalysisResults()) {
                    if (analysisResult.getProgrammingCode() != null) {
                        DocumentReference documentReference = analysisResult.getProgrammingCode().getDocumentReference();
                        analysisResults.getRow(rowCount).createCell(analysisResultsCellColumn).setCellValue(
                                documentReference != null
                                        ? buildDocumentReferencesString(Collections.singletonList(documentReference))
                                        : null
                        );
                    }
                    rowCount++;
                }
            }

            autoFilter(analysisResults);

            /* ****START CREATING ANALYSIS CRITERIA SHEET**** */
            analysisCriteria.getRow(0).createCell(analysisCriteriaCellColumn).setCellValue("Display");
            analysisCriteria.setColumnWidth(analysisCriteriaCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                for (AnalysisResult analysisResult : resultDisplay.getAnalysisResults()) {
                    for (int i = 0; i < analysisResult.getAnalysisDatasets().getDatasets().size(); i++) {
                        analysisCriteria.createRow(rowCount).createCell(analysisCriteriaCellColumn).setCellValue(resultDisplay.getName());
                        rowCount++;
                    }
                }
            }
            analysisCriteriaCellColumn++;

            analysisCriteria.getRow(0).createCell(analysisCriteriaCellColumn).setCellValue("Result");
            analysisCriteria.setColumnWidth(analysisCriteriaCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                List<AnalysisResult> analysisResultList = resultDisplay.getAnalysisResults();
                for (int i = 0; i < analysisResultList.size(); i++) {
                    AnalysisResult analysisResult = analysisResultList.get(i);
                    for (int j = 0; j < analysisResult.getAnalysisDatasets().getDatasets().size(); j++) {
                        analysisCriteria.getRow(rowCount).createCell(analysisCriteriaCellColumn).setCellValue(analysisResult.getOid());
                        rowCount++;
                    }
                }
            }
            analysisCriteriaCellColumn++;

            analysisCriteria.getRow(0).createCell(analysisCriteriaCellColumn).setCellValue("Dataset");
            analysisCriteria.setColumnWidth(analysisCriteriaCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                for (AnalysisResult analysisResult : resultDisplay.getAnalysisResults()) {
                    for (AnalysisDataset analysisDataset : analysisResult.getAnalysisDatasets().getDatasets()) {
                        analysisCriteria.getRow(rowCount).createCell(analysisCriteriaCellColumn).setCellValue(
                                analysisDataset.getItemGroup().getName());
                        rowCount++;
                    }
                }
            }
            analysisCriteriaCellColumn++;

            analysisCriteria.getRow(0).createCell(analysisCriteriaCellColumn).setCellValue("Variables");
            analysisCriteria.setColumnWidth(analysisCriteriaCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                for (AnalysisResult analysisResult : resultDisplay.getAnalysisResults()) {
                    for (AnalysisDataset analysisDataset : analysisResult.getAnalysisDatasets().getDatasets()) {
                        List<AnalysisVariable> variables = analysisDataset.getAnalysisVariables();
                        if (variables != null) {
                            StringBuilder analysisVariablesString = new StringBuilder();
                            for (int i = 0; i < variables.size(); i++) {
                                analysisVariablesString.append(variables.get(i).getItem().getName());
                                if (i < variables.size() - 1) {
                                    analysisVariablesString.append(", ");
                                }
                            }
                            analysisCriteria.getRow(rowCount).createCell(analysisCriteriaCellColumn).setCellValue(
                                    analysisVariablesString.toString());
                            rowCount++;
                        }
                    }
                }
            }
            analysisCriteriaCellColumn++;

            analysisCriteria.getRow(0).createCell(analysisCriteriaCellColumn).setCellValue("Where Clause");
            analysisCriteria.setColumnWidth(analysisCriteriaCellColumn, 5000);
            rowCount = 1;
            for (ResultDisplay resultDisplay : resultDisplayList) {
                for (AnalysisResult analysisResult : resultDisplay.getAnalysisResults()) {
                    for (AnalysisDataset analysisDataset : analysisResult.getAnalysisDatasets().getDatasets()) {
                        if (analysisDataset.getWhereClause() != null) {
                            analysisCriteria.getRow(rowCount).createCell(analysisCriteriaCellColumn).setCellValue(
                                    analysisDataset.getWhereClause().getOid());
                        }
                        rowCount++;
                    }
                }
            }

            autoFilter(analysisCriteria);
        }

        //Set default styles
        defaultStyleSet(studySheet, defaultStyle, stylesNotToChange);
        defaultStyleSet(domains, defaultStyle, stylesNotToChange);
        defaultStyleSet(vars, defaultStyle, stylesNotToChange);
        defaultStyleSet(codeLists, defaultStyle, stylesNotToChange);
        defaultStyleSet(dictionaries, defaultStyle, stylesNotToChange);
        defaultStyleSet(valLists, defaultStyle, stylesNotToChange);
        defaultStyleSet(methods, defaultStyle, stylesNotToChange);
        defaultStyleSet(whereClauses, defaultStyle, stylesNotToChange);
        defaultStyleSet(comments, defaultStyle, stylesNotToChange);
        defaultStyleSet(documents, defaultStyle, stylesNotToChange);
        if (isAdam && armSupport) {
            defaultStyleSet(analysisDisplays, defaultStyle, stylesNotToChange);
            defaultStyleSet(analysisResults, defaultStyle, stylesNotToChange);
            defaultStyleSet(analysisCriteria, defaultStyle, stylesNotToChange);
        }

        //Style headers
        styleHeader(studyHeader, headerStyle);
        styleHeader(domainHeader, headerStyle);
        styleHeader(varsHeader, headerStyle);
        styleHeader(codeListHeader, headerStyle);
        styleHeader(dictionariesHeader, headerStyle);
        styleHeader(valListHeader, headerStyle);
        styleHeader(methodsHeader, headerStyle);
        styleHeader(whereClausesHeader, headerStyle);
        styleHeader(commentsHeader, headerStyle);
        styleHeader(documentsHeader, headerStyle);
        if (isAdam && armSupport) {
            styleHeader(analysisDisplaysHeader, headerStyle);
            styleHeader(analysisResultsHeader, headerStyle);
            styleHeader(analysisCriteriaHeader, headerStyle);
        }

        return workbook;
    }

    private static void applyWorkbookMetadata(XSSFWorkbook workbook, DefineGenerator.Metadata metadata) {
        if (metadata == null) {
            metadata = DEFAULT_METADATA;
        }
        POIXMLProperties xmlProps = workbook.getProperties();
        POIXMLProperties.CoreProperties coreProps =  xmlProps.getCoreProperties();
        coreProps.setRevision("1");

        if (StringUtils.isNotBlank(metadata.getOriginator())) {
            coreProps.setCreator(metadata.getOriginator());
            coreProps.setLastModifiedByUser(metadata.getOriginator());
        }

        if (StringUtils.isNotBlank(metadata.getSourceSystem())) {
            coreProps.setLastModifiedByUser(metadata.getSourceSystem());

            String description = "Created with " + metadata.getSourceSystem();
            if (StringUtils.isNotBlank(metadata.getSourceSystemVersion())) {
                description += ", version " + metadata.getSourceSystemVersion();
            }
            coreProps.setDescription(description);
        }
    }

    private static String buildDocumentReferencesString(List<DocumentReference> documentReferences) {
        StringBuilder documentReferencesString = new StringBuilder();
        if (documentReferences != null) {
            for (int i = 0; i < documentReferences.size(); i++) {
                DocumentReference documentReference = documentReferences.get(i);
                if (documentReference.getDocument() != null) {
                    documentReferencesString.append(documentReference.getDocument().getOid());
                    if (StringUtils.isNotBlank(documentReference.getPages())) {
                        documentReferencesString.append(String.format("(%s)", documentReference.getPages()));
                    }
                    if (i < documentReferences.size() - 1) {
                        documentReferencesString.append(", ");
                    }
                }
            }
        }
        return documentReferencesString.toString();
    }

    /**
     *
     * @param armSupport <p>if true, will write the ARM tabs, otherwise it will not write the tabs.</p>
     *                   <p>The Study also has to be an ADaM study for ARM tabs to appear.</p>
     *
     */
    public static void writeToStream(boolean armSupport, Study study, OutputStream outputStream) throws IOException {
        writeToStream(armSupport, study, outputStream, DEFAULT_METADATA);
    }

    /**
     * Disable ARM Support by default.
     */
    public static void writeToStream(Study study, OutputStream outputStream) throws IOException {
        writeToStream(false, study, outputStream, DEFAULT_METADATA);
    }

    /**
     *
     * @param armSupport <p>if true, will write the ARM tabs, otherwise it will not write the tabs.</p>
     *                   <p>The Study also has to be an ADaM study for ARM tabs to appear.</p>
     *
     */
    public static void writeToStream(boolean armSupport, Study study, OutputStream outputStream, DefineGenerator.Metadata metadata) throws IOException {
        if (!armSupport) {
            disableArmSupport();
        } else {
            // just to make sure that its enabled, statics are weird.
            enableArmSupport();
        }
        Workbook workbook = writeToWorkbook(study, metadata);
        workbook.write(outputStream);
        outputStream.close();
        workbook.close();
        if (!armSupport) {
            enableArmSupport();
        }
    }

    /**
     * Disable ARM Support by default.
     */
    public static File writeToDirectory(Study study, File directory) throws IOException {
        return writeToDirectory(false, study, directory, DEFAULT_METADATA);
    }

    public static File writeToDirectory(boolean armSupport, Study study, File directory) throws IOException {
        return writeToDirectory(armSupport, study, directory, DEFAULT_METADATA);
    }

    /**
     *
     * @param armSupport <p>if true, will write the ARM tabs, otherwise it will not write the tabs.</p>
     *                   <p>The Study also has to be an ADaM study for ARM tabs to appear.</p>
     *
     */
    public static File writeToDirectory(boolean armSupport, Study study, File directory, DefineGenerator.Metadata metadata) throws IOException {
        String dateTime = Date.date() + "T" + Date.time().substring(0, 5).replace(":", "-");
        String studyName = StringUtils.defaultString(study.getName()).replaceAll("/|\\\\|:|\\*|\\?|\"|<|>|\\|", "");
        String fileName = studyName + "-" + dateTime + ".xlsx";

        if (StringUtils.isBlank(studyName) || fileName.length() > 255) {
            fileName = "define" + "-" + dateTime + ".xlsx";
        }

        File defineExcel = new File(directory, fileName);
        FileOutputStream outputStream = new FileOutputStream(defineExcel);
        writeToStream(armSupport, study, outputStream, metadata);

        return defineExcel;
    }

    private static void defaultStyleSet(Sheet sheet, CellStyle style, List<CellStyle> stylesNotToChange) {
        for (Row row : sheet){
            for (Cell cell : row) {
                if (!stylesNotToChange.contains(cell.getCellStyle())) {
                    cell.setCellStyle(style);
                }
            }
        }
    }

    private static void styleHeader(Row row, CellStyle style) {
        for (Cell cell: row) {
            cell.setCellStyle(style);
        }
    }

    private static void autoFilter(Sheet sheet) {
        int columnCount = (sheet.getRow(0).getLastCellNum()) - 1;
        String columnLetter = CellReference.convertNumToColString(columnCount);
        int rowCount = sheet.getPhysicalNumberOfRows();
        String rowNum = Integer.toString(rowCount);
        sheet.setAutoFilter(CellRangeAddress.valueOf("A1:" + columnLetter + rowNum));
    }
}
