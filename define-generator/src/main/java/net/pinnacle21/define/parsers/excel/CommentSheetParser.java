/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.Comment;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.NamedRow;
import net.pinnacle21.parsing.excel.ValidationAwareExcelSheetParser;
import org.apache.commons.lang3.StringUtils;

public class CommentSheetParser extends ValidationAwareExcelSheetParser<ParsedStudy> {
    static final String ID_COLUMN = "ID";
    static final String DESCRIPTION_COLUMN = "Description";
    static final String DOCUMENT_COLUMN = "Document";
    static final String PAGES_COLUMN = "Pages";
    static final String DUPLICATE_ERROR = "The comment '%s' is already defined, duplicates are not allowed";
    static final String DOCUMENT_REF_ERROR = "'%s' references the document '%s', but that document was not found";

    CommentSheetParser() {
        this.addValidatedRequiredColumn(ID_COLUMN);
        this.addValidatedColumn(DESCRIPTION_COLUMN, FieldValidations.REALLY_LONG_LENGTH);
        this.addValidatedColumn(DOCUMENT_COLUMN);
        this.addValidatedColumn(PAGES_COLUMN, FieldValidations.MEDIUM_LENGTH);
    }

    @Override
    public void parse(NamedRow row, ParsedStudy parsedStudy) {
        if (!this.validate(row, parsedStudy)) {
            return;
        }
        Study study = parsedStudy.getStudy();

        String oid = StringUtils.trim(row.get(ID_COLUMN));

        if (study.hasComment(oid)) {
            parsedStudy.error(row.getIdentifier(), DUPLICATE_ERROR, oid);

            return;
        }

        Comment comment = new Comment();
        comment.setOid(oid);
        comment.setDescription(row.get(DESCRIPTION_COLUMN));
        comment.setPages(row.get(PAGES_COLUMN));

        String documentRef = StringUtils.trim(row.get(DOCUMENT_COLUMN));

        if (StringUtils.isNotBlank(documentRef)) {
            if (study.hasDocument(documentRef)) {
                comment.setDocumentOid(documentRef);
            } else {
                parsedStudy.error(row.getIdentifier(), DOCUMENT_REF_ERROR, DOCUMENT_COLUMN, documentRef);
            }
        }

        study.setComment(oid, comment);
    }
}
