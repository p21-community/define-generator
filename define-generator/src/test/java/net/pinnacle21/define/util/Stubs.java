/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.util;

import net.pinnacle21.define.models.*;
import org.dom4j.DocumentFactory;
import org.xml.sax.InputSource;

import java.io.StringReader;

public interface Stubs {

    Study DEFINE_2_CONTROL_STUDY = Helper.define2ControlStudy();
    Study DEFINE_1_CONTROL_STUDY = Helper.define1ControlStudy();
    Study ADAM_STUDY_NO_DOMAIN = Helper.newADaMStudy();
    ItemGroup ITEM_GROUP = Helper.newItemGroupDef();
    InputSource DEFINE_2_INPUT_SOURCE = Helper.define2InputSource();
    InputSource DEFINE_1_INPUT_SOURCE = Helper.define1InputSource();
    InputSource DEFINE_DEFAULT_INPUT_SOURCE = Helper.defineDefaultInputSource();
    ItemGroup itemGroup = Helper.newItemGroupDef();
    public static final String define1WhereClauseOid = "ItemGroupDef.Variable1QNAM.EQ.33158ef12a6ad2ac0b061583885217af86210757";
    public static final String define2WhereClauseOid = "ItemGroupDef.Variable2.EQ.ItemGroupDef.Variable3.EQ.8b38ef5e9ce5c2f6cb5299aaae492468a17c3240";

    class Helper {
        static ItemGroup newItemGroupDef() {
            ItemGroup itemGroupDef = new ItemGroup().setName("Test")
                    .setDescription("This is a test")
                    .setStructure("One record per subject")
                    .setCategory("FINDINGS")
                    .setPurpose("Tabulation")
                    .setIsRepeating("no")
                    .setIsReferenceData("no");
            itemGroupDef.add(new Item()
                            .setOrder("1")
                            .setName("TEST01")
                            .setLabel("TEST")
                        );

            return itemGroupDef;
        }

        static Study newADaMStudy() {
            return new Study().setName("Test").setStandardName("ADaM").setStandardVersion("0.0.1");
        }

        private static Study define1ControlStudy() {
            Study study = new Study()
                    .setName("Study Name")
                    .setDescription("Study Description")
                    .setProtocol("Study Protocol")
                    .setStandardName("SDTM-IG")
                    .setStandardVersion("3.1.2")
                    .setLanguage("en");
            study.setDocument("LEAF", new Document().setOid("LEAF").setTitle("LEAF Title").setHref("LEAF.pdf").setType(Document.Type.LEAF));
            study.setComment("Variable1", new Comment().setOid("Variable1").setDescription("Description"));

            Method method = new Method().setOid("Method");
            method.setName("Algorithm to derive Method");
            method.setType("Computation");
            method.setDescription("Description");

            CodeList codeList = new CodeList().setOid("CodeList");
            codeList.setName("Name");
            codeList.setDataType("DataType");

            CodeListItem codeListItem = new CodeListItem().setCodedValue("CodeListItem");
            codeListItem.setOrder("1.0");
            codeListItem.setLanguage("en");
            codeListItem.setDecodedValue("DecodedValue");
            codeList.put(codeListItem);

            CodeListItem codeListItem2 = new CodeListItem().setCodedValue("SameValue");
            codeListItem2.setOrder("2.0");
            codeListItem2.setLanguage("en");
            codeListItem2.setDecodedValue(codeListItem2.getCodedValue());
            codeList.put(codeListItem2);

            Dictionary externalCodeList = new Dictionary().setOid("ExternalCodeList");
            externalCodeList.setName("Name");
            externalCodeList.setDataType("DataType");
            externalCodeList.setDictionary("Dictionary");
            externalCodeList.setVersion("1.0");

            ItemGroup itemGroup = new ItemGroup().setName("ItemGroupDef");
            itemGroup.setDescription("Description");
            itemGroup.setIsRepeating("Repeating");
            itemGroup.setIsReferenceData("IsReferenceData");
            itemGroup.setPurpose("Purpose");
            itemGroup.setStructure("Structure");
            itemGroup.setCategory("Class");
            itemGroup.setKeys("Variable2,Variable1QNAM,Variable3");

            Item controlItem = new Item()
                    .setOID("Variable1")
                    .setName("Variable1QNAM");
            controlItem.setDataType("DataType");
            controlItem.setLabel("Description");
            controlItem.setLength("Length");
            controlItem.setSignificantDigits("SignificantDigits");
            controlItem.setFormat("Format");
            controlItem.setCommentOid("Variable1");
            controlItem.setCodeListOid("CodeList");
            controlItem.setOrigin("Predecessor");
            controlItem.setPages("6");
            controlItem.setKeySequence(2);
            controlItem.setMandatory("Yes");
            controlItem.setMethodOid("Method");
            controlItem.setOrder("1");
            controlItem.setRole("Role");
            controlItem.setValueListOid("ItemGroupDef.Variable1QNAM");

            itemGroup.add(controlItem);

            controlItem = new Item()
                    .setOID("Variable2")
                    .setName("Variable2");
            controlItem.setDataType("text");
            controlItem.setLength("7");
            controlItem.setOrder("2");
            controlItem.setMandatory("Yes");
            controlItem.setMethodOid("");
            controlItem.setValueListOid("");
            controlItem.setCodeListOid("NotDefined");

            itemGroup.add(controlItem);

            controlItem = new Item()
                    .setOID("Variable3")
                    .setName("Variable3");
            controlItem.setDataType("text");
            controlItem.setLength("7");
            controlItem.setOrder("3");
            controlItem.setMandatory("Yes");
            controlItem.setMethodOid("");
            controlItem.setValueListOid("");
            controlItem.setDictionaryOid("ExternalCodeList");

            itemGroup.add(controlItem);

            WhereClause whereClause = new WhereClause().setOid(define1WhereClauseOid);
            whereClause.add(new RangeCheck()
                    .setItemOid("Variable1QNAM")
                    .setItemGroupOid("ItemGroupDef")
                    .setComparator("EQ")
                    .addValue("ValueLevelVariable"));

            ValueList valueList = new ValueList("ItemGroupDef", "Variable1QNAM");
            Item controlValueItem = new Item()
                    .setOID("ValueLevelVariable")
                    .setName("Variable1QNAM." + define1WhereClauseOid);
            controlValueItem.setDataType("DataType");
            controlValueItem.setLength("Length");
            controlValueItem.setSignificantDigits("SignificantDigits");
            controlValueItem.setFormat("Format");
            controlValueItem.setCodeListOid("CodeList");
            controlValueItem.setOrigin("CRF");
            controlValueItem.setPages("6 7 8");
            controlValueItem.setMandatory("Yes");
            controlValueItem.setMethodOid("Method");
            controlValueItem.setOrder("1");
            controlValueItem.setWhereClauseOid(define1WhereClauseOid);
            valueList.add(controlValueItem);

            study.setMethod(method.getOid(), method);
            study.setCodeList(codeList.getOid(), codeList);
            study.setDictionary(externalCodeList.getOid(), externalCodeList);
            study.setItemGroup(itemGroup.getName(), itemGroup);
            study.setWhereClause(whereClause.getOid(), whereClause);
            study.setValueList(valueList.getSourceItemGroupName(), valueList.getSourceItemName(), valueList);

            return study;
        }

        static Study define2ControlStudy() {
            Study study = new Study()
                    .setName("Study Name")
                    .setDescription("Study Description")
                    .setProtocol("Study Protocol")
                    .setStandardName("SDTM-IG")
                    .setStandardVersion("3.1.2")
                    .setLanguage("en");
            study.setDocument("LEAF'S", new Document().setOid("LEAF'S").setTitle("LEAF Title").setHref("LEAF.pdf").setType(Document.Type.LEAF));
            study.setComment("Comment", new Comment().setOid("Comment").setDescription("Description").setDocumentOid("LEAF'S").setPages("1,2,3"));

            Method method = new Method().setOid("Method");
            method.setName("Name");
            method.setType("Type");
            method.setDescription("Description");
            method.setLanguage("en");
            method.setExpressionContext("Context");
            method.setExpressionCode("Code");
            method.setDocumentOid("LEAF'S");
            method.setPages("1,2,3");

            CodeList codeList = new CodeList().setOid("CodeList");
            codeList.setName("Name");
            codeList.setDataType("DataType");
            codeList.setCode("Code");

            CodeListItem codeListItem = new CodeListItem().setCodedValue("CodeListItem");
            codeListItem.setOrder("1");
            codeListItem.setLanguage("en");
            codeListItem.setDecodedValue("DecodedValue");
            codeListItem.setCode("ItemCode");
            codeList.put(codeListItem);

            CodeListItem enumeratedItem = new CodeListItem().setCodedValue("EnumeratedItem");
            enumeratedItem.setOrder("2");
            enumeratedItem.setCode("EnumeratedCode");
            codeList.put(enumeratedItem);

            Dictionary externalCodeList = new Dictionary().setOid("ExternalCodeList");
            externalCodeList.setName("Name");
            externalCodeList.setDataType("DataType");
            externalCodeList.setDictionary("Dictionary");
            externalCodeList.setVersion("1.0");

            ItemGroup itemGroup = new ItemGroup().setName("ItemGroupDef");
            itemGroup.setDescription("Description");
            itemGroup.setIsRepeating("Repeating");
            itemGroup.setIsReferenceData("IsReferenceData");
            itemGroup.setPurpose("Purpose");
            itemGroup.setStructure("Structure");
            itemGroup.setCategory("Class");
            itemGroup.setCommentOid("Comment");
            itemGroup.setKeys("Variable2,Variable1,Variable3");

            Item controlItem = new Item()
                    .setOID("Variable1")
                    .setName("Variable1");
            controlItem.setDataType("DataType");
            controlItem.setLabel("Description");
            controlItem.setLength("Length");
            controlItem.setSignificantDigits("SignificantDigits");
            controlItem.setFormat("Format");
            controlItem.setCommentOid("Comment");
            controlItem.setCodeListOid("CodeList");
            controlItem.setOrigin("Predecessor");
            controlItem.setPredecessor("Predecessor Description");
            controlItem.setPages("6");
            controlItem.setKeySequence(2);
            controlItem.setMandatory("Yes");
            controlItem.setMethodOid("Method");
            controlItem.setOrder("1");
            controlItem.setRole("Role");
            controlItem.setValueListOid("ItemGroupDef.Variable1");

            itemGroup.add(controlItem);

            controlItem = new Item()
                .setOID("Variable2")
                .setName("Variable2");
            controlItem.setDataType("text");
            controlItem.setLength("7");
            controlItem.setOrder("3");
            controlItem.setMandatory("Yes");
            controlItem.setMethodOid("");
            controlItem.setCommentOid("");
            controlItem.setDictionaryOid("ExternalCodeList");
            controlItem.setValueListOid("");

            itemGroup.add(controlItem);

            controlItem = new Item().setName("Variable3");
            controlItem.setDataType("text");
            controlItem.setLength("7");

            itemGroup.add(controlItem);

            WhereClause whereClause = new WhereClause().setOid(define2WhereClauseOid)
                    .setCommentOid("Comment");
            whereClause.add(new RangeCheck().setItemOid("Variable2")
                    .setItemGroupOid("ItemGroupDef")
                    .setComparator("EQ")
                    .addValue("Val2"));
            whereClause.add(new RangeCheck().setItemOid("Variable3")
                    .setItemGroupOid("ItemGroupDef")
                    .setComparator("EQ")
                    .addValue("Val3"));

            ValueList valueList = new ValueList("ItemGroupDef", "Variable1");
            Item controlValueItem = new Item()
                    .setOID("ValueLevelVariable")
                    .setName("Variable1." + define2WhereClauseOid);
            controlValueItem.setDataType("DataType");
            controlValueItem.setLength("Length");
            controlValueItem.setSignificantDigits("SignificantDigits");
            controlValueItem.setFormat("Format");
            controlValueItem.setCommentOid("Comment");
            controlValueItem.setCodeListOid("CodeList");
            controlValueItem.setOrigin("Predecessor");
            controlValueItem.setPredecessor("Predecessor Description");
            controlValueItem.setPages("6");
            controlValueItem.setKeySequence(2);
            controlValueItem.setMandatory("Yes");
            controlValueItem.setMethodOid("Method");
            controlValueItem.setOrder("1");
            controlValueItem.setWhereClauseOid(define2WhereClauseOid);
            valueList.add(controlValueItem);

            study.setMethod(method.getOid(), method);
            study.setCodeList(codeList.getOid(), codeList);
            study.setDictionary(externalCodeList.getOid(), externalCodeList);
            study.setItemGroup(itemGroup.getName(), itemGroup);
            study.setWhereClause(whereClause.getOid(), whereClause);
            study.setValueList(valueList.getSourceItemGroupName(), valueList.getSourceItemName(), valueList);

            return study;
        }

        static InputSource define2InputSource() {
            DefineMetadata metadata = DefineMetadata.V2;
            org.dom4j.Document define = new DocumentFactory().createDocument();

            define.addElement("ODM")
                        .addAttribute(metadata.getNamespaces().xmlnsPrefix(), metadata.getNamespaces().xmlnsURI())
                        .addAttribute(
                                metadata.getNamespaces().xmlnsPrefix() + ":" + metadata.getNamespaces().defPrefix(),
                                metadata.getNamespaces().defURI())
                    .addElement("Study")
                    .addElement("MetaDataVersion")
                    .addAttribute("def:DefineVersion", "2.0.0").asXML();

            return  new InputSource(new StringReader(define.asXML()));
        }

        static InputSource define1InputSource() {
            DefineMetadata metadata = DefineMetadata.V1;

            org.dom4j.Document define = new DocumentFactory().createDocument();

            define.addElement("ODM")
                    .addAttribute(metadata.getNamespaces().xmlnsPrefix(), metadata.getNamespaces().xmlnsURI())
                    .addAttribute(
                            metadata.getNamespaces().xmlnsPrefix() + ":" + metadata.getNamespaces().defPrefix(),
                            metadata.getNamespaces().defURI())
                    .addElement("Study")
                    .addElement("MetaDataVersion")
                    .addAttribute("def:DefineVersion", "1.0.0").asXML();

            return  new InputSource(new StringReader(define.asXML()));
        }

        static InputSource defineDefaultInputSource() {
            DefineMetadata metadata = DefineMetadata.V2;
            org.dom4j.Document define = new DocumentFactory().createDocument();

            define.addElement("ODM")
                    .addAttribute(metadata.getNamespaces().xmlnsPrefix(), metadata.getNamespaces().xmlnsURI())
                    .addAttribute(
                            metadata.getNamespaces().xmlnsPrefix() + ":" + metadata.getNamespaces().defPrefix(),
                            metadata.getNamespaces().defURI()).asXML();

            return  new InputSource(new StringReader(define.asXML()));
        }
    }
}
