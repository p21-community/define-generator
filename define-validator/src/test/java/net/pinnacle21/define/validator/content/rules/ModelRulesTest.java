/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.content.rules;

import com.google.common.collect.ImmutableMap;
import net.pinnacle21.define.models.DefineMetadata;
import net.pinnacle21.define.models.Item;
import net.pinnacle21.define.models.ItemGroup;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.validator.report.MessageTransformer;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.*;

import static org.junit.Assert.fail;

public class ModelRulesTest {

    private static final String DATA_TYPE_METADATA = "variableHasValueLevelDataTypes";

    private Answer fail = new Answer() {
        @Override
        public Object answer(InvocationOnMock invocation) {
            fail();
            return null;
        }
    };

    private Answer truthy = new Answer() {
        @Override
        public Object answer(InvocationOnMock invocation) {
            return true;
        }
    };

    /**
     * <p>
     * Rule should not fire when attribute was not supplied.
     * I.E, when attribute is <code>null</code>.
     * </p>
     * <p><a href=https://pinnacle21.atlassian.net/browse/DEF-170>DEF-170</a></p>
     */
    @Test
    public void hasValidIsReferenceDataValue() throws Exception {
        final ItemGroup emptyString = new ItemGroup()
                .setName("AE")
                .setIsReferenceData(StringUtils.EMPTY);
        ItemGroup realValue = new ItemGroup()
                .setIsReferenceData("true");
        ItemGroup nill = new ItemGroup()
                .setName("DM");

        MessageTransformer transformer = Mockito.mock(MessageTransformer.class);

        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) {
                return null;
            }
        }).when(transformer).map("OD0073", ImmutableMap.of(
            "Dataset Name", emptyString.getName(),
            "Reference Data", emptyString.getIsReferenceData()
        ));

        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) {
                throw new RuntimeException("Threw OD0073 when IsReferenceData was null");
            }
        }).when(transformer).map("OD0073", new LinkedHashMap<String, String>() {{
            put("Dataset Name", "DM");
            put("Reference Data", null);
        }});

        ModelRules rules = new ModelRules(DefineMetadata.V2, null, null, null, transformer);

        rules.hasValidIsReferenceDataValue(emptyString); //Should fire first Mock
        rules.hasValidIsReferenceDataValue(realValue); //no rule should fire
        rules.hasValidIsReferenceDataValue(nill); // no rule should fire
    }

    /**
     * <p>
     * Should only fire for CDISC Standards
     * </p>
     * <p><a href=https://pinnacle21.atlassian.net/browse/DEF-168>DEF-168</a></p>
     */
    @Test
    public void invalidClassValue() throws Exception {
        DefineMetadata.Standards standards = DefineMetadata.V2.getStandards();
        List<String> standardNames = Arrays.asList(
                standards.adam(),
                standards.send(),
                standards.sdtm(),
                "Legacy"
        );
        //This will fire every time. Its the job of standard name to short circuit the rule
        final ItemGroup itemGroup = new ItemGroup()
                .setName("AE")
                .setCategory("Very Important Purpose");

        for (final String standard : standardNames) {
            MessageTransformer transformer = Mockito.mock(MessageTransformer.class);

            Study study = new Study().setStandardName(standard);
            ModelRules rules = new ModelRules(DefineMetadata.V2, study, null, null, transformer);
                    //.setMetadata(new DefineV2Metadata());
            if (!standard.equals("Legacy")) {
                Mockito.doAnswer(new Answer() {
                    @Override
                    public Object answer(InvocationOnMock invocation) {
                        return null;
                    }
                }).when(transformer).map("DD0055", new LinkedHashMap<String, String>() {{
                    put("Dataset Name", itemGroup.getName());
                    put("Class", itemGroup.getCategory());
                }});
            } else {
                Mockito.doAnswer(new Answer() {
                    @Override
                    public Object answer(InvocationOnMock invocation) {
                        throw new RuntimeException("Threw DD0055 when Standard was Legacy");
                    }
                }).when(transformer).map("DD0055", new LinkedHashMap<String, String>() {{
                    put("Dataset Name", itemGroup.getName());
                    put("Class", itemGroup.getCategory());
                }});
            }
            rules.validateClass(itemGroup); //Should fire first Mock
        }
    }

    @Test
    public void validateDataTypeNotValueLevel() {
        final Item item = new Item()
                .setName("DADTC")
                .setDataType(DefineMetadata.DataTypes.TEXT);
        final ItemGroup itemGroup = new ItemGroup()
                .setName("DA");
        ModelRules rules = prepareRules(itemGroup, item, null, null);
        rules.validateDataType(itemGroup, item, null);
    }

    @Test(expected = AssertionError.class)
    public void validateDataTypeNullValueNotValueLevel() {
        final Item item = new Item()
                .setName("DADTC");
        final ItemGroup itemGroup = new ItemGroup()
                .setName("DA");
        ModelRules rules = prepareRules(itemGroup, item, null, null);
        rules.validateDataType(itemGroup, item, null);
    }

    /**
     * The parent variable will have data type set, so the value level items will not need it set.
     */
    @Test
    public void validateDataTypeValueLevel() {
        final Item source = new Item()
                .setName("DAORRES")
                .setDataType(DefineMetadata.DataTypes.TEXT);
        final Item valueLevel = new Item()
                .setName("DA.DISPAMNT");
        final ItemGroup itemGroup = new ItemGroup()
                .setName("DA");
        ModelRules rules = prepareRules(itemGroup, source, null, new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) {
                return new HashMap<String, Map<String, String>>() {{
                    put(DATA_TYPE_METADATA, new HashMap<String, String>() {{
                        put(itemGroup.getDomain() + "." + source.getName(), "false");
                    }});
                }};
            }
        });
        rules.validateDataType(itemGroup, valueLevel, source);
    }

    /**
     * The parent variable will NOT have data type set, so the value level items will need it set.
     */
    @Test
    public void validateDataTypeParentNull() {
        final Item source = new Item()
                .setName("DAORRES");
        final Item valueLevel = new Item()
                .setName("DA.DISPAMNT")
                .setDataType(DefineMetadata.DataTypes.TEXT);
        final ItemGroup itemGroup = new ItemGroup()
                .setName("DA");
        ModelRules rules = prepareRules(itemGroup, valueLevel, source, new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) {
                return new HashMap<String, Map<String, String>>() {{
                    put(DATA_TYPE_METADATA, new HashMap<String, String>() {{
                        put(itemGroup.getDomain() + "." + source.getName(), "true");
                    }});
                }};
            }
        });
        rules.validateDataType(itemGroup, valueLevel, source);
    }

    /**
     * Both variables will have data type set, everything should be fine.
     */
    @Test
    public void validateDataTypeBothSet() {
        final Item source = new Item()
                .setName("DAORRES")
                .setDataType(DefineMetadata.DataTypes.TEXT);
        final Item valueLevel = new Item()
                .setName("DA.DISPAMNT")
                .setDataType(DefineMetadata.DataTypes.TEXT);
        final ItemGroup itemGroup = new ItemGroup()
                .setName("DA");
        ModelRules rules = prepareRules(itemGroup, valueLevel, source, new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) {
                return new HashMap<String, Map<String, String>>() {{
                    put(DATA_TYPE_METADATA, new HashMap<String, String>() {{
                        put(itemGroup.getDomain() + "." + source.getName(), "true");
                    }});
                }};
            }
        });
        rules.validateDataType(itemGroup, valueLevel, source);
    }

    /**
     * If both are missing data type, we expect an issue to be raised...
     */
    @Test(expected = AssertionError.class)
    public void validateDataTypeBothNull() {
        final Item source = new Item()
                .setName("DAORRES");
        final Item valueLevel = new Item()
                .setName("DA.DISPAMNT");
        final ItemGroup itemGroup = new ItemGroup()
                .setName("DA");
        ModelRules rules = prepareRules(itemGroup, valueLevel, source, new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) {
                return new HashMap<String, Map<String, String>>() {{
                    put(DATA_TYPE_METADATA, new HashMap<String, String>() {{
                        put(itemGroup.getDomain() + "." + source.getName(), "false"); //Since datatype is null on DA.DISPAMNT, this would be false IRL
                    }});
                }};
            }
        });
        rules.validateDataType(itemGroup, valueLevel, source);
    }

    /**
     * Sets up a Mock ModelRules class with metadata needed to dataType validation for the provided ItemGroup and item.
     * @param itemGroup needs at least name set
     * @param vlItem needs name set, and data type is optional
     * @return a Mocked {@link ModelRules} ready to have {@link ModelRules#validateDataType(ItemGroup, Item, Item)} called against it.
     */
    private ModelRules prepareRules(final ItemGroup itemGroup, final Item vlItem, final Item sourceItem, Answer answer) {
        Answer defaultz = new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) {
                return new HashMap<String, HashMap<String, String>>() {{
                    put(DATA_TYPE_METADATA, new HashMap<String, String>() {{
                        new HashMap<String, String>() {{
                            put(itemGroup.getDomain() + "." + vlItem.getName(), "false");
                        }};
                    }});
                }};
            }
        };
        ModelRules rules = Mockito.mock(ModelRules.class);
        Mockito.doAnswer(fail).when(rules).throwOD0075(itemGroup, vlItem, sourceItem);
        Mockito.doAnswer(truthy).when(rules).isDefine2();
        Mockito.doAnswer(answer == null ? defaultz : answer).when(rules).getValueLevelMetadataInfo();
        Mockito.doCallRealMethod().when(rules).validateDataType(itemGroup, vlItem, sourceItem);
        return rules;
    }

}
