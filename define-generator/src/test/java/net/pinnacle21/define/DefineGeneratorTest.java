/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define;

import junit.framework.Assert;
import net.pinnacle21.define.models.DefineMetadata;
import net.pinnacle21.define.models.ItemGroup;
import net.pinnacle21.define.util.Stubs;
import net.pinnacle21.parsing.xml.XmlWriter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.transform.TransformerConfigurationException;
import java.io.IOException;
import java.util.Arrays;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;

public class DefineGeneratorTest {
    @Test
    public void noDomainAttributeInADaM() {
        try {
            DefineGenerator generator = new DefineGenerator(Stubs.ADAM_STUDY_NO_DOMAIN, FileUtils.getTempDirectory());
            XmlWriter.SimpleAttributes attributes =  generator.writeItemGroupDefAttributes(Stubs.ITEM_GROUP);

            Assert.assertTrue("Domain attribute is included in an ADaM Study",
                    StringUtils.isBlank(attributes.getValue(DefineMetadata.V2.getNamespaces().odmURI(), "Domain")));
        } catch (IOException | TransformerConfigurationException | SAXException e) {
            fail(e.getMessage() + "\n" + Arrays.toString(e.getStackTrace()));
        }
    }

    /**
     * Test to make sure that the compare logic for ItemGroups does not have the hard-coded logic for FA domains
     * Hard-coded logic: if domain name starts with "FA", the class used for ordering is "findings about"
     */
    @Test
    public void testClassOrderForFADatasets() {
        ItemGroup FA = new ItemGroup().setName("FA").setCategory("FINDINGS");
        ItemGroup VS = new ItemGroup().setName("VS").setCategory("FINDINGS");
        assertEquals(-16, DefineGenerator.Orders.SDTM.compare(FA, VS));
    }

    @Test
    public void testClassOrderForADaMDatasets() {
        assertEquals(-1, DefineGenerator.Orders.ADaM.compare(
                new ItemGroup().setName("ADSL").setCategory("SUBJECT LEVEL ANALYSIS DATASET"),
                new ItemGroup().setName("ADAE").setCategory("ADVERSE EVENTS ANALYSIS DATASET")
        ));
        assertEquals(-1, DefineGenerator.Orders.ADaM.compare(
                new ItemGroup().setName("ADSL").setCategory("ADVERSE EVENTS ANALYSIS DATASET"),
                new ItemGroup().setName("ADAE").setCategory("BASIC DATA STRUCTURE")
        ));
        assertEquals(-1, DefineGenerator.Orders.ADaM.compare(
                new ItemGroup().setName("ADSL").setCategory("BASIC DATA STRUCTURE"),
                new ItemGroup().setName("ADAE").setCategory("ADAM OTHER")
        ));
    }
}
