/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.CodeList;
import net.pinnacle21.define.models.CodeListItem;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.util.MockNamedRow;
import net.pinnacle21.parsing.excel.ExcelSheetParser;
import net.pinnacle21.parsing.excel.RowIdentifier;
import org.junit.Test;
import org.powermock.api.mockito.PowerMockito;

import java.util.Collections;
import java.util.List;

import static net.pinnacle21.define.parsers.excel.CodeListSheetParser.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CodeListSheetParserTest {

    @Test
    public void nonExistingCodelistGetsCreated() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new CodeListSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedId = "Id";
        String expectedName = "name";
        String expectedCode = "code";
        String expectedDataType = "text";

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedId + " ")
                        .with(NAME_COLUMN, expectedName)
                        .with(NCI_CODELIST_CODE_COLUMN, expectedCode)
                        .with(DATA_TYPE_COLUMN, expectedDataType),
                parsedStudy
        );

        assertTrue(study.hasCodeList(expectedId));

        CodeList codeList = study.getCodeList(expectedId);

        assertNotNull(codeList);
        assertEquals(expectedId, codeList.getOid());
        assertEquals(expectedName, codeList.getName());
        assertEquals(expectedCode, codeList.getCode());
        assertEquals(expectedDataType, codeList.getDataType());
    }
    
    @Test
    public void nonExistingCodelistItemGetsCreated() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new CodeListSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedId = "Id";
        String expectedName = "name";
        String expectedCode = "code";
        String expectedDataType = "text";
        String expectedOrder = "1";
        String expectedCodedValue = "Term";
        String expectedTermCode = "termCode";
        String expectedDecodedValue = "TermDecode";

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedId + " ")
                        .with(NAME_COLUMN, expectedName)
                        .with(NCI_CODELIST_CODE_COLUMN, expectedCode)
                        .with(DATA_TYPE_COLUMN, expectedDataType)
                        .with(ORDER_COLUMN, expectedOrder)
                        .with(TERM_COLUMN, expectedCodedValue + " ")
                        .with(NCI_TERM_CODE_COLUMN, expectedTermCode)
                        .with(DECODED_VALUE_COLUMN, expectedDecodedValue),
                parsedStudy
        );

        assertTrue(study.hasCodeListItem(expectedId, expectedCodedValue));

        CodeListItem codeListItem = study.getCodeListItem(expectedId, expectedCodedValue);

        assertEquals(expectedOrder, codeListItem.getOrder());
        assertEquals(expectedCodedValue, codeListItem.getCodedValue());
        assertEquals(expectedTermCode, codeListItem.getCode());
        assertEquals(expectedDecodedValue, codeListItem.getDecodedValue());
    }

    @Test
    public void missingTermErrorRaisedWhenOtherAttributesPresent() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new CodeListSheetParser();

        when(parsedStudy.getStudy()).thenReturn(new Study());

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, "ID")
                        .with(NAME_COLUMN, "TEST")
                        .with(DECODED_VALUE_COLUMN, "DECODE"),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(MISSING_TERM_ERROR), anyVararg());
    }

    // DEF-149
    @Test
    public void codelistWithSpacesInTerm() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new CodeListSheetParser();

        when(parsedStudy.getStudy()).thenReturn(new Study());

        parser.parse(
                new MockNamedRow()
                    .with(ID_COLUMN, "ID")
                    .with(NAME_COLUMN, "TEST")
                    .with(TERM_COLUMN, " "),
                parsedStudy
        );

        List<CodeList> codelists = parsedStudy.getStudy().getCodeLists();

        assertTrue(codelists.size() > 0
                && "ID".equals(codelists.get(0).getOid())
                && codelists.get(0).getCodelistItems().size() == 0
        );
    }

    // TODO: Why is this an error here but not for standards?
    @Test
    public void nonnumericOrderErrorRaised() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new CodeListSheetParser();

        when(parsedStudy.getStudy()).thenReturn(new Study());

        String expectedBadOrder = "NOT_A_NUMBER";

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, "ID")
                        .with(NAME_COLUMN, "TEST")
                        .with(TERM_COLUMN, "TERM")
                        .with(ORDER_COLUMN, expectedBadOrder),
                parsedStudy
        );

        verify(parsedStudy).error(any(RowIdentifier.class), eq(NONNUMERIC_TERM_ERROR), anyVararg());
    }
    
    @Test
    public void duplicateErrorRaised() throws Exception {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new CodeListSheetParser();
        Study study = PowerMockito.mock(Study.class);
        CodeList codeList = PowerMockito.mock(CodeList.class);
        CodeListItem codeListItem = PowerMockito.mock(CodeListItem.class);
        List<CodeListItem> codeListItems = Collections.singletonList(codeListItem);
        
        when(parsedStudy.getStudy()).thenReturn(study);
        
        String expectedId = "ID";
        String expectedTerm = "TERMS";
        
        study.setCodeList(expectedId, codeList);
        
        MockNamedRow row = new MockNamedRow()
            .with(ID_COLUMN, expectedId)
            .with(NAME_COLUMN, "TEST")
            .with(TERM_COLUMN, expectedTerm)
            .with(ORDER_COLUMN, "1");
        
        PowerMockito.when(study, "getCodeList", expectedId).thenReturn(codeList);
        PowerMockito.when(codeList, "getCodelistItems").thenReturn(codeListItems);
        PowerMockito.when(codeListItem, "getCodedValue").thenReturn(expectedTerm);
        
        parser.parse(row, parsedStudy);
        verify(parsedStudy).error(any(RowIdentifier.class), eq(DUPLICATE_TERM_ERROR), anyVararg());
    }
    
    @Test
    public void duplicateWithNonAsciiCharErrorRaised() throws Exception {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new CodeListSheetParser();
        Study study = PowerMockito.mock(Study.class);
        CodeList codeList = PowerMockito.mock(CodeList.class);
        CodeListItem codeListItem = PowerMockito.mock(CodeListItem.class);
        List<CodeListItem> codeListItems = Collections.singletonList(codeListItem);
        
        when(parsedStudy.getStudy()).thenReturn(study);
        
        String expectedId = "ID";
        String expectedTerm = "TERMS";
        String expectedTermWithNonAsciiChar = "TERM\u0092S";
        
        study.setCodeList(expectedId, codeList);
        
        MockNamedRow row = new MockNamedRow()
            .with(ID_COLUMN, expectedId)
            .with(NAME_COLUMN, "TEST")
            .with(TERM_COLUMN, expectedTerm)
            .with(ORDER_COLUMN, "1");
        
        PowerMockito.when(study, "getCodeList", expectedId).thenReturn(codeList);
        PowerMockito.when(codeList, "getCodelistItems").thenReturn(codeListItems);
        PowerMockito.when(codeListItem, "getCodedValue").thenReturn(expectedTermWithNonAsciiChar);
        
        parser.parse(row, parsedStudy);
        
        verify(parsedStudy).error(any(RowIdentifier.class), eq(DUPLICATE_TERM_WITH_NON_ASCII_CHAR_ERROR), anyVararg());
    }
}
