/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.content.rules;

import net.pinnacle21.define.models.DefineMetadata;
import net.pinnacle21.define.models.ItemGroup;
import net.pinnacle21.define.parsers.xml.XPathEvaluator;
import net.pinnacle21.define.validator.content.SupplementalValidationRule;
import net.pinnacle21.define.validator.content.ValidationRule;
import net.pinnacle21.define.validator.report.MessageTransformer;
import net.pinnacle21.define.validator.resources.ResourceManager;
import net.pinnacle21.define.validator.resources.Terminology;
import net.pinnacle21.define.validator.util.Flag;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.dom4j.*;
import org.dom4j.tree.DefaultElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class XPathRules extends AbstractRules {
    private static final Logger LOGGER = LoggerFactory.getLogger(XPathRules.class);
    private static final String X_MDV = "/ODM/odm:Study/odm:MetaDataVersion";
    private static final String X_ARM_RES = "/arm:AnalysisResultDisplays/arm:ResultDisplay/arm:AnalysisResult";
    private static final ThreadLocal<Pattern> DD0085 = ThreadLocal.withInitial(() -> Pattern.compile(
        "href=([\"'])(?<href>.*?)\\1"
    ));
    private static final ThreadLocal<Pattern> OD0011 = ThreadLocal.withInitial(() -> Pattern.compile(
        "encoding=([\"'])(?<encoding>.*?)\\1"
    ));

    private final DefineMetadata metadata;
    private final ResourceManager resourceManager;
    private final Terminology terminology;
    private final Flag isFatal;
    private final XPathEvaluator xpath;

    public XPathRules(DefineMetadata metadata, ResourceManager resourceManager, Terminology terminology,
            XPathEvaluator xpath, Flag isFatal, MessageTransformer transformer) {
        super(transformer);

        this.metadata = metadata;
        this.resourceManager = resourceManager;
        this.terminology = terminology;
        this.xpath = xpath;
        this.isFatal = isFatal;
    }

    @ValidationRule(id = "DD0002", isPreCheck = true)
    public void checkNamespaces() {
        Element odm = this.xpath.selectSingleElement("/ODM");

        if (odm == null) {
            return;
        }

        if (odm.attribute("ODMVersion") == null || StringUtils.isBlank(odm.attributeValue("ODMVersion"))) {
            this.transformer.map("DD0003", Arrays.asList("ODMVersion", "ODM"), mapOf(
                "Element", "ODM"
            ));
        }

        Namespace namespace = odm.getNamespace();

        if (namespace == null) {
            return;
        }

        String defaultNamespace = namespace.getStringValue();

        switch (this.metadata) {
            case V1:
                dd0019(odm);

                if (!defaultNamespace.equals(this.metadata.getNamespaces().odmURI())) {
                    this.transformer.map("DD0002", Collections.singletonList(this.metadata.getNamespaces().xmlnsPrefix()));
                    this.isFatal.mark();
                }

                namespace = odm.getNamespaceForPrefix(this.metadata.getNamespaces().defPrefix());

                if (namespace != null && !this.metadata.getNamespaces().defURI().equals(namespace.getStringValue())) {
                    this.transformer.map("DD0002", Collections.singletonList(this.metadata.getNamespaces().defPrefix()));
                    this.isFatal.mark();
                }
                break;
            case V2:
                if (!defaultNamespace.equals(this.metadata.getNamespaces().odmURI())) {
                    this.transformer.map("DD0002", Collections.singletonList(this.metadata.getNamespaces().xmlnsPrefix()));
                    this.isFatal.mark();
                }

                namespace = odm.getNamespaceForPrefix(this.metadata.getNamespaces().defPrefix());
                if (namespace != null && !this.metadata.getNamespaces().defURI().equals(namespace.getStringValue())) {
                    this.transformer.map("DD0002", Collections.singletonList(this.metadata.getNamespaces().defPrefix()));
                    this.isFatal.mark();
                }
             break;
            //case DefineMetadata.MISSING:
                //TODO
            default:
                //Invalid define version, should get caught by DD0020
        }

        namespace = odm.getNamespaceForPrefix(this.metadata.getNamespaces().xlinkPrefix());
        if (namespace != null && !this.metadata.getNamespaces().xlinkURI().equals(namespace.getStringValue())) {
            this.transformer.map("DD0002", Collections.singletonList(this.metadata.getNamespaces().xlinkPrefix()));
            this.isFatal.mark();
        }

        if (odm.attribute("schemalocation") != null) {
            if (!odm.attribute("schemalocation").getStringValue().equals(this.metadata.getNamespaces().xsiURI())) {
                this.transformer.map("DD0002", Collections.singletonList(this.metadata.getNamespaces().xsiPrefix()));
            }
        }
    }

    @SupplementalValidationRule(id = "DD0019")
    private void dd0019(Element odm) {
        final String fileType = odm.attributeValue("FileType", "");

        if (!"Snapshot".equals(fileType)) {
            this.transformer.map("DD0019", mapOf(
                "FileType", fileType
            ));
        }
    }

    @ValidationRule(id = "DD0004")
    public void attributeNotAllowed() {
        if (StringUtils.isBlank(getDefineVersion())) { //TODO this check seems like a waste of time is the version is not set correctly.
            return;
        }

        XPathEvaluator defineMetadata = new XPathEvaluator(this.resourceManager.getDefineMetadata(this.metadata));
        XPathEvaluator odmMetadata = new XPathEvaluator(this.resourceManager.getOdmMetadata(this.metadata));

        Element root = this.xpath.getDocument().getRootElement();

        if (root.getName().equals("ODM")) {
            validateAllMetadata(root, defineMetadata, odmMetadata);
        }
    }

    @SupplementalValidationRule(id = "DD0007")
    private void validateAllMetadata(Element currentElement, XPathEvaluator defineMetadata, XPathEvaluator odmMetadata) {
        String currentElementName = currentElement.getQualifiedName();

        //Get the current element from the define metadata file...
        Element defineElement = defineMetadata.selectSingleElement("/metadata//element[@name=$name]",
                mapOf("name", currentElementName));
        //Get the current element from the odm metadata file...
        Element odmElement = odmMetadata.selectSingleElement("/metadata//element[@name=$name]",
                mapOf("name", currentElementName));

        if (defineElement != null) {
            validateAllAttributes(currentElement, currentElementName, currentElement.attributes(), defineElement.elements("attribute"),
                    odmElement != null ? odmElement.elements("attribute") : Collections.emptyList());

            for (Iterator iterator = currentElement.elementIterator(); iterator.hasNext(); ) {
                validateAllMetadata((Element) iterator.next(), defineMetadata, odmMetadata);
            }

        } else if (odmElement == null) { // If its not in the ODM metadata
            this.transformer.map("DD0007", Collections.singletonList(currentElementName), mapOf(
                "Element", currentElementName
            )); //Its just invalid altogether
        } else {
            this.transformer.map("DD0011", Collections.singletonList(currentElementName), mapOf(
                "Element", currentElementName
            )); //Its an ODM Element only and shouldn't be in the define
        }
    }

    @SupplementalValidationRule(id = "DD0086, DD0010, DD0004")
    private void validateAllAttributes(Element currentElement, String currentElementName, List currentAttributes,
            List allowedDefineAttributes, List odmAttributes) {
        if (currentAttributes.isEmpty()) {
            return;
        }

        List<String> allowedAttributeList = new ArrayList<>();
        List<String> odmAttributeList = new ArrayList<>();

        for (Object object : allowedDefineAttributes) {
            Element element = (Element) object;
            allowedAttributeList.add(element.attributeValue("name"));
        }

        for (Object object : odmAttributes) {
            Element element = (Element) object;
            odmAttributeList.add(element.attributeValue("name"));
        }

        for (Object object : currentAttributes) {
           validateAttribute(currentElement, currentElementName, object, allowedAttributeList, odmAttributeList);
        }
    }

    @SupplementalValidationRule(id = "DD0086, DD0010, DD0004")
    private void validateAttribute(final Element currentElement, final String currentElementName, Object attribute,
            List allowedAttributeList, List odmAttributeList) {
        final Attribute currentAttribute = (Attribute) attribute;
        final String attributeName  = currentAttribute.getQualifiedName();

        //Check length regardless
        if (currentAttribute.getText().length() > 1000) {
            this.transformer.map("DD0086", mapOf(
                "Value", currentAttribute.getText(),
                "Attribute", attributeName
            ));
        }

        //if it exists in define we are fine
        if (allowedAttributeList.contains(attributeName)) {
            return;
        }

        //If the attribute is not on defined for that define.xml element but it IS defined for the ODM element
        if (odmAttributeList.contains(attributeName)) {
            //Then its an ODM attribute
            this.transformer.map("DD0010", Collections.singletonList(attributeName), mapOf(
                "Value", currentAttribute.getValue(),
                "Attribute", attributeName,
                "Element", currentElementName
            ));
        } else {
            //Filter for attributes i add manually during validation
            if (attributeName.equals("DD0024flag")) {
                return;
            }

            // if its not defined for define.xml and NOT defined for ODM
            // Its flat out invalid
            this.transformer.map("DD0004", Arrays.asList(attributeName, currentElementName), mapOf(
                "Element", currentElementName,
                //  These are just my best two guesses for identifying an element right now,
                //  since we have no idea what kind of element it is at this point without doing some sort of switch, which seems pointless.
                //  Anyway, the schema should catch some edge cases that we arent thinking of.
                "Element Identifier", StringUtils.isNotBlank(currentElement.attributeValue("OID"))
                        ? currentElement.attributeValue("OID") : StringUtils.isNotBlank(currentElement.attributeValue("Name"))
                        ? currentElement.attributeValue("Name") : "Could Not Find a Unique Identifier",
                "Attribute", attributeName,
                "Value", currentAttribute.getValue()
            ));
        }
    }


    @ValidationRule(id = "DD0006")
    public void requiredElements() {
        if (this.xpath.getDocument().getRootElement() == null) {
            return;
        }

        //Check for a study element
        if ("ODM".equals(this.xpath.getDocument().getRootElement().getName())) {
            if (!this.xpath.hasNodes("/ODM/odm:Study")) {
                this.transformer.map("DD0006", Collections.singletonList("Study"), mapOf(
                    "Element", "Study"
                ));
            } else if (!this.xpath.hasNodes(X_MDV)) {
                this.transformer.map("DD0006", Collections.singletonList("MetaDataVersion"), mapOf(
                    "Element", "MetaDataVersion"
                ));
            } else {
                if (!this.xpath.hasNodes(X_MDV + "/odm:ItemDef")) {
                    this.transformer.map("DD0006", Collections.singletonList("ItemDef"), mapOf(
                        "Element", "ItemDef"
                    ));
                }

                List<Element> itemGroups = this.xpath.selectElements(X_MDV + "/odm:ItemGroupDef");

                if (itemGroups.isEmpty()) {
                    this.transformer.map("DD0006", Collections.singletonList("ItemGroupDef"), mapOf(
                        "Element", "ItemGroupDef"
                    ));
                } else {
                    for (final Element itemGroup : itemGroups) {
                        if (itemGroup.elements("ItemRef").isEmpty()) {
                            this.transformer.map("DD0006", Collections.singletonList("ItemRef"), mapOf(
                                "ItemGroupDef OID", itemGroup.attributeValue("OID")
                            ));
                        }
                    }
                }
            }
        }


        //Global Variables checking
        Element globalVariables = this.xpath.selectSingleElement("/ODM/odm:Study/odm:GlobalVariables");

        if (globalVariables != null) {
            String studyOID = this.xpath.selectSingleElement("/ODM/odm:Study").attributeValue("OID");
            Node studyDescription = this.xpath.selectSingleNode("/ODM/odm:Study/odm:GlobalVariables/odm:StudyDescription");
            Node protocolName = this.xpath.selectSingleNode("/ODM/odm:Study/odm:GlobalVariables/odm:ProtocolName");

            if (textElementSchemaSafe(globalVariables, "StudyName")) {
                this.transformer.map("DD0006", Collections.singletonList("StudyName"), mapOf(
                    "Study OID", studyOID
                ));
            }

            if (studyDescription == null || StringUtils.isBlank(studyDescription.getText())) {
                this.transformer.map("DD0006", Collections.singletonList("StudyDescription"), mapOf(
                    "Study OID", studyOID
                ));
            }

            if (protocolName == null || StringUtils.isBlank(protocolName.getText())) {
                transformer.map("DD0006", Collections.singletonList("ProtocolName"), mapOf(
                    "Study OID", studyOID
                ));
            }
        }
    }

    @ValidationRule(id = "DD0013")
    public void duplicateMethodDefs() {
        List<Element> methodList;

        if (isDefine2()) {
            methodList = this.xpath.selectElements(X_MDV + "/odm:MethodDef");
        } else {
            methodList = this.xpath.selectElements(X_MDV + "/def:ComputationMethod");
        }

        List<String> methodOIDs = new LinkedList<>();

        for (Element method : methodList) {
            String oid = method.attributeValue("OID");

            if (StringUtils.isNotBlank(oid)) {
                if (methodOIDs.contains(oid)) {
                    this.transformer.map("DD0013", mapOf(
                        "Method", oid
                    ));
                } else {
                    methodOIDs.add(oid);
                }
            }
        }
    }

    @ValidationRule(id = "DD0014")
    public void duplicateValueListDefs() {
        List<Element> valueListList = this.xpath.selectElements(X_MDV + "/def:ValueListDef");
        List<String> valueListOIDs = new LinkedList<>();

        for (Element valueList : valueListList) {
            String oid = valueList.attributeValue("OID");

            if (StringUtils.isNotBlank(oid)) {
                if (valueListOIDs.contains(oid)) {
                   this.transformer.map("DD0014", mapOf(
                       "Value Level", oid
                   ));
                } else {
                    valueListOIDs.add(oid);
                }
                //Check for duplicate Itemrefs\
                defineRuleOD0041(valueList);
            }
        }
    }

    @ValidationRule(id = "DD0015")
    public void referencedDocumentMissing() {
        List<Element> documentRefs = this.xpath.selectElements(X_MDV + "//def:DocumentRef");

        for (Element element : documentRefs) {
            String leafID = element.attributeValue("leafID");

            if (StringUtils.isNotBlank(leafID)) {
                if (!this.xpath.hasNodes(X_MDV + "//def:leaf[@ID=$id]", mapOf("id", leafID))) {
                    this.transformer.map("DD0015", mapOf(
                        "Document", leafID
                    ));
                }
            }
        }
    }

    // Not necessary to be model rule since our system doesn't allow unreferenced valueLevel creation
    @ValidationRule(id = "DD0017")
    public void referencedValueLevelMetadataIsMissing() {
        List<Element> documentRefs = this.xpath.selectElements(X_MDV + "//def:ValueListRef");

        for (final Element element : documentRefs) {
            if (attributeValueSchemaSafe(element, "ValueListOID")) {
                this.transformer.map("DD0003", Arrays.asList("ValueListOID", "def:ValueListRef"), mapOf(
                    "Variable", element.getParent().attributeValue("OID")
                ));
            } else if (!this.xpath.hasNodes(X_MDV + "//def:ValueListDef[@OID=$id]", mapOf("id",
                    element.attributeValue("ValueListOID")))) {
                this.transformer.map("DD0017", mapOf(
                    "Value Level", element.attributeValue("ValueListOID")
                ));
            }
        }
    }

    @ValidationRule(id = "DD0018")
    public void archiveLocationVsLeaf() {
        List<Element> itemGroupDefs = this.xpath.selectElements(X_MDV + "/odm:ItemGroupDef");

        for (final Element itemGroup : itemGroupDefs) {
            String itemGroupOid = itemGroup.attributeValue("OID");
            String archiveLocation = itemGroup.attributeValue("ArchiveLocationID");
            Element leaf = itemGroup.element("leaf");

            if (itemGroup.elements("leaf").isEmpty()) {
                this.transformer.map("DD0006", Collections.singletonList("def:leaf"), mapOf(
                    "Element", "ItemGroupDef",
                    "LeafID", itemGroup.attributeValue("OID")
                ));
            } else {
                String leafId = leaf.attributeValue("ID");

                if (itemGroup.attribute("ArchiveLocationID") == null || StringUtils.isBlank(itemGroup.attributeValue("ArchiveLocationID"))) {
                    this.transformer.map("DD0056", mapOf(
                        "Datasets", itemGroupOid
                    ));
                } else if (StringUtils.isNotBlank(archiveLocation) && !archiveLocation.equals(StringUtils.defaultString(leafId))) {
                    this.transformer.map("DD0018", mapOf(
                        "LeafID", leafId,
                        "ItemGroupDef", itemGroupOid,
                        "ArchiveLocationID", archiveLocation
                    ));
                }
            }
        }
    }

    @ValidationRule(id = "DD0020", isPreCheck = true)
    public void defineRuleDD0020() { // TODO: Name
        Element metaDataVersion = this.xpath.selectSingleElement(X_MDV);

        if (metaDataVersion == null) {
            return;
        }

        String defineVersion = cleanAttribute(metaDataVersion.attributeValue("DefineVersion"));
        String oid = metaDataVersion.attributeValue("OID");

        Set<String> validVersions = Stream.of(DefineMetadata.values())
                .map(DefineMetadata::getVersion)
                .collect(Collectors.toSet());

        if (defineVersion.equals(DefineMetadata.V2.getVersion())) {
            return;
        }
        if (defineVersion.equals(DefineMetadata.V1.getVersion())) {
            return;
        }

        if (!validVersions.contains(defineVersion)) {
            if (attributeValueSchemaSafe(metaDataVersion, "DefineVersion")) {
                this.transformer.map("DD0003", Arrays.asList("def:DefineVersion", "MetaDataVersion"),
                        mapOf("MetaDataVersion OID", oid)); //Missing the attribute
            } else if (metaDataVersion.attribute("DefineVersion") != null) {
                this.transformer.map("DD0020", Collections.singletonList(defineVersion)); //Invalid
                this.isFatal.mark();
            }
        }
    }

    @ValidationRule(id = "DD0021", isPreCheck = true)
    public void defineRuleDD0021() {
        Element metaDataVersion = this.xpath.selectSingleElement(X_MDV);
        String standardName, standardVersion;

        if (metaDataVersion == null) {
            return;
        }

        String oid = metaDataVersion.attributeValue("OID");
        standardName = metaDataVersion.attributeValue("StandardName");
        standardVersion = metaDataVersion.attributeValue("StandardVersion");
        boolean checkable = true;

        if (attributeValueSchemaSafe(metaDataVersion, "StandardName")) {
            this.transformer.map("DD0003", Arrays.asList("def:StandardName", "MetaDataVersion"), mapOf(
                "MetaDataVersion OID", oid
            )); //Missing the attribute

            checkable = false;
        } else if (metaDataVersion.attribute("StandardName") == null) {
            checkable = false;
        }


        if (attributeValueSchemaSafe(metaDataVersion, "StandardVersion")) {
            this.transformer.map("DD0003", Arrays.asList("def:StandardVersion", "MetaDataVersion"), mapOf(
                "MetaDataVersion OID", oid
            )); //Missing the attribute
        }

        if (checkable) {
            if (!isValidStandardName(standardName)) {
                this.transformer.map("DD0021", Collections.singletonList(standardName), mapOf(
                    "MetaDataVersion", oid
                ));
            } else {
                defineRuleDD0022(standardName, standardVersion, oid);
            }
        }
    }

    @SupplementalValidationRule(id = "DD0022")
    private void defineRuleDD0022(String standardName, String standardVersion, String oid) {
        // Safety check to avoid null pointer exceptions, but this should never happen
        if (isDefine2()) {
            if (this.metadata.getStandards().standardToVersions().get(standardName) == null) {
                return;
            }

            if (!this.metadata.getStandards().standardToVersions().get(standardName).contains(standardVersion)) {
                this.transformer.map("DD0022", Arrays.asList(standardVersion, standardName, "MetaDataVersion", oid)); // Invalid Standard Version
            }
        } else {
            if (this.metadata.getStandards().standardToVersions().get(standardName) == null) {
                return;
            }

            if (!this.metadata.getStandards().standardToVersions().get(standardName).contains(standardVersion)) {
                this.transformer.map("DD0022", Arrays.asList(standardVersion, standardName, "MetaDataVersion", oid)); // Invalid Standard Version
            }
        }
    }

    @ValidationRule(id = "DD0025")
    public void meddraVersion() {
        List<Element> externalCodeLists = this.xpath.selectElements(X_MDV + "/odm:CodeList/odm:ExternalCodeList");

        for (Element externalCodeList : externalCodeLists) {
            if (externalCodeList.attributeValue("Dictionary").toUpperCase().contains("MEDDRA")) {
                String version = externalCodeList.attributeValue("Version");

                if (!version.matches("^[0-9]*\\.[0-1]")) {
                    this.transformer.map("DD0025", Collections.singletonList(version));
                }
            }

        }
    }

    // Tried moving to models. Duplicates were not saved to model because it is saved in a key map so did not work.
    // TODO: Need to verify the screen will not allow duplicate keySequence (otherwise need to move to model checks)
    @ValidationRule(id = "DD0041", isPreCheck = true)
    public void duplicateKeySequence() {
        List<Element> itemGroupDefs = this.xpath.selectElements(X_MDV + "/odm:ItemGroupDef");

        for (Element itemGroup : itemGroupDefs) {
            List<String> keySequence = new ArrayList<>();

            for (Element itemRef : itemGroup.elements("ItemRef")) {
                String sequenceNumber = itemRef.attributeValue("KeySequence");

                if (StringUtils.isNotBlank(sequenceNumber)) {
                    if (keySequence.contains(sequenceNumber)) {
                        this.transformer.map("DD0041", mapOf(
                            "ItemRef", itemRef.attributeValue("ItemOID")
                        ));
                    } else {
                        keySequence.add(sequenceNumber);
                    }
                }
            }
        }
    }

    @ValidationRule(id = "DD0009")
    public void validateVariableOrigin() {
        List<Element> itemDefs = this.xpath.selectElements(X_MDV + "/odm:ItemDef");

        for (Element itemDef : itemDefs) {
            //Duplicate ValueListRef check
            List<Element> valueListRefs = itemDef.elements("ValueListRef");

            if (valueListRefs.size() > 1) {
                for (int i = 1; i < valueListRefs.size(); i++) {
                    Element valueListRef = valueListRefs.get(i);

                    this.transformer.map("DD0009", mapOf(
                        "ItemDef", itemDef.attributeValue("OID"),
                        "ValueListOID", valueListRef.attributeValue("ValueListOID")
                    ));
                }
            }

            // Only run this version of DD0111 for Define 1
            if (this.metadata == DefineMetadata.V1) {
                String origin = itemDef.attributeValue(DefineMetadata.Attributes.ORIGIN, "").toLowerCase();

                if (!origin.contains("crf")
                        && (origin.contains("pages") || origin.contains("page") || origin.matches(".* \\d+ .*"))) {
                    Element itemGroup = ObjectUtils.defaultIfNull(
                            itemGroupFor(itemDef),
                            new DefaultElement("ItemGroupDef")
                    );

                    this.transformer.map("DD0111", mapOf(
                        "Dataset Name", itemGroup.attributeValue(DefineMetadata.Attributes.NAME, ""),
                        "Variable Name", itemDef.attributeValue(DefineMetadata.Attributes.NAME),
                        "Variable Origin", itemDef.attributeValue(DefineMetadata.Attributes.ORIGIN)
                    ));
                }
            }
        }
    }

    @ValidationRule(id = "DD0045")
    public void missingDomain() {
        if (!isValidStandardName(getDefineStandardName())) {
            return;
        }

        String standardName = getDefineStandardName();
        List<String> allowedStandards = Arrays.asList(this.metadata.getStandards().send(), this.metadata.getStandards().sdtm());
        List<Element> itemGroupDefs = this.xpath.selectElements(X_MDV + "/odm:ItemGroupDef");

        for (Element itemGroup : itemGroupDefs) {
            String domain = itemGroup.attributeValue("Domain");

            if (allowedStandards.contains(standardName) && StringUtils.isBlank(domain)) {
                if (this.metadata != DefineMetadata.V1) {
                    this.transformer.map("DD0045", mapOf(
                        "ItemGroupDef", itemGroup.attributeValue("Name")
                    ));
                }
            } else {
                invalidDomainAttribute(allowedStandards, standardName, itemGroup, domain);
            }
        }
    }

    @SupplementalValidationRule(id = "DD0046")
    private void invalidDomainAttribute(List<String> allowedStandards, String standardName, final Element itemGroup, final String domain) {
        if (!allowedStandards.contains(standardName) && StringUtils.isNotBlank(domain)) {
            if (this.metadata != DefineMetadata.V1) {
                this.transformer.map("DD0046", mapOf(
                    "Domain", domain,
                    "Dataset", itemGroup.attributeValue("Name")
                ));
            }
        }
    }

    @ValidationRule(id = "DD0047")
    public void missingSasDatasetName() {
        if (!isDefine2()) {
            return;
        }

        List<Element> itemGroupDefs = this.xpath.selectElements(X_MDV + "/odm:ItemGroupDef");

        for (Element itemGroup : itemGroupDefs) {
            String sasDatasetName = itemGroup.attributeValue("SASDatasetName");
            String name = itemGroup.attributeValue("Name");

            if (isBlank(sasDatasetName)) {
                this.transformer.map("DD0047", mapOf(
                    "ItemGroupDef", name
                ));
            }
        }
    }

    @ValidationRule(id = "DD0049")
    public void nameDomainSasDataset() {
        List<Element> itemGroupDefs = this.xpath.selectElements(X_MDV + "/odm:ItemGroupDef");
        final String standard = getDefineStandardName();
        for (Element itemGroup : itemGroupDefs) {
            String name = itemGroup.attributeValue("Name");
            String domain = itemGroup.attributeValue("Domain");
            String sasDatasetName = itemGroup.attributeValue("SASDatasetName");

            if (StringUtils.isNotBlank(sasDatasetName)
                    && StringUtils.isNotBlank(domain) 
                    && StringUtils.isNotBlank(name)
                    && !defineIsADaM() 
                    && !new ItemGroup().setName(name).isSplit(standard)
                    && !name.startsWith("SUPP")
                    && (!sasDatasetName.equals(domain) || !domain.equals(name) || !name.equals(sasDatasetName))) {
                this.transformer.map("DD0049", mapOf(
                    "ItemGroupDef", name,
                    "Domain", domain,
                    "SASDatasetName", sasDatasetName,
                    "Name", name
                ));
            }

            //Duplicate ValueListRef check
            List<Element> leaves = itemGroup.elements("leaf");

            if (leaves.size() > 1) {
                for (int i = 1; i < leaves.size(); i++) {
                    Element leaf = leaves.get(i);

                    this.transformer.map("DD0009", mapOf(
                        "ItemGroupDef", itemGroup.attributeValue("OID"),
                        "LeafID", leaf.attributeValue("ID")
                    ));
                }
            }
        }
    }

    @ValidationRule(id = "DD0050")
    public void nameSasDatasetMismatch() {
        if (defineIsADaM()) {
            return;
        }
        List<Element> itemGroupDefs = this.xpath.selectElements(X_MDV + "/odm:ItemGroupDef");
        final String standard = getDefineStandardName();

        for (Element itemGroup : itemGroupDefs) {
            String name = itemGroup.attributeValue("Name");
            String domain = itemGroup.attributeValue("Domain");
            String sasDatasetName = itemGroup.attributeValue("SASDatasetName");

            if (this.metadata != DefineMetadata.V1
                    && new ItemGroup().setName(name).isSplit(standard)
                    && !RuleUtils.validatePrefix(domain, sasDatasetName)) {
                this.transformer.map("DD0050", mapOf(
                    "SASDatasetName", sasDatasetName,
                    "Domain", domain,
                    "ItemGroupDef", name
                ));
            }
        }
    }

    @ValidationRule(id = "DD0051")
    public void duplicateSasDatasetNames() {
        if (!isDefine2()) {
            return;
        }

        List<Element> metaDataVersions = this.xpath.selectElements(X_MDV);

        for (Element metaDataVersion : metaDataVersions) {
            List<String> sasDatasetNames = new ArrayList<>();
            List<Element> itemGroupDefs = metaDataVersion.elements("ItemGroupDef");

            for (Element itemGroup : itemGroupDefs) {
                String sasDatasetName = itemGroup.attributeValue("SASDatasetName");

                if (sasDatasetNames.contains(sasDatasetName)) {
                    this.transformer.map("DD0051", mapOf(
                        "ItemGroupDef", itemGroup.attributeValue("Name"),
                        "SASDatasetName", sasDatasetName
                    ));
                } else {
                    sasDatasetNames.add(sasDatasetName);
                }
            }
        }
    }

    @ValidationRule(id = "DD0052")
    public void sasDatasetArchiveLocation() {
        List<Element> itemGroupDefs = this.xpath.selectElements(X_MDV + "/odm:ItemGroupDef");

        for (Element itemGroup : itemGroupDefs) {
            String sasDatasetName = itemGroup.attributeValue("SASDatasetName");
            Element leaf = itemGroup.element("leaf");
            String archiveLocation = RuleUtils.getBaseHref(leaf);

            if (StringUtils.isNotBlank(sasDatasetName) && StringUtils.isNotBlank(archiveLocation)
                    && !sasDatasetName.equalsIgnoreCase(archiveLocation)) {
                this.transformer.map("DD0052", mapOf(
                    "Dataset", itemGroup.attributeValue("OID"),
                    "SASDatasetName", sasDatasetName,
                    "ArchiveLocationID", leaf.attributeValue("href")
                ));
            }
        }
    }

    @ValidationRule(id = "DD0050, DD0063, DD0064")
    public void missingAlias() {
        List<Element> elements = this.xpath.selectElements(X_MDV + "/odm:ItemGroupDef");
        final String standard = getDefineStandardName();

        for (Element itemGroupDef : elements) {
            final String name = itemGroupDef.attributeValue("Name");
            if (new ItemGroup().setName(name).isSplit(standard)) {
                final Element aliasElement = itemGroupDef.element("Alias");
                if (aliasElement == null) {
                    this.transformer.map("DD0063", mapOf(
                        "Dataset", name
                    ));
                } else {
                    if (!aliasElement.attributeValue("Context").equals("DomainDescription")) {
                        this.transformer.map("DD0064", mapOf(
                            "Dataset", name,
                            "Context", aliasElement.attributeValue("Context")
                        ));
                    }
                }
            }
        }
    }

    @ValidationRule(id = "DD0067")
    public void variableReferences() {
        List<Element> itemDefs = this.xpath.selectElements(X_MDV + "/odm:ItemDef");

        for (Element element : itemDefs) {
            String oid = element.attributeValue("OID");

            if (StringUtils.isNotBlank(oid) && !this.xpath.hasNodes(
                X_MDV + "/*[self::odm:ItemGroupDef or self::def:ValueListDef]/odm:ItemRef[@ItemOID=$id]",
                mapOf("id", oid)
            )) {
                this.transformer.map("DD0067", Collections.singletonList(oid));
            }
        }
    }

    @ValidationRule(id = "DD0070")
    public void missingSasFieldName() {
        if (!isDefine2()) {
            return;
        }

        List<Element> itemDefs = this.xpath.selectElements(X_MDV + "/odm:ItemDef");

        for (Element element : itemDefs) {
            String oid = element.attributeValue("OID");

            if (StringUtils.isBlank(element.attributeValue("SASFieldName"))) {
                this.transformer.map("DD0070", mapOf(
                    "Variable", oid
                ));
            }
        }
    }

    @ValidationRule(id = "DD0075")
    public void defineRuleDD0075() {
        Element crfDocElement = this.xpath.selectSingleElement(X_MDV + "/def:AnnotatedCRF/def:DocumentRef");
        if (crfDocElement == null) {
            return;
        }
        String crfDocOID = crfDocElement.attributeValue("leafID");
        if (isBlank(crfDocOID)) {
            return;
        }
        List<Element> crfDocs = this.xpath.selectElements(X_MDV + "/odm:ItemDef/def:Origin[@Type=\"CRF\"]/def:DocumentRef");
        for (Element crfDoc : crfDocs) {
            String leafID = crfDoc.attributeValue("leafID");
            String itemDefOID = getItemOID(crfDoc);
            if (!crfDocOID.equals(leafID)) {
                this.transformer.map("DD0075", mapOf(
                    "Variable", itemDefOID
                ));
            }
        }
    }

    private static String getItemOID(Element crfDoc) {
        Element origin = crfDoc.getParent();
        if (origin == null || !"def:Origin".equals(origin.getQualifiedName())) {
            return "";
        }
        Element itemDef = origin.getParent();
        if (itemDef == null || !"ItemDef".equals(itemDef.getQualifiedName())) {
            return "";
        }
        return itemDef.attributeValue("OID");
    }

    // Not necessary to be model rule since our system doesn't allow unreferenced valueLevel creation
    @ValidationRule(id = "DD0081")
    public void defineRuleDD0081() {
        Set<String> referencedValueLevelMetadatas = new HashSet<>();
        List<Element> vlmRefs = this.xpath.selectElements(X_MDV + "/odm:ItemDef/def:ValueListRef[@ValueListOID]");

        for (Element vlmRef: vlmRefs) {
            referencedValueLevelMetadatas.add(vlmRef.attributeValue("ValueListOID"));
        }

        List<Element> valueListDefs = this.xpath.selectElements(X_MDV + "/def:ValueListDef");
        List<String> referencedOids = new ArrayList<>();

        for (Element valueListDef : valueListDefs) {
            String valueListOID = valueListDef.attributeValue("OID");

            if (attributeValueSchemaSafe(valueListDef, "OID")) {
                this.transformer.map("DD0003", Arrays.asList("OID", "def:ValueListDef"), mapOf(
                    "Element", "def:ValueListDef"
                ));
            } else if (!referencedOids.contains(valueListOID)
                    && StringUtils.isNotBlank(valueListOID) && !referencedValueLevelMetadatas.contains(valueListOID)) {
                this.transformer.map("DD0081", Collections.singletonList(valueListOID), mapOf(
                    "ValueList", valueListOID
                ));
                referencedOids.add(valueListOID);
            }
        }
    }

    @ValidationRule(id = "DD0076")
    public void defineRuleDD0076() {
        List<Element> codeLists = this.xpath.selectElements(X_MDV + "/odm:CodeList");

        // Unfortunate waste of iterating here. Couldn't move DD0076 to model rule because we don't store rank distinctly
        for (final Element codeList : codeLists) {
            int numItemsWithRank = 0;
            List<Element> items = new ArrayList<>();
            items.addAll(codeList.elements("EnumeratedItem"));
            items.addAll(codeList.elements("CodeListItem"));

            for (final Element term : items) {

                if (StringUtils.isNotBlank(term.attributeValue("Rank"))) {
                    numItemsWithRank++;
                }

                //DEF-155
                Element decode = term.element("Decode");
                if (decode != null && decode.element("TranslatedText") != null) {
                    if (StringUtils.isBlank(decode.elementText("TranslatedText"))) {
                        this.transformer.map("DD0006", Arrays.asList("TranslatedText", "Identifier", codeList.attributeValue("OID")));
                    }
                }
            }

            // If any items have order, all items must have order
            if (numItemsWithRank > 0 && items.size() != numItemsWithRank) {
                this.transformer.map("DD0076", mapOf(
                    "CodeList Name", codeList.attributeValue("Name")
                ));
            }
        }
    }

    @ValidationRule(id = "DD0083")
    public void defineRuleDD0083() {
        Set<String> commentOIDs = new HashSet<>();
        List<Element> comments = this.xpath.selectElements(X_MDV + "/def:CommentDef");

        for (Element comment : comments) {
            String commentOID = comment.attributeValue("OID");

            if (commentOIDs.contains(commentOID)) {
                this.transformer.map("DD0083", mapOf(
                    "Comment", commentOID
                ));
            }

            commentOIDs.add(commentOID);
        }
    }

    @ValidationRule(id = "DD0084")
    public void defineRuleDD0084() {
        List<Element> leaves = new ArrayList<>();

        leaves.addAll(this.xpath.selectElements(X_MDV + "/def:leaf"));
        leaves.addAll(this.xpath.selectElements(X_MDV + "/odm:ItemGroupDef/def:leaf"));

        for (final Element leaf : leaves) {
            if (textElementSchemaSafe(leaf, "title")) {
                this.transformer.map("DD0006", Collections.singletonList("def:title"), mapOf(
                    "Element", "Leaf",
                    "LeafID", leaf.attributeValue("ID")
                ));
            }

            String href = leaf.attributeValue("href");

            if (isNotBlank(href)) {
                // TODO: There's no guarantee this even has to be a file path, right?
                File target = this.resourceManager.getDefineResource(href);

                if (!target.isFile()) {
                    this.transformer.map("DD0084", mapOf(
                        "File", href
                    ));
                }
            }
        }
    }

    @ValidationRule(id = "DD0085")
    public void defineRuleDD0085() {
        List<Node> stylesheets = this.xpath.selectNodes("/processing-instruction('xml-stylesheet')");

        for (Node stylesheet : stylesheets) {
            Matcher matcher = DD0085.get().matcher(stylesheet.getText());

            if (matcher.find()) {
                String href = matcher.group("href");

                // TODO: There's no guarantee this even has to be a file path, right?
                File target = this.resourceManager.getDefineResource(href);

                if (!target.isFile()) {
                    this.transformer.map("DD0085", mapOf(
                            "File", href
                    ));
                }
            }
        }
    }

    @ValidationRule(id = "DD0087")
    public void executeDD0087() {
        Element analysisResult = this.xpath.selectSingleElement(X_MDV + "/arm:AnalysisResultDisplays");

        if (!defineIsADaM() && analysisResult != null) {
            this.transformer.map("DD0087");
        }
    }

    @ValidationRule(id = "DD0088")
    public void executeDD0088() {
        Element analysisResult = this.xpath.selectSingleElement(X_MDV + "/arm:AnalysisResultDisplays");

        if (analysisResult != null) {
            List resultDisplays = analysisResult.selectNodes("arm:ResultDisplay/@OID");
            Set<String> oidSet = new HashSet<>();

            for (Object resultDisplay : resultDisplays) {
                Attribute oidAttr = (Attribute) resultDisplay;
                String oid = oidAttr.getValue();

                if (oidSet.contains(oidAttr.getValue())) {
                    this.transformer.map("DD0088", mapOf(
                        "arm:ResultDisplay OID", oid
                    ));
                }

                oidSet.add(oid);
            }
        }
    }

    @ValidationRule(id = "DD0089")
    public void executeDD0089() {
        Element analysisResult = this.xpath.selectSingleElement(X_MDV + "/arm:AnalysisResultDisplays");

        if (analysisResult != null) {
            List resultDisplays = analysisResult.selectNodes("arm:ResultDisplay/@Name");
            Set<String> nameSet = new HashSet<>();

            for (Object resultDisplay : resultDisplays) {
                Attribute nameAttr = (Attribute) resultDisplay;
                String name = nameAttr.getValue();

                if (nameSet.contains(nameAttr.getValue())) {
                    this.transformer.map("DD0089", mapOf(
                        "arm:ResultDisplay Name", name
                    ));
                }

                nameSet.add(name);
            }
        }
    }

    @ValidationRule(id = "DD0090")
    public void executeDD0090() {
        Element analysisResult = this.xpath.selectSingleElement(X_MDV + "/arm:AnalysisResultDisplays");

        if (analysisResult != null) {
            for (Element resultDisplay : analysisResult.elements("ResultDisplay")) {
                if (resultDisplay.element("Description") == null) {
                    this.transformer.map("DD0090");
                }
            }
        }
    }

    @ValidationRule(id = "DD0091")
    public void executeDD0091() {
        Element analysisResult = this.xpath.selectSingleElement(X_MDV + "/arm:AnalysisResultDisplays");

        if (analysisResult != null) {
            // TODO: Select Attribute objects instead
            List resultDisplays = analysisResult.selectNodes("arm:ResultDisplay/arm:AnalysisResult/@OID");
            Set<String> oidSet = new HashSet<>();

            for (Object resultDisplay : resultDisplays) {
                Attribute oidAttr = (Attribute) resultDisplay;
                String oid = oidAttr.getValue();

                if (oidSet.contains(oidAttr.getValue())) {
                    this.transformer.map("DD0091", mapOf(
                        "arm:AnalysisResult OID", oid
                    ));
                }

                oidSet.add(oid);
            }
        }
    }

    @ValidationRule(id = "DD0092")
    public void executeDD0092() {
        Element analysisResultDisplay = this.xpath.selectSingleElement(X_MDV + "/arm:AnalysisResultDisplays");

        if (analysisResultDisplay != null) {
            // TODO: Select Element objects instead
            List analysisResults = analysisResultDisplay.selectNodes("arm:ResultDisplay/arm:AnalysisResult");

            for (Object analysisObject : analysisResults) {
                Element analysisResult = (Element) analysisObject;

                if (StringUtils.isBlank(analysisResult.attributeValue("ParameterOID"))) {
                    List itemGroupOIDAttrList = analysisResult.selectNodes("arm:AnalysisDatasets/arm:AnalysisDataset/@ItemGroupOID");

                    for (Object object : itemGroupOIDAttrList) {
                        String itemGroupOID = ((Attribute) object).getValue();

                        if (this.xpath.hasNodes(X_MDV + "/odm:ItemGroupDef[@OID=$id and @def:Class='BASIC DATA STRUCTURE']",
                                mapOf("id", itemGroupOID))) {
                            this.transformer.map("DD0092", mapOf(
                                "arm:AnalysisResult OID", analysisResult.attributeValue("OID")
                            ));
                        }
                    }
                }
            }
        }
    }

    @ValidationRule(id = "DD0093")
    public void executeDD0093() {
        Element analysisResultDisplay = this.xpath.selectSingleElement(X_MDV + "/arm:AnalysisResultDisplays");

        if (analysisResultDisplay != null) {
            // TODO: Select Element objects instead
            List analysisResults = analysisResultDisplay.selectNodes("arm:ResultDisplay/arm:AnalysisResult");

            for (Object analysisObject : analysisResults) {
                Element analysisResult = (Element) analysisObject;
                String parameterOID = analysisResult.attributeValue("ParameterOID");

                if (StringUtils.isNotBlank(parameterOID)) {
                    // TODO: Select Attribute objects instead
                    List<Node> itemGroupOIDAttrList = analysisResult.selectNodes("arm:AnalysisDatasets/arm:AnalysisDataset/@ItemGroupOID");
                    boolean rulePassed = false;

                    for (Object itemGroupOIDAttr : itemGroupOIDAttrList) {
                        String itemGroupOID = ((Attribute) itemGroupOIDAttr).getValue();

                        Node paramcdFromBDSDataset = this.xpath.selectSingleNode(
                            X_MDV + "/odm:ItemGroupDef[@OID=$groupId and @def:Class='BASIC DATA STRUCTURE']/odm:ItemRef[@ItemOID=$refId]",
                            mapOf(
                                "groupId", itemGroupOID,
                                "refId", parameterOID
                            )
                        );

                        if (paramcdFromBDSDataset != null) {
                            Element itemDef = this.xpath.selectSingleElement(X_MDV + "/odm:ItemDef[@OID=$id]",
                                    mapOf("id", parameterOID));

                            if (itemDef != null && "PARAMCD".equals(itemDef.attributeValue("Name"))) {
                                rulePassed = true;
                                break;
                            }
                        }
                    }

                    if (!rulePassed) {
                        this.transformer.map("DD0093", mapOf(
                            "arm:AnalysisResult OID", analysisResult.attributeValue("OID")
                        ));
                    }
                }
            }
        }
    }

    @ValidationRule(id = "DD0094")
    public void executeDD0094() {
        List<Element> analysisResults = this.xpath.selectElements(X_MDV + X_ARM_RES);

        for (Element analysisResult : analysisResults) {
            Element analysisDatasetsElement = analysisResult.element("AnalysisDatasets");
            List<Node> analysisDatasetList = analysisResult.selectNodes("arm:AnalysisDatasets/arm:AnalysisDataset");

            if (analysisDatasetList.size() > 1 && analysisDatasetsElement != null
                    && StringUtils.isBlank(analysisDatasetsElement.attributeValue("CommentOID"))) {
                this.transformer.map("DD0094", mapOf(
                    "arm:AnalysisResult OID", analysisResult.attributeValue("OID")
                ));
            }
        }
    }

    @ValidationRule(id = "DD0095")
    public void executeDD0095() {
        List<Element> analysisDatasetsList = this.xpath.selectElements(
            X_MDV + X_ARM_RES + "/arm:AnalysisDatasets/arm:AnalysisDataset[@ItemGroupOID]"
        );

        for (Element analysisDataset : analysisDatasetsList) {
            String itemGroupOID = analysisDataset.attributeValue("ItemGroupOID");

            if (!this.xpath.hasNodes(X_MDV + "/odm:ItemGroupDef[@OID=$id]",
                    mapOf("id", itemGroupOID))) {
                this.transformer.map("DD0095", mapOf(
                    "ItemGroupOID", itemGroupOID
                ));
            }
        }
    }

    @ValidationRule(id = "DD0096")
    public void executeDD0096() {
        List<Element> analysisResultList = this.xpath.selectElements(X_MDV + X_ARM_RES);

        for (Element analysisResult : analysisResultList) {
            Set<String> variableOidSet = new HashSet<>();

            if (!analysisResult.selectNodes("arm:AnalysisDatasets/arm:AnalysisDataset").isEmpty()) {
                List<Node> analysisVariables = analysisResult.selectNodes(
                    "arm:AnalysisDatasets/arm:AnalysisDataset/arm:AnalysisVariable"
                );

                if (analysisVariables.isEmpty()) {
                    this.transformer.map("DD0096", mapOf(
                        "arm:AnalysisResult OID", analysisResult.attributeValue("OID")
                    ));
                }

                for (Object varObj : analysisVariables) {
                    String itemOID = ((Element) varObj).attributeValue("ItemOID");

                    if (variableOidSet.contains(itemOID)) {
                        this.transformer.map("DD0096", mapOf(
                            "arm:AnalysisResult OID", analysisResult.attributeValue("OID")
                        ));
                    }

                    variableOidSet.add(itemOID);
                }
            }
        }
    }

    @ValidationRule(id = "DD0097")
    public void executeDD0097() {
        List<Element> analysisVariableList = this.xpath.selectElements(
            X_MDV + X_ARM_RES + "/arm:AnalysisDatasets/arm:AnalysisDataset/arm:AnalysisVariable[@ItemOID]"
        );

        for (Element analysisVariable : analysisVariableList) {
            String itemOID = analysisVariable.attributeValue("ItemOID");

            if (!this.xpath.hasNodes(X_MDV + "/odm:ItemDef[@OID=$id]", mapOf("id", itemOID))) {
                this.transformer.map("DD0097", mapOf(
                    "ItemOID", itemOID
                ));
            }
        }
    }

    @ValidationRule(id = "DD0098")
    public void executeDD0098() {
        List<Element> programmingCodeList = this.xpath.selectElements(
            X_MDV + X_ARM_RES + "/arm:ProgrammingCode[arm:Code]"
        );

        for (Element programmingCode : programmingCodeList) {
            String context = programmingCode.attributeValue("Context");
            Element analysisResult = (Element) programmingCode.selectSingleNode("..");

            if (StringUtils.isBlank(context)) {
                this.transformer.map("DD0098", mapOf(
                    "arm:AnalysisResult OID", analysisResult.attributeValue("OID")
                ));
            }
        }
    }

    @ValidationRule(id = "DD0099")
    public void executeDD0099() {
        List<Element> analysisReasonList = this.xpath.selectElements(X_MDV + X_ARM_RES  + "[@AnalysisReason]");

        for (Element analysisElement : analysisReasonList) {
            String analysisReason = analysisElement.attributeValue("AnalysisReason");

            if (!this.terminology.hasNamedCodeListWithCodedValue("Analysis Reason", analysisReason)) {
                this.transformer.map("DD0099", mapOf(
                    "AnalysisReason", analysisReason
                ));
            }
        }
    }

    @ValidationRule(id = "DD0100")
    public void executeDD0100() {
        List<Element> analysisReasonList = this.xpath.selectElements(X_MDV + X_ARM_RES + "[@AnalysisPurpose]");

        for (Element analysisElement : analysisReasonList) {
            String analysisPurpose = analysisElement.attributeValue("AnalysisPurpose");

            if (!this.terminology.hasNamedCodeListWithCodedValue("Analysis Purpose", analysisPurpose)) {
                this.transformer.map("DD0100", mapOf(
                    "AnalysisPurpose", analysisPurpose
                ));
            }
        }
    }

    @ValidationRule(id = "DD0103")
    public void executeDD0103() {
        if (defineIsADaM() || defineIsSEND()) {
            return;
        }
        boolean referencesCRF = false;

        List<Element> itemDefs = this.xpath.selectElements(X_MDV + "/odm:ItemDef");

        for (Element item : itemDefs) {
            Element origin = item.element("Origin");

            if (origin != null && DefineMetadata.Origins.CRF.equals(origin.attributeValue("Type"))
                    && (origin.element("DocumentRef") != null)) {
                Element pdfPageRef = origin.element("DocumentRef").element("PDFPageRef");

                if (pdfPageRef != null && (StringUtils.isNotBlank(pdfPageRef.attributeValue("PageRefs"))
                        || (StringUtils.isNotBlank(pdfPageRef.attributeValue("FirstPage"))
                        && (StringUtils.isNotBlank(pdfPageRef.attributeValue("LastPage")))))) {

                    referencesCRF = true;
                    break;
                }
            }
        }

        if (referencesCRF) {
            Element acrf = this.xpath.selectSingleElement(X_MDV + "/def:AnnotatedCRF/def:DocumentRef");
            String crfOID = StringUtils.EMPTY;

            if (acrf != null) {
                crfOID = acrf.attributeValue("leafID");
            }

            Element leaf = this.xpath.selectSingleElement(X_MDV + "/def:leaf[@ID=$id]",
                    mapOf("id", crfOID));

            if (leaf != null && !Arrays.asList("acrf.pdf", "blankcrf.pdf").contains(leaf.attributeValue("href"))) {
                this.transformer.map("DD0103");
            }
        }
    }

    // TODO: Does not handle blank lines
    @ValidationRule(id = "OD0010")
    public void defineRuleOD0010() {
        String firstLine = this.resourceManager.getLineReader().getLine(1);

        if (firstLine == null) {
            LOGGER.warn("Define Validation Rules OD0010/OD0011 were unable to be run because of failure to read first line of define");

            return;
        }

        if (!firstLine.startsWith("<?xml ")) {
            this.transformer.map("OD0010");
        }

        defineRuleOD0011(firstLine);
    }

    @SupplementalValidationRule(id = "OD0011")
    public void defineRuleOD0011(String xmlDeclaration) {
        Set<String> validEncodings = new HashSet<>(Arrays.asList("UTF-8", "UTF-16", "ISO-8859-1"));
        Matcher matcher = OD0011.get().matcher(xmlDeclaration);

        if (matcher.find()) {
            // Parse the encoding version as a string
            String encoding = matcher.group("encoding");

            if (!validEncodings.contains(encoding)) {
                this.transformer.map("OD0011");
            }
        }
    }

    // TODO: DD0002 is a fatal check so this doesn't get reported
    @ValidationRule(id = "OD0012")
    public void defineRuleOD0012() {
        List odmRootNodes = this.xpath.selectNodes("/ODM");

        if (odmRootNodes.isEmpty()) {
            this.transformer.map("OD0012");
            this.isFatal.mark();
        }
    }

    /**
     * Since I couldn't find a reliable list of language codes anywhere for use in define.xml,
     * I am just using the Java ISO languages.
     */
    @ValidationRule(id = "OD0021")
    public void validateTranslatedTextLang() {
        String[] languages = null;
        List<Element> translatedTexts = this.xpath.selectElements(
            X_MDV + "/*[self::odm:ItemGroupDef or self::odm:ItemDef or self::odm:MethodDef or self::def:CommentDef]/odm:Description/odm:TranslatedText" +
            " | " + X_MDV + "/odm:CodeList/odm:CodeListItem/odm:Decode/odm:TranslatedText"
        );

        for (Element element : translatedTexts) {
            String attribute = element.attributeValue("lang");

            if (StringUtils.isNotBlank(attribute)) {
                if (languages == null) {
                    languages = Locale.getISOLanguages(); //If the case where the language code is valid but not in this method arises, we can deal with it then.
                    Arrays.sort(languages); //Sort for safety...(it looks like the array comes pre sorted but I'm not sure if we can guarantee that).
                }

                int result = Arrays.binarySearch(languages, attribute);
                if (result < 0) {
                    final Element parent = element.getParent().getParent();

                    if (isDefine2()) {
                        this.transformer.map("OD0021", mapOf(
                            "OID", StringUtils.isNotBlank(parent.attributeValue("OID"))
                                    ? parent.attributeValue("OID")
                                    : parent.getParent().attributeValue("CodedValue"), //Assumed from the XPath that the parent identifier is two elements up, or in one case 3
                            "Lang", attribute
                        ));
                    } else {
                        //In define 1.0 the only thing with translated text is a CodeListItem
                        this.transformer.map("OD0021", mapOf(
                            "CodeList", parent.getParent().attributeValue("OID"),
                            "CodedValue", parent.attributeValue("CodedValue"),
                            "Lang", attribute
                        ));
                    }
                }
            }
        }
    }

    @ValidationRule(id = "OD0022")
    public void defineRuleOD0022() {
        // TODO: Alot of other checks assume only one study element...so either this check is useless or all others will not validate whole doc???
        Set<String> studyOIDs = new HashSet<>();
        List<Element> studies = this.xpath.selectElements("/ODM/odm:Study");

        for (Element study : studies) {
            final String studyOID = study.attributeValue("OID");

            if (studyOIDs.contains(studyOID)) {
                this.transformer.map("OD0022", mapOf(
                    "Study", studyOID
                ));
            }

            studyOIDs.add(studyOID);
        }
    }

    @ValidationRule(id = "OD0027")
    public void defineRuleOD0027() {
        // TODO: Will there ever be more than one? If so it could break some other rules. Example: If multiple metaDataVersions, the unique itemGroupDef check will break
        Set<ImmutablePair<Node, String>> metaDataVersionOIDs = new HashSet<>();
        List<Element> metaDataVersions = this.xpath.selectElements(X_MDV);

        for (Element metaDataVersion : metaDataVersions) {
            final String metaDataVersionOID = metaDataVersion.attributeValue("OID");
            //Metadataversion elements oid are unique per study element.
            ImmutablePair<Node, String> pair = new ImmutablePair<>(metaDataVersion.getParent(), metaDataVersionOID);

            if (metaDataVersionOIDs.contains(pair)) {
                this.transformer.map("OD0027", mapOf(
                    "MetaDataVersion", metaDataVersionOID
                ));
            }

            metaDataVersionOIDs.add(pair);
        }
    }

    // TODO: Replace with model
    @ValidationRule(id = "OD0030")
    public void defineRuleOD0030() {
        List<Element> metaDataVersions = this.xpath.selectElements(X_MDV);

        for (Element metaDataVersion : metaDataVersions) {
            Set<String> itemGroupOIDs = new HashSet<>();
            List<Element> itemGroupDefs = metaDataVersion.elements("ItemGroupDef");

            for (Element itemGroupDef : itemGroupDefs) {
                final String itemGroupOID = itemGroupDef.attributeValue("OID");

                if (itemGroupOIDs.contains(itemGroupOID)) {
                    this.transformer.map("OD0030", mapOf(
                        "Dataset", itemGroupOID
                    ));
                }

                itemGroupOIDs.add(itemGroupOID);

                defineRuleOD0041(itemGroupDef);
            }
        }
    }

    // TODO: Replace with model
    @ValidationRule(id = "OD0031")
    public void defineRuleOD0031() {
        // TODO: If we are checking every single ItemDef here, would be better to do some of the other checks here
        List<Element> metaDataVersions =this.xpath.selectElements(X_MDV);

        for (Element metaDataVersion : metaDataVersions) {
            Set<String> itemDefOIDs = new HashSet<>();
            List<Element> itemDefs = metaDataVersion.elements("ItemDef");

            for (Element itemDef : itemDefs) {
                final String itemDefOID = itemDef.attributeValue("OID");

                if (itemDefOIDs.contains(itemDefOID)) {
                    this.transformer.map("OD0031", mapOf(
                        "Variable", itemDefOID
                    ));
                }

                itemDefOIDs.add(itemDefOID);
            }
        }
    }

    @ValidationRule(id = "OD0032")
    public void defineRuleOD0032() {
        // TODO: If we are checking every single codelist here, would be better to do some of the other checks here
        List<Element> metaDataVersions = this.xpath.selectElements(X_MDV);

        for (Element metaDataVersion : metaDataVersions) {
            List<String> codeListOIDs = new ArrayList<>();
            List<Element> codeLists = metaDataVersion.elements("CodeList");

            for (Element codeList : codeLists) {
                invalidSASFormatName(codeList);
                String codeListOID = codeList.attributeValue("OID");

                if (codeListOIDs.contains(codeListOID)) {
                    this.transformer.map("OD0032", mapOf(
                        "Codelist", codeListOID
                    ));
                }

                codeListOIDs.add(codeListOID);

                // Check for duplicate codeListTerms
                List<Element> codeListTerms = new ArrayList<>();
                codeListTerms.addAll(codeList.elements("EnumeratedItem"));
                codeListTerms.addAll(codeList.elements("CodeListItem"));

                defineRuleOD0079(codeListOID, codeListTerms);
            }
        }
    }

    @SupplementalValidationRule(id = "OD0041")
    public void defineRuleOD0041(Element elementWithItemRefs) {
        Set<String> itemOIDs = new HashSet<>();

        for (Element itemRef : elementWithItemRefs.elements("ItemRef")) {
            String itemOID = itemRef.attributeValue("ItemOID");

            if (itemOIDs.contains(itemOID)) {
                this.transformer.map("OD0041", mapOf(
                    "Variables", itemRef.attributeValue("ItemOID")
                ));
            }

            itemOIDs.add(itemOID);
        }
    }

    @ValidationRule(id = "OD0046")
    public void defineRuleOD0046() {
        List<Element> metaDataVersions = this.xpath.selectElements("/ODM/odm:Study/odm:MetaDataVersion");

        for (Element metaDataVersion : metaDataVersions) {
            List<Element> itemRefs = this.xpath.selectElements(metaDataVersion,
                "*[self::odm:ItemGroupDef or self::def:ValueListDef]/odm:ItemRef | def:WhereClauseDef/odm:RangeCheck"); //RangeCheck has an ItemOID attribute

            for (final Element itemRef : itemRefs) {
                String itemOID = itemRef.attributeValue("ItemOID");

                if (StringUtils.isNotBlank(itemOID) && !this.xpath.hasNodes(metaDataVersion,"odm:ItemDef[@OID=$id]",
                        mapOf("id", itemOID))) {
                    this.transformer.map("OD0046", mapOf(
                        "Referencing Element", itemRef.getParent().getName(),
                        "Referencing Element OID", itemRef.getParent().attributeValue("OID"),
                        "Variable", itemOID
                    ));
                }
            }
        }
    }

    @SupplementalValidationRule(id = "OD0078")
    public void invalidSASFormatName(final Element codeList) {
        final String sasFormatName = "SASFormatName";
        if (!StringUtils.defaultString(codeList.attributeValue("DataType")).equals("text")
                || StringUtils.isBlank(codeList.attributeValue(sasFormatName))) {
            return;
        }

        if (!codeList.attributeValue(sasFormatName).startsWith("$")) {
            this.transformer.map("OD0078", mapOf(
                "CodeList Name", codeList.attributeValue("Name"),
                sasFormatName, codeList.attributeValue(sasFormatName)
            ));
        }
    }

    @SupplementalValidationRule(id = "OD0079")
    public void defineRuleOD0079(String codeListOID, List<Element> codeListTerms) {
        Set<String> cachedCodedValues = new HashSet<>();

        for (Element codeListTerm : codeListTerms) {
            String codedValue = codeListTerm.attributeValue("CodedValue");

            if (cachedCodedValues.contains(codedValue)) {
                this.transformer.map("OD0079", Arrays.asList(codeListOID, "CodeList", codedValue));
            }

            cachedCodedValues.add(codedValue);
        }
    }

    private boolean attributeValueSchemaSafe(final Element element, String attribute) {
        return (StringUtils.isBlank(element.attributeValue(attribute)) && element.attribute(attribute) != null);
    }

    private boolean textElementSchemaSafe(final Element element, String textElement) {
        return (element.element(textElement) != null && StringUtils.isBlank(element.elementText(textElement)));
    }

    private boolean isValidStandardName(String standardName) {
        return StringUtils.isNotBlank(standardName) && this.metadata.getStandards().standardToVersions().containsKey(standardName);
    }

    private boolean isValidStandardVersion(String standardName, String standardVersion) {
        return this.metadata.getStandards().standardToVersions().containsKey(standardName)
                && this.metadata.getStandards().standardToVersions().get(standardName).contains(standardVersion);
    }

    private Element itemGroupFor(Element itemDef) {
        return this.xpath.selectSingleElement(X_MDV + "/odm:ItemGroupDef[odm:ItemRef[@ItemOID=$id]]",
                mapOf("id", itemDef.attributeValue(DefineMetadata.Attributes.OID)));
    }

    private boolean defineIsADaM() {
        return this.metadata.getStandards().adam().equals(getDefineStandardName());
    }

    private boolean defineIsSEND() {
        return this.metadata.getStandards().send().equals(getDefineStandardName());
    }

    private String getDefineVersion() {
        Element element = this.xpath.selectSingleElement(X_MDV);

        return element != null ? StringUtils.defaultString(element.attributeValue("DefineVersion")) : StringUtils.EMPTY;
    }

    private String getDefineStandardName() {
        Element element = this.xpath.selectSingleElement(X_MDV);

        return element != null ? StringUtils.defaultString(element.attributeValue("StandardName")) : StringUtils.EMPTY;
    }

    private boolean isDefine2() {
        return this.metadata == DefineMetadata.V2;
    }

    private static String cleanAttribute(String value) {
        if (value == null) {
            return "";
        }

        return value.replaceAll("[\r\n\t]", "").trim();
    }
}
