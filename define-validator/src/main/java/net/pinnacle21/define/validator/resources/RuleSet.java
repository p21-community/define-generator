/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.resources;

import net.pinnacle21.parsing.xml.XmlUtils;
import net.pinnacle21.validator.api.events.RuleListener;
import net.pinnacle21.validator.api.events.util.Dispatcher;
import net.pinnacle21.validator.api.model.ConfigOptions;
import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.api.model.RuleInstance;
import net.pinnacle21.validator.api.model.SourceDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import static net.pinnacle21.parsing.xml.XmlUtils.each;
import static net.pinnacle21.parsing.xml.XmlUtils.only;

public class RuleSet {
    private static final String CONFIG_NAMESPACE_URI = "http://www.opencdisc.org/schema/validator";
    private static final Logger LOGGER = LoggerFactory.getLogger(RuleSet.class);
    private static final Dispatcher DISPATCHER = new Dispatcher(c -> LoggerFactory.getLogger(c)::error);

    private final Map<String, Rule> rules;

    private RuleSet(Map<String, Rule> rules) {
        this.rules = rules;
    }

    public boolean has(String id) {
        return this.rules.containsKey(id);
    }

    public Rule get(String id) {
        return this.rules.get(id);
    }

    public RuleInstance toInstance(Diagnostic diagnostic) {
        Rule rule = this.get(diagnostic.getId());

        return new RuleInstance() {
            @Override
            public String getDomain() {
                return diagnostic.getSourceDetails().getString(SourceDetails.Property.Name);
            }

            @Override
            public String getContext() {
                return diagnostic.getContext();
            }

            @Override
            public String getCategory() {
                return rule.getCategory();
            }

            @Override
            public String getId() {
                return rule.getId();
            }

            @Override
            public String getDescription() {
                return rule.getDescription();
            }

            @Override
            public String getMessage() {
                return diagnostic.getMessage();
            }

            @Override
            public String getPublisherId() {
                return rule.getPublisherId();
            }

            @Override
            public Diagnostic.Type getType() {
                return rule.getType();
            }
        };
    }

    public static RuleSet parseConfig(ConfigOptions config, Set<RuleListener> ruleListeners) {
        Map<String, Rule> rules = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        File source = new File(config.getConfigs()
                .iterator()
                .next());

        try (
            InputStream is = new FileInputStream(source);
            InputStream bs = new BufferedInputStream(is)
        ) {
            Document document = XmlUtils.newSafeDocumentBuilder().parse(bs);
            Element metadata = only("MetaDataVersion", document);
            Element ruleset = only(CONFIG_NAMESPACE_URI, "ValidationRules", metadata);

            if (ruleset == null) {
                throw new RuntimeException(String.format("Unable to find ValidationRules element in config file %s",
                        source.getAbsolutePath()));
            }

            for (Element element : each(ruleset.getChildNodes())) {
                String id = element.getAttribute("ID");
                Rule rule = new Rule(
                    id,
                    element.getAttribute("Category"),
                    element.getAttribute("Message"),
                    element.getAttribute("Description"),
                    Diagnostic.Type.fromString(element.getAttribute("Type")),
                    element.hasAttribute("PublisherID") ? element.getAttribute("PublisherID") : null
                );

                rules.put(id, rule);

                DISPATCHER.dispatchTo(ruleListeners, l -> l::acceptTemplate, rule);
            }
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            LOGGER.error("Unable to parse rules from config file {} due to exception", source, ex);

            throw new RuntimeException("Failed to parse rules", ex);
        }

        return new RuleSet(rules);
    }
}
