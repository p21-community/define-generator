/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import net.pinnacle21.define.models.CodeList;
import net.pinnacle21.define.models.CodeListItem;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.NamedRow;
import net.pinnacle21.parsing.excel.ValidationAwareExcelSheetParser;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

public class CodeListSheetParser extends ValidationAwareExcelSheetParser<ParsedStudy> {
    static final String ID_COLUMN = "ID";
    static final String NAME_COLUMN = "Name";
    static final String NCI_CODELIST_CODE_COLUMN = "NCI Codelist Code";
    static final String DATA_TYPE_COLUMN = "Data Type";
    static final String ORDER_COLUMN = "Order";
    static final String TERM_COLUMN = "Term";
    static final String NCI_TERM_CODE_COLUMN = "NCI Term Code";
    static final String DECODED_VALUE_COLUMN = "Decoded Value";
    static final String MISSING_TERM_ERROR = "%s is missing even though other columns are populated";
    static final String NONNUMERIC_TERM_ERROR = "Code list item '%s' specifies a non-numeric %s value, %s";
    static final String DUPLICATE_TERM_ERROR = "The term '%s' has already been defined for the code list '%s', duplicates "
            + "are not allowed";
    static final String DUPLICATE_TERM_WITH_NON_ASCII_CHAR_ERROR = "The term '%s' has already been defined for the code list '%s', duplicates "
            + "are not allowed. At least one of these duplicates may also contain a hidden non-ASCII character`.";
    
    private final Pattern nonAscii = Pattern.compile("[^\\x00-\\x7f]");
    
    CodeListSheetParser() {
        this.addValidatedRequiredColumn(ID_COLUMN);
        this.addValidatedColumn(NAME_COLUMN);
        this.addValidatedColumn(NCI_CODELIST_CODE_COLUMN);
        this.addValidatedColumn(DATA_TYPE_COLUMN);
        this.addValidatedColumn(ORDER_COLUMN);
        this.addValidatedColumn(TERM_COLUMN, FieldValidations.REALLY_LONG_LENGTH);
        this.addValidatedColumn(NCI_TERM_CODE_COLUMN);
        this.addValidatedColumn(DECODED_VALUE_COLUMN, FieldValidations.REALLY_LONG_LENGTH);
    }

    @Override
    public void parse(NamedRow row, ParsedStudy parsedStudy) {
        if (!this.validate(row, parsedStudy)) {
            return;
        }
        Study study = parsedStudy.getStudy();

        String id = StringUtils.trim(row.get(ID_COLUMN));
        String codedValue = StringUtils.trim(row.get(TERM_COLUMN));

        CodeList codeList = study.getCodeList(id);
        if (codeList == null) {
            codeList = new CodeList();
            codeList.setOid(id);
            codeList.setName(row.get(NAME_COLUMN));
            codeList.setCode(row.get(NCI_CODELIST_CODE_COLUMN));
            codeList.setDataType(row.get(DATA_TYPE_COLUMN));
            study.setCodeList(id, codeList);
        }

        String term = row.get(TERM_COLUMN),
               decode = row.get(DECODED_VALUE_COLUMN),
               code = row.get(NCI_TERM_CODE_COLUMN),
               order = row.get(ORDER_COLUMN);

        boolean hasOtherValue = Iterables.any(Arrays.asList(decode, code, order),
                new Predicate<String>() {
                    @Override
                    public boolean apply(String value) {
                        return StringUtils.isNotBlank(value);
                    }
                });

        if (StringUtils.isBlank(term)) {
            if (hasOtherValue) {
                parsedStudy.error(row.getIdentifier(), MISSING_TERM_ERROR, TERM_COLUMN);
            }

            return;
        }

        if (StringUtils.isNotBlank(order) && !StringUtils.isNumeric(order)) {
            parsedStudy.error(row.getIdentifier(), NONNUMERIC_TERM_ERROR, term, ORDER_COLUMN, order);

            return;
        }
    
        Optional.ofNullable(codeList.getCodelistItems()).ifPresent(existingCodelistItems ->
            checkForDuplicateTerms(parsedStudy, row, existingCodelistItems, id, codedValue)
        );
        
        CodeListItem codeListItem = new CodeListItem();
        codeListItem.setOrder(row.get(ORDER_COLUMN));
        codeListItem.setCodedValue(codedValue);
        codeListItem.setCode(row.get(NCI_TERM_CODE_COLUMN));
        codeListItem.setDecodedValue(row.get(DECODED_VALUE_COLUMN));
        codeListItem.setLanguage(study.getLanguage());

        codeList.put(codeListItem);
        study.setCodeListItem(id, codedValue, codeListItem);
    }
    
    private void checkForDuplicateTerms(ParsedStudy parsedStudy, NamedRow row, List<CodeListItem> existingCodelistItems, String id, String codedValue) {
        existingCodelistItems.forEach(existingCodelistItem -> {
            if (removeNonAsciiChars(existingCodelistItem.getCodedValue()).equals(removeNonAsciiChars(codedValue))) {
                if (containsNonAsciiChars(existingCodelistItem.getCodedValue()) || containsNonAsciiChars(codedValue)) {
                    parsedStudy.error(row.getIdentifier(), DUPLICATE_TERM_WITH_NON_ASCII_CHAR_ERROR, codedValue, id);
                } else {
                    parsedStudy.error(row.getIdentifier(), DUPLICATE_TERM_ERROR, codedValue, id);
                }
            }
        });
    }
    
    private String removeNonAsciiChars(String value) {
        return nonAscii.matcher(value).replaceAll("");
    }
    
    private Boolean containsNonAsciiChars(String value) {
        return nonAscii.matcher(value).find();
    }
}
