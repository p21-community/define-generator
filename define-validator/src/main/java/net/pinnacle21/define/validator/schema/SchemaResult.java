/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.schema;

import org.dom4j.io.DOMReader;
import org.w3c.dom.Document;

public class SchemaResult {
    private final boolean isSuccessful;
    private final Document document;

    SchemaResult(boolean isSuccessful, Document document) {
        this.isSuccessful = isSuccessful;
        this.document = document;
    }

    public boolean isSuccessful() {
        return isSuccessful;
    }

    public org.dom4j.Document getDocument() {
        return new DOMReader().read(this.document);
    }
}
