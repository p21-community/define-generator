/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class CodeList extends ExcelRecord {
    private String oid;
    private String name;
    private String dataType;
    private String code;
    private String dictionary;
    private String version;
    private boolean isExtensible;
    private HashMap<String, CodeListItem> codelistItems = new LinkedHashMap<>();
    private boolean external;

    public void put(CodeListItem item) { this.codelistItems.put(item.getCodedValue(), item); }

    void addAll(List<CodeListItem> items) {
        for (CodeListItem item : items) {
            this.codelistItems.put(item.getCodedValue(), item);
        }
    }

    public List<CodeListItem> getCodelistItems() {
        return new ArrayList<>(this.codelistItems.values());
    }

    public CodeListItem getItem(String codedValue) {
        return this.codelistItems.get(codedValue);
    }

    public String getOid() {
        return oid;
    }

    public CodeList setOid(String oid) {
        this.oid = oid != null ? oid.trim() : null;
        return this;
    }

    public String getName() {
        return name;
    }

    public CodeList setName(String name) {
        this.name = name != null ? name.trim() : null;
        return this;
    }

    public String getDataType() {
        return dataType;
    }

    public CodeList setDataType(String dataType) {
        this.dataType = dataType != null ? dataType.trim() : null;
        return this;
    }

    public String getCode() {
        return code;
    }

    public CodeList setCode(String code) {
        this.code = code != null ? code.trim() : null;
        return this;
    }

    public String getDictionary() {
        return dictionary;
    }

    public CodeList setDictionary(String dictionary) {
        this.dictionary = dictionary != null ? dictionary.trim() : null;
        return this;
    }

    public String getVersion() {
        return version;
    }

    public CodeList setVersion(String version) {
        this.version = version != null ? version.trim() : null;
        return this;
    }

    public boolean isExtensible() {
        return isExtensible;
    }

    public CodeList setIsExtensible(String isExtensible) {
        this.isExtensible = isExtensible != null
                && (isExtensible.equalsIgnoreCase("Yes") || isExtensible.equalsIgnoreCase("True"));
        return this;
    }

    public CodeList setIsExtensible(boolean isExtensible) {
        this.isExtensible = isExtensible;
        return this;
    }

    public boolean isExternal() {
        return external;
    }

    public CodeList setIsExternal(boolean external) {
        this.external = external;
        return this;
    }
}
