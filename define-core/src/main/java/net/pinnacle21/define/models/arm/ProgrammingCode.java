/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models.arm;

import net.pinnacle21.define.models.DocumentReference;

public class ProgrammingCode {
    private String context;
    private String code;
    private DocumentReference documentReference;

    public String getContext() {
        return context;
    }

    public ProgrammingCode setContext(String context) {
        this.context = context;
        return this;
    }

    public String getCode() {
        return code;
    }

    public ProgrammingCode setCode(String code) {
        this.code = code;
        return this;
    }

    public DocumentReference getDocumentReference() {
        return documentReference;
    }

    public ProgrammingCode setDocumentReference(DocumentReference documentReference) {
        this.documentReference = documentReference;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProgrammingCode code1 = (ProgrammingCode) o;

        if (getContext() != null ? !getContext().equals(code1.getContext()) : code1.getContext() != null) return false;
        if (getCode() != null ? !getCode().equals(code1.getCode()) : code1.getCode() != null) return false;
        return getDocumentReference() != null ? getDocumentReference().equals(code1.getDocumentReference()) : code1.getDocumentReference() == null;

    }
}
