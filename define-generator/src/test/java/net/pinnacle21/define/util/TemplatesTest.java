/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.util;

import org.junit.Test;
import net.pinnacle21.define.models.RangeCheck;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TemplatesTest {
    @Test
    public void testWhereClauseOIDGenerator() {
        String itemGroup = "ItemGroup";
        String item = "Item";
        String comparator = "Comparator";
        String value = "Value";
        RangeCheck rangeCheck = new RangeCheck();
        rangeCheck.setItemGroupOid(itemGroup);
        rangeCheck.setItemOid(item);
        rangeCheck.setComparator(comparator);
        rangeCheck.addValue(value);

        String firstWhereClauseOid = Templates.generateWhereClauseOid(Collections.singletonList(rangeCheck));
        rangeCheck.addValue("Value2");
        String secondWhereClauseOid = Templates.generateWhereClauseOid(Collections.singletonList(rangeCheck));

        assertNotEquals(firstWhereClauseOid, secondWhereClauseOid);
    }

    @Test
    public void testSimpleUserFriendlyWhereClause() {
        String expectedValue = "ItemGroup.Item.EQ.Value";

        String itemGroup = "ItemGroup";
        String item = "Item";
        String comparator = "EQ";
        String value = "Value";
        RangeCheck rangeCheck = new RangeCheck();
        rangeCheck.setItemGroupOid(itemGroup);
        rangeCheck.setItemOid(item);
        rangeCheck.setComparator(comparator);
        rangeCheck.addValue(value);

        String userFriendlyWhereClause = Templates.generateWhereClauseString(Collections.singletonList(rangeCheck));

        assertEquals(expectedValue, userFriendlyWhereClause);
    }

    @Test
    public void testMultiConditionUserFriendlyWhereClause() {
        String expectedValue = "ItemGroup1.Item1.EQ.Value1 AND ItemGroup2.Item2.LE.Value2 AND ItemGroup3.Item3.GE.Value3";

        String itemGroup1 = "ItemGroup1";
        String item1 = "Item1";
        String comparator1 = "EQ";
        String value1 = "Value1";
        RangeCheck rangeCheck1 = new RangeCheck();
        rangeCheck1.setItemGroupOid(itemGroup1);
        rangeCheck1.setItemOid(item1);
        rangeCheck1.setComparator(comparator1);
        rangeCheck1.addValue(value1);

        String itemGroup2 = "ItemGroup2";
        String item2 = "Item2";
        String comparator2 = "LE";
        String value2 = "Value2";
        RangeCheck rangeCheck2 = new RangeCheck();
        rangeCheck2.setItemGroupOid(itemGroup2);
        rangeCheck2.setItemOid(item2);
        rangeCheck2.setComparator(comparator2);
        rangeCheck2.addValue(value2);

        String itemGroup3 = "ItemGroup3";
        String item3 = "Item3";
        String comparator3 = "GE";
        String value3 = "Value3";
        RangeCheck rangeCheck3 = new RangeCheck();
        rangeCheck3.setItemGroupOid(itemGroup3);
        rangeCheck3.setItemOid(item3);
        rangeCheck3.setComparator(comparator3);
        rangeCheck3.addValue(value3);

        List<RangeCheck> rangeCheckList = new ArrayList<RangeCheck>();
        rangeCheckList.add(rangeCheck1);
        rangeCheckList.add(rangeCheck2);
        rangeCheckList.add(rangeCheck3);

        String userFriendlyWhereClause = Templates.generateWhereClauseString(rangeCheckList);

        assertEquals(expectedValue, userFriendlyWhereClause);
    }

    @Test
    public void testInConditionUserFriendlyWhereClause() {
        String expectedValue = "ItemGroup.Item.IN. (Value1, Value2, Value3) ";

        String itemGroup = "ItemGroup";
        String item = "Item";
        String comparator = "IN";
        String value1 = "Value1";
        String value2 = "Value2";
        String value3 = "Value3";
        RangeCheck rangeCheck = new RangeCheck();
        rangeCheck.setItemGroupOid(itemGroup);
        rangeCheck.setItemOid(item);
        rangeCheck.setComparator(comparator);
        rangeCheck.addValue(value1);
        rangeCheck.addValue(value2);
        rangeCheck.addValue(value3);

        String userFriendlyWhereClause = Templates.generateWhereClauseString(Collections.singletonList(rangeCheck));

        assertEquals(expectedValue, userFriendlyWhereClause);
    }
}
