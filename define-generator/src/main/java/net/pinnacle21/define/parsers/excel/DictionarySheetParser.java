/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.Dictionary;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.parsing.excel.NamedRow;
import net.pinnacle21.parsing.excel.ValidationAwareExcelSheetParser;
import org.apache.commons.lang3.StringUtils;

public class DictionarySheetParser extends ValidationAwareExcelSheetParser<ParsedStudy> {
    static final String ID_COLUMN = "ID";
    static final String NAME_COLUMN = "Name";
    static final String DATA_TYPE_COLUMN = "Data Type";
    static final String DICTIONARY_COLUMN = "Dictionary";
    static final String VERSION_COLUMN = "Version";
    static final String DUPLICATE_ERROR = "The dictionary '%s' is already defined, duplicates are not allowed";

    DictionarySheetParser() {
        this.addValidatedRequiredColumn(ID_COLUMN);
        this.addValidatedColumn(NAME_COLUMN);
        this.addValidatedColumn(DATA_TYPE_COLUMN);
        this.addValidatedColumn(DICTIONARY_COLUMN);
        this.addValidatedColumn(VERSION_COLUMN);
    }

    @Override
    public void parse(NamedRow row, ParsedStudy parsedStudy) {
        if (!this.validate(row, parsedStudy)) {
            return;
        }
        Study study = parsedStudy.getStudy();

        String id = StringUtils.trim(row.get(ID_COLUMN));

        if (study.hasDictionary(id)) {
            parsedStudy.error(row.getIdentifier(), DUPLICATE_ERROR, id);

            return;
        }

        Dictionary dictionary = new Dictionary();
        dictionary.setOid(id);
        dictionary.setName(row.get(NAME_COLUMN));
        dictionary.setDataType(row.get(DATA_TYPE_COLUMN));
        dictionary.setDictionary(row.get(DICTIONARY_COLUMN));
        dictionary.setVersion(row.get(VERSION_COLUMN));

        study.setDictionary(id, dictionary);
    }
}
