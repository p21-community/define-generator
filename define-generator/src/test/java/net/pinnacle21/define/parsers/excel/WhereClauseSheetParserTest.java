/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.parsers.excel;

import net.pinnacle21.define.models.*;
import net.pinnacle21.define.util.MockNamedRow;
import net.pinnacle21.parsing.excel.ExcelSheetParser;
import net.pinnacle21.parsing.excel.FieldValidations;
import net.pinnacle21.parsing.excel.RowIdentifier;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import java.util.List;
import java.util.Set;

import static net.pinnacle21.define.parsers.excel.WhereClauseSheetParser.*;
import static org.junit.Assert.*;
import static org.mockito.Matchers.anyVararg;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class WhereClauseSheetParserTest {

    @Test
    public void nonexistingWhereClauseGetsCreated() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new WhereClauseSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedOid = "whereClauseOid";
        String expectedDataset = "Dataset";
        String expectedVariable = "Variable";
        String expectedComparator1 = "EQ";
        String expectedValue1 = "Value";
        String expectedComparator2 = "IN";
        String expectedValue2 = "Value1, Value2";
        String expectedGeneratedOid = expectedDataset + "." +
                expectedVariable + "." +
                expectedComparator1 + "." +
                expectedDataset + "." +
                expectedVariable + "." +
                expectedComparator2 + "." +
                "9cec7ff7b689cb8aac5616b73a2fc4d0e75c33e2";

        study.setItemGroup(expectedDataset, new ItemGroup().setName(expectedDataset));
        study.setItem(expectedDataset, expectedVariable, new Item().setName(expectedVariable));

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedOid + " ")
                        .with(DATASET_COLUMN, expectedDataset + " ")
                        .with(VARIABLE_COLUMN, expectedVariable + " ")
                        .with(COMPARATOR_COLUMN, expectedComparator1)
                        .with(VALUE_COLUMN, expectedValue1),
                parsedStudy
        );

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedOid)
                        .with(DATASET_COLUMN, expectedDataset)
                        .with(VARIABLE_COLUMN, expectedVariable)
                        .with(COMPARATOR_COLUMN, expectedComparator2)
                        .with(VALUE_COLUMN, expectedValue2),
                parsedStudy
        );

        assertTrue(study.hasWhereClause(expectedOid));
        assertTrue(study.hasRangeCheck(expectedOid, expectedDataset, expectedVariable, expectedComparator1, expectedValue1));
        assertTrue(study.hasRangeCheck(expectedOid, expectedDataset, expectedVariable, expectedComparator2, expectedValue2));

        WhereClause whereClause = study.getWhereClause(expectedOid);

        assertEquals(2, whereClause.getRangeChecks().size());
        assertEquals(expectedGeneratedOid, whereClause.getOid());

        RangeCheck rangeCheck = study.getRangeCheck(expectedOid, expectedDataset, expectedVariable, expectedComparator2, expectedValue2);

        assertEquals(expectedDataset, rangeCheck.getItemGroupOid());
        assertEquals(expectedVariable, rangeCheck.getItemOid());
        assertEquals(expectedComparator2, rangeCheck.getComparator());
        assertArrayEquals(expectedValue2.split(", "), rangeCheck.getValues().toArray());
    }

    @Test
    public void idColumnIsRequiredWarning() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new WhereClauseSheetParser();

        String expectedStructure = "DATASET";
        String expectedVariable = "VAR";
        String expectedComparator = "EQ";

        parser.parse(
                new MockNamedRow()
                        .with(DATASET_COLUMN, expectedStructure)
                        .with(VARIABLE_COLUMN, expectedVariable)
                        .with(COMPARATOR_COLUMN, expectedComparator),
                parsedStudy
        );

        verify(parsedStudy).error(Mockito.any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR),
                eq(ID_COLUMN));
    }

    @Test
    public void datasetColumnIsRequiredWarning() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new WhereClauseSheetParser();

        String expectedID = "WC";
        String expectedVariable = "VAR";
        String expectedComparator = "EQ";

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID)
                        .with(VARIABLE_COLUMN, expectedVariable)
                        .with(COMPARATOR_COLUMN, expectedComparator),
                parsedStudy
        );

        verify(parsedStudy).error(Mockito.any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR),
                eq(DATASET_COLUMN));
    }

    @Test
    public void variableColumnIsRequiredWarning() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new WhereClauseSheetParser();

        String expectedID = "WC";
        String expectedDataset = "DATASET";
        String expectedComparator = "EQ";

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID)
                        .with(DATASET_COLUMN, expectedDataset)
                        .with(COMPARATOR_COLUMN, expectedComparator),
                parsedStudy
        );

        verify(parsedStudy).error(Mockito.any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR),
                eq(VARIABLE_COLUMN));
    }

    @Test
    public void comparatorColumnIsRequiredWarning() {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new WhereClauseSheetParser();

        String expectedID = "WC";
        String expectedDataset = "DATASET";
        String expectedVariable = "VAR";

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID)
                        .with(DATASET_COLUMN, expectedDataset)
                        .with(VARIABLE_COLUMN, expectedVariable),
                parsedStudy
        );

        verify(parsedStudy).error(Mockito.any(RowIdentifier.class), eq(FieldValidations.REQUIRED_COLUMN_ERROR),
                eq(COMPARATOR_COLUMN));
    }

    @Test
    public void emptyValueColumnImports() {
        Study study = new Study();
        WhereClauseSheetParser parser = new WhereClauseSheetParser();

        study.setItem("Dataset", "Variable", new Item());

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, "ID")
                        .with(DATASET_COLUMN, "Dataset")
                        .with(VARIABLE_COLUMN, "Variable")
                        .with(COMPARATOR_COLUMN, "EQ")
                , new ParsedStudy(study)
        );

        List<RangeCheck> rangeChecks = study.getRangeChecks();
        assertEquals(1, rangeChecks.size());
        Set<String> values = rangeChecks.get(0).getValues();
        assertEquals(1, values.size());
        assertEquals("", values.iterator().next());
    }

    @Test
    public void invalidVariableReferenceErrorRaised() {
        Study study = PowerMockito.mock(Study.class);
        ParsedStudy parsedStudy = Mockito.spy(new ParsedStudy(study));
        ExcelSheetParser<ParsedStudy> parser = new WhereClauseSheetParser();

        String expectedID = "WC";
        String expectedDataset = "DATASET";
        String expectedVariable = "VAR";
        String expectedComparator = "EQ";
        String expectedValue = "VAL";

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID)
                        .with(DATASET_COLUMN, expectedDataset)
                        .with(VARIABLE_COLUMN, expectedVariable)
                        .with(COMPARATOR_COLUMN, expectedComparator)
                        .with(VALUE_COLUMN, expectedValue),
                parsedStudy
        );

        verify(parsedStudy).error(Mockito.any(RowIdentifier.class), eq(VARIABLE_REF_ERROR), anyVararg());
    }

    @Test
    public void duplicateErrorRaised() throws Exception {
        ParsedStudy parsedStudy = PowerMockito.mock(ParsedStudy.class);
        ExcelSheetParser<ParsedStudy> parser = new WhereClauseSheetParser();
        Study study = PowerMockito.mock(Study.class);

        when(parsedStudy.getStudy()).thenReturn(study);

        String expectedID = "WC";
        String expectedDataset = "DATASET";
        String expectedVariable = "VAR";
        String expectedComparator = "EQ";
        String expectedValue = "VAL";

        Item item = new Item();

        when(study.hasItem(expectedDataset, expectedVariable)).thenReturn(true);
        when(study.getItem(expectedDataset, expectedVariable)).thenReturn(item);
        when(study.hasRangeCheck(expectedID, expectedDataset, expectedVariable, expectedComparator, expectedValue)).thenReturn(true);

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID)
                        .with(DATASET_COLUMN, expectedDataset)
                        .with(VARIABLE_COLUMN, expectedVariable)
                        .with(COMPARATOR_COLUMN, expectedComparator)
                        .with(VALUE_COLUMN, expectedValue),
                parsedStudy
        );

        verify(parsedStudy).error(Mockito.any(RowIdentifier.class), eq(DUPLICATE_ERROR), anyVararg());
    }

    @Test
    public void duplicateErrorNotRaisedWithDifferentCasing() {
        ParsedStudy parsedStudy = new ParsedStudy(new Study());
        ExcelSheetParser<ParsedStudy> parser = new WhereClauseSheetParser();
        Study study = parsedStudy.getStudy();

        String expectedID = "WC";
        String expectedDataset = "DATASET";
        String expectedVariable = "VAR";
        String expectedComparator = "EQ";
        String expectedValue = "VAL";

        Item item = new Item();
        RangeCheck rangeCheck = new RangeCheck();

        rangeCheck.setItemGroupOid(expectedDataset);
        rangeCheck.setItemOid(expectedVariable);
        rangeCheck.setComparator(expectedComparator);
        rangeCheck.addValue(expectedValue);

        study.setItem(expectedDataset, expectedVariable, item);
        study.setRangeCheck(expectedID.toLowerCase(), expectedDataset, expectedVariable, expectedComparator, expectedValue, rangeCheck);

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, expectedID)
                        .with(DATASET_COLUMN, expectedDataset)
                        .with(VARIABLE_COLUMN, expectedVariable)
                        .with(COMPARATOR_COLUMN, expectedComparator)
                        .with(VALUE_COLUMN, expectedValue),
                parsedStudy
        );

        assertTrue(study.hasRangeCheck(expectedID, expectedDataset, expectedVariable, expectedComparator, expectedValue));
    }

    @Test
    public void valueWithQuotesIsParsedCorrectly() {
        Study study = new Study();
        WhereClauseSheetParser parser = new WhereClauseSheetParser();

        study.setItem("Dataset", "Variable", new Item());

        String value = "\"One, Two\", Three, Four";

        parser.parse(
            new MockNamedRow()
                .with(ID_COLUMN, "ID")
                .with(DATASET_COLUMN, "Dataset")
                .with(VARIABLE_COLUMN, "Variable")
                .with(COMPARATOR_COLUMN, "EQ")
                .with(VALUE_COLUMN, value)
            , new ParsedStudy(study)
        );

        assertEquals(value, study.getRangeChecks().get(0).getValues().toArray()[0]);

        study.setItem("Dataset", "Variable2", new Item());

        String[] expectedValues = {"One, Two", "Three", "Four"};

        parser.parse(
                new MockNamedRow()
                        .with(ID_COLUMN, "ID")
                        .with(DATASET_COLUMN, "Dataset")
                        .with(VARIABLE_COLUMN, "Variable2")
                        .with(COMPARATOR_COLUMN, "IN")
                        .with(VALUE_COLUMN, value)
                , new ParsedStudy(study)
        );

        String[] rangeCheckValues = study.getRangeChecks().get(1).getValues().toArray(new String[3]);

        assertEquals(expectedValues[0], rangeCheckValues[0]);
        assertEquals(expectedValues[1], rangeCheckValues[1]);
        assertEquals(expectedValues[2], rangeCheckValues[2]);
    }
}
