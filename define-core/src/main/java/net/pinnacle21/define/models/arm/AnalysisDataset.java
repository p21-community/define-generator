/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.models.arm;

import net.pinnacle21.define.models.ItemGroup;
import net.pinnacle21.define.models.WhereClause;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AnalysisDataset {
    private ItemGroup itemGroup;
    private WhereClause whereClause;
    private List<AnalysisVariable> analysisVariables = new ArrayList<>();

    public ItemGroup getItemGroup() {
        return itemGroup;
    }

    public AnalysisDataset setItemGroup(ItemGroup itemGroup) {
        this.itemGroup = itemGroup;
        return this;

    }

    public WhereClause getWhereClause() {
        return whereClause;
    }

    public AnalysisDataset setWhereClause(WhereClause whereClause) {
        this.whereClause = whereClause;
        return this;
    }

    public List<AnalysisVariable> getAnalysisVariables() {
        return Collections.unmodifiableList(analysisVariables);
    }

    public AnalysisDataset addAll(List<AnalysisVariable> analysisVariables) {
        this.analysisVariables.addAll(analysisVariables);
        return this;
    }

    public AnalysisDataset add(AnalysisVariable analysisVariable) {
        this.analysisVariables.add(analysisVariable);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AnalysisDataset dataset = (AnalysisDataset) o;

        if (getItemGroup() != null ? !getItemGroup().equals(dataset.getItemGroup()) : dataset.getItemGroup() != null)
            return false;
        if (getWhereClause() != null ? !getWhereClause().equals(dataset.getWhereClause()) : dataset.getWhereClause() != null)
            return false;
        return getAnalysisVariables() != null ? getAnalysisVariables().equals(dataset.getAnalysisVariables()) : dataset.getAnalysisVariables() == null;

    }
}
