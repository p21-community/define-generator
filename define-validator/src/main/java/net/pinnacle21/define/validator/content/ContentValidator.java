/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.define.validator.content;

import net.pinnacle21.define.models.DefineMetadata;
import net.pinnacle21.define.models.Study;
import net.pinnacle21.define.parsers.xml.XMLParser;
import net.pinnacle21.define.parsers.xml.XPathEvaluator;
import net.pinnacle21.define.parsers.xml.XPathEvaluatorFactory;
import net.pinnacle21.define.validator.content.rules.ModelRules;
import net.pinnacle21.define.validator.content.rules.XPathRules;
import net.pinnacle21.define.validator.report.MessageTransformer;
import net.pinnacle21.define.validator.resources.ResourceManager;
import net.pinnacle21.define.validator.resources.Standard;
import net.pinnacle21.define.validator.resources.Terminology;
import net.pinnacle21.validator.api.model.ConfigOptions;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

public class ContentValidator {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContentValidator.class);

    private final DefineMetadata metadata;
    private final ResourceManager resourceManager;
    private final MessageTransformer transformer;
    private boolean isFatal;

    public ContentValidator(DefineMetadata metadata, ResourceManager resourceManager, MessageTransformer transformer) {
        this.metadata = metadata;
        this.resourceManager = resourceManager;
        this.transformer = transformer;
    }

    public boolean validate(Document document) throws IOException {
        try {
            XPathEvaluator xpath = this.getXPathEvaluator(document);
            Standard standard = this.getStandard(xpath);
            Terminology terminology = this.getTerminology(standard);

            this.validateWithXPath(xpath, terminology);

            if (!this.isFatal) {
                this.validateStudy(xpath, standard, terminology);
            }
        } catch (DocumentException ex) {
            LOGGER.error("Validation of {} aborted due to unexpected exception while performing content validation",
                    this.resourceManager.getDefine(), ex);

            return false;
        }

        // TODO: Unsure if a fatal failure here is acceptable; i.e. if schema validation issues were triggered we still actually did a successful validation
        return true;
    }

    private void validateWithXPath(XPathEvaluator xpath, Terminology terminology) {
        Set<Method> preChecks = new LinkedHashSet<>();
        Set<Method> postChecks = new LinkedHashSet<>();
        XPathRules xPathRules = new XPathRules(this.metadata, this.resourceManager, terminology, xpath, this::markFatal,
                this.transformer);

        filterArrayForRules(xPathRules.getClass().getDeclaredMethods(), preChecks, postChecks);

        for (Method preCheck : preChecks) {
            try {
                preCheck.invoke(xPathRules);
            } catch (IllegalAccessException | InvocationTargetException e) {
                LOGGER.error("Exception invoking check {}", preCheck.getName(), e);
            }
        }

        //If none of the pre checks failed
        if (!this.isFatal) {
            for (Method postCheck : postChecks) {
                try {
                    postCheck.invoke(xPathRules);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    LOGGER.error("Exception invoking check {}", postCheck.getName(), e);
                }
            }
        }
    }

    /**
     * Model checks don't require metadata files from the ResourceManager.
     */
    private void validateStudy(XPathEvaluator xpath, Standard standard, Terminology terminology)
            throws DocumentException {
        Study study = new XMLParser(xpath.getDocument()).parse();

        ModelRules modelRules = new ModelRules(this.metadata, study, standard, terminology, this.transformer);

        Set<Method> checks = new LinkedHashSet<>();

        filterArrayForRules(modelRules.getClass().getDeclaredMethods(), null, checks);

        for (Method contentCheck : checks) {
            try {
                contentCheck.invoke(modelRules);
            } catch (IllegalAccessException | InvocationTargetException e) {
                LOGGER.error("Exception invoking check {}", contentCheck.getName(), e);
            }
        }
    }

    private XPathEvaluator getXPathEvaluator(Document document) {
        XPathEvaluatorFactory factory = XPathEvaluatorFactory.newInstance();

        switch (this.metadata) {
            case V1:
                return factory.define1XPathEvaluator(document);
            case V2:
                return factory.define2XPathEvaluator(document);
            default:
                return factory.defaultEvaluator();
        }
    }

    private Standard getStandard(XPathEvaluator xpath) throws IOException, DocumentException {
        ConfigOptions options = this.resourceManager.getConfigOptions();
        Iterator<String> paths = options.getConfigs().iterator();

        // The default root is the parent of wherever the main config is
        File defineConfig = new File(paths.next());
        File defaultRoot = defineConfig.getParentFile();
        File targetStandard = null;

        // IF two configs were provided, the second is the reference standard and we should use that
        if (paths.hasNext()) {
            targetStandard = new File(paths.next());
        }

        if (targetStandard == null) {

        }

        // TODO: There's not really any point in continuing here right, if we don't have a standard?
        return targetStandard != null
                ? Standard.parseFile(targetStandard, this.resourceManager.getConfigOptions())
                : null;
    }

    private Terminology getTerminology(Standard standard) throws IOException, DocumentException {
        if (standard == null) {
            // FIXME
            return null;
        }

        return Terminology.parseFiles(standard.getTerminologies()
                .stream()
                .map(File::new)
                .filter(f -> {
                    if (!f.isFile()) {
                        LOGGER.warn("Terminology {} is not an existing file, will not be parsed", f);

                        return false;
                    }

                    return true;
                })
                .toArray(File[]::new));
    }

    private void markFatal() {
        this.isFatal = true;
    }

    private void filterArrayForRules(Method[] methods, Set<Method> preChecks, Set<Method> postChecks) {
        for (Method m : methods) {
            ValidationRule annotation = m.<ValidationRule>getAnnotation(ValidationRule.class);
            if (annotation != null && StringUtils.isNotBlank(annotation.id())) {
                if (preChecks != null && (postChecks == null || annotation.isPreCheck())) {
                    preChecks.add(m);
                } else if (postChecks != null && (preChecks == null || !annotation.isPreCheck())) {
                    postChecks.add(m);
                }
            }
        }
    }
}
